<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'facebook' => [
        'client_id'     => '426666732115737',//env('FACEBOOK_CLIENT_ID'),
        'client_secret' => 'b180bc00398278b759fafa124271c0b3',//env('FACEBOOK_CLIENT_SECRET'),
        'redirect'      => env('FACEBOOK_URL'),
    ],

    'google' => [
        'client_id'     => '643399621395-vbtmt51795v3jila2i2mmdkuskim6o0c.apps.googleusercontent.com',//env('GOOGLE_CLIENT_ID'),
        'client_secret' => 'WBRRqaUO1TgrR2sSMZ9Tzd0e',//env('GOOGLE_CLIENT_SECRET'),
        'redirect'      => env('GOOGLE_URL'),
       
    ],

];
