<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|->prefix('admin')->as('admin.')
*/
/*
|-----------------------------------------------------------------------------------------
|   location manager
|-----------------------------------------------------------------------------------------
*/
 
 Route::post('/broadcast', 'LocationController@broadcast')->name('broadcast');
  Route::get('/location', 'LocationController@index')->name('location');
  Route::get('set/location', 'LocationController@setlocation')->name('location.set');
  Route::get('/test', 'OrderController@test')->name('test');
/*
|-----------------------------------------------------------------------------------------
|   invite new user by exits user
|-----------------------------------------------------------------------------------------
*/
Route::middleware('auth')->as('user.')->group(function() {
  Route::get('/invite', 'UserController@invite')->name('invite');
  Route::get('/profile', 'UserController@index')->name('profile');
  Route::get('/my-orders', 'UserController@orders')->name('orders');
  Route::get('/my-orders/{order_id}', 'UserController@ordersDetails')->name('orders_details');
  Route::get('/invoice/{order_id}', 'InvoiceController@order_invoice')->name('invoice');
  Route::get('/my-address', 'UserController@address')->name('address');
  Route::get('/my-reviews', 'UserController@reviews')->name('reviews');
  Route::get('/my-coupons', 'UserController@coupons')->name('coupons');
    Route::get('/order-cancell/{id}/{status}', 'OrderController@statusCancel')->name('order.status');
});
/*
|-----------------------------------------------------------------------------------------
|   login & register
|-----------------------------------------------------------------------------------------
*/
  Auth::routes(['verify' => true]);
/*google & fb login*/
  Route::get('/auth/redirect/{provider}', 'Auth\SocialController@redirect')->name('auth2');
  Route::get('login/{provider}/callback','Auth\SocialController@Callback')->name('auth2_callback');
/*confirm register user by mobile/email otp */
  Route::get('/user-confirmation/{id}', 'Auth\RegisterController@userResendMail')->name('user.confirmation');
  Route::post('/login/user/{id}', 'Auth\LoginController@LoginOtp')->middleware("throttle:5,1")->name('login.otp');


//Route::middleware('location')->group(function() {

      Route::get('/', 'HomeController@index')->name('home');
      /*Route::get('/', function () {
          return view('welcome');
      });*/
      Route::get('/home', 'HomeController@index')->name('home');
      Route::get('/mc/{slug}', 'HomeController@master')->name('master');
      Route::get('/search', 'SearchController@index')->name('search');
      Route::get('/store/{type}/{id}/{slug}', 'HomeController@vendor')->name('vendor');
      /*
      |-----------------------------------------------------------------------------------------
      |   Cart
      |-----------------------------------------------------------------------------------------
      */
      Route::get('/cart', 'CartController@index')->name('cart');
      Route::get('/cart/empty', 'CartController@deleteCart')->name('cart.empty');
      /*
      |-----------------------------------------------------------------------------------------
      |   Order
      |-----------------------------------------------------------------------------------------
      */
      Route::get('/checkout', 'OrderController@index')->name('checkout');
      Route::get('/confirm/order', 'OrderController@order')->name('order.confirm');
      Route::get('/order-done/{status}', 'OrderController@thanks')->name('order.done');
      /*
      |-----------------------------------------------------------------------------------------
      |   PAyment
      |-----------------------------------------------------------------------------------------
      */
      Route::get('/payment/{array}', 'PaymentController@index')->name('payment');
      # Call Route
     // Route::get('payumoney', 'PaymentController@payumoney')->name('payumoney');

      # Status Route
      Route::get('payment/status','PaymentController@status')->name('payment.status');
      /*
      |----------------------------------------------------------------------------
      | Other Pages
      |----------------------------------------------------------------------------
      */
      Route::get('/discover-our-world', 'PageController@about')->name('about');
      //Route::get('/our-world', 'PageController@about')->name('team');
      Route::get('/delivery-rider', 'PageController@rider')->name('rider');
      Route::get('/support', 'PageController@contact')->name('contact');
      Route::get('/terms-and-conditions', 'PageController@terms')->name('terms');
      Route::get('/refund-policy', 'PageController@refund')->name('refund');
      Route::get('/privacy-policy', 'PageController@privacy')->name('privacy');
      Route::get('/beware-of-phishing-and-fraud', 'PageController@fraud')->name('fraud');
      
//});

/*
|--------------------------------------------------------------------------
| Admin  Routes
|--------------------------------------------------------------------------
|
*/
    Route::get('/login/admin', 'Auth\LoginController@showAdminLoginForm')->name('login.admin');    
    /* only 5 request per 1 min for Vendor & Admin Login Post */
    Route::post('/login/admin', 'Auth\LoginController@adminLogin')->middleware("throttle:5,1");    
  
    Route::get('/forget/admin', 'Auth\ForgotPasswordController@showAdminForgetForm')->name('forget.admin');
    Route::post('/forget/admin', 'Auth\ForgotPasswordController@adminForget')->name('forget.admin');



//Route::prefix('/admin')->name('admin.')->namespace('Admin')->group(function(){
  //All the admin routes will be defined here...
  
Route::namespace('Admin')->middleware('auth:admin')->prefix('admin')->as('admin.')->group(function() {

  // Auth::routes(['register' => false]);
   Route::get('/', 'HomeController@index')->name('dashboard');
   Route::post('/logout', 'Auth\LoginController@Adminlogout')->name('logout');
   Route::get('/change-password', 'Auth\LoginController@changepassword')->name('change-password');
   Route::post('/update-password', 'Auth\LoginController@updatePassword')->name('update-password');
   /*//////////////////////////////////////////////////////////////////////////*/
  // Route::get('/vendor', 'VendorController@index')->name('vendor');
    Route::get('/add-vendor', 'VendorController@create')->name('add-vendor');
     Route::post('/add-vendor', 'VendorController@store')->name('store-vendor');
      Route::get('/vendor-status/{id}/{status}', 'VendorController@status')->name('vendor.status');
      Route::get('/vendor-verify/{id}/{status}', 'VendorController@verify')->name('vendor.verify');
  // Route::get('/show-vendor/{id}', 'VendorController@show')->name('show-vendor');
   Route::get('/edit-vendor/{id}', 'VendorController@edit')->name('edit-vendor');
   Route::get('/update-vendor/{id}', 'VendorController@update')->name('vendor.update');
   // ///////////////////////////////////////////////////////////
   Route::get('/user', 'UserController@index')->name('user');

   /*////////////////////////////////////////////////////////////////*/
   /*//////////////////////////////////////////////////////////////*/
    Route::get('/city', 'CityController@index')->name('city');
    Route::post('/city/store', 'CityController@store')->name('city.store');
    Route::get('/city/{id}', 'CityController@index')->name('city.edit');
    Route::get('/city/update/{id}', 'CityController@update')->name('city.update');
    //Route::delete('/city/destroy/{id}', 'CityController@destroy')->name('city.destroy');
    /*////////////////////////////////////////////*/
    Route::get('/vertical', 'MasterController@index')->name('master');
    Route::post('/vertical/store', 'MasterController@store')->name('master.store');
    Route::get('/vertical/{id}', 'MasterController@index')->name('master.edit');
    Route::get('/vertical/update/{id}', 'MasterController@update')->name('master.update');
    Route::get('/vertical/{id}/{status}', 'MasterController@status')->name('master.status');
    //Route::delete('/category/destroy/{id}', 'CategorieController@destroy')->name('category.destroy');
    /*////////////////////////////////////////////*/
    Route::get('/category', 'CategorieController@index')->name('category');
    Route::post('/category/store', 'CategorieController@store')->name('category.store');
    Route::get('/category/{id}', 'CategorieController@index')->name('category.edit');
    Route::get('/category/update/{id}', 'CategorieController@update')->name('category.update');
    //Route::delete('/category/destroy/{id}', 'CategorieController@destroy')->name('category.destroy');
    /*/////////////////////////////////////////////////////////////////////////*/
     /*////////////////////////////////////////////*/
    Route::get('/commission', 'CommissionController@index')->name('commission');
    Route::post('/commission/store', 'CommissionController@store')->name('commission.store');
    Route::get('/commission/{id}', 'CommissionController@index')->name('commission.edit');
    Route::get('/commission/update/{id}', 'CommissionController@update')->name('commission.update');
    //Route::delete('/category/destroy/{id}', 'CategorieController@destroy')->name('category.destroy');
    /*/////////////////////////////////////////////////////////////////////////*/
     /*////////////////////////////////////////////*/
    Route::get('/coupon', 'CouponController@index')->name('coupon');
    Route::post('/coupon/store', 'CouponController@store')->name('coupon.store');
    Route::get('/coupon/{id}', 'CouponController@index')->name('coupon.edit');
    Route::get('/coupon/update/{id}', 'CouponController@update')->name('coupon.update');
    //Route::delete('/category/destroy/{id}', 'CategorieController@destroy')->name('category.destroy');
    /*/////////////////////////////////////////////////////////////////////////*/
    Route::get('/subcategory', 'SubCategorieController@index')->name('subcategory');
    Route::post('/subcategory/store', 'SubCategorieController@store')->name('subcategory.store');
    Route::get('/subcategory/{id}', 'SubCategorieController@index')->name('subcategory.edit');
    Route::get('/subcategory/update/{id}', 'SubCategorieController@update')->name('subcategory.update');
   // Route::delete('/subcategory/destroy/{id}', 'SubCategorieController@destroy')->name('subcategory.destroy');
    /*////////////////////////////////////////////*/
    Route::get('/attribute', 'AttributeController@index')->name('attribute');
    Route::post('/attribute/store', 'AttributeController@store')->name('attribute.store');
    Route::get('/attribute/{id}', 'AttributeController@index')->name('attribute.edit');
    Route::get('/attribute/update/{id}', 'AttributeController@update')->name('attribute.update');
    //Route::delete('/attribute/destroy/{id}', 'AttributeController@destroy')->name('attribute.destroy');
     /*////////////////////////////////////////////*/
     /*//////////////////////////////////////////////////////////////////////////////////*/
     //Route::get('/item', 'ProductController@index')->name('item');
     Route::get('/add-item', 'ProductController@create')->name('add-item');
     Route::post('/add-item', 'ProductController@store')->name('store-item');
     Route::get('/edit-item/{id}', 'ProductController@edit')->name('edit-item');
     Route::get('/update-item/{id}', 'ProductController@update')->name('update-item');
     Route::get('/item-status/{id}/{status}', 'ProductController@status')->name('item.status');
     Route::get('/item-featured/{id}/{status}', 'ProductController@featured')->name('item.featured');
     Route::get('/item-verify/{id}/{status}', 'ProductController@verify')->name('item.verify');

     Route::get('/manage-item/{id}', 'ProductController@manage')->name('item.manage');
     Route::post('/manage-item/store/{id}', 'ProductController@manageStore')->name('item.manage.store');
     Route::get('/manage-item/store/{id}', 'ProductController@manageStore')->name('item.manage.store');
     /*//////////////////////////////////////////////////////////////////////////////////*/
   

     Route::get('/vendor-verify/{id}/{status}', 'VendorController@verify')->name('vendor.verify');
   


    /*Reporting*/
    Route::get('/vendor-list', 'ReportController@vendor')->name('vendor-list');
    Route::get('/item-list', 'ReportController@item')->name('item-list');
     Route::get('/item-list', 'ReportController@item')->name('item-list');
      Route::get('/customer-list', 'ReportController@user')->name('customer-list');

      Route::get('/order-list', 'OrderController@index')->name('order-list');
      Route::get('/order-status/{id}/{status}', 'OrderController@status')->name('order.status');
      Route::get('/order-status-all/{id}/{status}/{ostatus}', 'OrderController@statusAll')->name('order.status-all');

      Route::get('/payment-list', 'ReportController@payment')->name('payment-list');
  //****************************************************************************************
  //  Rider
  //******************************************************************************************
    Route::get('/rider-list', 'RiderController@index')->name('rider-list');
    Route::get('/add-rider', 'RiderController@create')->name('add-rider');
     Route::post('/add-rider', 'RiderController@store')->name('store-rider');
     Route::get('/edit-rider/{id}', 'RiderController@create')->name('edit-rider');
      Route::get('/update-rider/{id}', 'RiderController@update')->name('update-rider');
    Route::get('/rider/{id}/{status}', 'RiderController@status')->name('rider.status');
    Route::get('/rider-duty/{id}/{status}', 'RiderController@onDuty')->name('rider.duty');


      Route::get('/assign-rider', 'ReportController@rider')->name('rider.order-list');
      Route::get('/assign-rider/{order_id}', 'ReportController@rider')->name('rider.assign');
      Route::get('/rider-assign/{order_id}', 'RiderController@assign')->name('order-rider-assign');
});

/*
|--------------------------------------------------------------------------
| Vendor Routes
|--------------------------------------------------------------------------
|
*/
	Route::get('/login/vendor', 'Auth\LoginController@showVendorLoginForm')->name('login.vendor');
	 /* only 5 request per 1 min for Vendor & Admin Login Post */
	Route::post('/login/vendor', 'Auth\LoginController@vendorLogin')->middleware("throttle:5,1");

	Route::get('/register/vendor', 'Auth\RegisterController@showVendorRegisterForm')->name('register.vendor');
	Route::post('/register/vendor', 'Auth\RegisterController@createVendor');

  Route::get('/vendor-confirmation/{id}', 'Auth\RegisterController@vendorResendMail')->name('vendor.confirmation');

	Route::get('/vendor-verify/{token}', 'Auth\RegisterController@VendorVerify');
	Route::get('/vendor-rest/{token}', 'Auth\RegisterController@VendorSendPassword');
	Route::get('/vendor-profile-update/{id}', 'Auth\RegisterController@VendorUpdate')->name('vendor-profile-update');

	Route::get('/forget/vendor', 'Auth\ForgotPasswordController@showVendorForgetForm')->name('forget.vendor');
	Route::post('/forget/vendor', 'Auth\ForgotPasswordController@vendorForget')->name('forget.vendor');
  Route::get('/reset-password-vendor/{token}', 'Auth\RegisterController@vendorNewPass');

Route::namespace('Vendor')->middleware('auth:vendor')->prefix('vendor')->as('vendor.')->group(function() {

   Route::post('/logout', 'Auth\LoginController@Vendorlogout')->name('logout');
   Route::get('/change-password', 'Auth\LoginController@changepassword')->name('change-password');
   Route::post('/update-password', 'Auth\LoginController@updatePassword')->name('update-password');

  Route::get('/profile', 'VendorController@profile')->name('profile');
  Route::get('/profile/edit', 'VendorController@edit')->name('profile.edit');
  Route::get('/update/profile', 'VendorController@store')->name('profile.update');

   Route::get('/', 'VendorController@index')->name('dashboard');
   Route::get('/store-status/{status}', 'VendorController@statusStore')->name('store.status');
   Route::get('/add-vendor', 'VendorController@create')->name('add-vendor');
   Route::post('/add-vendor', 'VendorController@store')->name('store-vendor');

     // ///////////////////////////////////////////////////////////////////////
/*//////////////////////////////////////////////////////////////////////////////////*/
   Route::get('/product', 'ProductController@index')->name('product');
   Route::post('/product/store', 'ProductController@store')->name('product.store');

   Route::get('/add-product', 'ProductController@create')->name('add-product');
    Route::post('/product/store2', 'ProductController@create2')->name('product-store2');

 /*  Route::get('/product-status/{id}/{status}', 'ProductController@status')->name('item.status');
    Route::get('/product-featured/{id}/{status}', 'ProductController@featured')->name('item.featured');*/
    /* Route::get('/add-product', 'ProductController@create')->name('add-product');
     Route::post('/add-product', 'ProductController@store')->name('store-product');
     Route::get('/edit-product/{id}', 'ProductController@edit')->name('edit-product');
     Route::get('/update-product/{id}', 'ProductController@update')->name('update-product');*/
     // ///////////////////////////////////////////////////////////////////////
   Route::get('/manage-product/{id}', 'ProductController@manage')->name('product.manage');
   Route::post('/manage-product/store/{id}', 'ProductController@manageStore')->name('product.manage.store');
    Route::get('/manage-product/store/{id}', 'ProductController@manageStore')->name('product.manage.store');
   // Route::get('/manage-product/{id}/{pid}', 'ProductController@manage')->name('product.manage');
     //Route::post('/manage-product/store/{id}/{pid}', 'ProductController@manageStore')->name('product.manage.store');
   // Route::get('/manage-product/update/{id}', 'ProductController@manageStore')->name('product.manage.update');

     /*Reporting*/
    Route::get('/item-list', 'ReportController@item')->name('item-list');
    Route::get('/add-item', 'ProductController@create')->name('add-item');
     Route::post('/add-item', 'ProductController@store')->name('store-item');
     Route::get('/edit-item/{id}', 'ProductController@edit')->name('edit-item');
     Route::get('/update-item/{id}', 'ProductController@update')->name('update-item');

     Route::get('/item-status/{id}/{status}', 'ProductController@status')->name('item.status');
     Route::get('/item-featured/{id}/{status}', 'ProductController@featured')->name('item.featured');
    // Route::get('/item-verify/{id}/{status}', 'ProductController@verify')->name('item.verify');

     Route::get('/manage-item/{id}', 'ProductController@manage')->name('item.manage');
     Route::post('/manage-item/store/{id}', 'ProductController@manageStore')->name('item.manage.store');
     Route::get('/manage-item/store/{id}', 'ProductController@manageStore')->name('item.manage.store');
     /*//  Product Discount*/
     Route::get('/item/discount', 'DiscountController@index')->name('item-discount');
     Route::get('/item/discount/update', 'DiscountController@update')->name('item-discount-update');

     Route::get('/order-list', 'OrderController@index')->name('order-list');
      Route::get('/order-status/{id}/{status}', 'OrderController@status')->name('order.status');
      Route::get('/order-status-all/{id}/{status}/{ostatus}', 'OrderController@statusAll')->name('order.status-all');
});

/*
|--------------------------------------------------------------------------
| AJAX Routes
|--------------------------------------------------------------------------
|
*/

//->middleware(['guest','auth'])
Route::prefix('ajax')->group(function () {
  /*Admin*/
  Route::get('item/check_slug', 'Admin\ProductController@check_slug')->name('ajax.item.check_slug');
  //Route::get('session/setpincode', 'CategoryController@setpincode')->name('ajax.session.setpincode');
  /*//////////////////////////////////////////////////////////////////////////////////////////*/

   Route::get('product/check_slug', 'Vendor\ProductController@check_slug')->name('ajax.product.check_slug');


   Route::get('product/partnercategory', 'Vendor\AjaxController@partnerCategoies')->name('ajax.product.partnercategory');

   Route::get('vendor/options', 'Vendor\AjaxController@getVendorOptions')->name('ajax.vendor.options');
   Route::get('master/options', 'Vendor\AjaxController@getMastersOptions')->name('ajax.master.options');
   Route::get('category/options', 'Vendor\AjaxController@getCategoiesOptions')->name('ajax.category.options');
   /*///////////////////////////////*/
   Route::get('category_sub_category/items', 'Vendor\AjaxController@getItemOptions')->name('ajax.category_sub_category.items');

   Route::get('vendor/exits', 'Vendor\AjaxController@checkVendoeExits')->name('ajax.vendor.exits');

   //Route::post('file-upload', 'FileUploadController@upload')->name('ajax.file.upload');
   Route::post('file-upload/{folder}', 'FileUploadController@upload')->name('ajax.file.upload');

   /*
   |-------------------------------------------------------------------------------------------
   |  Cart & Wishlist and addons
   |---------------------------------------------------------------------------------------------*/
   Route::get('cart/add', 'CartController@add')->name('ajax.addCart');
   Route::get('cart/get', 'CartController@get')->name('ajax.getCart');
   Route::get('cart/update/', 'CartController@update')->name('ajax.updateCart');
   Route::get('cart/delete', 'CartController@delete')->name('ajax.deleteCart');
   /*
   |-------------------------------------------------------------------------------------------
   |  Order
   |---------------------------------------------------------------------------------------------*/

   Route::get('user/address', 'UserController@allAddress')->name('ajax.getUserAddress');
    /*
   |-------------------------------------------------------------------------------------------
   |  Vendor ,items and comman search
   |---------------------------------------------------------------------------------------------*/

   Route::get('search/vendors/{masters_id}', 'SearchController@vendor')->name('ajax.vendor.search');
   Route::get('search/items/{masters_id}', 'SearchController@item')->name('ajax.item.search');
   Route::get('search/comman/{masters_id}', 'SearchController@comman')->name('ajax.comman.search');

   Route::get('search/this', 'SearchController@this')->name('ajax.this.search');


});