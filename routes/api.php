<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/
//Route::group(['prefix' => 'v1'], function () {
//->middleware('auth:api')
/*Route::prefix('v1')->group(function() {
    Route::post('/login', 'Api\UsersController@login');
    Route::post('/register', 'Api\UsersController@register');
    Route::get('/logout', 'Api\UsersController@logout')->middleware('auth:api');

    	Route::prefix('category')->as('category')->group(function() {
            Route::get('/', 'Api\CategoryController@index');
            Route::get('/subcategory', 'Api\CategoryController@getSubCategoies');       
                   
        });
        Route::prefix('subcategory')->as('subcategory')->group(function() {
            Route::get('/', 'Api\SubCategoryController@index');        
                   
        });
});*/
Route::prefix('v1')->group(function() {    
    Route::post('/login', 'Api\User\LoginController@login');
    /*---Home---*/
    Route::get('/masters', 'Api\HomeController@masters');
    Route::get('/vendors', 'Api\HomeController@vendors');
    Route::get('/vendor/details', 'Api\HomeController@vendorDetails');   

    Route::group(['middleware' => ['auth:sanctum']], function () {
        Route::get('/me', function(Request $request) {
            return auth()->user();
        });
        Route::post('/auth/logout', 'Api\User\LoginController@logout');
        
    });
});
Route::prefix('r1')->group(function() {    
    Route::post('/login', 'Api\Rider\LoginController@login');   

    Route::group(['middleware' => ['auth:sanctum']], function () {
        Route::get('/me', function(Request $request) {
            return auth()->user();
        });
        Route::post('/auth/logout', 'Api\Rider\LoginController@logout');
        
    });
});