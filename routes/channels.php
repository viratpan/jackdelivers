<?php

use Illuminate\Support\Facades\Broadcast;

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/
/*Broadcast::channel('App.Admin.{id}', function ($model, $id) {
      return $model->id === $id && get_class($model) === 'App\Admin';
});

Broadcast::channel('App.Vendor.{id}', function ($model, $id) {
      return $model->id === $id && get_class($model) === 'App\Vendor';
});*/

Broadcast::channel('App.User.{id}', function ($user, $id) {
    return (int) uth()->user()->id === (int) $id;
});
Broadcast::channel('App.Vendor.{id}', function ($user, $id) {
    return (int) auth()->guard('vendor')->user()->id;
});
Broadcast::channel('App.Admin.{id}', function ($user, $id) {
    return (int) auth()->guard('admin')->user()->id;// return (int) $user->id === (int) $id;
});

Broadcast::channel('events', function ($user) {
    return true;
});
