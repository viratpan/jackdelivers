(function ($) {
    'use strict';
    jQuery('.mean-menu').meanmenu({
        meanScreenWidth: "991"
    });
    $(window).on('scroll', function () {
        if ($(this).scrollTop() > 150) {
            $('.navbar-area').addClass("sticky-nav");
        } else {
            $('.navbar-area').removeClass("sticky-nav");
        }
    });
    $('.close-btn').on('click', function () {
        $('.search-overlay').fadeOut();
        $('.search-btn').show();
        $('.close-btn').removeClass('active');
    });
    $(document).ready(function () {
        // Add minus icon for collapse element which is open by default
        $(".collapse.show").each(function () {
            $(this).prev(".card-header").find(".fa").addClass("fa-minus").removeClass("fa-plus");
        });

        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function () {
            $(this).prev(".card-header").find(".fa").removeClass("fa-plus").addClass("fa-minus");
        }).on('hide.bs.collapse', function () {
            $(this).prev(".card-header").find(".fa").removeClass("fa-minus").addClass("fa-plus");
        });
    });
})(jQuery);

  
//for sticky navbar
window.onscroll = function() {myFunction()};

var header = document.getElementById("myHeader");
var sticky = header.offsetTop;

function myFunction() {
  if (window.pageYOffset > sticky) {
    header.classList.add("sticky-vendor");
  } else {
    header.classList.remove("sticky-vendor");
  }
}


//quantity increment and decrement
//function incrementValue() {
//    var value = parseInt(document.getElementById('number').value, 10);
//    value = isNaN(value) ? 0 : value;
//    if (value < 100) {
//        value++;
//        document.getElementById('number').value = value;
//    }
//}
//
//function decrementValue() {
//    var value = parseInt(document.getElementById('number').value, 10);
//    value = isNaN(value) ? 0 : value;
//    if (value > 1) {
//        value--;
//        document.getElementById('number').value = value;
//    }
//
//}
