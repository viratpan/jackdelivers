

let autocomplete;
var lat;
var lng;
var address;        //A-142 sector 63, Noida Uttarpardesh
var lc;             //like Sector 63,Noida
var city;           //like Noida/Delhi

/*
 |*****************************************************************************************************
 |     MAP-init  AUTOCOMPLETE
 |*****************************************************************************************************
 */
function initMap(){
  //console.log('map init');
 	var input = document.getElementById('autocomplete');
    autocomplete = new google.maps.places.Autocomplete(input);
    
    // place fields that are returned to just the address components.
  	autocomplete.setFields(["address_component","formatted_address","geometry"]);
  	//on chage 
  	autocomplete.addListener('place_changed', function (){
        console.log('search');
        var place = autocomplete.getPlace(); 
        //console.log(place);
        if (place.geometry) 
        {
           lat = place.geometry.location.lat();
           lng = place.geometry.location.lng();
        }
        if(place.address_components){
          //setTimeout(function(){
                  // set states
                    for (var i = 0; i < place.address_components.length; i++) {                       
                        var arr=place.address_components[i].types;

                        if(jQuery.inArray("sublocality_level_1", arr) !== -1){
                            lc=place.address_components[i].long_name;
                            //console.log('Location '+place.address_components[i].long_name); 
                        }
                        if(jQuery.inArray("locality", arr) !== -1){
                            city=place.address_components[i].long_name;
                            //console.log('City '+place.address_components[i].long_name); 
                        }  
                    }
          //      }, 1000);
          address=place.formatted_address;
          //console.log('address '+place.formatted_address); 
        }       
        
        console.log("lat: "+lat+" lng: "+lng+" address: "+address+" sector: "+lc+" city: "+city);
        setLocation(); 
         
  	});
}

 /*
 |*****************************************************************************************************
 |     GET MY CURRENT LOCATION
 |*****************************************************************************************************
 */
function geolocate() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(showPosition);
    
  }
}

function showPosition(position){ 
    location.latitude=position.coords.latitude;
    location.longitude=position.coords.longitude;
    
    var geocoder = new google.maps.Geocoder();
    var latLng = new google.maps.LatLng(location.latitude, location.longitude);

 if (geocoder) {
    geocoder.geocode({ 'latLng': latLng}, function (place, status) {
       if (status == google.maps.GeocoderStatus.OK) {
         console.log(place[0].formatted_address);
         lat = location.latitude;
         lng = location.longitude;

           if(place[0].address_components){
           // setTimeout(function(){
                    // set states
                      for (var i = 0; i < place[0].address_components.length; i++) {
                        var arr=place[0].address_components[i].types;

                        if(jQuery.inArray("sublocality_level_1", arr) !== -1){
                            lc=place[0].address_components[i].long_name;
                            //console.log('Location '+place[0].address_components[i].long_name); 
                        }
                        if(jQuery.inArray("locality", arr) !== -1){
                            city=place[0].address_components[i].long_name;
                            //console.log('City '+place[0].address_components[i].long_name); 
                        } 
                          
                      }
             //     }, 1000);
             //address=place.address_components[0].formatted_address;
             address=place[0].formatted_address;
             //console.log('address '+place[0].formatted_address); 
          }        
         
         console.log("lat: "+lat+" lng: "+lng+" address: "+address+" sector: "+lc+" city: "+city);
         setLocation();
         
       }
       else {
         toastr.error("<span style='color:black;'>Geolocation failed</span>", { closeButton: !0 });
       }
    }); //geocoder.geocode()
  }      
} //showPosition
/*
 |*****************************************************************************************************
 |     SET MY LOCATION
 |*****************************************************************************************************
 */

function setLocation(){
  $('#autocomplete').attr('placeholder','fetching your location.....');
   //alert('fetching your location.....');
          $.get(setLocationurl,
            { 
              'lat': lat,
              'lng': lng,
              'address': address,
              'lc': lc,
              'city': city,
            },
             function( data ) {
                $('#autocomplete').attr('placeholder','Search for area, street name..');
                //location=home;
             }
          );
   
	
}