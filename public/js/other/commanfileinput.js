$(document).ready(function () {
    if($('.documentFileInput').length>0){
        $('.documentFileInput').each(function(){
            var files=$(this).attr('data-value');
            var path=base_url+'assets/uploads/'+$(this).attr('data-path')+'/';
            //path=path.replace('//','/');
            var img_url=[];
            var initialPreviewConfig=[];
            if(files){
                files=files.split(';');
                $(files).each(function(index,value){
                    img_url.push(path+value);
                    initialPreviewConfig.push({
                        caption: value,
                        key: value
                    });
                });
            }
            $(this).fileinput({
                theme: "fas",
                showUpload:false,
                dropZoneEnabled: false,
                overwriteInitial: false,
                previewFileIcon: '<i class="fas fa-file"></i>',
                preferIconicPreview: true,
                previewFileIconSettings: { // configure your icon file extensions
                    'doc': '<i class="fas fa-file-word text-primary"></i>',
                    'xls': '<i class="fas fa-file-excel text-success"></i>',
                    'ppt': '<i class="fas fa-file-powerpoint text-danger"></i>',
                    'pdf': '<i class="fas fa-file-pdf text-danger"></i>',
                    'zip': '<i class="fas fa-file-archive text-muted"></i>',
                    'htm': '<i class="fas fa-file-code text-info"></i>',
                    'txt': '<i class="fas fa-file-text text-info"></i>',    
                },
                initialPreview: img_url,
                initialPreviewConfig: initialPreviewConfig,
                initialPreviewAsData: true,
            });
        });
    }
    if($('.fileInputImage').length>0){
        $('.fileInputImage').each(function(){
            var files=$(this).attr('data-value');
            var path=base_url+'assets/uploads/'+$(this).attr('data-path')+'/';
            //path=path.replace('//','/');
            var img_url=[];
            var initialPreviewConfig=[];
            if(files){
                files=files.split(';');
                $(files).each(function(index,value){
                    img_url.push(path+value);
                    initialPreviewConfig.push({
                            caption: value,
                            key: value
                        });
                });
            }
            $(this).fileinput({
                theme: "fas",
                allowedFileTypes: ['image'],
                showUpload:false,
                overwriteInitial: false,
                initialPreview: img_url,
                initialPreviewConfig: initialPreviewConfig,
                initialPreviewAsData: true,
            });
        });
    }
    
});