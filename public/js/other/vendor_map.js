var map;  var v_lat=''; var v_long='';
    getLocation();
    var x = document.getElementById("admin_location");

    function getLocation() {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
        
      } else { 
        x.innerHTML = "Geolocation is not supported by this browser.";
      }
    }

    function showPosition(position) {
        //console.log(position);
        v_lat=position.coords.latitude;
        v_long=position.coords.longitude; //alert();
     /* x.innerHTML = "Lat: " + position.coords.latitude + 
      " | Long: " + position.coords.longitude;*/
     // &callback=initMap
      initMap();
    }

    function initMap() 
    {     console.log('intMap'); 
        // set defult location of Indrapuram
        var ip_lat = '28.6460176';
        var ip_long = '77.3695166';
        var zoom=15;
        var draggable=true;
              //ip_lat = v_lat;
             // ip_long = v_long;
        if(vendor_lat){ip_lat = vendor_lat;}
        if(vendor_long){ip_long = vendor_long; zoom=18;}
        if(vendor_draggable==0){ draggable=false; }
        var myLatlng = new google.maps.LatLng(ip_lat,ip_long);
        var mapOptions = {
                            zoom             : zoom,
                            center           : myLatlng,
                            disableDefaultUI : true,
                            panControl       : true,
                            zoomControl      : true,
                            mapTypeControl   : true,
                            streetViewControl: true,
                            mapTypeId        : google.maps.MapTypeId.ROADMAP,
                            fullscreenControl: true
                         };

        map = new google.maps.Map(document.getElementById('map'),mapOptions);
        var marker = new google.maps.Marker({
                                                position: myLatlng,
                                                map: map,
                                                draggable:draggable,    
                                            }); 

       // var geocoder = new google.maps.Geocoder;
        var infowindow = new google.maps.InfoWindow;    

        google.maps.event.addListener(marker, 'dragend', function(e) 
        { console.log('moved: '+this);
            var lat = this.getPosition().lat();  //alert('moved');
            var lng = this.getPosition().lng();
            $('#latitude').val(lat);
            $('#longitude').val(lng);
            ip_lat = lat;     ip_long = lng;    //set latlong 
            // geocodeLatLng(geocoder, map, infowindow);
        });
        var input = document.getElementById('address');
        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', map);
        autocomplete.setFields(["address_component","geometry"]);
        google.maps.event.addListener(autocomplete, 'place_changed', function () 
        {
            var place = autocomplete.getPlace(); console.log(place);
            var lat = place.geometry.location.lat();
            var lng = place.geometry.location.lng();

             setTimeout(function(){
                  // set states
                    for (var i = 0; i < place.address_components.length; i++) {                       
                        if(place.address_components[i].types[0] == 'locality'){ 
                             console.log('City '+place.address_components[i].long_name);               
                            $("select[name='city']").find('option[data-name="'+place.address_components[i].long_name+'"]').attr('selected','selected').trigger('change');                            
                        }
                    }
                }, 1000);

            if (place.geometry.viewport) 
            {
                map.fitBounds(place.geometry.viewport);
                var myLatlng = place.geometry.location; 
                var latlng = new google.maps.LatLng(lat, lng);
                infowindow.setContent(place.address_components.formatted_address);
                marker.setPosition(latlng);
            } 
            else 
            {
                map.setCenter(place.geometry.location); 
                map.setZoom(17);
            }
            $('#latitude').val(lat);
            $('#longitude').val(lng);
        });
   }
   function geocodeLatLng(geocoder, map, infowindow) {
     // var input = document.getElementById('latlng').value;
      //var latlngStr = input.split(',', 2); ip_lat = lat;     ip_long = lng;    //set latlong 
      var latlng = {lat: v_lat, lng: v_long};
      geocoder.geocode({'location': latlng}, function(results, status) {
        if (status === 'OK') {
          if (results[0]) {
            map.setZoom(11);
            var marker = new google.maps.Marker({
              position: latlng,
              draggable:true,
              map: map
            });
            infowindow.setContent(results[0].formatted_address);
            infowindow.open(map, marker);
          } else {
            window.alert('No results found');
          }
        } else {
          window.alert('Geocoder failed due to: ' + status);
        }
      });
    }
    //initialize_restaurant_loc();
    //google.maps.event.addDomListener(window, 'load', initialize_restaurant_loc);