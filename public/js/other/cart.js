function getCart() {
      
        $('#cartDiv').html(`<div class="detial-cart">
                              <div class="your-cart">
                                <h2>Cart</h2>
                                <span class="cart-items">....</span>
                              </div>
                            </div>`);        
            $.ajax( {
            url: getCartUrl,
           dataType: "json",
           
            success: function( data ) {
              console.log(data);
                  if(data.count==0){
                 var html =`<div class="detial-cart">
                              <div class="your-cart">
                                <h2>Cart</h2>
                                <span class="cart-items">Empty</span>
                              </div>
                              <div class="all-food" style="display:block;">
                                <img src="`+baseUrl+`/images/empty-cart.jpg" alt="Cart Empty" width="100%">
                                <div class="waights">Good food is always cooking! Go ahead, order some yummy items from the menu.</div>
                              </div>
                            </div>`;
                $('#cartDiv').html(html); 
            }
            else{
                // normal response
                var price=0;
                var items=``;

                
                $.map(data.item, function (item) {

                  var imgHtml=`<img src="{{ asset(config('app.logo')) }}" alt="`+item.name+`" >;`
                    if(item.image){
                        imgHtml=`<img src="{{ asset('`+item.image+`')}}" alt="`+item.name+`" >`;
                    }
                  var discountHtml=``;
                  if(item.discount!=0){
                      `<span style="text-decoration: line-through;color:gray;">&#8377 `+item.cost+`</span>;`
                  }
                    items+=`<div class="all-food" id='cart_item_`+item.id+'_'+item.price_id+`'>
                              <div class="food-image">`+imgHtml+`</div>
                            <div class="food-name">
                              <h2>`+item.name+`</h2>
                              <div class="waights">`+item.attributes+`</div>

                                        <div class="price">&#8377 `+item.price+discountHtml+`
                                       
                                        </div>
                            </div>
                            <div class="counter-number" id="cart_counter_`+item.id+'_'+item.price_id+`">
                               <div class="cartUpdate">
                                <div class="value-button" id="decrease2" onclick="updateCart(this,`+item.id+`);" value ="-" data-id="`+item.id+`" data-name="`+item.name+`" data-vendor_id="`+item.vendor_id+`" data-Pid="`+item.price_id+`" data-price="`+item.price+`" data-qty="`+item.qty+`" data-action="0">-</div>
                                <input type="number" style="width:24px;" id="cart_qty_`+item.id+'_'+item.price_id+`" readonly value="`+item.qty+`" />
                                <div class="value-button" id="increase2" onclick="updateCart(this,`+item.id+`);" value="+" data-id="`+item.id+`" data-name="`+item.name+`" data-vendor_id="`+item.vendor_id+`" data-Pid="`+item.price_id+`" data-price="`+item.price+`" data-qty="`+item.qty+`" data-action="1">+</div>
                              </div>
                            </div>
                            <div class="food-price">
                              <div class="price" id="cart_price_`+item.id+'_'+item.price_id+`">&#8377 `+(item.price*item.qty).toFixed(2)+`</div>
                            </div>
                          </div>`;
                  
                    price+= item.price*item.qty;
                    total=price.toFixed(2);
                // set item count
                 $('#qty_'+item.id+'_'+item.price_id).val(item.qty);
                // hide addCart Button & show Counter Buttons
                 $('#cart_btn_'+item.id).hide();
                 $('#item_counter_'+item.id).css('opacity',1);
                 $('#item_counter_'+item.id+'_'+item.price_id).css('opacity',1);

                 $('#cart_btn_'+item.id+'_'+item.price_id).hide();                         
                 $('#cart_counter_'+item.id+'_'+item.price_id).css('opacity',1); //alert();

                 /*update model detail*/               
                  $('#customize_btn_'+item.id).html('+Added');
                  $('#cart_btn_'+item.id+'_'+item.price_id).hide();
                  $('#cart_counter_'+item.id+'_'+item.price_id).show();
                  $('#qty_'+item.id+'_'+item.price_id).val(item.qty);
                  
                });
               /* update cart total*/
                cartTotal=total;
            var action=`<div class="totle-price">
                          <a href="`+checkoutUrl+`" style="color:unset;"
                            <span class="check-out-name">CheckOut</span>
                            <span class="check-out-price" id="cartPrice">&#8377 `+total+`</span>
                          </a>
                          <a href="`+checkoutUrl+`" style="color:red;"
                            <span class="check-out-name">Empty cart</span>
                            
                          </a>
                        </div>`;     
            var main=`<div class="detial-cart">
                              <div class="your-cart">
                                <h2>Cart</h2>
                                <span class="cart-items" id="cartDivCount">`+(data.item).length+` Items</span>
                              </div>
                              `+items+action+`
                      </div>`;
            $('#cartDiv').html(main);
            }
                  
                  
                }
              });
    }

    function updateCart(this_obj,item,price_id) {
      alert('update item');
      var product_id=$(this_obj).attr('data-id');
          var product_name=$(this_obj).attr('data-name');
          var product_price_id=$(this_obj).attr('data-Pid');
          var vendor_id=$(this_obj).attr('data-vendor_id');
          var action=$(this_obj).attr('data-action');
          var price=$(this_obj).attr('data-price');
          var qty=$(this_obj).attr('data-qty');
          /*console.log('product_id: '+ product_id,
                  'product_name: '+ product_name,
                  'product_price_id: '+ product_price_id,
                  'vendor_id: '+ vendor_id,
                  'action: '+ action,
                  'qty: '+qty);*/
       $.get(updateCartUrl, 
               { 'product_id': product_id,
                  'product_name': product_name,
                  'product_price_id': product_price_id,
                  'vendor_id': vendor_id,
                  'action': action,
                  'qty': qty },
               
                function( res ) {
                  if(res){
                      if(res.status=='success'){
                          //updateCartCount();
                         if(res.cartCount==0){
                          getCart();
                         }
                         else{
                          //getCart();
                         }
                          $(this_obj).attr('data-qty',res.count);
                          $('#cartDivCount').html(res.cartCount +' Items');
                          $('#cart_count').html(res.cartCount);
                          $('#cartCount').addClass('cartCount');

                          $('#cart_btn_'+product_id).hide();
                          $('#cart_counter_'+product_id).css('opacity',1); 

                          $('#cart_btn_'+product_id+'_'+product_price_id).hide();                         
                          $('#cart_counter_'+product_id+'_'+product_price_id).css('opacity',1); //alert();

                          $('#qty_'+product_id+'_'+product_price_id).val(res.count);
                          $('#cart_qty_'+product_id+'_'+product_price_id).val(res.count);

                         $('#cartDiv').find('#cart_price_'+product_id+'_'+product_price_id).html('₹'+res.price);
                         // console.log($('#cart_price'+product_id+'_'+product_price_id).html());
                         
                         // console.log($(this_obj).parents('#cart_item_'+product_id+'_'+product_price_id).html());
                         //$(this_obj).parents().find('#cart_price'+product_id+'_'+product_price_id).html('₹'+res.price);
                          //$(this_obj).parents().find('#cart_qty_'+product_id+'_'+product_price_id).val(res.count);
                         $(this_obj).parents('#cart_item_'+product_id+'_'+product_price_id).find('#cart_price'+product_id+'_'+product_price_id).html('₹'+res.price);
                          
                          if(res.count==0){
                            $('#cart_btn_'+product_id).show();
                            $('#cart_btn_'+product_id+'_'+product_price_id).show();

                            $('#item_counter_'+product_id).css('opacity',0);
                            $('#item_counter_'+product_id+'_'+product_price_id).css('opacity',0);
                            $('#cart_counter_'+product_id).css('opacity',0);

                            $('#qty_'+product_id+'_'+product_price_id).val(res.count);
                            $('#cart_qty_'+product_id+'_'+product_price_id).val(res.count);
                            $('#cartDiv').find('#cart_item_'+product_id+'_'+product_price_id).remove();
                               
                          }
                         // alert('cartTotal : '+cartTotal)
                          var tprice=cartTotal;//$('#cartDiv').find('#cartPrice').html('...');
                          tprice=parseFloat(tprice) || 0;
                          price=parseFloat(price) || 0;
                          var total=0;
                          if(action==1){
                            total=(tprice+price).toFixed(2);
                            $('#cartDiv').find('#cartPrice').html('₹ '+res.data.cartTotal);
                          }else{
                            total=(tprice-price).toFixed(2);
                            $('#cartDiv').find('#cartPrice').html('₹ '+res.data.cartTotal);
                          }
                          cartTotal=res.data.cartTotal;
                          
                      }
                      else{
                          toastr.warning(res.msg, { closeButton: !0 });
                      }
                  }else{
                      toastr.error("Refresh Page and Try Again..", { closeButton: !0 });                   
                  }
                  
                  
                }
              );
    }

/*/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////*/



    function getCart2() {
    	
        $('#cartDiv').html(`<div align="center" style="text-align:center;">Waiting......</div>`);         
            $.ajax( {
	          url: "{{route('ajax.getCart')}}",
	         dataType: "json",
	         
	          success: function( data ) {
                  if(!data.item.length){
				        var html =`<div align="center" style="text-align:center;">
                              
                              <img src="{{ asset('images/empty-cart.jpg')}}">
                          </div>`;
				        $('#cartDiv').html(html); 
                /*<h2>Your cart is empty<br>Add items to get started</h2>*/
				    }
				    else{
				        // normal response
				        var price=0;
				       // var html='<div class="row" style="max-height: 350px;overflow-y: scroll;">';	
                 var html='<div class="row" style="">';  
				        $.map(data.item, function (item) {
				            
				          html +=`<div class="item" id="citem_`+item.id+`" >
						    		<div id="item_btn_`+item.id+`" class="">
						    			
						    			
	<div id="cart_gbtn_`+item.id+`" class="quantity-button" style="">
        <input type="button" onclick="updateCart(this,`+item.id+`);" value ="-" data-id="`+item.id+`" data-name="`+item.name+`" data-vendor_id="`+item.vendor_id+`" data-Pid="`+item.price_id+`" data-price="`+item.price+`" data-qty="`+item.qty+`" data-action="0" class="inc-dec form-controll">
        <input type="text" id="qty_`+item.id+`" min="0" readonly="" style="text-align: center;" value="`+item.qty+`" class="form-controll number-btn">
        <input type="button" onclick="updateCart(this,`+item.id+`);" value="+" data-id="`+item.id+`" data-name="`+item.name+`" data-vendor_id="`+item.vendor_id+`" data-Pid="`+item.price_id+`" data-price="`+item.price+`" data-qty="`+item.qty+`" data-action="1" class="form-controll inc-dec">
    </div>
						    		</div>
						    		<span><b>`+item.name+`</b>
                    <i style="padding-left: 1rem;color: gray;">( `+item.attributes+` )</i><br>
						    			<span>Rs. `+item.price+`</span> 
                      <span style="text-decoration: line-through;color:gray;">Rs. `+item.cost+`</span> 
                       <b style="color:tomato">(`+item.discount+`%)</b>
						    			
						    		</span>
						    		
				            	</div>`; 
				            price+= item.price*item.qty;
				            total=price.toFixed(2);
				        });
				        html+="</div>";
				        html+=`<div id="cartprice" align="center" style="margin-top:1rem;">
				        		<h5  style="padding:10px;padding: 1rem;text-align: center;">
                            		<b>Total Amount:&nbsp;&nbsp;&nbsp;&nbsp;Rs.<span id="cartcost">`+total+`</span></b>
                            	</h5><br>
						        <a href="{{ route('checkout')}}" class="btn btn-lg btn-success">Proceed to checkout</a>
                     <a href="{{ route('cart.empty')}}" class="btn btn-lg btn-danger">Empty cart</a>
						       </div>`;
				        $('#cartDiv').html(html);
				    }
                  
                  
                }
              });
    }
    function updateCart2(this_obj,item) {
    	var product_id=$(this_obj).attr('data-id');
          var product_name=$(this_obj).attr('data-name');
          var product_price_id=$(this_obj).attr('data-Pid');
          var vendor_id=$(this_obj).attr('data-vendor_id');
          var action=$(this_obj).attr('data-action');
          var price=$(this_obj).attr('data-price');
          var qty=$(this_obj).attr('data-qty');
          console.log('product_id: '+ product_id,
                  'product_name: '+ product_name,
                  'product_price_id: '+ product_price_id,
                  'vendor_id: '+ vendor_id,
                  'action: '+ action,
                  'qty: '+qty);
    	 $.get('{{ route('ajax.updateCart') }}', 
               { 'product_id': product_id,
                  'product_name': product_name,
                  'product_price_id': product_price_id,
                  'vendor_id': vendor_id,
                  'action': action,
                  'qty': qty },
               
                function( res ) {
                  if(res){
                      if(res.status=='success'){
                      	  updateCartCount();
                         if(res.cartCount==0){
                         	getCart();
                         }

                          $('#cart_btn_'+product_id).hide();
                          $('#cart_gbtn_'+product_id).show();

                          $('#qty_'+product_id).val(res.count);
                          $(this_obj).parents('#cart_gbtn_'+product_id).find('#cqty_'+product_id).val(res.count);
                          var tprice=$('#cartDiv').find('#cartprice h5 #cartcost').html();
                          if(res.count==0){
                          	//alert('remove');
                          		 $('#cartDiv').find('#citem_'+product_id).remove();
                          		 $('#cart_btn_'+product_id).show();
                          		$('#cart_gbtn_'+product_id).hide();
                          }
                          tprice=parseFloat(tprice) || 0;
                          price=parseFloat(price) || 0;
                          var total=0;
                          if(action==1){
                          	total=(tprice+price).toFixed(2);
                          	$('#cartDiv').find('#cartprice h5 #cartcost').html(total);
                          }else{
                          	total=(tprice-price).toFixed(2);
                          	$('#cartDiv').find('#cartprice h5 #cartcost').html(total);
                          }
                          
                      }
                      else{
                          toastr.warning(res.msg, { closeButton: !0 });
                      }
                  }else{
                      toastr.error("Refresh Page and Try Again..", { closeButton: !0 });                   
                  }
                  
                  
                }
              );
    }
    function updateCart22(this_obj,item) {
    	var product_id=$(this_obj).attr('data-id');
          var product_name=$(this_obj).attr('data-name');
          var product_price_id=$(this_obj).attr('data-Pid');
          var vendor_id=$(this_obj).attr('data-vendor_id');
          var action=$(this_obj).attr('data-action');
          var price=$(this_obj).attr('data-price');
          var qty=$(this_obj).attr('data-qty');
          console.log('product_id: '+ product_id,
                  'product_name: '+ product_name,
                  'product_price_id: '+ product_price_id,
                  'vendor_id: '+ vendor_id,
                  'action: '+ action,
                  'qty: '+qty);
    	 $.get('{{ route('ajax.updateCart') }}', 
               { 'product_id': product_id,
                  'product_name': product_name,
                  'product_price_id': product_price_id,
                  'vendor_id': vendor_id,
                  'action': action,
                  'qty': qty },
               
                function( res ) {
                  if(res){
                      if(res.status=='success'){
                      	  updateCartCount();
                          $('#cartDiv').find('.item .item_btn_'+product_id).find('#cart_gbtn_'+product_id).find('#qty_'+product_id).val(res.count);
                          $(this_obj).parents('#cart_gbtn_'+product_id).find('#qty_'+product_id).val(res.count);
                          var tprice=$('#cartDiv').find('#cartprice h5 #cartcost').html();
                          if(res.count==0){
                          		 $('#cartDiv').parents('#item').find('#item_btn_'+product_id).remove();
                          }
                          tprice=parseFloat(tprice) || 0;
                          price=parseFloat(price) || 0;
                          var total=0;
                          if(action==1){
                          	total=(tprice+price).toFixed(2);
                          	$('#cartDiv').find('#cartprice h5 #cartcost').html(total);
                          }else{
                          	total=(tprice-price).toFixed(2);
                          	$('#cartDiv').find('#cartprice h5 #cartcost').html(total);
                          }
                          
                      }
                      else{
                          toastr.warning(res.msg, { closeButton: !0 });
                      }
                  }else{
                      toastr.error("Refresh Page and Try Again..", { closeButton: !0 });                   
                  }
                  
                  
                }
              );
    }
</script>