    $(document).ready(function () {
        $.ajaxSetup({
           headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
       });

    // search clear btn 
      $('.clear').click(function() {
        $('.sea-search').val('');
         $('#searchResult').hide();
      });

        $('#filter').change(function(){
            var id=$(this).val();
            var selectedText = $("#filter option:selected").html();
            //$("#filter option:selected").html('Sort by : '+selectedText);;
        });
        
        $('#search').on('keypress',function(e) {
            if(e.which == 13) {
                var val=$( "#search" ).val();
                if(val){
                    getThisData();
                    $('#ui-id-1').hide();
                }
            }
        });
        {{ isset($request['term']) ? 'getThisData();' : ""}}
        $('#search').autocomplete({
           // appendTo: "#searchResult",
          source: function( request, response ) {
            $.ajax( {
              url: "{{route('ajax.comman.search',$master->id)}}",
              dataType: "json",
              data: {
                term: request.term
              },
              success: function( data ) {
                //response( data );
                    if(!data.length){
                        var result = [
                            {
                                label: 'No matches found', 
                                value: response.term
                            }
                        ];
                        response(result);
                    }
                    else{
                        // normal response
                        response($.map(data, function (item) {
                            return {
                                label: item.label,// + " ( Vendor )",
                                value: item.value,
                                url: item.url,
                                title: item.title,
                                image: item.image,
                                type: item.type,


                            }
                        }));
                    }
              }
            } );
          },
          minLength: 1,
          select: function( event, ui ) {
            $( "#search" ).val( ui.item.label );
            $( "#type" ).val( ui.item.type );
           
            //$('#search').trigger('keypress');
            getThisData();
          }
        } ).autocomplete("instance")._renderItem = function(ul, item) {
                        //console.log(item.label);                           
                           var label = '';
                           if (item.image){
                              label =`<div class="search-icon"><img src="`+ item.image + `" alt=""/>
                                </div> `;
                           }
                           label +=`<div class="search-details"><span>`+ item.label+`</span>`;   
                           if (item.title){
                              //label += " <span class='ui-autocomplete-item-sub'style='float:right;margin-right:8px;'><i>" + item.title + "</i></span>";
                              label+=`<span>`+item.title +`</span>`;
                           }
                           label +=`</div>`;
                           return $("<li></li>").append( label ).appendTo(ul);
                            //return $("<li></li>").data("item.autocomplete", item).append("<a>" + label + "</a>").appendTo(ul);
                        };

/*.autocomplete( "instance" )._renderItem = function( ul, item ) {

              return $( "<li>" )
                .append()
                .append( "<div>" + item.label + "<br>" + item.desc + "</div>" )
                .appendTo( ul );
            };*/
 
    });
 function getThisData(){
    var term=$( "#search" ).val();
    var type=$( "#type" ).val();
            $.get('{{route('ajax.this.search')}}', 
                { 'term': term,
                  'type': type,
                  'master_id':'{{$master->id}}'
                   },
              //  dataType: "json",               
             function( res ) {
                
                var vendorHtml=''; var itemHtml='';
                 if(res.vendorCount==0){alert('vendor 0');
                      vendorHtml =`<div class="detial-cart">
                                  <div class="your-cart">
                                    <h2>Cart</h2>
                                    <span class="cart-items">Empty</span>
                                  </div>
                                  <div class="all-food" style="display:block;">
                                    <img src="`+baseUrl+`/images/empty-cart.jpg" alt="Cart Empty" width="100%">
                                    <div class="waights">Good food is always cooking! Go ahead, order some yummy items from the menu.</div>
                                  </div>
                                </div>`;
                    
                }
                else{
                    
                    $.map(res.vendor, function (item) {
                        
                        var imgHtml=`<img src="{{ asset(config('app.logo')) }}" alt="`+item.name+`" >;`
                        if(item.image){
                            imgHtml=`<img src="{{ asset('`+item.image+`')}}" alt="`+item.name+`" >`;
                        }
                        var costof2=``;
                        if(item.costoftwo){
                            costof2=`<div class="item-price">&#8377; `+item.costoftwo+` <span class="words">for two</span></div>`;
                        }
                        
                        vendorHtml+=`<div class="col-md-4 col-sm-6 col-xm-6 animatable bounceIn">
                                        <a href="">
                                            <div class="items">
                                                <div class="item-img">`+imgHtml+`</div>
                                                <div class="item-name">
                                                    <h2>`+item.company_name+`</h2>
                                                    <div class="item-title">
                                                        <p>`+item.city_name+`</p>
                                                    </div>
                                                </div>
                                                <div class="item-dtl">
                                                    <div class="item-rating">
                                                        <span class="item-stars"><i class="fas fa-star"></i></span>
                                                        <span class="item-rate">4.1</span>
                                                    </div>
                                                    <div class="item-hur">23 Min</div>
                                                    `+costof2+`
                                                </div>
                                                <div class="dsicount">
                                                    <span class="dis-logo"><img src="images/icons-images/discount.png"
                                                            alt=""></span>
                                                    <span class="dis-dtl">60% off | Use DEAL60</span>
                                                </div>
                                            </div>
                                        </a>
                                    </div>`;
                        console.log(vendorHtml);
                    });
                    
                }
/*items maping*/
                var itemHtml='';
                 if(res.itemsCount==0){alert('vendor 0');
                      itemHtml =`<div class="detial-cart">
                                  <div class="your-cart">
                                    <h2>Cart</h2>
                                    <span class="cart-items">Empty</span>
                                  </div>
                                  <div class="all-food" style="display:block;">
                                    <img src="`+baseUrl+`/images/empty-cart.jpg" alt="Cart Empty" width="100%">
                                    <div class="waights">Good food is always cooking! Go ahead, order some yummy items from the menu.</div>
                                  </div>
                                </div>`;
                    
                }
                else{
                     $.map(res.items, function (item) {
                        var imgHtml=`<img src="{{ asset(config('app.logo')) }}" alt="`+item[0].name+`" >;`
                        if(item[0].image){
                            imgHtml=`<img src="{{ asset('`+item[0].image+`')}}" alt="`+item[0].name+`" >`;
                        }
                        var discount=``;
                        if(item[0].product_cost){
                            discount=` <span style="text-decoration: line-through;color:gray;">&#8377 `+item[0].product_cost+`</span>`;
                        }
                        var itemsHtml=``;
                        if(item.length==1){
                            itemsHtml=`<button id="cart_btn_`+item[0].productsId+`" data-id="`+item[0].productsId+`" data-name="`+item[0].name+`" data-vendor_id="`+item[0].vendor_id+`" data-Pid="`+item[0].product_price_id+`" data-qty="" data-price="`+item[0].product_price+`" onclick="addCart(this);" data-action="1" type="button">
                                          <span class="add-icon"><i class="fas fa-plus"></i></span>Add
                                        </button>
                                        <div class="counter-number" id="item_counter_`+item[0].productsId+`">
                                          
                                            <div class="cartUpdate">
                                                <div class="value-button" id="decrease2" onclick="updateCart(this,`+item[0].productsId+`);" value ="-" data-id="`+item[0].productsId+`" data-name="`+item[0].name+`" data-vendor_id="`+item[0].vendor_id+`" data-Pid="`+item[0].product_price_id+`" data-qty="" data-price="`+item[0].product_price+`" data-action="0">-</div>

                                                <input type="number" style="width:24px" id="qty_`+item[0].productsId+`_`+item[0].product_price_id+`" value="0" />
                                                <div class="value-button" id="increase2" onclick="updateCart(this,`+item[0].productsId+`);" value ="+" data-id="`+item[0].productsId+`" data-name="`+item[0].name+`" data-vendor_id="`+item[0].vendor_id+`" data-Pid="`+item[0].product_price_id+`" data-qty="" data-price="`+item[0].product_price+`" data-action="1">+</div>
                                            </div>
                                        </div>`;
                        }else{
                           var customize=``;
                           $.map(item, function (vitem) {
                                customize+=`<div class="input-radio">
                                    <span class="custom-icons"></span>
                                    <span class="radio-title">
                                        `+vitem.product_qty+` `+vitem.attributes_name+`</span>
                                        <span>&#8377 `+vitem.product_price+`</span>
                                        <div class="add-food">
                                          <button id="cart_btn_`+vitem.productsId+`_`+vitem.product_price_id+`" data-id="`+vitem.productsId+`" data-name="`+vitem.name+`" data-vendor_id="`+vitem.vendor_id+`" data-Pid="`+vitem.product_price_id+`" data-qty="" data-price="`+vitem.product_price+`" onclick="addCart(this);" data-action="1" type="button">
                                                      <span class="add-icon"><i class="fas fa-plus"></i></span>Add
                                                    </button>
                                           <div class="counter-number" id="item_counter_`+vitem.productsId+`_`+vitem.product_price_id+`">
                                                <div class="cartUpdate">

                                                    <div class="value-button" id="decrease2" onclick="updateCart(this,`+vitem.productsId+`);" value ="-" data-id="`+vitem.productsId+`" data-name="`+vitem.name+`" data-vendor_id="`+vitem.vendor_id+`" data-Pid="`+vitem.product_price_id+`" data-qty="" data-price="`+vitem.product_price+`" data-action="0">-</div>
                                                    <input type="number" style="width:24px" id="qty_`+vitem.productsId+`_`+vitem.product_price_id+`" value="0" />
                                                    <div class="value-button" id="increase2" onclick="updateCart(this,`+vitem.productsId+`);" value ="+" data-id="`+vitem.productsId+`" data-name="`+vitem.name+`" data-vendor_id="`+vitem.vendor_id+`" data-Pid="`+vitem.product_price_id+`" data-qty="" data-price="`+vitem.product_price+`" data-action="1">+</div>

                                                </div>
                                            </div>
                                        </div>
                                  </div>`;
                             });
                           /*///////////////////////////*/
                           itemsHtml=`<button data-toggle="modal" id="customize_btn_`+item[0].productsId+`" data-target="#customize_`+item[0].productsId+`"><spna class="add-icon"><i class="fas fa-plus"></i></spna>Add</button>
                                <div class="modal fade customize-model" id="customize_`+item[0].productsId+`" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                      <div class="modal-content" style="background-color: #fff;padding: 1rem;text-align:left;">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                          </button>
                                          <div class="customize-details">
                                              <div class="custom-headind">
                                                  <h2>
                                                    <span class="custom-icons"></span>
                                                    Customize “`+item[0].name+`”
                                                  </h2>
                                                  
                                              </div>
                                              <div class="custom-items">`+customize+`</div>
                                             
                                          </div>
                                    </div>
                                  </div>
                                </div>`;
                        }

                        itemHtml+=`<div class="food-detials" data-price="`+ item[0].product_price+`">
                                    <div class="food-image">`+imgHtml+`</div>
                                    <div class="food-name">
                                        <h2>`+item[0].name+`</h2>
                                         <div class="amounts">`+item[0].product_qty+' '+item[0].attributes_name+`</div>
                                        <div class="price">&#8377 `+ item[0].product_price+discount+`
                                        </div>
                                    </div>
                                    <div class="add-food">
                                    `+itemsHtml+`
                                    </div>
                                </div>
                      `;
                      });
                }
                $('#restaurantsData').html(vendorHtml);
                $('#dishes').html(itemHtml);

                $('#searchResult').show();
             }/*else{
                 //toastr.error("Refresh Page and Try Again..", { closeButton: !0 }); 
             }*/
            );
    
 }   