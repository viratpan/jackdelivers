$(document).ready(function () {
        $.ajaxSetup({
           headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
       });
        $('#filter').change(function(){
            var id=$(this).val();
            var selectedText = $("#filter option:selected").html();
            //$("#filter option:selected").html('Sort by : '+selectedText);;
        });
        
        $('#search').on('keypress',function(e) {
            if(e.which == 13) {
                var val=$( "#search" ).val();
                if(val){
                    getThisData();
                }
            }
        });
        $('#search').autocomplete({
           // appendTo: "#searchResult",
          source: function( request, response ) {
            $.ajax( {
              url: "{{route('ajax.comman.search',$master->id)}}",
              dataType: "json",
              data: {
                term: request.term
              },
              success: function( data ) {
                //response( data );
                    if(!data.length){
                        var result = [
                            {
                                label: 'No matches found', 
                                value: response.term
                            }
                        ];
                        response(result);
                    }
                    else{
                        // normal response
                        response($.map(data, function (item) {
                            return {
                                label: item.label,// + " ( Vendor )",
                                value: item.value,
                                url: item.url,
                                title: item.title,
                                image: item.image,
                                type: item.type,


                            }
                        }));
                    }
              }
            } );
          },
          minLength: 1,
          select: function( event, ui ) {
            $( "#search" ).val( ui.item.label );
            $( "#type" ).val( ui.item.type );
           
            //$('#search').trigger('keypress');
            getThisData();
          }
        } ).autocomplete("instance")._renderItem = function(ul, item) {
                        //console.log(item.label);                           
                           var label = '';
                           if (item.image){
                              label =`<div class="search-icon"><img src="`+ item.image + `" alt=""/>
                                </div> `;
                           }
                           label +=`<div class="search-details"><span>`+ item.label+`</span>`;   
                           if (item.title){
                              //label += " <span class='ui-autocomplete-item-sub'style='float:right;margin-right:8px;'><i>" + item.title + "</i></span>";
                              label+=`<span>`+item.title +`</span>`;
                           }
                           label +=`</div>`;
                           return $("<li></li>").append( label ).appendTo(ul);
                            //return $("<li></li>").data("item.autocomplete", item).append("<a>" + label + "</a>").appendTo(ul);
                        };

/*.autocomplete( "instance" )._renderItem = function( ul, item ) {

              return $( "<li>" )
                .append()
                .append( "<div>" + item.label + "<br>" + item.desc + "</div>" )
                .appendTo( ul );
            };*/
        

        $( "#vendorSearch" ).autocomplete({
          source: function( request, response ) {
            $.ajax( {
              url: "{{route('ajax.vendor.search',$master->id)}}",
              dataType: "json",
              data: {
                term: request.term
              },
              success: function( data ) {
                //response( data );
                    if(!data.length){
                        var result = [
                            {
                                label: 'No matches found', 
                                value: response.term
                            }
                        ];
                        response(result);
                    }
                    else{
                        // normal response
                        response($.map(data, function (item) {
                            return {
                                label: item.label,// + " ( Vendor )",
                                value: item.value,
                                url: item.url
                            }
                        }));
                    }
              }
            } );
          },
          minLength: 1,
          select: function( event, ui ) {
            // similar behavior as an HTTP redirect
            //window.location.replace(ui.item.url);

            // similar behavior as clicking on a link
            window.location.href = ui.item.url;
          }
        } );


        $( "#itemSearch" ).autocomplete({
          source: function( request, response ) {
            $.ajax( {
              url: "{{route('ajax.item.search',$master->id)}}",
              dataType: "json",
              data: {
                term: request.term
              },
              success: function( data ) {
                //response( data );
                    if(!data.length){
                        var result = [
                            {
                                label: 'No matches found', 
                                value: response.term
                            }
                        ];
                        response(result);
                    }
                    else{
                        // normal response
                        response($.map(data, function (item) {
                            return {
                                label: item.label,//+ " ( Product )",
                                value: item.value,
                                url: item.url
                            }
                        }));
                    }
              }
            } );
          },
          minLength: 1,
          select: function( event, ui ) {
            //window.location.replace(ui.item.url);
            window.location.href = ui.item.url;
          }
        } );




    });
 function getThisData(){
    var term=$( "#search" ).val();
    var type=$( "#type" ).val();
            $.get('{{route('ajax.this.search')}}', 
                { 'term': term,
                  'type': type,
                  'master_id':'{{$master->id}}'
                   },
              //  dataType: "json", 
               
             function( data ) {
                alert('on hit');
             }
            );
    $('#searchResult').show();
 }   
   