function addCart(this_obj){ 
          var product_id=$(this_obj).attr('data-id');
          var product_name=$(this_obj).attr('data-name');
          var product_price_id=$(this_obj).attr('data-Pid');
          var vendor_id=$(this_obj).attr('data-vendor_id');
         // var price=$(this_obj).attr('data-price');
          var qty=$(this_obj).attr('data-qty');
          
           $.get(addCartUrl, 
                { 'product_id': product_id,
                  'product_name': product_name,
                  'product_price_id': product_price_id,
                  'vendor_id': vendor_id,
                 // 'price': price,
                  'qty': qty },
              //  dataType: "json", 
                function( res ) {
                  if(res){
                      if(res.status=='success'){
                        getCart();
                        //updateCartDiv();
                       // updateCartCount();
                        $(this_obj).attr('data-qty',res.count);
                         $('#cart_count').html(res.cartCount);
                          $('#cartCount').addClass('cartCount');

                          $('#cart_btn_'+product_id).hide();
                          //$('#cart_counter_'+product_id).css('opacity',1); //alert();
                          //$('#item_counter_'+product_id+'_'+product_price_id).css('opacity',1);

                          $('#cart_counter_'+product_id).show(); //alert();
                          $('#item_counter_'+product_id+'_'+product_price_id).show();

                          $('#cart_btn_'+product_id+'_'+product_price_id).hide();                         
                          //$('#cart_counter_'+product_id+'_'+product_price_id).css('opacity',1); //alert();
                          $('#cart_counter_'+product_id+'_'+product_price_id).show(); //alert();



                          $('#qty_'+product_id+product_price_id).val(res.count);
                         /*$('#item_btn_'+product_id).html();
                         $('#qty_'+product_id).val(res.count);*/
                         toastr.success(product_name+" Added to cart.", { closeButton: !0 });
                      }
                      else{
                          toastr.warning(res.msg, { closeButton: !0 });
                      }
                  }else{
                      toastr.error("Refresh Page and Try Again..", { closeButton: !0 });                   
                  }
                  
                  
                }
              );
          
      }
    function addCart2(this_obj){ 
          var product_id=$(this_obj).attr('data-id');
          var product_name=$(this_obj).attr('data-name');
          var product_price_id=$(this_obj).attr('data-Pid');
          var vendor_id=$(this_obj).attr('data-vendor_id');
         // var price=$(this_obj).attr('data-price');
          var qty=$(this_obj).attr('data-qty');
          console.log('product_id: '+ product_id,
                  'product_name: '+ product_name,
                  'product_price_id: '+ product_price_id,
                  'vendor_id: '+ vendor_id,
                  'qty: '+qty);
         /* $.ajax( {
             type:"GET",
            url: "{{route('ajax.addCart')}}",
           dataType: "json",*/
           $.get(addCartUrl, 
                { 'product_id': product_id,
                  'product_name': product_name,
                  'product_price_id': product_price_id,
                  'vendor_id': vendor_id,
                  'action': action,
                 // 'price': price,
                  'qty': qty },
              //  dataType: "json", 
                function( res ) {
                  if(res){
                      if(res.status=='success'){
                       // getCart();
                        //updateCartDiv();
                        //updateCartCount();
                        $(this_obj).attr('data-qty',res.count);
                         $('#cart_count').html(res.cartCount);
                          $('#cartCount').addClass('cartCount');
                          $('#cart_btn_'+product_id).hide();
                          $('#cart_counter_'+product_id).css('opacity',1); alert();
                          $('#qty_'+product_id+product_price_id).val(res.count);


                        $(this_obj).attr('data-qty',res.count);
                         $('#cart_count').html(res.cartCount);
                          $('#cartCount').addClass('cartCount');
                          $(this_obj).html('ADDED');
                          $('#model_'+product_id).html('ADDED');
                          $(this_obj).attr('disable',true);
                          $('#cart_btn_'+product_id+'_'+product_price_id).hide();
                          $('#cart_gbtn_'+product_id+'_'+product_price_id).show();
                         $('#item_btn_'+product_id+'_'+product_price_id).html();
                         $('#qty_'+product_id+'_'+product_price_id).val(res.count);
                         toastr.success(product_name+" Added to cart.", { closeButton: !0 });
                      }
                      else{
                          toastr.warning(res.msg, { closeButton: !0 });
                      }
                  }else{
                      toastr.error("Refresh Page and Try Again..", { closeButton: !0 });                   
                  }
                  
                  
                }
              );
          
      }
    function getCart() {
      
        $('#cartDiv').html(`<div class="detial-cart">
                              <div class="your-cart">
                                <h2>Cart</h2>
                                <span class="cart-items">....</span>
                              </div>
                            </div>`);        
            $.ajax( {
            url: getCartUrl,
           dataType: "json",
           
            success: function( data ) {
              console.log(data);
                  if(data.count==0){
                 var html =`<div class="detial-cart">
                              <div class="your-cart">
                                <h2>Cart</h2>
                                <span class="cart-items">Empty</span>
                              </div>
                              <div class="all-food" style="display:block;">
                                <img src="`+baseUrl+`/images/empty-cart.jpg" alt="Cart Empty" width="100%">
                                <div class="waights">Good food is always cooking! Go ahead, order some yummy items from the menu.</div>
                              </div>
                            </div>`;
                $('#cartDiv').html(html); 
            }
            else{
                // normal response
                var price=0;
                var items=``;

                
                $.map(data.item, function (item) {
                    
                    items+=`<div class="all-food" id='cart_item_`+item.id+'_'+item.price_id+`'>
                            <div class="food-name">
                              <h2>`+item.name+`</h2>
                              <div class="waights">`+item.attributes+`</div>
                            </div>
                            <div class="counter-number" id="cart_counter_`+item.id+'_'+item.price_id+`">
                               <div class="cartUpdate">
                                <div class="value-button" id="decrease2" onclick="updateCart(this,`+item.id+`);" value ="-" data-id="`+item.id+`" data-name="`+item.name+`" data-vendor_id="`+item.vendor_id+`" data-Pid="`+item.price_id+`" data-price="`+item.price+`" data-qty="`+item.qty+`" data-action="0">-</div>
                                <input type="number" style="width:24px;" id="cart_qty_`+item.id+'_'+item.price_id+`" readonly value="`+item.qty+`" />
                                <div class="value-button" id="increase2" onclick="updateCart(this,`+item.id+`);" value="+" data-id="`+item.id+`" data-name="`+item.name+`" data-vendor_id="`+item.vendor_id+`" data-Pid="`+item.price_id+`" data-price="`+item.price+`" data-qty="`+item.qty+`" data-action="1">+</div>
                              </div>
                            </div>
                            <div class="food-price">
                              <div class="price" id="cart_price_`+item.id+'_'+item.price_id+`">&#8377 `+(item.price*item.qty).toFixed(2)+`</div>
                            </div>
                          </div>`;
                  
                    price+= item.price*item.qty;
                    total=price.toFixed(2);
                // set item count
                 $('#qty_'+item.id+'_'+item.price_id).val(item.qty);
                // hide addCart Button & show Counter Buttons
                 $('#cart_btn_'+item.id).hide();
                 //$('#item_counter_'+item.id).css('opacity',1);
                 //$('#item_counter_'+item.id+'_'+item.price_id).css('opacity',1);

                 $('#item_counter_'+item.id).show();
                 $('#item_counter_'+item.id+'_'+item.price_id).show();

                 $('#cart_btn_'+item.id+'_'+item.price_id).hide();                         
                 //$('#cart_counter_'+item.id+'_'+item.price_id).css('opacity',1); //alert();
                 $('#cart_counter_'+item.id+'_'+item.price_id).show(); //alert();

                 /*update model detail*/               
                  $('#customize_btn_'+item.id).html('+Added');
                  $('#cart_btn_'+item.id+'_'+item.price_id).hide();
                  $('#cart_counter_'+item.id+'_'+item.price_id).show();
                  $('#qty_'+item.id+'_'+item.price_id).val(item.qty);
                  
                });
               /* update cart total*/
                cartTotal=total;
            var action=`<div class="totle-price">
                          <a href="`+checkoutUrl+`" style="color:unset;"
                            <span class="check-out-name">CheckOut</span>
                            <span class="check-out-price" id="cartPrice">&#8377 `+total+`</span>
                          </a>
                        </div>`;     
            var main=`<div class="detial-cart">
                              <div class="your-cart">
                                <h2>Cart</h2>
                                <span class="cart-items" id="cartDivCount">`+(data.item).length+` Items</span>
                              </div>
                              `+items+action+`
                      </div>`;
            $('#cartDiv').html(main);
            }
                  
                  
                }
              });
    }
    function cartDivManage(argument) {
      // body...
    }
    function updateCart(this_obj,item,price_id) {
      alert('update item');
      var product_id=$(this_obj).attr('data-id');
          var product_name=$(this_obj).attr('data-name');
          var product_price_id=$(this_obj).attr('data-Pid');
          var vendor_id=$(this_obj).attr('data-vendor_id');
          var action=$(this_obj).attr('data-action');
          var price=$(this_obj).attr('data-price');
          var qty=$(this_obj).attr('data-qty');
          /*console.log('product_id: '+ product_id,
                  'product_name: '+ product_name,
                  'product_price_id: '+ product_price_id,
                  'vendor_id: '+ vendor_id,
                  'action: '+ action,
                  'qty: '+qty);*/
       $.get(updateCartUrl, 
               { 'product_id': product_id,
                  'product_name': product_name,
                  'product_price_id': product_price_id,
                  'vendor_id': vendor_id,
                  'action': action,
                  'qty': qty },
               
                function( res ) {
                  if(res){
                      if(res.status=='success'){
                          //updateCartCount();
                         if(res.cartCount==0){
                          getCart();
                         }
                         else{
                          //getCart();
                         }
                          $(this_obj).attr('data-qty',res.count);
                          $('#cartDivCount').html(res.cartCount +' Items');
                          $('#cart_count').html(res.cartCount);
                          $('#cartCount').addClass('cartCount');

                          $('#cart_btn_'+product_id).hide();
                          //$('#cart_counter_'+product_id).css('opacity',1); 
                          $('#cart_counter_'+product_id).show(); 

                          $('#cart_btn_'+product_id+'_'+product_price_id).hide();                         
                          //$('#cart_counter_'+product_id+'_'+product_price_id).css('opacity',1); //alert();
                          $('#cart_counter_'+product_id+'_'+product_price_id).show(); //alert();

                          $('#qty_'+product_id+'_'+product_price_id).val(res.count);
                          $('#cart_qty_'+product_id+'_'+product_price_id).val(res.count);

                         $('#cartDiv').find('#cart_price_'+product_id+'_'+product_price_id).html('₹'+res.price);
                         // console.log($('#cart_price'+product_id+'_'+product_price_id).html());
                         
                         // console.log($(this_obj).parents('#cart_item_'+product_id+'_'+product_price_id).html());
                         //$(this_obj).parents().find('#cart_price'+product_id+'_'+product_price_id).html('₹'+res.price);
                          //$(this_obj).parents().find('#cart_qty_'+product_id+'_'+product_price_id).val(res.count);
                         $(this_obj).parents('#cart_item_'+product_id+'_'+product_price_id).find('#cart_price'+product_id+'_'+product_price_id).html('₹'+res.price);
                          
                          if(res.count==0){
                            $('#cart_btn_'+product_id).show();
                            $('#cart_btn_'+product_id+'_'+product_price_id).show();

                            // $('#item_counter_'+product_id).css('opacity',0);
                            // $('#item_counter_'+product_id+'_'+product_price_id).css('opacity',0);
                            // $('#cart_counter_'+product_id).css('opacity',0);

                            $('#item_counter_'+product_id).hide();
                            $('#item_counter_'+product_id+'_'+product_price_id).hide();
                            $('#cart_counter_'+product_id).hide();

                            $('#qty_'+product_id+'_'+product_price_id).val(res.count);
                            $('#cart_qty_'+product_id+'_'+product_price_id).val(res.count);
                            $('#cartDiv').find('#cart_item_'+product_id+'_'+product_price_id).remove();
                               
                          }
                         // alert('cartTotal : '+cartTotal)
                          var tprice=cartTotal;//$('#cartDiv').find('#cartPrice').html('...');
                          tprice=parseFloat(tprice) || 0;
                          price=parseFloat(price) || 0;
                          var total=0;
                          if(action==1){
                            total=(tprice+price).toFixed(2);
                            $('#cartDiv').find('#cartPrice').html('₹ '+res.data.cartTotal);
                          }else{
                            total=(tprice-price).toFixed(2);
                            $('#cartDiv').find('#cartPrice').html('₹ '+res.data.cartTotal);
                          }
                          cartTotal=res.data.cartTotal;
                          
                      }
                      else{
                          toastr.warning(res.msg, { closeButton: !0 });
                      }
                  }else{
                      toastr.error("Refresh Page and Try Again..", { closeButton: !0 });                   
                  }
                  
                  
                }
              );
    }
    function updateCart2(this_obj,item) {
      var product_id=$(this_obj).attr('data-id');
          var product_name=$(this_obj).attr('data-name');
          var product_price_id=$(this_obj).attr('data-Pid');
          var vendor_id=$(this_obj).attr('data-vendor_id');
          var action=$(this_obj).attr('data-action');
          var price=$(this_obj).attr('data-price');
          var qty=$(this_obj).attr('data-qty');
          console.log('product_id: '+ product_id,
                  'product_name: '+ product_name,
                  'product_price_id: '+ product_price_id,
                  'vendor_id: '+ vendor_id,
                  'action: '+ action,
                  'qty: '+qty);
       $.get(updateCartUrl, 
               { 'product_id': product_id,
                  'product_name': product_name,
                  'product_price_id': product_price_id,
                  'vendor_id': vendor_id,
                  'action': action,
                  'qty': qty },
               
                function( res ) {
                  if(res){
                      if(res.status=='success'){
                          updateCartCount();
                         if(res.cartCount==0){
                          getCart();
                         } getCart();

                          $('#cart_btn_'+product_id).hide();
                          $('#cart_gbtn_'+product_id).show();
                           $('#qty_'+product_id).val(res.count);
                          $('#qty_'+product_id+'_'+product_price_id).val(res.count);
                         // alert('#qty_'+product_id+'_'+product_price_id);
                         // $('#qty_'+item.id+'_'+item.price_id).val(item.qty);
           $(this_obj).parents('#cart_gbtn_'+product_id+'_'+product_price_id).find('#cqty_'+product_id+'_'+product_price_id).val(res.count);// cart div qty
                          var tprice=$('#cartDiv').find('#cartprice h5 #cartcost').html();
                          if(res.count==0){
                            //alert('remove');
                               $('#cartDiv').find('#citem_'+product_id).remove();
                               $('#cart_btn_'+product_id).show();
                              $('#cart_gbtn_'+product_id).hide();
                              console.log('#cart_btn_'+product_id+'_'+product_price_id);
                              $('#model_'+product_id).html('+ADD');
                              $('#cart_btn_'+product_id+'_'+product_price_id).show();
                              $('#cart_gbtn_'+product_id+'_'+product_price_id).hide();
                              $('#qty_'+product_id+'_'+product_price_id).val(item.qty);
                          }
                          tprice=parseFloat(tprice) || 0;
                          price=parseFloat(price) || 0;
                          var total=0;
                          if(action==1){
                            total=(tprice+price).toFixed(2);
                            $('#cartDiv').find('#cartprice h5 #cartcost').html(total);
                          }else{
                            total=(tprice-price).toFixed(2);
                            $('#cartDiv').find('#cartprice h5 #cartcost').html(total);
                          }
                          
                      }
                      else{
                          toastr.warning(res.msg, { closeButton: !0 });
                      }
                  }else{
                      toastr.error("Refresh Page and Try Again..", { closeButton: !0 });                   
                  }
                  
                  
                }
              );
    }