<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use App\Vendor;
//Model
class VendorRating extends extends Eloquent
{
    /*public function user()
	{
	    //return $this->belongsTo(Vendor::class);
	}*/

	// Validation rules for the ratings
    public function getCreateRules()
    {
        return array(
            'comment'=>'required|min:10',
            'rating'=>'required|integer|between:1,5'
        );
    }

    // Relationships
    public function user()
    {
        return $this->belongsTo('User');
    }

    public function vendor()
    {
        return $this->belongsTo('Vendor');
    }

    // Scopes
    public function scopeApproved($query)
    {
       	return $query->where('approved', true);
    }

    public function scopeSpam($query)
    {
       	return $query->where('spam', true);
    }

    public function scopeNotSpam($query)
    {
       	return $query->where('spam', false);
    }

    // Attribute presenters
    public function getTimeagoAttribute()
    {
    	$date = \Carbon\Carbon::createFromTimeStamp(strtotime($this->created_at))->diffForHumans();
    	return $date;
    }

    // this function takes in vendor ID, comment and the rating and attaches the review to the vendor by its ID, then the average rating for the vendor is recalculated
    public function storeReviewForProduct($productID, $comment, $rating)
    {
        $vendor = Product::find($productID);

        //$this->user_id = Auth::user()->id;
        $this->comment = $comment;
        $this->rating = $rating;
        $vendor->reviews()->save($this);

        // recalculate ratings for the specified vendor
        $vendor->recalculateRating($rating);
    }
}
