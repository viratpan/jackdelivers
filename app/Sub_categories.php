<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Cviebrock\EloquentSluggable\Sluggable;

class Sub_categories extends Model
{
    use Notifiable;    

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name','slug','categry_id','image'
    ];

    use Sluggable;

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
}
