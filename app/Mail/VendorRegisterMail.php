<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class VendorRegisterMail extends Mailable
{
    use Queueable, SerializesModels;
    public $vendor;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($vendor)
    {
        $this->vendor = $vendor;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //return $this->view('vendors.mail.register_verify');
         return $this->from('info@breadbutterbasket.com', 'BreadButterBasket')
                    ->to($this->vendor['email'])
                    ->subject('Welcome To BreadButterBasket.com For Vendor`s Registration Verification.')
                    ->view('vendors.mail.register_verify');
    }
}
