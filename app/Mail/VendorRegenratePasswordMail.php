<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class VendorRegenratePasswordMail extends Mailable
{
    use Queueable, SerializesModels;
    public $user;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user=$user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
         return $this->from('info@breadbutterbasket.com', 'BreadButterBasket')
                    ->to($this->user['email'])
                    ->subject('Welcome To BreadButterBasket.com For Vendor`s Registration Verification.')        
                    ->view('vendors.mail.password');
    }
}
