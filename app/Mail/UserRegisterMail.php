<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserRegisterMail extends Mailable
{
    use Queueable, SerializesModels;
     public $user;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      
        //return $this->view('vendors.mail.register_verify');
         return $this->from('info@breadbutterbasket.com', 'BreadButterBasket')
                    ->to($this->user['email'])
                    ->subject("BreadButterBasket 'My Account' One Time Password(OTP)")
                    ->view('mails.user_register_verify');
   
    }
}
