<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class OrderStatus extends Model
{
    use Notifiable;
   
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name'
    ];

    
}
