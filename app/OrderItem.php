<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
class OrderItem extends Model
{
    use Notifiable;
   
    protected $dates = ['deleted_at'];

    protected $fillable = [
         'order_id','order_rid','product_id','product_price_id','vendor_id'
    ];
}
