<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Cviebrock\EloquentSluggable\Sluggable;

class Product_price extends Model
{
    use Notifiable;    

    protected $dates = ['deleted_at'];

    protected $fillable = [
       'name','price','cost','discount','product_id'
    ];
}
