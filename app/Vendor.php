<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Cviebrock\EloquentSluggable\Sluggable;


use Illuminate\Database\Eloquent\Relations\BelongsTo;
use App\VendorRating;

class Vendor extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;
    use Sluggable;

   // protected $appends = ['average_rating'];

    protected $guard = 'vendor';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name', 'email','company_name','slug','mobile', 'password','masters_id','category_id','commission_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'verified_at' => 'datetime',
    ];
    
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'company_name'
            ]
        ];
    }
    
    
}
