<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Cviebrock\EloquentSluggable\Sluggable;


use Illuminate\Database\Eloquent\Relations\BelongsTo;
use App\VendorRating;

class Vendor extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;
    use Sluggable;

   // protected $appends = ['average_rating'];

    protected $guard = 'vendor';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name', 'email','company_name','slug','mobile', 'password','masters_id','category_id','commission_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'verified_at' => 'datetime',
    ];
    
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'company_name'
            ]
        ];
    }
    
    public function reviews()
    {
        return $this->hasMany('Review');
    }

    // The way average rating is calculated (and stored) is by getting an average of all ratings, 
    // storing the calculated value in the rating_cache column (so that we don't have to do calculations later)
    // and incrementing the rating_count column by 1

    public function recalculateRating($rating)
    {
        $reviews = $this->reviews()->notSpam()->approved();
        $avgRating = $reviews->avg('rating');
        $this->rating_cache = round($avgRating,1);
        $this->rating_count = $reviews->count();
        $this->save();
    }

}
