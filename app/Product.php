<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Cviebrock\EloquentSluggable\Sluggable;

class Product extends Model
{
    use Notifiable;
    use Sluggable;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'categry_id','sub_categry_id',
        'name','slug','meta_title','meta_keywords','meta_description','description',
        'new','featured','status','vendor_id'
    ];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
    public function all_price()
    {
        return $this->hasmany('App\Product_price')->get();
    }
}
