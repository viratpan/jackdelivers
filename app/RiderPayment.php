<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class RiderPayment extends Model
{
    use Notifiable;
   
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'rider_id','order_id','start_latitude','start_longitude','end_latitude','end_longitude'
    ];
}
