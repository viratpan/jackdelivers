<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Sanctum\HasApiTokens;

class Rider extends Authenticatable
{
    use Notifiable;
    use HasApiTokens;

    protected $guard = 'rider';
   
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name','email','mobile','dl','dl_valid'
    ];   

    protected $hidden = [
        'password', 'remember_token',
    ];


    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
