<?php

namespace App\Providers;

use Illuminate\Support\Facades\Broadcast;
use Illuminate\Support\ServiceProvider;

class BroadcastServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {      
         
        //Broadcast::routes();
        Broadcast::routes(['middleware' => ['web','auth:vendor']]);
        //match any of the 3 auth guards
       //     Broadcast::routes(['middleware' => ['web','auth:admin,vendor']]);
        require base_path('routes/channels.php');
    }
}
