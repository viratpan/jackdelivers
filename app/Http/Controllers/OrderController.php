<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
use App\Order;
use App\OrderItem;
use App\Payment;

use App\Address;
use App\Vendor;
//use App\Address;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;

class OrderController extends Controller
{
    function __construct()
    {
    	$this->middleware('auth');
    }
    public function index()
    {
        $user = auth()->user();
        $user_id=$user->id;
        $address=Address::where('user_id',$user_id)->get();
       // echo "Address : <pre>",print_r($address),"</pre></hr>";  die(); 
    	$tcart = Session::get('cart');
        //echo "item: <pre>",print_r($tcart),"</pre></hr>";     
        $data=$this->getCartData();        
        //echo "item: <pre>",print_r($temp),"</pre></hr>";die();
        $count=$data['count'];
        if($count==0){
             return redirect()->route('cart')->with('Error','');
        }
        $item=collect($data['item']);
       // echo "item: <pre>",print_r($item),"</pre></hr>";//die();
        $idata = collect($item->groupBy('masters_id')->toArray());
        //$idata = $temp->groupBy('vendor_id')->toArray();
        
        return view('order',compact('count','item','idata','address'));
    }
    public function order(Request $request)
    {
        $request->validate([
            'user_address' => ['required', 'string'],
            'payment' => ['required', 'string'],
        ]);
        $user_address=$request->user_address;
        $payment=$request->payment;/// 1 cod   2 online
        $user = auth()->user();
        $user_id=$user->id;
        if($user_address=='new'){
            $adata =new Address;
            $adata->name = $request->name;
            $adata->mobile = $request->mobile;
            $adata->address = $request->address;
            $adata->state = $request->state;
            $adata->city = $request->city;
            $adata->state = $request->state;
            $adata->user_id = $user_id;
           
            $adata->save();
            $user_address=$adata->id;
        }
         // echo "item: <pre>",print_r($request->all()),"</pre></hr>";die();
        $payment_type=$request->payment;
        //$user_address=1;
        $order_id=$this->getTableColumUnique('orders','order_id');
        $transition_id=$this->getTableColumUnique('orders','transition_id');
        $data=$this->getCartData();        
        //echo "item: <pre>",print_r($temp),"</pre></hr>";die();
        $count=$data['count'];
        if($count==0){
             return redirect()->route('cart')->with('Error','');
        }
        $item=collect($data['item']);

        $idata = $item->groupBy('vendor_id')->toArray();
        DB::beginTransaction();
        try {
                $order_arr=array(); $amount=0;
                foreach ($idata as $key => $value) {
                    $vendor_id='';$vendor_name='';$vendor_address=''; $price=0; $items=0;
                   
                    if(isset($value[0])){
                        $items=count($value);
                        $vendor_id=$value[0]['vendor_id'];
                        $vendor_name=$value[0]['vendor_name'];
                        $vendor_address=$value[0]['vendor_address'];
                        $masters_id=$value[0]['masters_id'];
                      //  echo "Start<br>"; echo $vendor_id."<br>";
                   $vendor=Vendor::where('id',$vendor_id)->select('latitude','longitude','address')->first();
                   $address=Address::where('id',$user_address)->first();
                   $to=$vendor->address;
                   $from=$address->address.','.$address->city;
                   $mapData=$this->getDistance($from,$to);

                        $order=new Order;
                            $order->order_id=$this->getTableColumUnique('orders','order_id');
                            $order->transition_id=$this->getTableColumUnique('orders','transition_id');
                            $order->master_id=$masters_id;
                            $order->user_id=$user_id;
                            $order->user_address_id=$user_address;
                            $order->otp=rand(100000,999999);
                            $order->payment_type=$payment_type; // 1 cash, 2 online
                            $order->items=$items;
                            $order->vendor_id=$vendor_id;
                                $order->start_latitude=$vendor->latitude;
                                $order->start_longitude=$vendor->longitude;

                                $order->end_latitude=$mapData['lat'];
                                $order->end_longitude=$mapData['lng'];
                                $order->distance=$mapData['distance'];

                         // echo "item: <pre>",print_r($order),"</pre></hr>";   
                        $order->save();
                        //print_r($order);echo "<hr><br>";
                        if($order->id){
                            //echo "<hr>";
                            foreach ($value as $key => $product) {
                               // echo $product['name']."<br>";
                                $orderItem=new OrderItem;
                                    $orderItem->order_id=$order->id;
                                    $orderItem->order_rid=$order->id;
                                    
                                    $orderItem->product_id=$product['id'];
                                    $orderItem->product_price_id=$product['price_id'];
                                    $orderItem->vendor_id=$order->vendor_id;

                                    $orderItem->cost=$product['cost'];
                                    $orderItem->discount=$product['discount'];
                                    $orderItem->price=$product['price_id'];
                                    $orderItem->qty=$product['qty'];
                                    $orderItem->price=$product['price'];
                                    $orderItem->amount=$orderItem->price*$orderItem->qty;
                                    //echo "item: <pre>",print_r($orderItem),"</pre></br>";
                                $orderItem->save();
                                //print_r($orderItem);echo "<hr>";

                                $price+=$orderItem->amount;
                            }
                            //echo "<br><hr>";
                            $uorder = Order::find($order->id);
                                $uorder->price = $price;
                                // manage shipping cost
                                $uorder->shipping=50; 

                                $uorder->amount = $uorder->price+$uorder->shipping;
                                // manage gat& others taxs
                                if($masters_id==4){
                                    $uorder->tax=($uorder->amount*5)/100;
                                }
                                //  final cost
                                $uorder->total_amount=$uorder->amount+$uorder->tax;
                                // echo "item: <pre>",print_r($uorder),"</pre></hr>";
                            $uorder->save();
            /*///////////////////////////////////////////////////////////////////////////////////////*/
                            $payment=new Payment;
                                    $payment->order_id=$uorder->id;
                                    $payment->OD_id=$uorder->order_id;                                    
                                    $payment->transition_id=$uorder->transition_id; 
                                    $payment->payment_type=$payment_type; // 1 cash, 2 online
                                    $payment->coupon_code='';
                                    
                                    $payment->discount=0; 
                                    $payment->shipping=$uorder->shipping;
                                    $payment->price=$uorder->price; 
                                    $payment->amount=$uorder->amount; 
                                    $payment->tax=$uorder->tax; 
                                    $payment->total_amount=$uorder->total_amount;                                     
                                $payment->save();
                             // echo "payment: <pre>",print_r($payment->id),"</pre></hr>";
            /*///////////////////////////////////////////////////////////////////////////////////////*/
                            array_push($order_arr, $order->id);
                            $amount=$uorder->amount+$uorder->tax;
                           // echo"order_rid  ".$order->id.' End';
                            //print_r($uorder);echo "<br>End<hr>";
                        }

                        
                    }
                }
                // All Good
                DB::commit();
                
                //destroy cart session
                Session::pull('cart');
                Session::forget('cart');
                Session::save();
                Session::put('cart',[]);

                // redirect to order-complete/payment page
                if($payment_type==2){
                   // return  "<h1>go to gateway</h1>";
                    $order_arr=implode(',',$order_arr);
                    //echo "All Orders : <pre>",print_r($order_arr),"</pre></hr>";
                   // echo "<br> ready to go Payment-Gateway";
                    return redirect()->route('payment',$order_arr);
                }
                else{
                    //echo "done";
                 return redirect()->route('order.done','success');
                }
            }
            catch (\Exception $e) {
                DB::rollback();
                report($e);
                return redirect()->route('order.done','error');
            }
        
    }
    function thanks($status){
        $msg;
        if($status=='success'){
            $msg="Your Order has been successfully submited.";
        }else{
            $msg="Your Order has faild. Please try again!";
        }
        return view('thanks',compact('msg'));
    }
    public function getDistance($from,$to)
    {
       //$to = 'Balaji PG Noida Sector 62 PG, B Block Road, behind Fortis Hospital, Block B, Industrial Area, Sector 62, Noida, Uttar Pradesh';
       //$from='Urban Punjab, Windsor Road, Vaibhav Khand, Indirapuram, Ghaziabad, Uttar Pradesh';
      
        $url = "https://maps.googleapis.com/maps/api/directions/json?key=".config('app.map_key')."&origin=".str_replace(' ', '+', $to)."&destination=".str_replace(' ', '+', $from)."&sensor=false";
        $distance='0';
        $lat='0';
        $lng='0';

        try {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            $response = curl_exec($ch);
            curl_close($ch);
            $response_all = json_decode($response);
            if($response_all){
                $distance=$response_all->routes[0]->legs[0]->distance->value;   // distance in meter
                $lat=$response_all->routes[0]->legs[0]->end_location->lat;
                $lng=$response_all->routes[0]->legs[0]->end_location->lng;    
            }
            //echo "all data: <pre>",print_r($response_all),"</pre></hr>";            

        }catch (\Exception $e) {   report($e);      }

         return [
                    'distance'=>$distance,
                    'lat'=>$lat,
                    'lng'=>$lng
                ];       
              
    }
    function statusCancel($id,$status){
        $data= Order::where('id',$id)->first();//var_dump($data);
         //$data = OrderItem::where('order_rid',$id)->where('status',$ostatus)->select('id')->get();
         if($data){
        DB::beginTransaction();
            try {
                
                
                Order::where('id',$id)->update(['status' => $status]);
                OrderItem::where('order_rid',$id)->update(['status' => $status]);
               // $temp = OrderItem::whereIn('id',$data)->update(['status' => $status]);
                //echo"<br>";                      
                // All Good
                DB::commit();
            }catch (\Exception $e) {
                DB::rollback();
                report($e);                
            }
        }
         return redirect()->route('user.orders_details',$data->order_id)->with('success',' Order Status Updated successfully.');
    }
    /*////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
    public function temp($value='')
    {
        $idata = collect($item->groupBy('masters_id')->toArray());
        //$idata = $temp->groupBy('vendor_id')->toArray();
        $total=0;
        $p_arr=array();     // all items Price array
        $d_arr=array();     // all items Delivery array
        $g_arr=array();     // all items GST array
        foreach ($idata as $key => $mvalue) {
            $masters_id='';$masters_name='';$price=0; $m_items=0;
            if(isset($mvalue[0])){
                $m_items=count($mvalue);
                $masters_id=$mvalue[0]['masters_id'];
            echo$masters_name=$mvalue[0]['masters_name'];
                
                echo "<hr>";
                $temp=collect($mvalue);
                $vitems = $temp->groupBy('vendor_id')->toArray();
                foreach ($vitems as $key => $value) {
                    $vendor_id='';$vendor_name='';$vendor_address=''; $price=0; $items=0;
                    if(isset($value[0])){
                        $items=count($value);
                        $vendor_id=$value[0]['vendor_id'];
                        $vendor_name=$value[0]['vendor_name'];
                        $vendor_address=$value[0]['vendor_address'];
                        echo "<hr>";
                        foreach ($value as $key => $product) {
                            echo $product['name'];echo "<br>";
                            $price+=$product['price'];
                        }
                    }
                }
            }
            
        }
        echo "<hr>";
        echo "item: <pre>",print_r($idata),"</pre></hr>";die();
    }
    public function test()
    {
       $vendor=Vendor::where('id',3)->select('latitude','longitude','address')->first();
       $address=Address::where('id',2)->first();
       $from=$vendor->address;
       $to=$address->address.','.$address->city;
       $mapData=$this->getDistance($from,$to);
       echo "item: <pre>",print_r($mapData),"</pre></hr>";
       echo $mapData['distance'];
    }
}
