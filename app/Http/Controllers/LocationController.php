<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;

use App\User;
use App\Vendor;
use App\Admin;
use Notification;
use App\Notifications\OrderNotification;
use App\Notifications\RealTimeNotification;

use Pusher;
use Illuminate\Support\Facades\Broadcast;
use Illuminate\Support\ServiceProvider;

class LocationController extends Controller
{
	public $address;
    public $sector;
    public $city;
    public $lat=Null;
    public $lng=Null;
    public $lat_lng=false;

    function __construct()
    {
    	/*$this->lat=Session::get('lat');
        $this->lng=Session::get('lng');
        $this->lat_lng=Session::get('lat_lng');

        $this->address=Session::get('address');
        $this->sector=Session::get('sector');
        $this->city=Session::get('city');*/
    }

    public function index()
    {   
        return view('location');
    }

    public function setlocation(Request $request)
    {
        //echo"<pre>",print_r($request->all()),"</pre>";
        if($request->lat && $request->lng){
            Session::put('lat',$request->lat);
            Session::put('lng',$request->lng);
            Session::put('lat_lng',true);

            Session::put('address',$request->address);
            Session::put('sector',$request->lc);
            Session::put('city',$request->city);
             return response()->json(array('status'=>"success",'msg'=>"Location Set",'redirect' => true ));
        }
        return response()->json(array('status'=>"success",'msg'=>"Try Again",'redirect' => false ));
    }

    
}
