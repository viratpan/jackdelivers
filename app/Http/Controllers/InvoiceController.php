<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;

use Auth;
use Session;
use App\Master;
use App\User;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;

class InvoiceController extends Controller
{
    function __construct()
    {
    	//$pdf = PDF::loadView('pdf_view', $data);
      	//return $pdf->download('pdf_file.pdf');
      	$this->middleware('auth');
    }
    function order_invoice($orders_id)
    {
    	$user = auth()->user();
         $data=DB::table('orders')->where('orders.user_id',$user->id)
                ->leftJoin('users','users.id','=','orders.user_id')
                ->leftJoin('addresses','addresses.id','orders.user_address_id')
                ->leftJoin('order_items','order_items.order_rid','=','orders.id')
                    ->leftJoin('vendors','vendors.id','=','orders.vendor_id')
                        ->leftJoin('masters','masters.id','=','vendors.masters_id')
                ->leftJoin('product_prices','product_prices.id','=','order_items.product_price_id')
                ->leftJoin('products','products.id','=','product_prices.product_id')
                ->leftJoin('attributes','attributes.id','=','products.attrubute_id')
                    ->leftJoin('riders','riders.id','=','orders.rider_id')
                ->where('orders.order_id',$orders_id)
                ->select('users.name as user_names','users.email as user_email','users.mobile as user_mobile',
                    'addresses.*','orders.*','orders.id as order_rid','orders.status as orders_status',
                    'products.name as product_name','product_prices.qty as product_qty','attributes.name as attributes_name','vendors.company_name as vendor_company_name','masters.name as masters_name',
                    'order_items.id as item_id','order_items.status as item_status',
                    'order_items.cost as item_cost','order_items.discount as item_discount','order_items.price as item_price','order_items.qty as item_qty','order_items.amount as item_amount','riders.name as rider_name'
                )->get();
                try {
                    view()->share('data',$data);
                    $pdf = PDF::loadView('pdf.customer_order_invoice', $data);
                    return $pdf->download('invoice.pdf'); 
                }
                catch (\Exception $e) {
                    
                    report($e);
                    return redirect()->back()->with('success','Invoice Not Downloaded.');
                }
               return redirect()->back()->with('success','Invoice Downloaded.');

             
    		//return view('pdf.customer_order_invoice', compact('data'));
    }
}
