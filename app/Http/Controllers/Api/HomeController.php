<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Master;
use App\Vendor;
use App\Product;

use App\Traits\ApiResponser;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;

class HomeController extends Controller
{
	use ApiResponser;

	public $cart;
    public $wishlist;

    public $address='';
    public $sector='';
    public $city='';

    public $lat='';
    public $lng='';
    public $lat_lng=false;
    
    public $radius=15;

    public function masters()
    {
    	$master=Master::orderBy('order', 'ASC')->select('id','name','slug','image','active')->get();
    	//var_dump($master);
    	if($master){
    		return $this->success([
	            'masters' => $master
	        ]);
    	}else{
    		return $this->error('No Data Found',400);
    	}
	    	
	    
    }
    function vendors(Request $request){

    	$slug=$request->slug;
    	$lat=$request->lat;
    	$lng=$request->lng;
    	$radius =  $this->radius;

    	$master=Master::where('slug', $slug)->select('id','name','slug','image','active')->first();
    	if(!isset($master->id)){
            return $this->error('No Data Found',400);
        }
        	$category=DB::table('categories')->where('masters_id',$master->id)->select('id','name','slug')->get();
	        $subcategory=DB::table('sub_categories')->where('masters_id',$master->id)->select('id','name','slug')->get();
        if($master->active!=1){
            return $this->success([
                'masters' => $master,
            	'category' => $category,
            	'subcategory' => $subcategory,
	            'vendor' => null
	        ]);
        }
    	if(isset($master->id)){
    		$master_id=$master->id;
	        
    		$vendor=Vendor::where('vendors.masters_id',$master->id)
    		//$vendor=DB::table('vendors')
                /*->selectRaw("vendors.*, cities.name as city_name,
                     ( 6371 * acos( cos( radians(?) ) *
                       cos( radians( latitude ) )
                       * cos( radians( longitude ) - radians(?)
                       ) + sin( radians(?) ) *
                       sin( radians( latitude ) ) )
                     ) AS distance", [$lat,$lng,$lat])
                ->having("distance", "<", $radius)
                ->orderBy("distance",'asc')*/
                ->leftJoin('cities','cities.id','vendors.city')
                ->leftJoin('vendor_ratings', 'vendor_ratings.vendor_id', '=', 'vendors.id')
                ->where('vendors.verify',1)
                ->where('vendors.active',1)
                //->where('vendors.masters_id',$master->id)
                ->select('vendors.id','vendors.company_name','vendors.slug','vendors.image',
                	//'vendors.latitude','vendors.longitude',
                	'vendors.ontime','vendors.offtime','vendors.open','vendors.costoftwo',
                	'cities.name as city')
                //->selectRaw('AVG(vendor_ratings.rating) AS average_rating')
                //->groupBy('vendors.id')
                ->get();

            return $this->success([
                'masters' => $master,
            	'category' => $category,
            	'subcategory' => $subcategory,
	            'vendor' => $vendor
	        ]);

    	}
    	
    }
    public function vendorDetails(Request $request)
    {
    	$type=$request->type;
    	$vendor_id=$request->vendor_id;
    	$slug=$request->slug;

    	$master=Master::where('slug', $type)->select('id','name','slug','image','active')->first();
    	if(!isset($master->id)){
            return $this->error('No Vertical Found',400);
        }
        if($vendor_id){
	        $vendor=Vendor::where('vendors.id', $vendor_id)
	        				->leftJoin('cities','cities.id','vendors.city')
			                ->leftJoin('vendor_ratings', 'vendor_ratings.vendor_id', '=', 'vendors.id')		                
			                ->select('vendors.id','vendors.company_name','vendors.slug','vendors.image',
			                	'vendors.address',
			                	'vendors.ontime','vendors.offtime','vendors.open','vendors.costoftwo',
			                	'cities.name as city')		                
			                ->first();
	    	if(!isset($vendor->id)){
	            return $this->error('No Data Found',400);
	        }
	        $item =Product::where('products.vendor_id',$vendor->id)                
	                ->leftJoin('categories','categories.id','=','products.categry_id')
	                ->leftJoin('sub_categories','sub_categories.id','=','products.sub_categry_id')
	                ->leftJoin('attributes','attributes.id','=','products.attrubute_id')                
	                ->leftJoin('product_prices','product_prices.product_id','=','products.id')
	                ->select('products.id as productsId','products.created_at as pcreated_at','products.*','categories.name as categories_name','sub_categories.name as sub_categories_name',
	                    'attributes.name as attributes_name','product_prices.id as product_price_id',
	                'product_prices.qty as product_qty','product_prices.cost as product_cost',
	                'product_prices.discount as product_discount','product_prices.price as product_price',
	                //DB::raw('CASE WHEN products.id=1 THEN 1 ELSE 0 END AS in_cart')
	                 )
	                //->orderBy('productsId')
	                ->orderBy('product_price')
	                ->get();

	       
	        //$itemcat = $item->groupBy('categories.id');
	        $itemcat = $item->groupBy('categories_name')->toArray();     
	        
	        
	        $item = $item->groupBy('productsId');

	        return $this->success([
                'masters' => $master,           	
	            'vendor' => $vendor,
	            'itemcat' => $itemcat,
            	'item' => $item,
	        ]);
	    }else{
	            return $this->error('No Vendor Found',400);
	        
	    }
    }
}
