<?php

namespace App\Http\Controllers\Api\Rider;

use App\Rider;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
	use ApiResponser;

    public function register(Request $request)
    {
        $attr = $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|unique:users,email',
            'mobile' => 'required|string|email|unique:users,mobile',
            'password' => 'required|string|min:6|confirmed'
        ]);

        $user = User::create([
            'name' => $attr['name'],
            'password' => bcrypt($attr['password']),
            'email' => $attr['email']
        ]);

        return $this->success([
            'token' => $user->createToken('API Token')->plainTextToken
        ]);
    }

    public function login(Request $request)
    {
    	$this->validate($request, [
            'email' => 'required|string|email|',
            'password' => 'required|string|min:6'
        ]);

        if (Auth::guard('rider')->attempt(['email' => $request->email, 'password' => $request->password,'active' => 1])) {
            return $this->success([
	            'token' => Auth::guard('rider')->user()->createToken('API Token')->plainTextToken
	        ]);
        }
        return $this->error('Credentials not match', 401);       
    }
    function login2(Request $request)
    {
    	$attr = $request->validate([
            'email' => 'required|string|email|',
            'password' => 'required|string|min:6'
        ]);

        if (!Auth::guard('rider')->attempt($attr)) {
            return $this->error('Credentials not match', 401);
        }
        //'token' => auth()::guard('rider')->user()->createToken('API Token')->plainTextToken
        return $this->success([
            'token' => Auth::guard('rider')->user()->createToken('API Token')->plainTextToken
        ]);
    }

    public function logout()
    {
        //auth()::guard('rider')->user()->tokens()->delete();
    	Auth::guard('admin')->user()->tokens()->delete();
    	return $this->success([
            'message' => 'Tokens Revoked'
        ]);
       /* return [
            'message' => 'Tokens Revoked'
        ];*/
    }
}
