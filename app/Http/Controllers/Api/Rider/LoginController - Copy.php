<?php

namespace App\Http\Controllers\Api\Rider;

use App\Rider;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
	use ApiResponser;

    public function register(Request $request)
    {
        $attr = $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|unique:users,email',
            'password' => 'required|string|min:6|confirmed'
        ]);

        $user = User::create([
            'name' => $attr['name'],
            'password' => bcrypt($attr['password']),
            'email' => $attr['email']
        ]);

        return $this->success([
            'token' => $user->createToken('API Token')->plainTextToken
        ]);
    }

    public function login(Request $request)
    {
        $attr = $request->validate([
            'email' => 'required|string|email|',
            'password' => 'required|string|min:6'
        ]);

        if (!Auth::guard('rider')->attempt($attr)) {
            return $this->error('Credentials not match', 401);
        }
        //'token' => auth()::guard('rider')->user()->createToken('API Token')->plainTextToken
        return $this->success([
            'token' => Auth::guard('rider')->user()->createToken('API Token')->plainTextToken
        ]);
    }

    public function logout()
    {
        //auth()::guard('rider')->user()->tokens()->delete();
    	Auth::guard('rider')->user()->tokens()->delete();
        return [
            'message' => 'Tokens Revoked'
        ];
    }
}
