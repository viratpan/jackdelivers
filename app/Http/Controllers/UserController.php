<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;

use App\User;
use App\Address;
use App\Order;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
	function __construct()
    {
    	$this->middleware('auth');
    }

    public function index()
    {
        $user = auth()->user();
        $data=User::where('id',$user->id)->first();
        $address=Address::where('user_id',$user->id)->get();
        $order=DB::table('orders')->where('orders.user_id',$user->id)
                 //->leftJoin('order_items','order_items.order_rid','=','orders.id')
                    ->leftJoin('vendors','vendors.id','=','orders.vendor_id')
                    ->leftJoin('masters','masters.id','=','vendors.masters_id')
                 ->select('orders.*','orders.id as order_rid','orders.status as orders_status',
                    'vendors.company_name as vendor_company_name','masters.name as masters_name',
                    )->get();
         return View('user/profile',compact('data','address','order'));
    }
    public function orders()
    {
        $user = auth()->user();
        $data=DB::table('orders')->where('orders.user_id',$user->id)
                 //->leftJoin('order_items','order_items.order_rid','=','orders.id')
                    ->leftJoin('vendors','vendors.id','=','orders.vendor_id')
                    ->leftJoin('masters','masters.id','=','vendors.masters_id')
                 ->select('orders.*','orders.id as order_rid','orders.status as orders_status',
                    'vendors.company_name as vendor_company_name','masters.name as masters_name','masters.cancellation_till','masters.cancellation_cost'
                    )->get();
         return View('user/order',compact('data'));
    }
     public function ordersDetails($orders_id)
    { 
        $user = auth()->user();
         $data=DB::table('orders')->where('orders.user_id',$user->id)
                ->leftJoin('users','users.id','=','orders.user_id')
               
                ->leftJoin('addresses','addresses.id','orders.user_address_id')
                ->leftJoin('order_items','order_items.order_rid','=','orders.id')
                    ->leftJoin('vendors','vendors.id','=','orders.vendor_id')
                        ->leftJoin('masters','masters.id','=','vendors.masters_id')
                ->leftJoin('product_prices','product_prices.id','=','order_items.product_price_id')
                ->leftJoin('products','products.id','=','product_prices.product_id')
                ->leftJoin('attributes','attributes.id','=','products.attrubute_id')
                    ->leftJoin('riders','riders.id','=','orders.rider_id')
                ->where('orders.order_id',$orders_id)
                ->select('users.name as user_names','users.email as user_email','users.mobile as user_mobile',
                    'addresses.*','orders.*','orders.id as order_rid','orders.status as orders_status',
                    'products.name as product_name','product_prices.qty as product_qty','attributes.name as attributes_name','vendors.company_name as vendor_company_name','masters.name as masters_name',
                    'order_items.id as item_id','order_items.status as item_status',
                    'order_items.cost as item_cost','order_items.discount as item_discount','order_items.price as item_price','order_items.qty as item_qty','order_items.amount as item_amount','riders.name as rider_name'
                    ,'masters.name as masters_name','masters.cancellation_till','masters.cancellation_cost','masters.returnable'
                )->get();
       if(!$data){
            return redirect()->route('user.order')->with('error',' Order Not Found.');
        }
         return View('user/order_details',compact('data'));
    }

    public function address()
    {
        $user = auth()->user();
        $data=Address::where('user_id',$user->id)->get();
        return View('user/address',compact('data'));
    }
    public function reviews()
    {
        $user = auth()->user();
        $data=Address::where('user_id',$user->id)->get();
        return View('user/reviews',compact('data'));
    }
    public function coupons()
    {
        $user = auth()->user();
        $data=Address::where('user_id',$user->id)->get();
        return View('user/coupons',compact('data'));
    }
    public function invite()
    {
        $user = auth()->user();
        $data=User::where('id',$user->id)->first();
        return View('user/invite',compact('data'));
    }


    /* order gaet*/
    public function allAddress()
    {
        $user = auth()->user();
        $id=$user->id;
        $data=Address::where('user_id',$id)->get();      
        
        return response()->json($data);
    }
}
