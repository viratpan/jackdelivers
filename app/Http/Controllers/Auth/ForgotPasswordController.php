<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Str;
 use App\Mail\VendorAdminForget;
 use App\Mail\VendorRegenratePasswordMail;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    | 
    */

    use SendsPasswordResetEmails;
    /*
    |--------------------------------------------------------------------------
    | Password Reset Admin
    |--------------------------------------------------------------------------
    |    
    */
    public function showAdminForgetForm()
    {
        return view('admin.email-forget', ['url' => 'admin']);
    }
    public function adminForget (Request $request)
    {
        print_r($request->all());
    }
    
    /*
    |--------------------------------------------------------------------------
    | Password Reset Admin
    |--------------------------------------------------------------------------
    |    
    */
    public function showVendorForgetForm()
    {
        return view('admin.email-forget', ['url' => 'vendor']);
    }
    public function vendorForget (Request $request)
    {
        //You can add validation login here
        $user = DB::table('vendors')->where('email', '=', $request->email)->first();
        //Check if the user exists
        if ($user){
            $token;

            //Create Password Reset Token
            DB::table('password_resets')->insert([
                'email' => $request->email,
                'token' => Str::random(60),
                'created_at' => Carbon::now()
            ]);
            //Get the token just created above
            $tokenData = DB::table('password_resets')->where('email', $request->email)->first();
           
            if(isset($tokenData->token)){
               $token=$tokenData->token;               
            }else{
                $token=Str::random(60);
                DB::table('password_resets')->insert([
                    'email' => $request->email,
                    'token' => $token,
                    'created_at' => Carbon::now()
                ]);
            }
            $userData=array(
                'name' => $user->name,
                'email' => $user->email,
                'mobile' => $user->mobile,
                'token' => $token,
                'text' => 'Vendor',
              );

             $link = config('base_url') . 'new-password/vendor/' . $tokenData->token . '?email=' . urlencode($user->email);
            //$tempData=['name'=>$user->name,'email'=>$request->email,'url'=>$link];
            //$mail= \Mail::to($request->email)->send(new VendorAdminForget($tempData));
             
            try { 
              //$user->id->sendEmailVerificationNotification();
             \Mail::to($user->email)->send(new VendorAdminForget($userData));
            } catch (\Exception $e) {
                report($e);
                 return redirect()->back()->withErrors(['error' => trans('A Network Error occurred. Please try again.')]);
            }
            return redirect()->back()->with('success', trans('A reset link has been sent to your email address.'));
           

        }else{
            return redirect()->back()->withErrors(['email' => trans('Vendor does not exist')]);
        }

    
    }
    /*
    |--------------------------------------------------------------------------
    | Password Reset Mail
    |--------------------------------------------------------------------------
    |    
    */
    function vendorNewPass($token){
        die('vendor pass request goo');
        $status = "Your Account Password Reset Request Invalid.";
          $temp = DB::table('password_resets')->where('token', $token)->first();
        // var_dump($temp);
          if(isset($temp) ){
             $email = $temp->email; //'email_verified_at' =>now(),
            $vendor = DB::table('vendors')->where('email', $email)->first();
            if($vendor){
              
                    $password=Str::random(10);
                    $data = Vendor::find($vendor->id);                    
                        $data->password = Hash::make($password);
                    $data->remember_token = Str::random(10);

                    $tempData=['name'=>$vendor->name,'email'=>$email,'crankpasswoed'=>$password];

                    $data->save();

                    $status = "Your Account Password Reset. Please Check your mail and login.";
              try {
                    //\Mail::to($email)->send(new VendorRegenratePasswordMail($tempData));
                   } catch (\Exception $e) { report($e);  }
                   
                   // DB::table('password_resets')->where('token', $token)->delete();

                
            }

          } else {
            return redirect()->route('login.vendor')->with('warning', "Sorry your email cannot be identified.");
            //echo "not mached";
          }
          //echo "Done";
          return redirect()->route('login.vendor')->with('success', $status);
    }
}
