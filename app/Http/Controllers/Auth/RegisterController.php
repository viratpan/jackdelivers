<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Mail;
use App\User;
use App\Vendor;
use App\Master;
use App\City;
use App\Categorie;
use App\Commission;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
    use Illuminate\Http\Request;
    use Illuminate\Support\Str;
    use Illuminate\Support\Facades\DB;
      use Carbon\Carbon;
      use App\Mail\VendorRegisterMail;
      use App\Mail\UserRegisterMail;
      use App\Mail\VendorRegenratePasswordMail;
      use Cviebrock\EloquentSluggable\Services\SlugService;
  use Illuminate\Foundation\Auth\VerifiesEmails;
  use Illuminate\Auth\Events\Verified;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'mobile' => ['required', 'string', 'min:10','max:10', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }
    /*
    |--------------------------------------------------------------------------
    | Overwite Method
    |--------------------------------------------------------------------------
    |
    */
    public function showRegistrationForm()
    {
      return view('auth.register');
    }
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        /*return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);*/
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'mobile' => $data['mobile'],
            'provider_id'   => 1,
            'provider'      => 'Auth',
            'wallet_id' => Str::random(16),
            'ammount' => 0,
            'password' => Hash::make($data['password']),
        ]);
    }
    public function register(Request $request)
    {
      //echo "register";
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'mobile' => ['required', 'string', 'min:10','max:10', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
        $referrer_id=0;
        if(isset($request->code)){
             $code=$request->code;
             $user=User::where('referrer_code',$code)->first();
             if(isset($user->id)){
               $referrer_id=$user->id;
             }
        }
        $data =new User;
             $data->name = $request->name;
             $data->email = $request->email;
             $data->mobile = $request->mobile;           
             $data->wallet_id = Str::random(16);
             $data->referrer_code = $this->getTableColumUnique('users','referrer_code');
             $data->ammount = 0;
              $data->tpassword = $request->password;
             $data->password = Hash::make($request->password);
             if($referrer_id!=0){
                $data->referrer_id = $referrer_id;
             }
            $data->save();
        if(isset($data->id)){
            return redirect()->route('user.confirmation',['id' => $data->id,'type' => 'confirmation']);
         }else{
            return redirect()->route('register')->with('error','Please Try Again.');
         }
    }
    function userResendMail(Request $request,$id)
    {
       $user = DB::table('users')->where('id', $id)->first();
       $type=$request->type;
       if($user){
        $email=$user->email;
        $token=''; $text='';
              
          if($type=='confirmation'){
            //DB::table('password_resets')->where('email', $email)->delete();
            $text="confirmation";
            $otp=Str::random(6);       
               $data = User::find($id);
               $data->otp = $otp;
               $data->save();
            $token=$otp;
          }else{
            $text="Re-Confirmation";
            $pass_rest = $user->otp;
            if(isset($pass_rest)){
              
              $token=$pass_rest;
            }else{
              
               $otp=Str::random(6);           
                  $data = User::find($id);
                  $data->otp = $otp;
                  $data->save();
               $token=$otp;
            }
                      
          }
          $userData=array(
                'name' => $user->name,
                'email' => $user->email,
                'mobile' => $user->mobile,
                'otp' => $token,
                'text' => $text,
              );
          $url=route('user.confirmation',['id' => $id, 'type' => 'Re-confirmation']);
        try { 
          //$user->id->sendEmailVerificationNotification();
          \Mail::to($email)->send(new UserRegisterMail($userData));
        } catch (\Exception $e) { report($e);  }

          return view('auth.login_otp', compact('url','user'));
       }
       
    }
    /*
    |  Redirect THis Gaurd
    */
    protected function registered(Request $request, $user)
    {
        return redirect()->intended($this->redirectPath());
    }
    /*
    |---------------------------------------------------
    |  Vendor Registration
    |---------------------------------------------------
    */
    public function showVendorRegisterForm()
    {
        return view('vendors.register', ['url' => 'vendor']);
    }
    protected function createVendor(Request $request)
    {
       //echo "<pre>",print_r($request->all()),"</pre>";//die();
        $request->validate([
            'email' => ['required', 'string', 'email', 'max:255', 'unique:vendors'],
            'masters_id' => ['required'],
            'company_name' => ['required', 'max:255', 'unique:vendors'],
            'mobile' => ['required','string'],
        ]);

         $email=$request->email;
        list($name,$temp)=explode('@', $email);
         $name;
       
        $masters_id=$request->masters_id;
        $token=Str::random(60);
        $password=Hash::make(Str::random(10));
        $slug = SlugService::createSlug(Vendor::class, 'slug', $request->company_name);
         $data =new Vendor;
            $data->name = $name;
            $data->email = $email;
            $data->password = $password;
            $data->masters_id = $masters_id;
             $data->company_name = $request->company_name;
             $data->slug=$slug;
             $data->mobile = $request->mobile;
            $data->commission_id = 1;
            $data->save();
        //echo "<pre>",print_r($data),"</pre>";die();
         if($data->id){
            return redirect()->route('vendor.confirmation',['id' => $data->id, 'email' => 'confirmation']);
         }else{
            return redirect()->route('register.vendor')->with('error','Please Try Again.');
         }
         /* Vendor::create([
            'name' => $name,
            'email' => $email,
            'password' => Hash::make(Str::random(10)),
            'commission_id' =>1,
            'masters_id' =>$masters_id,
          ]);
          $user=array(
            'name' => $name,
            'email' => $email,
            'token' => $token,
          );
          //Create Password Reset Token
            DB::table('password_resets')->insert([
                'email' => $email,
                'token' => $token,
                'created_at' => Carbon::now()
            ]);

          \Mail::to($email)->send(new VendorRegisterMail($data));
         // return $user;
        return redirect()->route('login.vendor')->with('success','Please Verify Your Mail.');
        //return redirect()->intended('login/lco');*/
    }
    /*
    |-----------------------------------------------------------------------------
    |  Vendor Re-Send Verification mail
    |-----------------------------------------------------------------------------
    */
    function vendorResendMail(Request $request,$id)
    {
       $vendor = DB::table('vendors')->where('id', $id)->first();
       $type=$request->email;
       if($vendor){
        $email=$vendor->email;
        $token=''; $text='';
              
          if($type=='confirmation'){
            //DB::table('password_resets')->where('email', $email)->delete();
            $text="confirmation";
            $token=Str::random(60);
            $vendor->token=$token;
            //Create Password Reset Token

              DB::table('password_resets')->insert([
                  'email' => $email,
                  'token' => $token,
                  'created_at' => Carbon::now()
              ]);

          }else{
            $text="Re-Confirmation";
            $pass_rest = DB::table('password_resets')->where('email', $email)->first();
            if(isset($pass_rest->token)){
              $vendor->token=$pass_rest->token;
              $token=$pass_rest->token;
            }else{
              $token=Str::random(60);
              DB::table('password_resets')->insert([
                  'email' => $email,
                  'token' => $token,
                  'created_at' => Carbon::now()
              ]);
            }
                      
          }
          $user=array(
                'name' => $vendor->name,
                'email' => $vendor->email,
                'token' => $token,
                'text' => $text,
              );
          $url=route('vendor.confirmation',['id' => $id, 'email' => 'Re-confirmation']);
         try {
            \Mail::to($email)->send(new VendorRegisterMail($user));
           } catch (\Exception $e) { report($e);  }

          return view('vendors.mail.send', ['url' => $url]);
       }
       
    }
    /*
    |---------------------------------------------------
    |  Vendor Verifiction
    |---------------------------------------------------
    */
    public function VendorVerify($token)
    {
         $temp = DB::table('password_resets')->where('token', $token)->first();
        // var_dump($temp);
          if(isset($temp) ){
             $email = $temp->email; //'email_verified_at' =>now(),
            $vendor = DB::table('vendors')->where('email', $email)->first();
            if($vendor){
                if($vendor->email_verified_at==null){
             // if(is_null($vendor->email_verified_at))
                    $password=Str::random(10);
                    $data = Vendor::find($vendor->id);
                    $data->email_verified_at = now();
                    $data->active = 1;
                        $data->password = Hash::make($password);
                    $data->remember_token = Str::random(10);

                    $tempData=['name'=>$vendor->name,'email'=>$email,'crankpasswoed'=>$password];

                    $data->save();

                    $status = "Your e-mail is verified. Please Check your mail and login.";
              try {
                    \Mail::to($email)->send(new VendorRegenratePasswordMail($tempData));
                   } catch (\Exception $e) { report($e);  }
                   
                   // DB::table('password_resets')->where('token', $token)->delete();

                }else{
                    //DB::table('password_resets')->where('token', $token)->delete();
                     $status = "Your e-mail is already verified. You can now login."; 
                }
                if($vendor->verify==0){
                    $master=Master::all();
                     $category=Categorie::all();
                     $commission=Commission::all();
                     $city=City::all();
                     return view('vendors.profile.update', compact('vendor','city','category','commission','master'));
                      //return view('vendors.profile.edit', compact('vendor','city','category','commission'));
                }
            }

          } else {
            return redirect()->route('login.vendor')->with('warning', "Sorry your email cannot be identified.");
            //echo "not mached";
          }
          //echo "Done";
          return redirect()->route('login.vendor')->with('success', $status);
       
     }
    /*
    |---------------------------------------------------
    |  Vendor Profile Update
    |---------------------------------------------------
    */
     public function VendorUpdate(Request $request,$id)
     {         
        
        $request->validate([
            'name' => ['required', 'string'],
            'email' => ['required', 'string', 'email', 'max:255', ],
            'mobile' => 'required',

        ]);

         $data = Vendor::find($id);
            $data->email = $request->email;
            $data->name = $request->name;
            $data->org_type = $request->org_type;
            $data->company_name = $request->company_name;
            $data->designation = $request->designation;
            $data->mobile = $request->mobile;
            $data->telephone = $request->telephone;
            $data->city = $request->city;
            $data->address = $request->address;
            $data->pin = $request->pin;
                
                $data->latitude = $request->latitude;
                $data->longitude = $request->longitude;

                $data->category_id = $request->category_id;
                $data->commission_id = $request->commission_id;
                $data->image = $request->image;
                $data->menu = $request->menu;
                $data->coverimage = $request->coverimage;
                
            $data->aadhar = $request->aadhar;
            $data->aadharupload = $request->aadharupload;
            $data->pan = $request->pan;
            $data->panupload = $request->panupload;
            $data->gstin = $request->gstin;
            $data->gstinupload = $request->gstinupload;
            $data->fssai = $request->fssai;
            $data->fssaiupload = $request->fssaiupload;
            $data->save();
          DB::table('password_resets')->where('email', $data->email)->delete();
        return redirect()->route('login.vendor')->with('success',' Profile Updated successfully, Wait for Verifaction.');
        //return redirect()->route('vendor.dashboard')->with('success',' Profile Updated successfully, Wait for Verifaction.');
    }
/*vendor reset password request*/
    function vendorNewPass($token){
       // die('vendor pass request goo1');
        $status = "Your Account Password Reset Request Invalid.";
          $temp = DB::table('password_resets')->where('token', $token)->first();
        // var_dump($temp);
          if(isset($temp) ){
             $email = $temp->email; //'email_verified_at' =>now(),
            $vendor = DB::table('vendors')->where('email', $email)->first();
            if($vendor){
              
                    $password=Str::random(10);
                    $data = Vendor::find($vendor->id);                    
                        $data->password = Hash::make($password);
                    $data->remember_token = Str::random(10);

                    $tempData=['name'=>$vendor->name,'email'=>$email,'crankpasswoed'=>$password];

                    $data->save();

                    $status = "Your Account Password Reset. Please Check your mail and login.";
                    
              try {
                    \Mail::to($email)->send(new VendorRegenratePasswordMail($tempData));
                   } catch (\Exception $e) { report($e);  }
                   
                    DB::table('password_resets')->where('token', $token)->delete();

                
            }

          } else {
            return redirect()->route('login.vendor')->with('warning', "Sorry your email cannot be identified.");
            //echo "not mached";
          }
          //echo "Done";
          return redirect()->route('login.vendor')->with('success', $status);
    }
}
