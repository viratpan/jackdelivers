<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

    use Illuminate\Http\Request;
    use Auth;
    use Illuminate\Auth\SessionGuard;
    use Illuminate\Support\Facades\DB;
    use App\User;
    use App\Coupon;
    use Carbon\Carbon;
    
    

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    protected $maxAttempts = 3; // Default is 5
    protected $decayMinutes = 1; // Default is 1

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    /*
    |--------------------------------------------------------------------------
    | Overwite Method
    |--------------------------------------------------------------------------
    |
    */
    public function showLoginForm()
    {
        // Get URLs
        $urlPrevious = url()->previous();
        $urlBase = url()->to('/');

        // Set the previous url that we came from to redirect to after successful login but only if is internal
        if(($urlPrevious != $urlBase . '/login') && (substr($urlPrevious, 0, strlen($urlBase)) === $urlBase)) {
            session()->put('url.intended', $urlPrevious);
        }

        return view('auth.login');
    }
    public function LoginOtp(Request $request,$id)
    {
        $this->validate($request, [
            'otp'   => 'required|min:6'
        ]);
        $user = DB::table('users')->where('id', $id)->first();
        //var_dump(['email' => $user->email,'password'=>$user->password, 'otp' => $request->otp]);
        if (Auth::attempt(['email' => $user->email,'password'=>$user->tpassword, 'otp' => $request->otp])) {
                $data = User::find($id);
                $data->otp = '';$data->tpassword = '';
                $data->save();
                $user_id=$id;
                $user_name=$data->name;
                $user_ref=$data->referrer_id;
            DB::beginTransaction();
            try { 
              if(config('app.newUserOffer')==true){
                    $coupon=new Coupon;
                        $coupon->name = 'New User '.$user_name;
                        $coupon->code = 'NewUser'.$user_id;
                        $coupon->type = 1;// 1 discount,2 wallet_amount, 3 min_price
                        $coupon->for = 5;/// fixed for user
                        $coupon->user_id = $user_id;
                        $coupon->start_at=Carbon::now();
                        $coupon->discount=0;// 1 discount,2 wallet_amount, 3 min_price
                        $coupon->order_at=3; //'end_at'=>Carbon::now();
                        $coupon->description='auto-genrated';
                      //  var_dump($coupon);echo "<br>";
                    $coupon->save();
                    
                   
              }
              if(config('app.referrerUserOffer')==true){
                $code='refUser'.$data->id;
                $check=Coupon::where('code',$code)->where('active',1)->first();
                if(isset($check->id)){
                    //
                }else{
                    $coupon=new Coupon;
                        $coupon->name = 'Referrer User to '.$user_name;
                        $coupon->code = 'ref2User'.$user_id;
                        $coupon->type = 1;// 1 discount,2 wallet_amount, 3 min_price
                        $coupon->for = 5;/// fixed for user
                        $coupon->user_id = $user_ref;
                        $coupon->start_at=Carbon::now();
                        $coupon->discount=0;// 1 discount,2 wallet_amount, 3 min_price
                        $coupon->order_at=1; //'end_at'=>Carbon::now();
                        $coupon->description='auto-genrated';
                        //var_dump($coupon);echo "<br>";
                    $coupon->save();
                    
                }
                    
              }
              DB::commit();
            } catch (\Exception $e) {  DB::rollback(); report($e);  }
            return redirect()->intended('/');
        }
        return back()->with('error','No User Found');
    }
    /*
    |--------------------------------------------------------------------------
    | Admin Login
    |--------------------------------------------------------------------------
    |
    */
    public function showAdminLoginForm()
    {
        if(Auth::guard('admin')->check()){
            return redirect()->intended('/admin');
        }
        return view('admin.login', ['url' => 'admin']);
    }

    public function adminLogin(Request $request)
    {
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);

        if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password,'active' => 1], $request->get('remember'))) {

            return redirect()->intended('/admin');
        }
        return back()->withInput($request->only('email', 'remember'))
                     ->with('error','No User Found');
    }
    /*
    |  Admin Logout
    */
    public function Adminlogout(Request $request)
    {
        Auth::guard('admin')->logout();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect()->route('admin.dashboard')->with('loggedOut', true);
    }
    /*
    |--------------------------------------------------------------------------
    | Vendor Login
    |--------------------------------------------------------------------------
    |
    */
    public function showVendorLoginForm()
    {
        if(Auth::guard('vendor')->check()){
            return redirect()->intended('/vendor');
        }
        return view('admin.login', ['url' => 'vendor']);
    }

    public function vendorLogin(Request $request)
    {   
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);

        if (Auth::guard('vendor')->attempt(['email' => $request->email, 'password' => $request->password,'active' => 1,'verify' => 1], $request->get('remember'))) {
          
            return redirect()->intended('/vendor');
        }
        return back()->withInput($request->only('email', 'remember'))
                     ->with('error','No User Found Or Wait for Admin Verifaction.');
    }
    /*
    |  Vendor Logout
    */
    public function Vendorlogout(Request $request)
    {
        Auth::guard('vendor')->logout();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect()->route('vendor.dashboard')->with('loggedOut', true);
    }
}
