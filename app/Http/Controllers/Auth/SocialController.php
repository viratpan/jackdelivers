<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Socialite;
use Auth;
use App\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class SocialController extends Controller
{
    public function redirect($provider)
    {
     return Socialite::driver($provider)->redirect();
    }
    
	public function Callback($provider){
		try {
            //$user = Socialite::driver('google')->user();
            $userSocial =   Socialite::driver($provider)->stateless()->user();
        } catch (\Exception $e) {
            return redirect('/login');
        }
       // $userSocial =   Socialite::driver($provider)->stateless()->user();
        //echo "item: <pre>",print_r($userSocial),"</pre></hr>"; 
        $userSocial->getId();
        //var_dump($userSocial);
        $users=User::where(['email' => $userSocial->getEmail()])->first();
			if($users){
	            Auth::login($users);
	            return redirect('/');
	           // echo "<br>exits";
	        }else{// echo "<br>new";				
	         	return redirect()->route('home')->with('error','No User Found');;
	        }
	}
}
