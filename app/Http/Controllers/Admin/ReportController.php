<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


use App\Http\Controllers\Input;
use Illuminate\Support\Facades\DB;
use App\Vendor;
use App\User;
use App\Master;
use App\Rider;

use App\Product;
use App\Order;
use App\City;
use App\Categorie;
use App\Commission;
use App\Sub_categories;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Illuminate\Support\Arr;

class ReportController extends Controller
{
    function __construct($foo = null)
    {
        $this->middleware('auth:admin');
    }
    public function vendor(Request $request)
    {
    	$filters = [
				    'search' => 'search',				    
				    'vendors.active' => 'status',
				    'vendors.city' => 'city',
                    'vendors.masters_id' => 'master',
                    'vendors.category_id' => 'category',
                    'vendors.verify' => 'verify',
				  ];
    	$data=DB::table('vendors')
                ->leftJoin('cities','cities.id','vendors.city')
                ->leftJoin('categories','categories.id','=','vendors.category_id')
                //->leftJoin('commissions','commissions.id','=','vendors.commission_id')
                ->leftJoin('masters','masters.id','=','vendors.masters_id')

                 ->where(function ($query) use ($request, $filters) {
					    foreach ($filters as $column => $key) {
					        $query->when(Arr::get($request, $key), function ($query, $value) use ($column) {
					            if($column =='search'){
                                    $query->where('vendors.name','LIKE', '%' . $value . '%')
                                    ->orWhere('cities.name','LIKE', '%' . $value . '%')
                                    ->orWhere('vendors.company_name','LIKE', '%' . $value . '%')
                                    ->orWhere('vendors.address','LIKE', '%' . $value . '%')
                                    ->orWhere('vendors.pin','LIKE', '%' . $value . '%')
                                    ->orWhere('vendors.email','LIKE', '%' . $value . '%');
                                }else{
                                    $query->where($column, $value);
                                }
                                
					        });
					    }
					})
                ->select('vendors.*','masters.name as masters_name','cities.name as city_name','categories.name as categories_name',)
                //'commissions.name as commissions_name','commissions.commission as commissions_on'
                ->get();

        $city=City::all();
        $master=Master::all();
        $categry=Categorie::all();
        // $commission=Commission::all();
        //die($categry);
        return view('admin.report.vendor-list', compact('data','city','categry','master','request'));
    }


    public function item(Request $request)
    {
    	
    	$filters = [
				    'search' => 'search',   
				    'products.id' => 'item',
                    'vendors.masters_id' => 'master',
				    'products.categry_id' => 'category',
				    'products.sub_categry_id' => 'subcategory',
				    'products.vendor_id' => 'vendor',
				    'products.status' => 'status',
				    'vendors.city' => 'city',
				  ];
			
    	$data=DB::table('products')  
                //->leftJoin('items','items.id','=','products.item_id')
                ->leftJoin('categories','categories.id','=','products.categry_id')
                ->leftJoin('sub_categories','sub_categories.id','=','products.sub_categry_id')
                ->leftJoin('vendors','vendors.id','=','products.vendor_id')
                ->leftJoin('masters','masters.id','=','products.masters_id')
                 ->leftJoin('attributes','attributes.id','=','products.attrubute_id')
                ->leftJoin('cities','cities.id','vendors.city')
                ->where(function ($query) use ($request, $filters) {
					    foreach ($filters as $column => $key) {
					        $query->when(Arr::get($request, $key), function ($query, $value) use ($column) {
					            if($column =='search'){
                                    $query->where('products.name','LIKE', '%' . $value . '%')
                                    ->orWhere('products.slug','LIKE', '%' . $value . '%')
                                    ->orWhere('categories.name','LIKE', '%' . $value . '%')
                                    ->orWhere('sub_categories.name','LIKE', '%' . $value . '%')
                                    ->orWhere('cities.name','LIKE', '%' . $value . '%')
                                    ->orWhere('vendors.company_name','LIKE', '%' . $value . '%')
                                    ->orWhere('vendors.address','LIKE', '%' . $value . '%')
                                    ->orWhere('vendors.pin','LIKE', '%' . $value . '%')
                                    ->orWhere('vendors.email','LIKE', '%' . $value . '%');
                                }else{
                                    $query->where($column, $value);
                                }
					        });
					    }
					})
                ->select('products.id as productsId','products.created_at as pcreated_at','products.*','categories.name as categories_name','sub_categories.name as sub_categories_name',
                    'vendors.name as vendor_name','vendors.email as vendor_email','cities.name as city_name','attributes.name as attributes_name','masters.name as masters_name',)
                ->get();
            /*if ($request->has('search')) {
	           $tt->where('items.name', '>', $request->search);
	        }
            $data=$tt->get();*/

                
                
        $master=Master::all();
        $vendor=Vendor::all();
         $city=City::all();
          $categry=Categorie::all();
           $subcategry=Sub_categories::all();
         //echo"<pre>",print_r($data),"</pre>";die();
        return view('admin.report.item-list', compact('data','city','categry','subcategry','vendor','request','master'));
    }
    public function user(Request $request)
    {    	
    	$filters = [
				    'search' => 'search',
				    
				  ];
			
    	$data=DB::table('users')
    			->where(function ($query) use ($request, $filters) {
					    foreach ($filters as $column => $key) {
					        $query->when(Arr::get($request, $key), function ($query, $value) use ($column) {
					            if($column =='search'){
                                    $query->where('users.name','LIKE', '%' . $value . '%')
                                    ->orWhere('users.email','LIKE', '%' . $value . '%')
                                    ->orWhere('users.mobile','LIKE', '%' . $value . '%');
                                    
                                }else{
                                    $query->where($column, $value);
                                }
					        });
					    }
					})
    	 		->select('users.*')
                ->get(); 
         return view('admin.report.user-list', compact('data','request'));

    }
    
    public function rider(Request $request,$order_id=null)
    {
        $filters = [
                    'search' => 'search',
                    'date' => 'date',
                    'orders.confirmed' => 'confirmed',
                    'orders.status' => 'status',
                    'vendors.id' => 'vendor',
                    'riders.id' => 'rider',
                    'riders.status' => 'riders_status',
                  ];
        $assignTo=null;
        if($order_id){
            $assignTo=Order::find($order_id);

            if(!$assignTo){
                return redirect()->route('admin.rider.order-list')->with('error',' Order Not Match.');
           }
       }
        $data=DB::table('orders')
                ->leftJoin('users','users.id','=','orders.user_id')
                ->leftJoin('addresses','addresses.id','orders.user_address_id')                
                    ->leftJoin('vendors','vendors.id','=','orders.vendor_id')
                        ->leftJoin('masters','masters.id','=','vendors.masters_id')
                ->leftJoin('riders','riders.id','=','orders.rider_id')
                    
                ->where(function ($query) use ($request, $filters) {
                        foreach ($filters as $column => $key) {
                            $query->when(Arr::get($request, $key), function ($query, $value) use ($column) {
                                if($column =='search'){
                                    $query->where('users.name','LIKE', '%' . $value . '%')
                                    ->orWhere('users.email','LIKE', '%' . $value . '%')
                                    ->orWhere('users.mobile','LIKE', '%' . $value . '%');
                                    
                                }elseif($column =='date'){
                                    list($tstart,$tend)=explode('/', str_replace(' - ','/', $value));
                                    $start=$this->getdate($tstart);
                                    $end=$this->getdate($tend);
                                    $query->whereBetween('orders.created_at',[$start,$end]);
                                  // $query->whereBetween('orders.created_at',explode('-', $value));
                                    
                                }else{
                                    $query->where($column, $value);
                                }
                            });
                        }
                    })

                //->where('orders.vendor_id',$user->id)
                ->select('users.name as user_names','users.email as user_email','users.mobile as user_mobile',
                    'addresses.*','orders.*','orders.id as order_rid','orders.status as orders_status',
                    'vendors.company_name as vendor_company_name','masters.name as masters_name',
                    
                    'riders.name as rider_name','riders.email as rider_email','riders.mobile as rider_mobile')
                 
                ->orderby('orders.created_at','desc')
                ->get();
        $product=Product::all();
        $rider=Rider::all();
        $vendor=Vendor::all();
        $city=City::all();
        $categry=Categorie::all();
        $subcategry=Sub_categories::all();
        $rider=Rider::all();
        $rider_onduty=Rider::where('on_duty',1)->get();
        //echo"<pre>",print_r($data),"</pre>";die();
        return view('admin.report.rider-order-list', compact('data','product','city','categry','subcategry','vendor','request','assignTo','rider','rider_onduty'));
    }

    public function payment(Request $request)
    {
    	$filters = [
                    'search' => 'search',
                    'date' => 'date',                    
                    'payments.status' => 'status',
                    'payments.payment_type' => 'payment_type',                    
                  ];
        $data=DB::table('payments')
                ->where(function ($query) use ($request, $filters) {
                        foreach ($filters as $column => $key) {
                            $query->when(Arr::get($request, $key), function ($query, $value) use ($column) {
                                if($column =='search'){
                                    $query->where('payments.OD_id','LIKE', '%' . $value . '%')
                                    ->orWhere('payments.order_id','LIKE', '%' . $value . '%')
                                    ->orWhere('payments.transition_id','LIKE', '%' . $value . '%');
                                    
                                }elseif($column =='date'){
                                  list($tstart,$tend)=explode('/', str_replace(' - ','/', $value));
                                  $start=$this->getdate($tstart);
                                  $end=$this->getdate($tend);
                                  $query->whereBetween('payments.created_at',[$start,$end]);
                                    
                                }else{
                                    $query->where($column, $value);
                                }
                            });
                        }
                    })

                //->where('orders.vendor_id',$user->id)  2021-01-12
                ->select('payments.*')                 
                //->orderby('payments.created_at','desc')
                ->get();
         return view('admin.report.payment-list', compact('data','request'));
    }

    public function getdate($date)
    {
        return Carbon::parse($date);
        //return Carbon::createFromFormat('Y-m-d H:i:s', $date);
        //return Carbon::createFromFormat('Y-m-d ', $date);
    }
}
