<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Input;

class UserController extends Controller
{
  function __construct()
  {
      $this->middleware('auth:admin');

  }
  public function index(Request $request)
  {
       $data=DB::table('users')
              ->select('users.*')
              ->paginate(100);

          return View('admin/user/list',compact('data'));
  }

  


}
