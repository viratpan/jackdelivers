<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Master;
use App\Categorie;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Cviebrock\EloquentSluggable\Services\SlugService;

class CategorieController extends Controller
{
    function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id=null)
    {
        $stateData =Categorie::find($id);
       // $data = Categorie::all();
         $data=DB::table('categories')
                ->leftJoin('masters','masters.id','=','categories.masters_id')
                ->select('categories.*','masters.name as master_name')
                ->get();
         $master = Master::all();
        return view('admin.categorie.index', compact('data','stateData','master'));
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id=null)
    {
    	$request->validate([
            'name' => ['required', 'string','unique:categories'],
            'masters_id' => ['required', 'string'],
        ]);

      $data =new Categorie;
            $data->name = $request->name;
            $data->masters_id = $request->masters_id;
            $data->image = $request->image;
            $data->order = $request->order;
            $data->slug = SlugService::createSlug(Categorie::class, 'slug', $request->name);
            $data->save();

        return redirect()->route('admin.category')->with('success',$data->name. ' Category created successfully.');
    }



    public function update(Request $request, $id)
    {

        $request->validate([
            'name' => ['required', 'string'],
            'masters_id' => ['required', 'string'],
         ]);
         $data = Categorie::find($id);
            $data->name = request('name');
            $data->masters_id = $request->masters_id;
            $data->image = $request->image;
            $data->order = $request->order;
            $data->save();

        return redirect()->route('admin.category')
                        ->with('success',$data->name. ' Category  Updated successfully');
    }


    public function destroy($id)
    {
         Categorie::find($id)->delete();
        //$Lco = Lco::withTrashed()->get();

        return redirect()->route('admin.category')
                        ->with('success','Categorie deleted successfully');
    }
}
