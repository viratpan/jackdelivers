<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Vendor;
use App\Master;
use App\City;
use App\Categorie;
use App\Attributes;
use App\Commission;
use Illuminate\Support\Facades\DB;
use Auth;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Input;
    use Illuminate\Support\Facades\Hash;
    use Illuminate\Support\Str;

    use Carbon\Carbon;
    use App\Mail\VendorRegisterMail;
    use App\Mail\VendorRegenratePasswordMail;

class VendorController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth:admin');
    }
    public function index(Request $request)
    {
    	   $data=DB::table('vendors')
                ->leftJoin('cities','cities.id','vendors.city')
                ->leftJoin('masters','masters.id','=','vendors.masters_id')
                 ->leftJoin('categories','categories.id','=','vendors.category_id')
                  ->leftJoin('commissions','commissions.id','=','vendors.commission_id')
                 ->select('vendors.*','masters.name as masters_name','cities.name as city_name','categories.name as categories_name','commissions.name as commissions_name','commissions.commission as commissions_on')
                ->orderby('vendors.name')
                ->get();
            //print_r($data);die();
        return view('admin.vendor.list', compact('data'));
    }
    public function show($id)
    {
        $vendor=DB::table('vendors')
                ->leftJoin('cities','cities.id','=','vendors.city')
                ->leftJoin('categories','categories.id','=','vendors.category_id')
                ->leftJoin('masters','masters.id','=','vendors.masters_id')
                  ->leftJoin('commissions','commissions.id','=','vendors.commission_id')
                ->where('vendors.id',$id)
                 ->select('vendors.*','masters.name as masters_name','cities.name as city_name','categories.name as categories_name','commissions.name as commissions_name','commissions.commission as commissions_on')
                ->first();
         //$city=City::all();
         // $category=Categorie::all();
          //$commission=Commission::all();
        return view('vendors.profile.profile', compact('vendor','category'));
    }
    public function create()
    {
        $city=City::all();
         $master = Master::all();
        $category=Categorie::all();
          $commission=Commission::all();
        
        return view('vendors.profile.add', compact('city','category','commission','master'));
    }
    public function store(Request $request)
    {
        /*echo"<pre>",print_r($request->all()),"<pre>";echo "<hr>";
        $request=$request->all();

        foreach ($request as $key => $value) {
           echo"data->$key = request->$key;<br>";
        }
        //echo"<pre>",print_r($request->allFiles()),"<pre>";echo "<hr>";
        die();*/
        $request->validate([
            'email' => ['required', 'string', 'email', 'max:255', 'unique:vendors'],
        ]);
        $password=Str::random(10);   $token=Str::random(60);

        $data =new Vendor;
           $data->email = $request->email;
            $data->name = $request->name;
            $data->company_name = $request->company_name;
            $data->org_type = $request->org_type;
            $data->designation = $request->designation;
            $data->mobile = $request->mobile;
            $data->telephone = $request->telephone;
            $data->city = $request->city;
            $data->address = $request->address;
            $data->pin = $request->pin;
                
                $data->latitude = $request->latitude;
                $data->longitude = $request->longitude;

                 $data->masters_id = $request->masters_id;
                $data->category_id = $request->category_id;
                $data->commission_id = $request->commission_id;

                $data->costoftwo = $request->costoftwo;
                $data->ontime = $request->ontime;
                $data->offtime = $request->offtime;
                $data->about = $request->about;

                $data->image = $request->image;
                $data->coverimage = $request->coverimage;
                
            $data->aadhar = $request->aadhar;
            $data->aadharupload = $request->aadharupload;
            $data->pan = $request->pan;
            $data->panupload = $request->panupload;
            $data->gstin = $request->gstin;
            $data->gstinupload = $request->gstinupload;
            $data->fssai = $request->fssai;
            $data->fssaiupload = $request->fssaiupload;  

                $data->password = Hash::make($password);
                $data->active = 1;
                 $data->verify = 1;
                $data->email_verified_at = now();
                 $data->verified_at = now();
                $data->remember_token = Str::random(16);
            $data->save();


             $user=array(
                'name' => $data->name,
                'email' => $data->email,
                'token' => $token,
              );
             $tempData=['name'=>$data->name,'email'=>$data->email,'crankpasswoed'=>$password];
            //Create Password Reset Token
            /*DB::table('password_resets')->insert([
                'email' => $data->email,
                'token' => $token,
                'created_at' => Carbon::now()
            ]);*/

         // \Mail::to($data->email)->send(new VendorRegisterMail($user));
             \Mail::to($data->email)->send(new VendorRegenratePasswordMail($tempData));
           return redirect()->route('admin.vendor-list')->with('success','Please Confirm Vendor to Check Mail.');
    }
    public function edit($id)
    {
        $vendor=DB::table('vendors')
                ->leftJoin('cities','cities.id','=','vendors.city')
                ->where('vendors.id',$id)
                ->select('vendors.*','cities.name as city_name')
                ->first();
         if(!$vendor){
            return redirect()->route('admin.vendor-list')->with('error',' Item Not Match.');
       }
        $master = Master::all();
        $category=Categorie::all();
        $city=City::all();
        $commission=Commission::all();
        return view('vendors.profile.edit', compact('vendor','city','category','commission','master'));
    }
    public function update(Request $request, $id)
    {
        //$user = auth()->user();
        //$id=$user->id;
        $request->validate([
            'name' => ['required', 'string'],
            'email' => ['required', 'string', 'email', 'max:255', ],
            'mobile' => 'required',

        ]);

         $data = Vendor::find($id);
            $data->email = $request->email;
            $data->name = $request->name;
            $data->company_name = $request->company_name;
            $data->org_type = $request->org_type;
            $data->designation = $request->designation;
            $data->mobile = $request->mobile;
            $data->telephone = $request->telephone;
            $data->city = $request->city;
            $data->address = $request->address;
            $data->pin = $request->pin;
                
                $data->latitude = $request->latitude;
                $data->longitude = $request->longitude;

                 $data->masters_id = $request->masters_id;
                $data->category_id = $request->category_id;
                $data->commission_id = $request->commission_id;

                $data->costoftwo = $request->costoftwo;
                $data->ontime = $request->ontime;
                $data->offtime = $request->offtime;
                $data->about = $request->about;
                
                $data->image = $request->image;
                $data->coverimage = $request->coverimage;

            $data->aadhar = $request->aadhar;
            $data->aadharupload = $request->aadharupload;
            $data->pan = $request->pan;
            $data->panupload = $request->panupload;
            $data->gstin = $request->gstin;
            $data->gstinupload = $request->gstinupload;
            $data->fssai = $request->fssai;
            $data->fssaiupload = $request->fssaiupload;               

            $data->save();



        return redirect()->route('admin.vendor-list')->with('success',' Vendor '.$data->name.' Updated successfully.');

    }
    public function status($id,$status)
    {
         $data = Vendor::find($id);
            $data->active= $status;
        $data->save();
        return redirect()->route('admin.vendor-list')->with('success',' Vendor Activation Updated successfully.');
    }
    public function verify($id,$status)
    {
         $data = Vendor::find($id);
            $data->verify = $status;
            $data->verified_at = now();
        $data->save();
        return redirect()->route('admin.vendor-list')->with('success',' Vendor Verification Updated successfully.');
    }
}
