<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Rider;
use App\Order;
use App\City;
use App\RiderPayment;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Illuminate\Support\Arr;

class RiderController extends Controller
{
    function __construct()
    {
    	 $this->middleware('auth:admin');
    }
	public function index(Request $request,$id=null)
	{
		$filters = [
				    'search' => 'search',				    
				    'riders.active' => 'status',
				    'riders.on_duty' => 'onduty',
				    'riders.zone_id' => 'city',                  
                   
				  ];
    	$data=DB::table('riders')
                ->leftJoin('cities','cities.id','riders.zone_id')                
               

                 ->where(function ($query) use ($request, $filters) {
					    foreach ($filters as $column => $key) {
					        $query->when(Arr::get($request, $key), function ($query, $value) use ($column) {
					            if($column =='search'){
                                    $query->where('riders.name','LIKE', '%' . $value . '%');
                                    //->orWhere('cities.name','LIKE', '%' . $value . '%');
                                    
                                   
                                }else{
                                    $query->where($column, $value);
                                }
                                
					        });
					    }
					})
                 /*,'cities.name as city_name',*/
                ->select('riders.*','cities.name as city_name')
                ->get();

       $city=City::all();
       
        return view('admin.rider.index', compact('data','city','request'));
	}
	public function create($id=null)
	{
		$city=City::all();
		$data=null;
		if($id){
			$data=Rider::find($id);
			 if(!$data){
            return redirect()->route('admin.rider-list')->with('error',' Rider Not Match.');
       }
		}
		return view('admin.rider.add-rider', compact('city','data'));
	}
	public function store(Request $request)
	{
		$user = auth()->user();
		$request->validate([
			'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:riders'],
            'mobile' => ['required', 'string',  'max:15', 'unique:riders'],
            'dl' => ['required', 'string'],
            'aadhar' => ['required', 'string'],
            'address' => ['required', 'string'],
        ]);
          $data =new Rider;
            $data->name = $request->name;
			$data->email = $request->email;
			$data->mobile = $request->mobile;
			$data->emergency_mobile = $request->emergency_mobile;
			$data->blood_group = $request->blood_group;
			$data->image = $request->image;
			$data->address = $request->address;
			$data->dl = $request->dl;
			$data->dl_image = $request->dl_image;
			$data->dl_valid = $request->dl_valid;
			$data->aadhar = $request->aadhar;
			$data->aadhar_image = $request->aadhar_image;
			$data->pan = $request->pan;
			$data->pan_image = $request->pan_image;
			$data->ac_no = $request->ac_no;
			$data->ac_name = $request->ac_name;
			$data->ac_ifsc = $request->ac_ifsc;
			$data->upi = $request->upi;
				$data->zone_id=$request->zone_id;
			//$data->join_at=now();
			$data->approved_by=$user->id;
           $data->save();


        // if($data->id){
         	return redirect()->route('admin.rider-list')->with('success',$data->name.' Rider created successfully.');
         /*}else{
         	return redirect()->route('admin.rider-list')->with('error',$data->name.' Rider not created successfully.');
         }*/
        
	}
	
	public function update(Request $request,$id)
	{
		$request->validate([
			'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', ],
            'mobile' => ['required', 'string',  'max:15', ],
            'dl' => ['required', 'string'],
            'aadhar' => ['required', 'string'],
            'address' => ['required', 'string'],
        ]);
		$data = Rider::find($id);
            $data->name = $request->name;
			$data->email = $request->email;
			$data->mobile = $request->mobile;
			$data->emergency_mobile = $request->emergency_mobile;
			$data->blood_group = $request->blood_group;
			$data->image = $request->image;
			$data->address = $request->address;
			$data->dl = $request->dl;
			$data->dl_image = $request->dl_image;
			$data->dl_valid = $request->dl_valid;
			$data->aadhar = $request->aadhar;
			$data->aadhar_image = $request->aadhar_image;
			$data->pan = $request->pan;
			$data->pan_image = $request->pan_image;
			$data->ac_no = $request->ac_no;
			$data->ac_name = $request->ac_name;
			$data->ac_ifsc = $request->ac_ifsc;
			$data->upi = $request->upi;
				$data->zone_id=$request->zone_id;
        $data->save();
        return redirect()->route('admin.rider-list')->with('success',$data->name.' Rider created successfully.');
	}
	public function status($id,$status)
    {
        $data = Rider::find($id);
            $data->active = $status;
        $data->save();
        return redirect()->route('admin.rider-list')->with('success',' Rider Status Updated successfully.');
    }
    public function onDuty($id,$status)
    {
        $data = Rider::find($id);
            $data->on_duty = $status;
        $data->save();
        return redirect()->route('admin.rider-list')->with('success',' Rider Duty Updated successfully.');
    }
    public function assign(Request $request,$order_id)
    {
    	// echo"<pre>",print_r($request->all()),"</pre>";die();
    	$data = Order::find($order_id);
    	
    	if(!$data){
            return redirect()->route('admin.rider.order-list')->with('error',' Order Not Found.');
        }
            $data->rider_id = $request->rider_id;
        $data->save();       
       
        return redirect()->route('admin.rider.order-list')->with('success',' Rider Assigned successfully.');
    }


    function  getDistance(){
    	$address = $value["street"]." ".$value["region"]." ".$value["country_id"]." ".$value["zip_code"];
    	$url = "http://maps.googleapis.com/maps/api/directions/json?origin=".str_replace(' ', '+', $source_address)."&destination=".str_replace(' ', '+', $destination_address)."&sensor=false";
	            $ch = curl_init();
	            curl_setopt($ch, CURLOPT_URL, $url);
	            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	            curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
	            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	            $response = curl_exec($ch);
	            curl_close($ch);
	            $response_all = json_decode($response);
	            // print_r($response);
	            echo"<hr><pre>",print_r($response),"</pre><hr>";
	            $distance = $response_all->routes[0]->legs[0]->distance->text;
    }
}
