<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\City;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Cviebrock\EloquentSluggable\Services\SlugService;

class CityController extends Controller
{

    function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id=null)
    {
        $stateData =City::find($id);
        $data = City::all();
        return view('admin.city.index', compact('data','stateData'));
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id=null)
    {
            $request->validate([
            'name' => ['required', 'string','unique:cities'],
            ]);
            $data =new City;
            $data->name = $request->name;
            $data->slug = SlugService::createSlug(City::class, 'slug', $request->name);
            $data->save();
        //die($password);
            return redirect()->route('admin.city')->with('success','City created successfully.');
    }



    public function update(Request $request, $id)
    {
            $request->validate([
            'name' => ['required', 'string'],

        ]);

            $data = City::find($id);
            $data->name = request('name');
            $data->save();

        return redirect()->route('admin.city')
                        ->with('success',$data->name.'  updated successfully');
    }


    public function destroy($id)
    {
              City::find($id)->delete();
        //$Lco = Lco::withTrashed()->get();
              return redirect()->route('admin.categorie')
              ->with('success','City deleted successfully');
    }

}
