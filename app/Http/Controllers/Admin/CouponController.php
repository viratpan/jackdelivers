<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Coupon;
use App\Vendor;
use App\User;
use App\Master;
use App\Product;
use App\Categorie;
use App\Sub_categories;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;


class CouponController extends Controller
{
    function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index($id=null)
    {
        $stateData =Coupon::find($id);
        $data = DB::table('coupons')
        				->leftJoin('masters','masters.id','=','coupons.masters_id')                 
                 		->leftJoin('categories','categories.id','=','coupons.categry_id')
                 		->leftJoin('sub_categories','sub_categories.id','=','coupons.sub_categry_id')
        				->leftJoin('vendors','vendors.id','=','coupons.vendor_id')
        				->leftJoin('users','users.id','=','coupons.user_id')
        				->leftJoin('products','products.id','=','coupons.product_id')
        				//	->leftJoin('vendors','vendors.id','=','products.vendor_id')
    ->select('coupons.*','masters.name as master_name','categories.name as cat_name','sub_categories.name as sub_cat_name','vendors.company_name as vendors_name','users.name as user_name','products.name as products_name')
        		         ->get();
        
        $category=Categorie::all();        
        $sub_category=Sub_categories::all();
        
        $master=Master::all();
        $vendor=Vendor::all();   
        $user=User::all();

        $product=DB::table('products')
        		         ->leftJoin('vendors','vendors.id','=','products.vendor_id')
        		         ->select('products.id','products.name','vendors.company_name')
        		         ->orderBy('vendors.id')
        		         ->get();


        return view('admin.coupon.index', compact('data','stateData','category','sub_category','master','vendor','user','product'));
    }

    public function store(Request $request,$id=null)
    {  
    	$request->validate([
            'code' => ['required', 'string','unique:coupons'],
			'name' => ['required'],
			'start_at' => ['required'],
			'end_at' => ['required'],
			//'created_by' => ['required'],
        ]);
    	$user = auth()->user();
        $data =new Coupon;
            $data->created_by = $user->id;
			$data->name = $request->name;
			$data->code = $request->code;
			$data->start_at = $request->start_at;
			$data->end_at = $request->end_at;

			$data->type = $request->type;
			$data->for = $request->for;
            if($data->for==0){
                $data->all = 1;
            }
				$data->discount = $request->discount;
				$data->wallet_amount = $request->wallet_amount;
				$data->min_price = $request->min_price;

				$data->masters_id = $request->masters_id;
				$data->categry_id = $request->categry_id;
				$data->sub_categry_id = $request->sub_categry_id;
				$data->vendor_id = $request->vendor_id;
				$data->user_id = $request->user_id;
				$data->product_id = $request->product_id;

			$data->description = $request->description;
        $data->save();

        return redirect()->route('admin.coupon')->with('success','Coupon created successfully.');
    }



    public function update(Request $request, $id)
    {

        $request->validate([
            'name' => ['required', 'string'],
			'commission' => ['required'],
         ]);
         $data = Commission::find($id);
            $data->name = request('name');
            $data->commission = $request->commission;
            $data->save();

        return redirect()->route('admin.commission')
                        ->with('success',$data->name.'  updated successfully');
    }
    public function status($id)
    {
         
    }
}
