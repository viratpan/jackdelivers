<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Auth\SessionGuard;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest')->except('logout');
       // $this->middleware('vendor')->except('logout');
    }

   
    /*logout functions*/
    public function Adminlogout(Request $request)
    {
        Auth::guard('admin')->logout();

        $request->session()->invalidate();

        return redirect()->route('admin.dashboard')->with('loggedOut', true);
    }
    function changepassword(){
         return view('admin.passwords.change');
    }
    function updatePassword(Request $request){
       // print_r($request->all());die();
        if (!(Hash::check($request->oldpassword, Auth::guard('admin')->user()->password))) {
            // The passwords matches
            return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
        }

        if(strcmp($request->oldpassword, $request->password_confirmation) == 0){
            //Current password and new password are same
            return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose a different password.");
        }

        $validatedData = $request->validate([
            'oldpassword' => 'required',
            'password_confirmation' => 'required|string|min:8|same:password',
        ]);

         //Change Password
        $user = Auth::guard('admin')->user();
        $user->password = Hash::make($request->password_confirmation);
        $user->save();

        return redirect()->back()->with("success","Password changed successfully !");

   
    }
   
}
