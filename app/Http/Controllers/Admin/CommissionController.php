<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Commission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Cviebrock\EloquentSluggable\Services\SlugService;

class CommissionController extends Controller
{
    function __construct()
    {
      $this->middleware('auth:admin');
    }
    public function index($id=null)
    {
        $stateData =Commission::find($id);
        $data = Commission::all();
        return view('admin.commission.index', compact('data','stateData'));
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id=null)
    {
    	$request->validate([
            'name' => ['required', 'string','unique:commissions'],
			      'commission' => ['required'],
        ]);

      $data =new Commission;
            $data->name = $request->name;
            $data->commission = $request->commission;
            $data->save();

        return redirect()->route('admin.commission')->with('success','Commission created successfully.');
    }



    public function update(Request $request, $id)
    {

        $request->validate([
            'name' => ['required', 'string'],
			'commission' => ['required'],
         ]);
         $data = Commission::find($id);
            $data->name = request('name');
            $data->commission = $request->commission;
            $data->save();

        return redirect()->route('admin.commission')
                        ->with('success',$data->name.'  updated successfully');
    }


    public function destroy($id)
    {
         Commission::find($id)->delete();
        //$Lco = Lco::withTrashed()->get();

        return redirect()->route('admin.commission')
                        ->with('success','Commission deleted successfully');
    }
}
