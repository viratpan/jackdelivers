<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Master;
use App\OrderStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Cviebrock\EloquentSluggable\Services\SlugService;

class MasterController extends Controller
{
    function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id=null)
    {
        $stateData =Master::find($id);
        $data = DB::table('masters')
                      ->leftJoin('order_statuses','order_statuses.id','=','masters.id')
                      ->select('masters.*','order_statuses.name as order_statuses_name')
                      ->get();
        $OrderStatus=OrderStatus::All();
        return view('admin.master.index', compact('data','stateData','OrderStatus'));
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id=null)
    {
    	$request->validate([
            'name' => ['required', 'string','unique:categories'],
            'cancellation_till' => ['required'],
            'cancellation_cost' => ['required'],
            'commission' => ['required'],
            'gst' => ['required'],
            'delivery_cost' => ['required'],


        ]);

      $data =new Master;
            $data->name = $request->name;
            $data->image = $request->image;
            $data->cancellation_till = $request->cancellation_till;
            $data->cancellation_cost = $request->cancellation_cost;
              $data->gst = $request->gst;
              $data->commission = $request->commission;
              $data->delivery_cost = $request->delivery_cost;
                $data->returnable = $request->returnable;
            $data->search_text = $request->search_text;
            $data->order = $request->order;
            $data->slug = SlugService::createSlug(Master::class, 'slug', $request->name);
            $data->save();

        return redirect()->route('admin.master')->with('success','Master Created successfully.');
    }



    public function update(Request $request, $id)
    {

        $request->validate([
            'name' => ['required', 'string'],
            'cancellation_till' => ['required'],
            'cancellation_cost' => ['required'],
            'commission' => ['required'],
            'gst' => ['required'],
            'delivery_cost' => ['required'],

         ]);
         $data = Master::find($id);
            $data->name = request('name');
            $data->image = $request->image;
            $data->cancellation_till = $request->cancellation_till;
            $data->cancellation_cost = $request->cancellation_cost;
              $data->gst = $request->gst;
              $data->commission = $request->commission;
              $data->delivery_cost = $request->delivery_cost;
                $data->returnable = $request->returnable;
            $data->search_text = $request->search_text;
            $data->order = $request->order;
            $data->save();

        return redirect()->route('admin.master')
                        ->with('success',$data->name.' Updated successfully');
    }
    public function status($id,$status)
    {
         $data = Master::find($id);
            $data->active = $status;
        $data->save();
        

        return redirect()->route('admin.master')->with('success',$data->name.'  Status Updated successfully.');
    }

    public function destroy($id)
    {
         Master::find($id)->delete();
        //$Lco = Lco::withTrashed()->get();

        return redirect()->route('admin.master')
                        ->with('success','Master deleted successfully');
    }
}
