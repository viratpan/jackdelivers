<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Categorie;
use App\Sub_categories;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Cviebrock\EloquentSluggable\Services\SlugService;

class SubCategorieController extends Controller
{
    function __construct()
    {
       // $this->middleware('auth:admin');
    }
    public function index($id=null)
    {
        $stateData =Sub_categories::find($id);
        //$data = City::all();
        $data=DB::table('sub_categories')
                ->leftJoin('categories','categories.id','=','sub_categories.categry_id')
                 ->leftJoin('masters','masters.id','=','categories.masters_id')
                ->select('sub_categories.*','categories.name as states_name','masters.name as master_name')
                ->get();
        $categry = Categorie::all();

        return view('admin.subcategorie.index', compact('data','stateData','categry'));
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id=null)
    {
        $request->validate([
            'name' => ['required', 'string','unique:sub_categories'],
            'categry_id' => ['required', ],
        ]);
            $data =new Sub_categories;
            $data->name = $request->name;
            $data->categry_id = $request->categry_id;
            $data->image = $request->image;
            $data->slug = SlugService::createSlug(Sub_categories::class, 'slug', $request->name);
        $data->save();
       // City::create($request->all());
        //die($password);
        return redirect()->route('admin.subcategory')->with('success',$data->name. ' Sub-Category created successfully.');
    }



    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => ['required', 'string'],
            'categry_id' => ['required', ],
         ]);
         $data = Sub_categories::find($id);
            $data->name = request('name');
            $data->categry_id = $request->categry_id;
            $data->image = $request->image;
            $data->save();

        return redirect()->route('admin.subcategory')
                        ->with('success',$data->name. ' Sub-Category Updated successfully');
    }


    public function destroy($id)
    {
         City::find($id)->delete();
        //$Lco = Lco::withTrashed()->get();

        return redirect()->route('admin.subcategory')
                        ->with('success','City deleted successfully');
    }

    public function getCatList(Request $request)
    {
        $cities = DB::table("sub_categories")
                    ->where("state_id",$request->state_id)
                    ->lists("name","id");
        return response()->json($cities);
    }
}
