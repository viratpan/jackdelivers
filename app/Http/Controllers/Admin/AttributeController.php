<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Attributes;
use App\Master;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class AttributeController extends Controller
{
    function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id=null)
    {
         $stateData =Attributes::find($id);
          $data=DB::table('attributes')
                ->leftJoin('masters','masters.id','=','attributes.masters_id')
                ->select('attributes.*','masters.name as master_name')
                ->get();
         $master = Master::all();
        return view('admin.attribute.index', compact('data','stateData','master'));
    }

    

    /**
     * Store a newly created resource in storage. 
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id=null)
    {
        $request->validate([
            'name' => ['required', 'string','unique:attributes'],
            'masters_id' => ['required', 'string'],
        ]);
        $data =new Attributes;
            $data->name = $request->name;
             $data->masters_id = $request->masters_id;  
        $data->save();
        //die($password);
        return redirect()->route('admin.attribute')->with('success','Attribute created successfully.');
    }


   
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => ['required', 'string'],
            'masters_id' => ['required', 'string'],
        ]);

        $data = Attributes::find($id);
            $data->name = request('name');
            $data->masters_id = $request->masters_id;       
            $data->save();

        return redirect()->route('admin.attribute')
                        ->with('success',$data->name.'  updated successfully');
    }

    
    public function destroy($id)
    {
         Attributes::find($id)->delete();   
        //$Lco = Lco::withTrashed()->get();
           
        return redirect()->route('admin.attribute')
                        ->with('success','Attribute deleted successfully');
    }
}
