<?php

namespace App\Http\Controllers\Admin;

use App\Vendor;
use App\Master;
use App\Product;
use App\Product_price;
use App\ProductImages;

use App\Attributes;
use App\Categorie;
use App\Sub_categories;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Auth;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Input;
use Cviebrock\EloquentSluggable\Services\SlugService;

class ProductController extends Controller
{
    function __construct()
    {
      $this->middleware('auth:admin');
    }

    
    public function check_slug(Request $request)
    {   
        $slug = SlugService::createSlug(Product::class, 'slug', $request->name);
        return response()->json(['slug' => $slug]);
    }
    
    public function create()
    {
        $master=Master::all();
        $category=Categorie::all();        
        $sub_category=Sub_categories::all(); 
         $attributes = Attributes::all(); 
         $vendor= DB::table('vendors')
                 ->leftJoin('masters','masters.id','=','vendors.masters_id')                 
                 ->leftJoin('categories','categories.id','=','vendors.category_id')                  
                 ->select('vendors.id','vendors.name','vendors.company_name','categories.name as categories_name','masters.name as masters_name')
                ->orderby('vendors.name')
                ->where('verify','!=',0)
                ->get();
        return view('admin.item.add-item', compact('master','category','sub_category','attributes','vendor'));
    }
    public function store(Request $request)
    {
       
        $request->validate([
            'name' => ['required', 'string'],
            'slug' => ['required', 'string', 'max:255', 'unique:products'],            
            'sub_categry_id' => ['required'],
            
        ]);
        //$user = auth()->user(); 
        $data =new Product;
            $data->masters_id = $request->masters_id;
            $data->categry_id = $request->categry_id;
            $data->sub_categry_id = $request->sub_categry_id;
            $data->name = $request->name;
            $data->slug = $request->slug;
            $data->meta_title = $request->meta_title;
            $data->meta_keywords = $request->meta_keywords;
            $data->meta_description = $request->meta_description;
            $data->description = $request->description;  

            $data->image = $request->image;
             $data->image2 = $request->image2;

                $data->aprice = $request->aprice;
                $data->type = $request->type;
                $data->costoftwo = $request->costoftwo;
                $data->attrubute_id = $request->attrubute_id;
            $data->vendor_id = $request->vendor_id;
                $data->verify = 1;
                $data->verified_at = now();
            $data->save();
           
       return redirect()->route('admin.item-list')->with('success',' Item created successfully.');
    }
    
     public function edit($id)
    {          
        $data=DB::table('products')
                ->leftJoin('masters','masters.id','=','products.masters_id') 
                ->leftJoin('categories','categories.id','=','products.categry_id')
                ->leftJoin('sub_categories','sub_categories.id','=','products.sub_categry_id')
                 ->leftJoin('attributes','attributes.id','=','products.attrubute_id')
                 ->where('products.id',$id)
                        
                ->select('products.*','masters.name as masters_name','categories.name as categories_name','sub_categories.name as sub_categories_name','attributes.name as attributes_name')
                ->first();
       if(!$data){
            return redirect()->route('admin.item-list')->with('error',' Item Not Match.');
       }
        //print_r($data);
        $category=Categorie::all();        
        $sub_category=Sub_categories::all(); 
         $attributes = Attributes::all(); 
         $vendor=Vendor::all();   
         $master=Master::all();        
        return view('admin.item.edit-item', compact('data','category','sub_category','attributes','vendor','master'));

    }
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => ['required', 'string'],          
            
        ]);

         $data = Product::find($id);
            $data->masters_id = $request->masters_id;
            $data->categry_id = $request->categry_id;
            $data->sub_categry_id = $request->sub_categry_id;
            $data->name = $request->name;
            //$data->slug = $request->slug;
            $data->meta_title = $request->meta_title;
            $data->meta_keywords = $request->meta_keywords;
            $data->meta_description = $request->meta_description;
            $data->description = $request->description;

            $data->image = $request->image;
            $data->image2 = $request->image2;
            
              $data->aprice = $request->aprice;
               $data->type = $request->type;
               $data->costoftwo = $request->costoftwo;
               $data->attrubute_id = $request->attrubute_id;
            $data->save();    

         return redirect()->route('admin.item-list')->with('success',' Item Updated successfully.');
    }
    public function status($id,$status)
    {
         $data = Product::find($id);
            $data->status = $status;
        $data->save();
        return redirect()->route('admin.item-list')->with('success',' Item Status Updated successfully.');
    }
    public function featured($id,$status)
    {
         $data = Product::find($id);
            $data->featured = $status;
        $data->save();
        return redirect()->route('admin.item-list')->with('success',' Item featured Updated successfully.');
    }
    public function verify($id,$status)
    {
         $data = Product::find($id);
            $data->verify = $status;
            $data->verified_at = now();
        $data->save();
        return redirect()->route('admin.item-list')->with('success',' Item Verification Updated successfully.');
    }
    public function manage(Request $request,$id)
    {
        $user = auth()->user();
         $data=DB::table('products')
                ->leftJoin('masters','masters.id','=','products.masters_id')
                ->leftJoin('categories','categories.id','=','products.categry_id')
                ->leftJoin('sub_categories','sub_categories.id','=','products.sub_categry_id')
                ->leftJoin('vendors','vendors.id','=','products.vendor_id')
                ->leftJoin('attributes','attributes.id','=','products.attrubute_id')
                ->where('products.id',$id)
               // ->where('vendor_id',$user->id)
                ->select('products.id as productsId','masters.name as masters_name','products.*','categories.name as categories_name','sub_categories.name as sub_categories_name','vendors.name as vendor_name','vendors.email as vendor_email','attributes.name as attributes_name')
                ->first();
        $prices=DB::table('product_prices')                
                ->where('product_prices.product_id',$id)
                ->select('product_prices.*')
                ->get();

        $pricesData=DB::table('product_prices')                
                ->where('product_prices.product_id',$id)
                ->where('product_prices.id',$request->pid)
                ->select('product_prices.*')
                ->first();
        $attributes = Attributes::all();
        //print_r($pricesData);
        return view('admin.item.manage-list2', compact('data','prices','attributes','pricesData'));
    }
    public function manageStore(Request $request, $id)
    { 
        //echo"<pre>",print_r($request->all()),"</pre>";
       $request->validate([
            'qty' => ['required', 'string'],
            'price' => ['required'],  'cost' => ['required'],
            'discount' => ['required'],
            //'attrubute_id' => ['required'],

        ]);

         if($request->id==null){
            $data =new Product_price;
             $data->qty = $request->qty;

           // $data->attrubute_id = $request->attrubute_id;
            $data->price = $request->price;
            $data->cost = $request->cost;
            $data->discount = $request->discount;
            $data->product_id = $id;
            $data->save();

        return redirect()->route('admin.item.manage',$id)->with('success',$data->name.' Pcice created successfully.');
         }else{
            $data = Product_price::find($request->id);
             $data->qty = $request->qty;
           // $data->attrubute_id = $request->attrubute_id;
            $data->price = $request->price;
            $data->cost = $request->cost;
            $data->discount = $request->discount;
            $data->product_id = $id;
            $data->save();

        return redirect()->route('admin.item.manage',$id)->with('success',$data->name.' Pcice Updated successfully.');
         }


    }
     public function create2(Request $request)
    {  // echo "<pre>",print_r($request->all()),"</pre>";
        $vendor=$request->vendor_id;
        $item=$request->item;
       
        foreach ($item as $key => $value) { 
            $product =new Product;
            $product->item_id =$value;
            $product->vendor_id = $vendor;
            $product->status = 1;
            if($product->save()){    //echo " productId - ";
                 $productId=$product->id;
                $data =new Product_price;               
                $data->qty = $request->qty[$product->item_id];
               // $data->attrubute_id = $request->attrubute_id;
                $data->price = $request->price[$product->item_id];
                $data->cost = $request->aprice[$product->item_id];
                $data->discount = $request->discount[$product->item_id];
                $data->product_id = $productId;
                $data->save(); //echo " | productPriceId - ";
                 $productPriceId=$data->id;
                //echo "<pre>",print_r($data),"</pre>";
            }
           // echo "<hr>";
            
        }
       /* $category=Categorie::all();
        $vendor=Vendor::all();
        $sub_category=Sub_categories::all();*/
         return redirect()->route('admin.product')->with('success',' Product Maped Successfully.');
    }
}
