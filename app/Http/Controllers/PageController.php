<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    function __construct()
    {
    	
    }

    function about()
    {
    	return view('pages.about');
    }
    function contact()
    {
    	return view('pages.contact');
    }
    function terms()
    {
    	return view('pages.terms');
    }
    function refund()
    {
    	return view('pages.refund');
    }

    function privacy()
    {
    	return view('pages.privacy');
    }

    function fraud()
    {
    	return view('pages.fraud');
    }
    public function rider()
    {
        return view('pages.rider');
    }
}
