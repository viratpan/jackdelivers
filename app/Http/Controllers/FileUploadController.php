<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Image;

class FileUploadController extends Controller
{
    public function upload(Request $request,$folder)
    {   
        $fileType = request()->file->getClientOriginalExtension();

        /*if ($fileType != 'pdf' ) {
           
                if($folder=='item' || $folder=='vendor'){
                     request()->validate(['file' => 'required|mimes:jpeg,png,jpg,gif,svg,webp|max:100',]);
                }else{
                     request()->validate(['file' => 'required|mimes:jpeg,png,jpg,gif,svg,webp|max:200',]);
                }
                
            
        }else{
            request()->validate([
                'file' => 'required|mimes:pdf|max:512',
            ]);
        }*/
        request()->validate([
            'file' => 'required|mimes:jpeg,png,jpg,gif,svg,webp,pdf|max:2048'
		]);
        $path='images/files';
        if($folder){
            $path='images/'.$folder;
        }
        $imgext=array('jpg','png','gif','webp');

        $fileName = date('dmY').'-'.time().Str::random(6).'.'.request()->file->getClientOriginalExtension();
       $ext=request()->file->getClientOriginalExtension();
        if(request()->file->move(public_path($path), $fileName)){
             /*thumb*/
             /*if(in_array($ext, $imgext)){
                $image = $path.'/'.$fileName;
                $img = Image::make($image);
                $img->resize(250, 250, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(public_path($path).'/thumb/'.$fileName);
             }*/
      // $this->cretethumb($mpath,$save,'288','344');
          /*thumb*/
        	/*thumb*/
            return response()->json(['message'=>'File Uploaded',
                                'path' => $path.'/'.$fileName,
                                'pathThumb' => $path.'/thumb/'.$fileName,
                                'ext'=>$ext,
                                'filetype'=>$fileType
                                 ]);
        }else{
        	return response()->json(['message'=>'File Not Uploaded. Pleasec Try Again!!!.',
        						'path' => null
        						 ]);
        }
    }
    public function uploadImage(Request $request,$folder=null)
    {   
           
        request()->validate([
            'file' => 'required|mimes:jpeg,png,jpg,gif,svg,webp,pdf|max:2048',
        ]);
        $path='images/files';
        if($folder){
            $path='images/'.$folder;
        }
        $imgext=array('jpg','png','gif','webp');

        $fileName = date('dmY').'-'.time().Str::random(6).'.'.request()->file->getClientOriginalExtension();
       $ext=request()->file->getClientOriginalExtension();
        if(request()->file->move(public_path($path), $fileName)){
             /*thumb*/
             /*if(in_array($ext, $imgext)){
                $image = $path.'/'.$fileName;
                $img = Image::make($image);
                $img->resize(250, 250, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(public_path($path).'/thumb/'.$fileName);
             }*/
      // $this->cretethumb($mpath,$save,'288','344');
          /*thumb*/
            /*thumb*/
            return response()->json(['message'=>'File Uploaded',
                                'path' => $path.'/'.$fileName,
                                'pathThumb' => $path.'/thumb/'.$fileName,
                                'ext'=>$ext
                                 ]);
        }else{
            return response()->json(['message'=>'File Not Uploaded. Pleasec Try Again!!!.',
                                'path' => null
                                 ]);
        }
    }
    public function GenrateFileName()
    {
    	$name=date('dmY').'-'.time().Str::random(6);
    }
    public function FileExitsORNOt($folder,$extension)
    {
    	$name=$this->GenrateFileName.$extension;
    	    $path = public_path('files/'.$name);
		    $isExists = file_exists($path);
		    if($isExits){
		    	echo "Exits";
		    }else{
		    	echo "Not";
		    }
    }
    /////////////////////////////////////////////////////////////////////////
    function cretethumb($path,$save,$width,$height)
    { 
       $info=getimagesize($path);
              //print_r($info);
             // echo $info['mime'];
        $size=array($info[0],$info[1]);
            if($info['mime']=='image/jpg')
            {
                $src=imagecreatefromjpeg($path);
            }
            elseif($info['mime']=='image/jpeg')
            {
                $src=imagecreatefromjpeg($path);
            }
            elseif($info['mime']=='image/png')
            {
                $src=imagecreatefrompng($path);
            }
            elseif($info['mime']=='image/gif')
            {
                $src=imagecreatefromgif($path);
            }
            else
            {
                return false;
            }
            
             $thumb = imagecreatetruecolor($width,$height);
            
            $src_aspect=$size[0]/$size[1];
            $thumb_aspect=$width/$height;
            
            if($src_aspect < $thumb_aspect)
            {
                /////narrower
                $scale = $width/ $size[0];
                $new_size=array($width,$width/$src_aspect);
                $src_pos=array(0,($size[1]*$scale - $height)/$scale/2 );
            }
            elseif($src_aspect > $thumb_aspect)
            {
                /////narrower
                $scale = $height/ $size[1];
                $new_size=array($height*$src_aspect,$height);
                $src_pos=array(($size[0]*$scale - $width)/$scale/2,0 );
            }
            else
            {
                $new_size=array($width,$height);
                $src_pos=array(0,0 );
            }
            
            $new_size[0]=max($new_size[0],1);
            $new_size[1]=max($new_size[1],1);
            
            imagecopyresampled($thumb,$src,0,0,$src_pos[0],$src_pos[1],$new_size[0],$new_size[1],$size[0],$size[1]);
            if($save ===false)
            {
                return imagepng($thumb);
            }
            else{
                return imagepng($thumb,$save); 
            }
    }
    
}
