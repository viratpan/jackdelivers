<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;

use App\Vendor;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;

class SearchController extends Controller
{
	public $lat;
    public $long;
    public $radius=15;

    public function __construct()
    {
        if(Auth::check()==true){
           // echo"in BD";
        }else{
           // echo"in Session";
            $this->cart = Session::get('cart');  
            $this->wishlist = Session::get('wishlist');   
           // var_dump($this->cart);       
        }
        
        //$this->middleware('auth');
    }

    function index(Request $request){
         $urlPrevious = url()->previous();   
         $urlBase = url()->to('/mc/');
          $master_slug=str_replace($urlBase,'',$urlPrevious);//echo "<br>";
         
          
            if(isset($request->master_id)) {
                $master=DB::table('masters')->where('id',$request->master_id)->first();
            }elseif($master_slug){
                $master=DB::table('masters')->where('slug',str_replace('/','',$master_slug))->first();
            }
            if($master==null){
                //echo "<br>not found";die();
                return redirect()->route('home')->with('Error','Vertical Not Founded.');
            }        
            
            $request=$request->all();
        return view('common.search',compact('urlPrevious','master','request'));
    }
    public function this(Request $request, $type=null)
    {
       $search = $request->get('term');
       $type = $request->get('type');
       $master_id = $request->get('master_id');
       
       
        $vendor=Vendor:://DB::table('vendors')
                /*->selectRaw("vendors.*, cities.name as city_name,
                     ( 6371 * acos( cos( radians(?) ) *
                       cos( radians( latitude ) )
                       * cos( radians( longitude ) - radians(?)
                       ) + sin( radians(?) ) *
                       sin( radians( latitude ) ) )
                     ) AS distance", [$lat,$lng,$lat])
                ->having("distance", "<", $radius)
                ->orderBy("distance",'asc')*/
                leftJoin('cities','cities.id','vendors.city')
                ->leftJoin('vendor_ratings', 'vendor_ratings.vendor_id', '=', 'vendors.id')
                ->where('vendors.verify',1)
                ->where('vendors.active',1)
                ->where('vendors.masters_id',$master_id)
                ->Where('vendors.company_name', 'like', '%' . $search . '%') 
                //->orWhere('vendors.company_name', 'like', '%' . $search . '%')                 
                ->select('vendors.id','vendors.company_name','vendors.slug','vendors.image','vendors.company_name','vendors.costoftwo','cities.name as city_name')
                //->selectRaw('AVG(vendor_ratings.rating) AS average_rating')
                //->groupBy('vendors.id')
                ->get();
        
        /*///////////////////////////////*/
         
         $titem =DB::table('products')        
             ->leftJoin('vendors','vendors.id','=','products.vendor_id')
                ->leftJoin('cities','cities.id','vendors.city')
             ->leftJoin('masters','masters.id','=','vendors.masters_id')
             ->leftJoin('categories','categories.id','=','products.categry_id')
                ->leftJoin('sub_categories','sub_categories.id','=','products.sub_categry_id')
              ->leftJoin('attributes','attributes.id','=','products.attrubute_id')
              ->Join('product_prices','product_prices.product_id','=','products.id')
              /*->selectRaw("products.id as productsId,products.created_at as pcreated_at,products.*,vendors.company_name as vendor_company_name,masters.name as master_name,attributes.name as attributes_name,product_prices.id as product_price_id,product_prices.qty as product_qty,product_prices.cost as product_cost,product_prices.discount as product_discount,product_prices.price as product_price,vendors.latitude,vendors.longitude,
                     ( 6371 * acos( cos( radians(?) ) *
                       cos( radians( latitude ) )
                       * cos( radians( longitude ) - radians(?)
                       ) + sin( radians(?) ) *
                       sin( radians( latitude ) ) )
                     ) AS distance", [$lat,$lng,$lat])
                ->having("distance", "<", $radius)
                ->orderBy("distance",'asc')*/
                    ->where('vendors.masters_id',$master_id)
                    //->where('product_prices.price ','>=',0)
                    ->where('products.name', 'like', '%' . $search . '%')
                    ->orwhere('vendors.company_name', 'like', '%' . $search . '%')
               
                ->select('products.id as productsId','products.created_at as pcreated_at','products.*','categories.name as categories_name','sub_categories.name as sub_categories_name',
                    'attributes.name as attributes_name','product_prices.id as product_price_id',
                'product_prices.qty as product_qty','product_prices.cost as product_cost',
                'product_prices.discount as product_discount','product_prices.price as product_price',
                'vendors.company_name as vendor_name','vendors.id as vendorId','vendors.image as vendorImage','vendors.costoftwo as vendorCostoftwo','cities.name as city_name','vendors.slug as vendorSlug'
                //DB::raw('CASE WHEN products.id=1 THEN 1 ELSE 0 END AS in_cart')
                 )
                //->orderBy('productsId')
                ->orderBy('product_price')//->groupBy('vendorId','productsId')
                ->get();
                $vitem= $titem->groupBy('vendorId')->toArray();
                $temp=collect($vitem);
                $item= $titem->groupBy('productsId')->toArray();

        $ivendor=null;
        if($type==2){
            $ivendor= $titem->groupBy('vendorId')->toArray();
        }

       return response()->json(array('vendor'=>$vendor,'vendorCount'=>count($vendor),'items'=>$item,'ivendor'=>$ivendor,'itemsCount'=>count($item),'request'=>$request->all()));
    }




    function comman(Request $request, $masters_id){
        $search = $request->get('term');
        $data=array();
        /* Vendor Search*/
        $vendor=array();
        $vdata=DB::table('vendors')
            ->leftJoin('masters','masters.id','=','vendors.masters_id')
            ->where('masters_id',$masters_id)
            ->where('vendors.verify',1)
            ->where('vendors.active',1)
            ->where('vendors.name', 'like', '%' . $search . '%')
            ->orWhere('vendors.company_name', 'like', '%' . $search . '%')
            ->select('vendors.id','vendors.name','vendors.image','vendors.company_name','masters.slug as master_slug')
            ->get();
        foreach ($vdata as $key => $value) {
        array_push($data,['label'=>$value->company_name,'value'=>$value->company_name,'type'=>1,
            'url'=>route('vendor',['type' => $value->master_slug,'id' => $value->id, 'slug' => Str::slug($value->company_name)]),
            'title'=>'Vendor','image'=>url()->to($value->image)]);
        }
        /*Items Search*/
        $items=array();   $dataitems=array();
         $idata=DB::table('products')
             //->leftJoin('vendors','vendors.id','=','products.vendor_id')
             ->leftJoin('masters','masters.id','=','products.masters_id')
            ->where('products.masters_id',$masters_id)
           // ->where('products.verify',1)
            // ->where('vendors.verify',1)
            // ->where('vendors.active',1)
            ->where('products.name', 'like', '%' . $search . '%')
            ->orwhere('products.slug', 'like', '%' . $search . '%')
           // ->select('vendors.id','products.name','products.slug','vendors.company_name','masters.slug as master_slug')
             ->select('products.name','products.image','masters.slug as master_slug')
            //->groupby('products.name', 'ASC')
            ->get();
        foreach ($idata as $key => $value) {           
            if(Arr::exists($dataitems,Str::slug($value->name))==false){
                array_push($dataitems,Str::slug($value->name)); //echo "pushed <br>"; 
                array_push($data,['label'=>$value->name,'value'=>$value->name,'type'=>2,             
                'url'=>route('master',['type' => 2,'search' => $value->name, 'slug' => $value->master_slug ]),
                'title'=>'Product','image'=>url()->to($value->image)]);
            }
          // echo "<hr>";
        }
        return response()->json($data);
    }
    public function vendor(Request $request, $masters_id)
    {
        $search = $request->get('term');
        $data=array();
        $vdata=DB::table('vendors')
            ->leftJoin('masters','masters.id','=','vendors.masters_id')
            ->where('masters_id',$masters_id)
            ->where('vendors.verify',1)
            ->where('vendors.active',1)
            ->where('vendors.name', 'like', '%' . $search . '%')
            ->orWhere('vendors.company_name', 'like', '%' . $search . '%')
            ->select('vendors.id','vendors.name','vendors.company_name','masters.slug as master_slug')
            ->get();
        foreach ($vdata as $key => $value) {
        array_push($data,['label'=>$value->company_name.' ( '.$value->name.' )','value'=>$value->company_name.' ( '.$value->name.' )',
            'url'=>route('vendor',['type' => $value->master_slug,'id' => $value->id, 'slug' => Str::slug($value->company_name)]),
            'title'=>'Vendor',]);
        }
        return response()->json($data);
    }
    public function item(Request $request, $masters_id)
    {
        $search = $request->get('term');
        $data=array();  $dataitems=array();
        $idata=DB::table('products')
             //->leftJoin('vendors','vendors.id','=','products.vendor_id')
             ->leftJoin('masters','masters.id','=','products.masters_id')
            ->where('products.masters_id',$masters_id)
           // ->where('products.verify',1)
            // ->where('vendors.verify',1)
            // ->where('vendors.active',1)
            ->where('products.name', 'like', '%' . $search . '%')
            ->orwhere('products.slug', 'like', '%' . $search . '%')
           // ->select('vendors.id','products.name','products.slug','vendors.company_name','masters.slug as master_slug')
             ->select('products.name','masters.slug as master_slug')
            //->groupby('products.name', 'ASC')
            ->get();
        foreach ($idata as $key => $value) {
           // echo Str::slug($value->name);echo "<br>";
           // var_dump( Arr::exists($dataitems,Str::slug($value->name))); echo "<br>"; 
            if(Arr::exists($dataitems,Str::slug($value->name))==false){
                array_push($dataitems,Str::slug($value->name)); //echo "pushed <br>"; 
                array_push($data,['label'=>$value->name,'value'=>$value->name,              
                'url'=>route('master',['type' => 2,'search' => $value->name, 'slug' => $value->master_slug ]),
                'title'=>'Product',]);

                 // 'url'=>route('vendor',['type' => $value->master_slug,'id' => $value->id, 'slug' => Str::slug($value->company_name),'item'=> $value->slug ]),
            }
          // echo "<hr>";
        }
        return response()->json($data);
        
    }
    /*public function getVendor(Request $request)
    {
    	$latitude  		=       "28.418715";
        $longitude      =       "77.0478997";

        $shops          =       DB::table("shops");

        $shops          =       $shops->select("*", DB::raw("6371 * acos(cos(radians(" . $latitude . "))
                                * cos(radians(latitude)) * cos(radians(longitude) - radians(" . $longitude . "))
                                + sin(radians(" .$latitude. ")) * sin(radians(latitude))) AS distance"));
        $shops          =       $shops->having('distance', '<', 20);
        $shops          =       $shops->orderBy('distance', 'asc');

        $shops          =       $shops->get();
    }*/
    function getVendor(){
        /*/////////////////////////////////////////////////////////////////////////////////*/
        $filters = [
                    'search' => 'search',                   
                    
                  ];
        
        $radius =  $this->radius;
        //$distance = DB::raw("*, ( 6371 * acos( cos( radians($lat) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians($lng) ) + sin( radians($lat) ) * sin( radians( lat ) ) ) ) AS distance");
        $vendor=DB::table('vendors')
                ->leftJoin('cities','cities.id','vendors.city')
                ->where('vendors.verify',1)
                ->where('vendors.active',1)
                ->where('vendors.masters_id',$master->id)

                 ->where(function ($query) use ($request, $filters) {
                        foreach ($filters as $column => $key) {
                            $query->when(Arr::get($request, $key), function ($query, $value) use ($column) {
                                if($column =='search'){
                                    $query->where('vendors.name','LIKE', '%' . $value . '%')
                                    ->orWhere('vendors.company_name','LIKE', '%' . $value . '%');
                                }else{
                                    $query->where($column, $value);
                                }
                                
                            });
                        }
                    })
                ->select('vendors.*','cities.name as city_name')
                ->get();
    }
}
