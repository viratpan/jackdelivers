<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Auth;
use Session;
use App\Order;

use Tzsk\Payu\Facade\Payment;


class PaymentController extends Controller
{
    
    function __construct()
    {
    	$this->middleware('auth');
    }
    
    function index($arr)
    {
    	//var_dump($arr);
    	$temp=explode(',',$arr);
    	//echo "<pre>",print_r($arr),"</pre></hr>";
    	$amount=Order::whereIn('id', $temp)->sum('total_amount');
    	//echo "<pre>",print_r($amount),"</pre></hr>";
    	//$this->payuMoney($amount);
    	$data = [
          'txnid' => strtoupper(Str::random(10)), # Transaction ID.
          'amount' => $amount,
          'productinfo' => "Order Information",
          'firstname' => Auth::user()->name, # Payee Name.
          'email' =>  Auth::user()->email, # Payee Email Address.
          'phone' =>  Auth::user()->mobile, # Payee Phone Number.

        ];
		//echo "<pre>",print_r($data),"</pre></hr>";
  		 return Payment::make($data, function($then) {
      		$then->redirectTo('payment/status');
      		});
    }

    function payuMoney($amount){
    	$data = [
          'txnid' => strtoupper(Str::random(10)), # Transaction ID.
          'amount' => $amount,
          'productinfo' => "Order Information",
          'firstname' => Auth::user()->name, # Payee Name.
          'email' =>  Auth::user()->email, # Payee Email Address.
          'phone' =>  Auth::user()->mobile, # Payee Phone Number.

        ];

  		 return Payment::make($data, function($then) {
      		$then->redirectTo('payment/status');
      		});
    }
    public function status($value='')
    { //die('pay status');
       $payment = Payment::capture($value);
        $payment->getData(); # Get the full response from Gateway.
        $payment->isCaptured(); # Is the payment captured or some internal failure occured.
        $payment->mihpayid; # Your Local Transaction ID.
        $payment->get('udf1');
      $payment->transaction_id; # Your Local Transaction ID.
        // $payment->transaction_id; # Your Local Transaction ID.
        $payment->payment_id; # PayU Payment ID.
        $payment->total_amount; # Get Tototal Amount Deducted.
        $payment->isCaptured(); # Returns boolean - true / false
          //echo "<pre>",print_r($payment),"</pre>";die;
      $payment->save();
      

        echo "<pre>",print_r($payment),"</pre>";
    }
}
