<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
use Illuminate\Support\Facades\DB;


class CartController extends Controller
{   
    public function index()
    {    
        $tcart = Session::get('cart');
        //echo "item: <pre>",print_r($tcart),"</pre></hr>";     
        $data=$this->getCartData();        
        //echo "item: <pre>",print_r($temp),"</pre></hr>";die();
        $count=$data['count'];
        $item=$data['item'];
         // echo "item: <pre>",print_r($item),"</pre></hr>";die();
        return view('cart',compact('count','item'));
    }
    public function add(Request $request)
    {
    	//	echo "Request: <pre>",print_r($request->all()),"</pre>";
        $status=''; $msg='';
    	$cart=array();  $count=1;
    	if(isset($request->qty)){	$count=$request->qty;} 
    	/*if(Auth::check()==true){
    		
    	}else{*/
    		//echo"in Session"; echo" name ". $request->product_name;
    		$cart = Session::get('cart');
            $cartItem = Session::get('cartItem');    		
            if($count==0){
                $cart[$request->product_price_id]=array();
                Session::pull('cart[$request->product_price_id]');
                Session::forget('cart[$request->product_price_id]');
                Session::save();
            }else{

                if(isset($cart[$request->product_price_id])){                    
                    $count= $cart[$request->product_price_id]['qty'];
                    $count++;
                }
                $item =DB::table('products')
                ->leftJoin('attributes','attributes.id','=','products.attrubute_id')                
                ->leftJoin('product_prices','product_prices.product_id','=','products.id')
                ->where('products.vendor_id',$request->vendor_id)
                ->where('product_prices.id',$request->product_price_id)
                ->select('products.id as productsId','product_prices.id as product_price_id','product_prices.price as product_price_price','products.name','attributes.name as attributes_name',)
                ->orderBy('product_prices.id')
                ->first();
                //echo "Cart: <pre>",print_r($item),"</pre>";//die();
               
                //if($item->productsId==$request->product_id && $item->productsId==$request->product_id){
                if(isset($item->product_price_id) && isset($item->productsId)){
                    $cart[$request->product_price_id] = array(
                    //$item = array(
                        "product_id" => $request->product_id,
                        "product_name" => $request->product_name,
                        "product_price_id" => $request->product_price_id,
                        "vendor_id" => $request->vendor_id,
                        "qty" => $count,
                        "price" => $item->product_price_price,
                    );               
                   $price=number_format($item->product_price_price*$count,2);
                    Session::put('cart', $cart); 
                    $status='success';
                    $msg='Product Added';
                }
                else{
                    $status='error';
                    $msg='Product not Added! Product not match';
                }
               
            }
		    
           		    
            $data = $this->getCartData();

		    //echo "Cart: <pre>",print_r(Session::get('cart')),"</pre>";
		    return response()->json(array('status'=>$status,'item'=>$item,'msg'=>$msg,'count'=>$count,'price'=>$price,'cartCount' => $data['count'],'data'=> $data));    
    	//}
    }
    public function update(Request $request)
    {
        $cart = Session::get('cart');
        $count=0; $price=0;
        if(isset($cart[$request->product_price_id])){ 
            $data=$cart[$request->product_price_id];

            $count= $cart[$request->product_price_id]['qty'];
           $item =DB::table('products')                
                ->leftJoin('product_prices','product_prices.product_id','=','products.id')
                ->where('products.vendor_id',$request->vendor_id)
                ->where('product_prices.id',$request->product_price_id)
                ->select('products.id as productsId','product_prices.id as product_price_id','product_prices.price as product_price_price')
                ->orderBy('product_prices.id')
                ->first();

            $action=$request->action;
                if($action==1){
                    $count+=1;
                }else{
                    $count-=1;
                    if($count<0){$count=0;}
                }
                if($count==0){
                    $cart[$request->product_price_id] = array(
                        "product_id" => null,
                        "product_name" => null,
                        "product_price_id" => null,
                        "vendor_id" => null,
                        "qty" => null,
                        "price" => null,
                    );
                    Session::put('cart', $cart); 
                    //Session::forget('cart',$cart[$request->product_id]);
                }else{
                    $cart[$request->product_price_id] = array(
                        //$item = array(
                        "product_id" => $request->product_id,
                        "product_name" => $request->product_name,
                        "product_price_id" => $request->product_price_id,
                        "vendor_id" => $request->vendor_id,
                        "qty" => $count,
                        "price" => $item->product_price_price,
                    );
                    $price=number_format($item->product_price_price*$count,2);
                    $price=number_format($price,2);
                    Session::put('cart', $cart); 
                }

                
            // echo "Cart: <pre>",print_r($cart[$request->product_id]),"</pre><hr>";
            // echo "Cart: <pre>",print_r(Session::get('cart')),"</pre>";
             
            $status='success';
            $msg='Product Added'; 
            
        }
        else{
                $status='error';
                $msg='Product not Added! Product not match';
            }
        $data = $this->getCartData();
        return response()->json(array('status'=>$status,'msg'=>$msg,'count'=>$count,'price'=>$price,'cartCount' => $data['count'],'data'=> $data));    
           
    }
    public function get()
    {
        $data=$this->getCartData();
        $item=$data['item'];
        
        return response()->json($data);
    }
    
    public function cartCount()
    {
       $data=$this->getCartData();
       $count=$data['count'];
        return $count;
    }





    public function deleteCart(Request $request)
    {
       Session::pull('cart');
        Session::forget('cart');
        Session::save();
        Session::put('cart',[]);
         return redirect()->route('cart')->with('Error','Cart Empty.');
    }
    public function delete(Request $request)
    {
        Session::pull('cart');
        Session::forget('cart');
        Session::save();
        Session::put('cart',[]);
        //echo "Cart: <pre>",print_r(Session::get('cart')),"</pre>";
    }
}
