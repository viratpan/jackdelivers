<?php

namespace App\Http\Controllers\Vendor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\VendorUser;
use App\VendorCategory;
use App\City;
use App\Categorie;
use App\Sub_categories;
use App\Product;
use DB;
use App\Http\Controllers\Input;
use Cviebrock\EloquentSluggable\Services\SlugService;


class AjaxController extends Controller
{
	public function partnerCategoies(Request $request)
    {
        $vendor_users=DB::table('vendor_users')
               // ->join('categories','categories.id','=','vendor_users.product_categry_id')
                ->select('product_categry_id')
                ->where('vendor_users.id','=',$request->name)
                ->first();

        $data = DB::table("categories")
            ->where("id",$vendor_users->product_categry_id)
            ->select("name","id")->get();
        return response()->json($data);
        //return response()->json(['category' => $vendor_users->product_categry_id]);
    }
    public function getVendorOptions(Request $request)
    {
        $data = DB::table("vendors")
            ->leftJoin('masters','masters.id','=','vendors.masters_id')
            ->where("vendors.id",$request->id)
            ->select("masters.name as name","masters.id as id")->get();
        return response()->json($data);
    }
    public function getMastersOptions(Request $request)
    {
        $data = DB::table("categories")
            ->where("masters_id",$request->id)
            ->select("name","id")->get();
        return response()->json($data);
    }
    public function getCategoiesOptions(Request $request)
    {
        $data = DB::table("sub_categories")
            ->where("categry_id",$request->id)
            ->select("name","id")->get();
        return response()->json($data);
    }
    function checkVendoeExits(Request $request){
        $data = DB::table("vendors")
            ->where("email",$request->email)
            ->select("name","id")->get();
        return response()->json(count($data));
    }
    public function getItemOptions(Request $request)
    {
        $vdata=array();
        $vtdata = DB::table("products")
            ->where("vendor_id",$request->vendor)            
            ->select("item_id")->get();
            foreach ($vtdata as $key => $value) {
                array_push($vdata, $value->item_id);
            }
        $data = DB::table("items")
            ->leftJoin('sub_categories','sub_categories.id','=','items.sub_categry_id')
            ->leftJoin('attributes','attributes.id','=','items.attrubute_id')
            ->where("items.categry_id",$request->category)
            ->whereNotIn('items.id',$vdata)
            ->where("sub_categry_id",$request->subcategory)
            ->select("items.name","items.id",'items.aprice','attributes.name as attributes_name')->get();
      
        return response()->json(['items'=>$data,'vdata'=>$vdata,'request'=>$request->all()]);
    }
}
