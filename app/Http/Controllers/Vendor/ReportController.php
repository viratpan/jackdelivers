<?php

namespace App\Http\Controllers\Vendor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Controllers\Input;
use Illuminate\Support\Facades\DB;
use App\Vendor;
use App\User;
use App\Master;

use App\Item;
use App\Product;
use App\City;
use App\Categorie;
use App\Sub_categories;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Illuminate\Support\Arr;

class ReportController extends Controller
{
     function __construct()
    {
        $this->middleware('auth:vendor');

    }
    public function item(Request $request)
    {
        $user = auth()->user();
        $filters = [
                    'search' => 'search',   
                    'products.id' => 'item',
                    'products.categry_id' => 'category',
                    'products.sub_categry_id' => 'subcategory',
                    'products.vendor_id' => 'vendor',
                    'products.status' => 'status',
                    'vendors.city' => 'city',
                  ];
            
        $data=DB::table('products')  
                //->leftJoin('items','items.id','=','products.item_id')
                ->leftJoin('categories','categories.id','=','products.categry_id')
                ->leftJoin('sub_categories','sub_categories.id','=','products.sub_categry_id')
                ->leftJoin('vendors','vendors.id','=','products.vendor_id')
                 ->leftJoin('masters','masters.id','=','products.masters_id')
                 ->leftJoin('attributes','attributes.id','=','products.attrubute_id')
                ->leftJoin('cities','cities.id','vendors.city')
                ->where('products.vendor_id',$user->id)
                ->where(function ($query) use ($request, $filters) {
                        foreach ($filters as $column => $key) {
                            $query->when(Arr::get($request, $key), function ($query, $value) use ($column) {
                                if($column =='search'){
                                    $query->where('products.name','LIKE', '%' . $value . '%')
                                    ->orWhere('products.slug','LIKE', '%' . $value . '%')
                                    ->orWhere('categories.name','LIKE', '%' . $value . '%')
                                    ->orWhere('sub_categories.name','LIKE', '%' . $value . '%')
                                    ->orWhere('cities.name','LIKE', '%' . $value . '%')
                                    ->orWhere('vendors.company_name','LIKE', '%' . $value . '%')
                                    ->orWhere('vendors.address','LIKE', '%' . $value . '%')
                                    ->orWhere('vendors.pin','LIKE', '%' . $value . '%')
                                    ->orWhere('vendors.email','LIKE', '%' . $value . '%');
                                }else{
                                    $query->where($column, $value);
                                }
                            });
                        }
                    })
                ->select('products.id as productsId','products.created_at as pcreated_at','products.*','categories.name as categories_name','sub_categories.name as sub_categories_name',
                    'vendors.name as vendor_name','vendors.email as vendor_email','cities.name as city_name','attributes.name as attributes_name','masters.name as masters_name')
                ->get();
                            
                
        $master=Master::all();
        $vendor=Vendor::all();
         $city=City::all();
          $categry=Categorie::all();
           $subcategry=Sub_categories::all();
         //echo"<pre>",print_r($data),"</pre>";die();
        return view('admin.report.item-list', compact('data','city','categry','subcategry','vendor','request','master'));
    }
}
