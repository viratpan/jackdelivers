<?php

namespace App\Http\Controllers\Vendor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


use App\Http\Controllers\Input;
use Illuminate\Support\Facades\DB;
use App\Vendor;
use App\User;

use App\Order;
use App\OrderItem;

use App\Product;
use App\City;
use App\Categorie;
use App\Sub_categories;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Illuminate\Support\Arr;



class OrderController extends Controller
{
    function __construct()
    {
        $this->middleware('auth:vendor');

    }
     public function index(Request $request)
    {   //die($request->date);
    	$user = auth()->user();
    	$filters = [
                    'search' => 'search',
                    'date' => 'date',
                    'orders.confirmed' => 'confirmed',
                    'orders.status' => 'status',
                  ];
        $data=DB::table('orders')
                ->leftJoin('users','users.id','=','orders.user_id')
                ->leftJoin('addresses','addresses.id','orders.user_address_id')
                ->leftJoin('order_items','order_items.order_rid','=','orders.id')
                
                ->leftJoin('product_prices','product_prices.id','=','order_items.product_price_id')
                ->leftJoin('products','products.id','=','product_prices.product_id')
                ->leftJoin('attributes','attributes.id','=','products.attrubute_id')
                ->leftJoin('riders','riders.id','=','orders.rider_id')
                ->where('orders.vendor_id',$user->id)
                ->where(function ($query) use ($request, $filters) {
                        foreach ($filters as $column => $key) {
                            $query->when(Arr::get($request, $key), function ($query, $value) use ($column) {
                                if($column =='search'){
                                    $query->where('users.name','LIKE', '%' . $value . '%')
                                    ->orWhere('users.email','LIKE', '%' . $value . '%')
                                    ->orWhere('users.email','LIKE', '%' . $value . '%')
                                    ->orWhere('orders.order_id','LIKE', '%' . str_replace('OD', '', $value) . '%');
                                    
                                }elseif($column =='date'){
                                   list($tstart,$tend)=explode('/', str_replace(' - ','/', $value));
                                  //$start=$this->getdate($tstart);
                                  //$end=$this->getdate($tend);
                                   $start=Carbon::parse($tstart);
                                   $end=Carbon::parse($tend);
                                  //@if(\Carbon\Carbon::now()->between(\Carbon\Carbon::parse($value->ontime),\Carbon\Carbon::parse($value->offtime) )==false)
                                  //$query->whereBetween('orders.created_at',[$start,$end]);
                                    
                                }else{
                                    $query->where($column, $value);
                                }
                            });
                        }
                    })

                //->where('orders.vendor_id',$user->id)
                ->select('users.name as user_names','users.email as user_email','users.mobile as user_mobile',
                    'addresses.*','orders.*','orders.id as order_rid','orders.status as orders_status',
                    'products.name as product_name','product_prices.qty as product_qty','attributes.name as attributes_name',
                    'order_items.id as item_id','order_items.status as item_status',
                    'order_items.cost as item_cost','order_items.discount as item_discount','order_items.price as item_price','order_items.qty as item_qty','order_items.amount as item_amount',
                'riders.name as rider_name','riders.email as rider_email','riders.mobile as rider_mobile')
                 
                 ->orderby('order_items.id','desc')
                ->get();
        $product=Product::all();
       
        $vendor=Vendor::all();
        $city=City::all();
        $categry=Categorie::all();
        $subcategry=Sub_categories::all();
       // echo"<pre>",print_r($data),"</pre>";die();
        return view('admin.report.order-list', compact('data','product','city','categry','subcategry','vendor','request'));
    }

    public function status($id,$status)
    {
        $data = OrderItem::find($id);
            $data->status = $status;
        $data->save();
        $order_id=$data->order_rid;
       // $allitems=OrderItem::where('order_rid',$order_id)->select('status')->get();
        Order::where('id',$order_id)->update(['status' => $status]);
        /*if(!in_array(0, $allitems)){
            // echo"<pre>",print_r($allitems),"</pre>";die();
        }*/

        return redirect()->route('vendor.order-list')->with('success',' Order Status Updated successfully.');
    }
    public function statusAll($id,$status,$ostatus)
    {
        $item_arr=array();
        $data= Order::where('id',$id)->first();
         //$data = OrderItem::where('order_rid',$id)->where('status',$ostatus)->select('id')->get();
         if($data){
            DB::beginTransaction();
            try {
                
                $temp = OrderItem::where('order_rid',$id)->update(['status' => $status]);
                Order::where('id',$id)->update(['status' => $status]);
               // $temp = OrderItem::whereIn('id',$data)->update(['status' => $status]);
                //echo"<br>";                      
                // All Good
                DB::commit();
            }catch (\Exception $e) {
                DB::rollback();
                report($e);                
            }
         }
        
        return redirect()->route('vendor.order-list')->with('success',' Order Status Updated successfully.');
    }
}
