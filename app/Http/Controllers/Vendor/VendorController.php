<?php

namespace App\Http\Controllers\Vendor;

use App\Vendor;
use App\Master;
use App\VendorCategory;
use App\City;
use App\Categorie;
use App\Commission;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Auth;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Input;

class VendorController extends Controller
{


    function __construct()
    {
        $this->middleware('auth:vendor');

    }

    public function index(Request $request)
    {
        $user = auth()->user();
       /* if($user->latitude==Null || $user->longitude==Null){
            return redirect()->route('vendor.profile.edit')->with('warning',$user->name.' Please Update Your Profile.');
        }*/
         $vendor=DB::table('vendors')
                ->leftJoin('cities','cities.id','=','vendors.city')
                 ->leftJoin('masters','masters.id','=','vendors.masters_id')
                 ->leftJoin('categories','categories.id','=','vendors.category_id')
                  //->leftJoin('commissions','commissions.id','=','vendors.commission_id')
                ->where('vendors.id',$user->id)
                ->select('vendors.*','masters.slug as master_name','cities.name as city_name','categories.name as categories_name',)
                //'commissions.name as commissions_name','commissions.commission as commissions_on'
                ->first();
        $order=DB::table('orders')
                ->leftJoin('users','users.id','=','orders.user_id')
                ->leftJoin('addresses','addresses.id','orders.user_address_id')
                ->leftJoin('order_items','order_items.order_rid','=','orders.id')
                
                ->leftJoin('product_prices','product_prices.id','=','order_items.product_price_id')
                ->leftJoin('products','products.id','=','product_prices.product_id')
                ->leftJoin('attributes','attributes.id','=','products.attrubute_id')
                ->leftJoin('riders','riders.id','=','orders.rider_id')
                ->where('orders.vendor_id',$user->id)
                ->select('users.name as user_names','users.email as user_email','users.mobile as user_mobile',
                    'addresses.*','orders.*','orders.id as order_rid','orders.status as orders_status',
                    'products.name as product_name','product_prices.qty as product_qty','attributes.name as attributes_name',
                    'order_items.id as item_id','order_items.status as item_status',
                    'order_items.cost as item_cost','order_items.discount as item_discount','order_items.price as item_price','order_items.qty as item_qty','order_items.amount as item_amount',
                'riders.name as rider_name','riders.email as rider_email','riders.mobile as rider_mobile')
                 
                 ->orderby('order_items.id','desc')
                 
                ->get();
        return view('vendors.dashboard', compact('vendor','order'));
    }
    public function profile($value='')
    {
        $user = auth()->user();
        //$productCategory=Categorie::all();
         $vendor=DB::table('vendors')
                ->leftJoin('cities','cities.id','=','vendors.city')
                 ->leftJoin('masters','masters.id','=','vendors.masters_id')
                 ->leftJoin('categories','categories.id','=','vendors.category_id')
                  //->leftJoin('commissions','commissions.id','=','vendors.commission_id')
                ->where('vendors.id',$user->id)
                ->select('vendors.*','masters.name as master_name','cities.name as city_name','categories.name as categories_name',)
                //'commissions.name as commissions_name','commissions.commission as commissions_on'
                ->first();
         //$city=City::all();
        //print_r($vendor);
       return view('vendors.profile.profile', compact('vendor'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $user = auth()->user();
        //$productCategory=Categorie::all();
        $vendor=Vendor::where('id',$user->id)->first();
        $category=Categorie::all();
        $commission=Commission::all();
        $city=City::all();
        $master = Master::all();
        return view('vendors.profile.edit', compact('vendor','city','category','commission','master'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $user = auth()->user();
         $id=$user->id;
        $request->validate([
            'name' => ['required', 'string'],
            'email' => ['required', 'string', 'email', 'max:255', ],
            'mobile' => 'required',

        ]);

         $data = Vendor::find($id);
            $data->email = $request->email;
            $data->name = $request->name;
            $data->company_name = $request->company_name;
            $data->org_type = $request->org_type;
            $data->designation = $request->designation;
            $data->mobile = $request->mobile;
            $data->telephone = $request->telephone;
            $data->city = $request->city;
            $data->address = $request->address;
            $data->pin = $request->pin;
                
                $data->latitude = $request->latitude;
                $data->longitude = $request->longitude;

                $data->masters_id = $request->masters_id;
                $data->category_id = $request->category_id;
                $data->commission_id = $request->commission_id;

                $data->costoftwo = $request->costoftwo;
                $data->ontime = $request->ontime;
                $data->offtime = $request->offtime;
                $data->about = $request->about;

                $data->image = $request->image;
                $data->menu = $request->menu;
                $data->coverimage = $request->coverimage;
                
            $data->aadhar = $request->aadhar;
            $data->aadharupload = $request->aadharupload;
            $data->pan = $request->pan;
            $data->panupload = $request->panupload;
            $data->gstin = $request->gstin;
            $data->gstinupload = $request->gstinupload;
            $data->fssai = $request->fssai;
            $data->fssaiupload = $request->fssaiupload;
            $data->save();


        return redirect()->route('vendor.dashboard')->with('success',' Profile Updated successfully.');

    }
    public function statusStore($status)
    {//echo $status;
      $user = auth()->user();
         $id=$user->id;
        $data = Vendor::find($id);
          //echo"<pre>",print_r($data),"</pre>";die();
       
            $data->open = $status;
        $data->save();        

        return redirect()->route('vendor.dashboard')->with('success','Store Status Updated successfully.');
    }
    
}
