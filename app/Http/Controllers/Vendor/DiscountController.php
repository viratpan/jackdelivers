<?php

namespace App\Http\Controllers\Vendor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;
use App\Product_price;
use App\Vendor;
use App\User;
use App\Master;

use App\City;
use App\Categorie;
use App\Sub_categories;
use App\Http\Controllers\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Illuminate\Support\Arr;

class DiscountController extends Controller
{
    function __construct()
    {
        $this->middleware('auth:vendor');
        
    }

    public function index(Request $request)
    {
        $user = auth()->user();
        $filters = [                    
                    'products.categry_id' => 'category',
                    'products.sub_categry_id' => 'subcategory',                   
                   ];
            
        $data=DB::table('products')  
                //->leftJoin('products','products.id','=','products.item_id')
                ->leftJoin('categories','categories.id','=','products.categry_id')
                ->leftJoin('sub_categories','sub_categories.id','=','products.sub_categry_id')
                ->leftJoin('vendors','vendors.id','=','products.vendor_id')
                 ->leftJoin('masters','masters.id','=','products.masters_id')
                 ->leftJoin('attributes','attributes.id','=','products.attrubute_id')
                ->leftJoin('cities','cities.id','vendors.city')
                 ->leftJoin('product_prices','product_prices.product_id','=','products.id')
                ->where('products.vendor_id',$user->id)
                ->where(function ($query) use ($request, $filters) {
                        foreach ($filters as $column => $key) {
                            $query->when(Arr::get($request, $key), function ($query, $value) use ($column) {                               
                                    $query->where($column, $value);
                                
                            });
                        }
                    })
                ->select('products.id as productsId','products.created_at as pcreated_at','products.id','products.name',
                	'categories.name as categories_name','sub_categories.name as sub_categories_name',
                    'vendors.name as vendor_name','vendors.email as vendor_email',
                    'cities.name as city_name','attributes.name as attributes_name','masters.name as masters_name',
                'product_prices.id as product_price_id',
                'product_prices.qty as product_qty','product_prices.cost as product_cost',
                'product_prices.discount as product_discount','product_prices.price as product_price',)
                ->orderBy('product_prices.id')
                ->get();
        //echo"<pre>",print_r($data),"</pre>";die();                    
                
        $master=Master::all();
        $vendor=Vendor::all();
         $city=City::all();
          $categry=Categorie::all();
           $subcategry=Sub_categories::all();
         //echo"<pre>",print_r($data),"</pre>";die();
        return view('admin.report.item-discount', compact('data','city','categry','subcategry','vendor','request','master'));
    }
    public function update(Request $request)
    {
    	//echo"<pre>",print_r($request->all()),"</pre>";//die();
    	$discount=$request->discount;
    	$items=$request->product;
    	if(isset($items)){
    		//echo $count($items);
    		foreach ($items as $key => $value) {
				//echo $value;echo "<hr>";
				$data = Product_price::find($value);
		            $cost=$data->cost;//echo "<br>";
		            //echo ($cost*$discount)/100;echo "<br>";
		            $price=$cost-($cost*$discount)/100;   //echo "<br>";        
		            //$data->cost = $request->cost;
		            $data->discount = $discount;
		            $data->price = $price;
		           
		        $data->save();
		        
			}
    	}
    	
    	 return redirect()->route('vendor.item-discount')->with('success','Product`s Discount Updated successfully.');
    	
    }
}
