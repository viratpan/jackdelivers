<?php

namespace App\Http\Controllers\Vendor;

use App\Vendor;
use App\Master;
use App\Product;
use App\Product_price;
use App\Attributes;
use App\Categorie;
use App\Sub_categories;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Auth;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Input;
use Cviebrock\EloquentSluggable\Services\SlugService;

class ProductController extends Controller
{
     function __construct()
    {
        $this->middleware('auth:vendor');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function create()
    {
        $user = auth()->user();
        $master=Master::where('id',$user->masters_id)->first();
        // echo"<pre>",print_r($master),"</pre>";die;
        $category=Categorie::where('masters_id',$user->masters_id)->get();    
        $sub_category=Sub_categories::all(); 
         $attributes = Attributes::all(); 
         $vendor=Vendor::all();      
        return view('admin.item.add-item', compact('category','sub_category','attributes','vendor','master'));
    }
    
        
    public function store(Request $request)
    {  // echo "item store";    die();
        $user = auth()->user();
        $request->validate([
            'name' => ['required', 'string'],
            'slug' => ['required', 'string', 'max:255', 'unique:products'],            
            'sub_categry_id' => ['required'],
            
        ]);
        //$user = auth()->user(); 
        $data =new Product;
            $data->masters_id = $user->masters_id;
            $data->categry_id = $request->categry_id;
            $data->sub_categry_id = $request->sub_categry_id;
            $data->name = $request->name;
            $data->slug = $request->slug;
            $data->meta_title = $request->meta_title;
            $data->meta_keywords = $request->meta_keywords;
            $data->meta_description = $request->meta_description;
            $data->description = $request->description;
            
             $data->image = $request->image;
             $data->image2 = $request->image2;

              $data->aprice = $request->aprice;
                $data->type = $request->type;
                $data->costoftwo = $request->costoftwo;
                $data->attrubute_id = $request->attrubute_id;
            $data->vendor_id = $user->id;
            $data->save();
            /*if($data->id){
                if($request->image){
                    $data2 =new ProductImages;
                    $data2->image = $request->image;
                    $data2->search_image = $data->name;
                    $data2->product_id=$data->id;
                    $data2->save();
                }
                if($request->image2){
                    $data2 =new ProductImages;
                    $data2->image = $request->image2;
                    $data2->search_image = $data->name;
                    $data2->product_id=$data->id;
                    $data2->save();
                }
                
            }*/
       return redirect()->route('vendor.item-list')->with('success',' Item created successfully.');
    }
     public function edit($id)
    {   
        $user = auth()->user();     
        $data=DB::table('products')  
                ->leftJoin('categories','categories.id','=','products.categry_id')
                ->leftJoin('sub_categories','sub_categories.id','=','products.sub_categry_id')
                 ->leftJoin('attributes','attributes.id','=','products.attrubute_id')
                 ->where('products.id',$id)
                  ->where('products.vendor_id',$user->id)       
                ->select('products.*','categories.name as categories_name','sub_categories.name as sub_categories_name','attributes.name as attributes_name')
                ->first();
       if(!$data){
            return redirect()->route('vendor.item-list')->with('error',' Item Not Match.');
       }
         $master=Master::where('id',$user->masters_id)->first();
        $category=Categorie::where('masters_id',$user->masters_id)->get();        
        $sub_category=Sub_categories::all(); 
         $attributes = Attributes::all(); 
         $vendor=Vendor::all();           
        return view('admin.item.edit-item', compact('data','category','sub_category','attributes','vendor','master'));

    }
    public function update(Request $request, $id)
    {   //echo "item update";    die();
        $request->validate([
            'name' => ['required', 'string'],          
            
        ]);

         $data = Product::find($id);
         if(!$data){
            return redirect()->route('vendor.item-list')->with('error',' Item Not Match.');
         }
            $data->categry_id = $request->categry_id;
            $data->sub_categry_id = $request->sub_categry_id;
            $data->name = $request->name;
            //$data->slug = $request->slug;
            $data->meta_title = $request->meta_title;
            $data->meta_keywords = $request->meta_keywords;
            $data->meta_description = $request->meta_description;
            $data->description = $request->description;

            $data->image = $request->image;
            $data->image2 = $request->image2;
            
              $data->aprice = $request->aprice;
               $data->type = $request->type;
               $data->costoftwo = $request->costoftwo;
               $data->attrubute_id = $request->attrubute_id;
            $data->save();    

         return redirect()->route('vendor.item-list')->with('success',' Item Updated successfully.');
    }
/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
    public function status($id,$status)
    {echo "product status";die();
        $user = auth()->user();
         $data = Product::find($id);
         if($data->vendor_id==$user->id){
            $data->status = $status;
            $data->save();
          return redirect()->route('vendor.item-list')->with('success',' Item Status Updated successfully.');
         }

       return redirect()->route('vendor.item-list')->with('warning',' Item Not Match.');
    }
     public function featured($id,$status)
    {
        $user = auth()->user();
         $data = Product::find($id);
         if($data->vendor_id==$user->id){
            $data->featured = $status;
            $data->save();
            return redirect()->route('vendor.item-list')->with('success',' Item featured Updated successfully.');
         }
         return redirect()->route('vendor.item-list')->with('warning',' Item Not Match.');
    }
    public function verify($id,$status)
    {
        $user = auth()->user();
         $data = Product::find($id);
         if($data->vendor_id==$user->id){
            $data->verify = $status;
            $data->verified_at = now();
            //$data->save();
            return redirect()->route('vendor.item-list')->with('success',' Item Verification Updated successfully.');
         }
         return redirect()->route('vendor.item-list')->with('warning',' Item Not Match.');
    
    }
    public function manage(Request $request,$id)
    {
        $user = auth()->user();
         $data=DB::table('products')
                //->leftJoin('items','items.id','=','products.item_id')
                ->leftJoin('categories','categories.id','=','products.categry_id')
                ->leftJoin('sub_categories','sub_categories.id','=','products.sub_categry_id')
                ->leftJoin('vendors','vendors.id','=','products.vendor_id')
                ->leftJoin('attributes','attributes.id','=','products.attrubute_id')
                ->where('products.id',$id)
                ->where('vendor_id',$user->id)
                ->select('products.id as productsId','products.*','categories.name as categories_name','sub_categories.name as sub_categories_name','vendors.name as vendor_name','vendors.email as vendor_email','attributes.name as attributes_name')
                ->first();
        if(!isset($data->productsId)){
             return redirect()->route('vendor.item-list')->with('Error','this Product not your.');
        }
        $prices=DB::table('product_prices')
                
                ->where('product_prices.product_id',$id)
                ->select('product_prices.*')
                ->get();

        $pricesData=DB::table('product_prices')                
                ->where('product_prices.product_id',$id)
                ->where('product_prices.id',$request->pid)
                ->select('product_prices.*')
                ->first();
        $attributes = Attributes::all();
        //print_r($pricesData);
        return view('admin.item.manage-list', compact('data','prices','attributes','pricesData'));
    }
    public function manageStore(Request $request, $id)
    { //die($request->id.' id ');
       // echo"<pre>",print_r($request->all()),"</pre>";
        $request->validate([
            'qty' => ['required', 'string'],
            'price' => ['required'],  'cost' => ['required'],
            'discount' => ['required'],
            //'attrubute_id' => ['required'],

        ]);

         if($request->id==null){
            $data =new Product_price;
             $data->qty = $request->qty;

           // $data->attrubute_id = $request->attrubute_id;
            $data->price = $request->price;
            $data->cost = $request->cost;
            $data->discount = $request->discount;
            $data->product_id = $id;
            $data->save();

        return redirect()->route('vendor.item.manage',$id)->with('success',$data->name.' Pcice created successfully.');
         }else{
            $data = Product_price::find($request->id);
             $data->qty = $request->qty;
           // $data->attrubute_id = $request->attrubute_id;
            $data->price = $request->price;
            $data->cost = $request->cost;
            $data->discount = $request->discount;
            $data->product_id = $id;
            $data->save();

        return redirect()->route('vendor.item.manage',$id)->with('success',$data->name.' Pcice Updated successfully.');
         }


    }

    
    public function destroy($id)
    {
        //
    }

    public function check_slug(Request $request)
    {
        // Old version: without uniqueness
        //$slug = str_slug($request->name);

        // New version: to generate unique slugs
        $slug = SlugService::createSlug(Product::class, 'slug', $request->name);

        return response()->json(['slug' => $slug]);
    }
    public function partnerOptions(Request $request)
    {
        $vendor_users=DB::table('vendor_users')
               // ->join('categories','categories.id','=','vendor_users.product_categry_id')
                ->select('product_categry_id')
                ->where('vendor_users.id','=',$request->name)
                ->first();
        return response()->json(['category' => $vendor_users->product_categry_id]);
    }
    /*/////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////*/
    public function createAmount()
    {   $user = auth()->user();
        $category=Categorie::all();
        $vendor=Vendor::find($user->id);
        $sub_category=Sub_categories::all();
        $attributes = Attributes::all();
        return view('vendors.product.add-product', compact('category','sub_category','vendor','attributes'));
    }
     public function create2(Request $request)
    {  // echo "<pre>",print_r($request->all()),"</pre>";
        $user = auth()->user();
        $vendor=$user->id;
        $item=$request->item;
       
        foreach ($item as $key => $value) { 
            $product =new Product;
            $product->item_id =$value;
            $product->vendor_id = $vendor;
            $product->status = 1;
            if($product->save()){    //echo " productId - ";
                 $productId=$product->id;
                $data =new Product_price;
                
               
                $data->qty = $request->qty[$product->item_id];
               // $data->attrubute_id = $request->attrubute_id;
                $data->price = $request->price[$product->item_id];
                $data->cost = $request->aprice[$product->item_id];
                $data->discount = $request->discount[$product->item_id];
                $data->product_id = $productId;
                $data->save(); //echo " | productPriceId - ";
                 $productPriceId=$data->id;
                //echo "<pre>",print_r($data),"</pre>";
            }
           // echo "<hr>";
            
        }
       /* $category=Categorie::all();
        $vendor=Vendor::all();
        $sub_category=Sub_categories::all();*/
         return redirect()->route('vendor.product')->with('success',' Product Maped Successfully.');
    }
}
