<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
use App\Master;
use App\User;
use App\Vendor;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;


class HomeController extends Controller
{
    public $cart;
    public $wishlist;

    public $address='';
    public $sector='';
    public $city='';

    public $lat='';
    public $lng='';
    public $lat_lng=false;

    
    public $radius=15;

    public function __construct()

    {            
       
        /*$this->lat=Session::get('lat');
        $this->lng=Session::get('lng');
        $this->lat_lng=Session::get('lat_lng');

        $this->address=Session::get('address');
        $this->sector=Session::get('sector');
        $this->city=Session::get('city');
        $this->cart = Session::get('cart');  
        $this->wishlist = Session::get('wishlist'); */  

        
    }

   
    public function index()
    {                 
        $master=Master::orderBy('order', 'ASC')->get();
        return view('home', compact('master'));
    }
    public function master(Request $request,$slug)
    {   
        
        $lat=Session::get('lat');
        $lng=Session::get('lng');
        $master=DB::table('masters')->where('slug',$slug)->first();
        //echo "Request: <pre>",print_r($master),"</pre>";
        if(!isset($master->id)){
            return redirect()->route('home')->with('Error','Master Category Not Founded.');
        }
        if($master->active!=1){
            return view('addons.soon',compact('master'));
        }
        $master_id=$master->id;
        $category=DB::table('categories')->where('masters_id',$master->id)->get();
        $subcategory=DB::table('sub_categories')->where('masters_id',$master->id)->get();
        /*/////////////////////////////////////////////////////////////////////////////////*/
        $filters = [
                    'search' => 'search',                  
                    
                   ];
        $radius =  $this->radius;
        $type=1;
       
        //$distance = DB::raw("*, ( 6371 * acos( cos( radians($lat) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians($lng) ) + sin( radians($lat) ) * sin( radians( lat ) ) ) ) AS distance");
        if(isset($request->type)){
            $type=$request->type;
        }
        if($type==1){//DB::table('vendors')
           
            $vendor=DB::table('vendors')
                /*->selectRaw("vendors.*, cities.name as city_name,
                     ( 6371 * acos( cos( radians(?) ) *
                       cos( radians( latitude ) )
                       * cos( radians( longitude ) - radians(?)
                       ) + sin( radians(?) ) *
                       sin( radians( latitude ) ) )
                     ) AS distance", [$lat,$lng,$lat])
                ->having("distance", "<", $radius)
                ->orderBy("distance",'asc')*/
                ->leftJoin('cities','cities.id','vendors.city')
                ->leftJoin('vendor_ratings', 'vendor_ratings.vendor_id', '=', 'vendors.id')
                ->where('vendors.verify',1)
                ->where('vendors.active',1)
                ->where('vendors.masters_id',$master->id)
                 ->where(function ($query) use ($request, $filters) {
                        foreach ($filters as $column => $key) {
                            $query->when(Arr::get($request, $key), function ($query, $value) use ($column) {
                                if($column =='search'){
                                    $query->where('vendors.name','LIKE', '%' . $value . '%')
                                    ->orWhere('vendors.company_name','LIKE', '%' . $value . '%');
                                }else{
                                    $query->where($column, $value);
                                }
                                
                            });
                        }
                    })
                ->select('vendors.*','cities.name as city_name')
                //->selectRaw('AVG(vendor_ratings.rating) AS average_rating')
                //->groupBy('vendors.id')
                ->get();

            $item=null;
        }else{
            $item=DB::table('products')            
             ->leftJoin('vendors','vendors.id','=','products.vendor_id')
             ->leftJoin('masters','masters.id','=','vendors.masters_id')
              ->leftJoin('attributes','attributes.id','=','products.attrubute_id')
              ->leftJoin('product_prices','product_prices.product_id','=','products.id')
              /*->selectRaw("products.id as productsId,products.created_at as pcreated_at,products.*,vendors.company_name as vendor_company_name,masters.name as master_name,attributes.name as attributes_name,product_prices.id as product_price_id,product_prices.qty as product_qty,product_prices.cost as product_cost,product_prices.discount as product_discount,product_prices.price as product_price,vendors.latitude,vendors.longitude,
                     ( 6371 * acos( cos( radians(?) ) *
                       cos( radians( latitude ) )
                       * cos( radians( longitude ) - radians(?)
                       ) + sin( radians(?) ) *
                       sin( radians( latitude ) ) )
                     ) AS distance", [$lat,$lng,$lat])
                ->having("distance", "<", $radius)
                ->orderBy("distance",'asc')*/
             ->where('products.masters_id',$master->id)
             ->where('products.verify',1)
             ->where('vendors.verify',1)
             ->where('vendors.active',1)           
             ->where(function ($query) use ($request, $filters) {
                        foreach ($filters as $column => $key) {
                            $query->when(Arr::get($request, $key), function ($query, $value) use ($column) {
                                if($column =='search'){
                                    $query->where('products.name','LIKE', '%' . $value . '%')
                                    ->orWhere('products.slug','LIKE', '%' . $value . '%');
                                }else{
                                    $query->where($column, $value);
                                }
                                
                            });
                        }
                    })   
                ->select('products.id as productsId','products.created_at as pcreated_at','products.*','vendors.company_name as vendor_company_name','masters.name as master_name','attributes.name as attributes_name',
                    'product_prices.id as product_price_id',
                'product_prices.qty as product_qty','product_prices.cost as product_cost',
                'product_prices.discount as product_discount','product_prices.price as product_price',)
                ->orderBy('product_prices.id')
                ->get();
            $vendor=null;
        }
        

        
       //echo "<hr>vendor: <pre>",print_r($vendor),"</pre>";die();
        return view('master', compact('master_id','master','category','subcategory','vendor','item','type'));
    }
    public function vendor(Request $request,$type,$id,$slug)
    {
        //echo"Vendor: ". $slug;
        $master=DB::table('masters')->where('slug',$type)->first();
        //echo "Request: <pre>",print_r($master),"</pre>";

        if(!isset($master->id)){
            return redirect()->route('home')->with('Error','Master Category Not Founded.');
        }
        
        $fillter=null;
        if(isset($request->item)){
            $fillter=$request->item;
        }
       // echo "$id";
        $vendor=DB::table('vendors')
                ->leftJoin('cities','cities.id','vendors.city')
                ->where('vendors.verify',1)
                ->where('vendors.active',1)
                ->where('vendors.masters_id',$master->id)
                ->where('vendors.id',$id)
                ->select('vendors.*','cities.name as city_name')
                ->first();
       //echo "<hr>vendor: <pre>",print_r($vendor),"</pre>";die();
        if(!isset($vendor->id)){
            return redirect()->route('master',$type)->with('Error','Shop Not Founded.');
        }
        //$cart = $this->getCartData();
        //$cartItem=$cart['item'];
        // echo "<hr>cartItem: <pre>",print_r($cartItem),"</pre>";die();
        $item =DB::table('products')  
                //->leftJoin('items','items.id','=','products.item_id')
                ->leftJoin('categories','categories.id','=','products.categry_id')
                ->leftJoin('sub_categories','sub_categories.id','=','products.sub_categry_id')
                ->leftJoin('attributes','attributes.id','=','products.attrubute_id')
                ->where('products.vendor_id',$vendor->id)
                ->leftJoin('product_prices','product_prices.product_id','=','products.id')
                ->select('products.id as productsId','products.created_at as pcreated_at','products.*','categories.name as categories_name','sub_categories.name as sub_categories_name',
                    'attributes.name as attributes_name','product_prices.id as product_price_id',
                'product_prices.qty as product_qty','product_prices.cost as product_cost',
                'product_prices.discount as product_discount','product_prices.price as product_price',
                //DB::raw('CASE WHEN products.id=1 THEN 1 ELSE 0 END AS in_cart')
                 )
                //->orderBy('productsId')
                ->orderBy('product_price')
                ->get();

       
        //$itemcat = $item->groupBy('categories.id');
        $itemcat = $item->groupBy('categories_name')->toArray();     
        
        
        $item = $item->groupBy('productsId');
        
          //echo "<hr>itemcat: <pre>",print_r($itemcat),"</pre>";//die();
           //echo "<hr>item: <pre>",print_r($item),"</pre>";die();
        $category=DB::table('categories')->where('masters_id',$master->id)->get();
        $subcategory=DB::table('sub_categories')->where('masters_id',$master->id)->get();
        return view('vendor', compact('master','category','subcategory','vendor','item','itemcat','fillter'));
    }
    /*
    |--------------------------------------------------------------------------------------
    |  invite new users
    |--------------------------------------------------------------------------------------
    */   

    public function invite($code)
    {        
        return view('auth.register', compact('code'));
    }
}
