<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

use Illuminate\Http\Request;
use Auth;
use Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    

    public function getCartItem()
    {
        $cart=array();
        $tcart = Session::get('cart');
        if($tcart){
            foreach ($tcart as $key => $value) {
               // echo $value['product_id']; 
                $id= $value['product_id'];
                if($id){
                    array_push($cart,$id);
                }
                
            }
        }
        return $cart;
    }
    public function getCartPItem()
    {
        $cart=array();
        $tcart = Session::get('cart');
        if($tcart){
            foreach ($tcart as $key => $value) {
               // echo $value['product_id']; 
                $id= $value['product_price_id'];
                if($id){
                    array_push($cart,$id);
                }
                
            }
        }
        return $cart;
    }
    public function getCartData()
    {
        $cart=array(); $cartCount=0; 
        $tcart = Session::get('cart');
        if($tcart){
            foreach ($tcart as $key => $value) {
               // echo $value['product_id']; 
                $id= $value['product_price_id'];
                if($id){                    
                    array_push($cart,$id);
                    $cartCount+= $value['qty'];
                }
                
            }
        }
        $item =DB::table('product_prices')
                    ->leftJoin('products','products.id','=','product_prices.product_id')
                    ->leftJoin('attributes','attributes.id','=','products.attrubute_id')         
                    ->leftJoin('vendors','vendors.id','=','products.vendor_id')
                    ->leftJoin('masters','masters.id','=','vendors.masters_id')
                    ->whereIn('product_prices.id',$cart)
                    /*->where('product_prices.id',$master->id)*/
                ->select('products.id','products.name','products.slug','products.image','products.vendor_id',
                    'vendors.name as vendor_name','vendors.address as vendor_address',
                    'masters.name as masters_name','vendors.masters_id as masters_id',
                    'product_prices.id as item_pid','product_prices.qty as item_qty',
                    'product_prices.cost as item_cost','product_prices.discount as item_discount','product_prices.price as item_price',
                    'attributes.name as attributes_name')
                ->get();
         $data=array();  $idata=array(); $cartTotal=0;
        if($item){
            foreach ($item as $key => $value) {
                $pqty=$value->attributes_name;
                //if($value->item_qty!=1){
                   $pqty= $value->item_qty.' '.$value->attributes_name;
                   $itemTotal=$value->item_price*$tcart[$value->item_pid]['qty'];
                //}

                $temp=array(
                    'id'=>$value->id,
                    'name'=>$value->name,
                    'slug'=>$value->slug,
                    'attributes'=>$pqty,
                    'image'=>$value->image,
                    'masters_id'=>$value->masters_id,
                    'masters_name'=>$value->masters_name,
                    'vendor_id'=>$value->vendor_id,
                    'vendor_name'=>$value->vendor_name,
                    'vendor_address'=>$value->vendor_address,
                    'price_id'=>$value->item_pid,
                    'qty'=> $tcart[$value->item_pid]['qty'],
                    'cost'=>$value->item_cost,
                    'discount'=>$value->item_discount,
                    'price'=>$value->item_price,
                    'amount'=>number_format($itemTotal,2),
                    
                );
                $cartTotal+= $itemTotal;
                array_push($data, $temp);
                array_push($idata, $value->id);
            }
        }
        //array_push($finalcart, ['count'=>$cartCount,'item'=>$data]);

        $finalcart=array(
            'count'=>$cartCount,
            'item'=>$data,
            'itemId'=>$idata,
            'cartTotal'=>number_format($cartTotal,2)
        );
        return $finalcart;
    }

    /*////////////////////////////////////////////*/
    protected function getToken()
    {
        return Hash::make(Str::random(40));
    }
    /*/////////////////////////////////////////////////////*/
    function getTableColumUnique($table,$colum){
        $no = rand(1,99999999999999);
        
        $temp=DB::table($table)->where($colum,$no)->get();
        if(count($temp)==0){
            return $no;
        }else{
            $this->getTableColumUnique($table,$colum);

        }
    }

    /*
    |
    |
    |
    */
    
}