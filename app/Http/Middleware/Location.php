<?php

namespace App\Http\Middleware;

use Closure;
use DB;
use Session;

class Location
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Session::get('lat_lng')==false) {
            $urlPrevious = url()->previous();
            $urlBase = url()->to('/');
            // Set the previous url that we came from to redirect to after successful login but only if is internal
            if(($urlPrevious != $urlBase . '/login') && (substr($urlPrevious, 0, strlen($urlBase)) === $urlBase)) {
                session()->put('url.intended', $urlPrevious);
            }
            return redirect()->route('location');
        }
        return $next($request);
    }
}
