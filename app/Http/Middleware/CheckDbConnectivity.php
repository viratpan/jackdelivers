<?php

namespace App\Http\Middleware;

use Closure;
use DB;

class CheckDbConnectivity
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //return $next($request);
        try
        {
            //DB::connection('mysql_ndoutils')->table(DB::raw('DUAL'))->first([DB::raw(1)]);
            DB::select(DB::raw("SELECT 1 From dual"));
            //DB::select(DB::raw("SELECT 1 From dual"));
        }
        catch(\Exception $e)
        {
            return 'Oh snap! Your DB is not available!';
        }
        return $next($request);
    }
}
