<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Payment extends Model
{
    use Notifiable;
   
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'order_id','OD_id','transition_id','payment_type','total_amount'
    ];
}
