@if($data)
    <div class="row text-secondary" style="padding:10px;margin-bottom: 4px;border-bottom: 1px solid gray;">
        <h3 class="col-12">Order <text style="color: black;">#{{$data[0]->order_id}}</text></h3>
        <div class="col-sm-12" >
          <b>From: {{$data[0]->vendor_company_name}}   ({{$data[0]->masters_name}})</b><br>
          <b>To: {{ $data[0]->address}}, {{ $data[0]->city}}, {{ $data[0]->state}}</b><br>               
        @if($data[0]->item_status==0)
        @endif
        
        <small>Delivered on {{ date('D, d M y, H:i:s A', strtotime($data[0]->created_at)) }}  by {{ $data[0]->rider_name}}</small><br><br>  
        <small>{{$data[0]->items}} Item</small>
                     
        </div> 
      
    </div>
    <div class="row text-secondary" style="padding:0px 10px; border-bottom: 1px dotted gray;">
  @foreach($data as $key => $aval)                            
        <div class="col-12 row">
            <div class="col-sm-10" >
              <b>{{$aval->product_name}} </b>
              <small>&nbsp;<i>( {{ $aval->product_qty.' '.$aval->attributes_name}} )</i></small>
              &nbsp;<i>X {{ $aval->item_qty}}</i>                     
            </div>                        
            {{-- <div class="col-sm-2" >
              <b>Rs.{{$aval->item_cost}}</b>                      
            </div>
            <div class="col-sm-2" >
              <b>{{$aval->item_discount}}%</b>                      
            </div> --}}
            <div class="col-sm-2" style="text-align:right">
              <small>Rs.{{$aval->item_amount}}</small>                    
            </div>
        </div>  
    @endforeach
     </div>
    <div class="row text-secondary" style="padding:0px 10px; border-bottom: 1px dotted gray;">
        <div class="col-12 row">
            <div class="col-sm-10" >
              <small>Item Total</small>                   
            </div>                        
            
            <div class="col-sm-2" style="text-align:right">
              <small>Rs.{{$aval->price}}</small>                    
            </div>
        </div>
        <div class="col-12 row" style="">
            <div class="col-sm-10" >
              <small>Delivery partner fee</small>                   
            </div>                        
            
            <div class="col-sm-2" style="text-align:right">
              <small>
                @if($aval->shipping==0.00)
                    Free
                @else
                    Rs.{{$aval->shipping}}</small>  
                @endif                 
            </div>
        </div> 
        @if($aval->discount!=0.00)
            <div class="col-12 row" style="">
            <div class="col-sm-10" >
              <small>Dicount</small>                   
            </div>                        
            
            <div class="col-sm-2" style="text-align:right">
              <small>Rs.{{$aval->discount}}</small>
            </div>
        </div> 
        @endif
        @if($aval->tax!=0.00)
            <div class="col-12 row" style="">
            <div class="col-sm-10" >
              <small>GST</small>                   
            </div>                        
            
            <div class="col-sm-2" style="text-align:right">
              <small>Rs.{{$aval->tax}}</small>
            </div>
        </div> 
        @endif
        <div class="col-12 row" style="border-top: 1px solid black;">
            <div class="col-8" >
              <small>Paid Via @if($aval->payment_type==1)Cash @else Online @endif</small>
            </div>
            <div class="col-2" > 
              <b>  Bill Total</b> 
            </div>
            <div class="col-2" style="text-align:right">
                <b>Rs.{{$aval->total_amount}}</b>
            </div>
        </div>
    </div> 
                          
@else
  <div align="center" style="text-align: center;margin-top: 5rem;margin-bottom: 5rem;">
    <b>No Orders<br>
            You have not order any product yet!</b>
  </div>
@endif