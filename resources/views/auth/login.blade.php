@extends('layouts.app')
@section('style')
<style type="text/css">
	.form-control:focus{
		box-shadow: unset;
	}
</style>
@endsection
@section('content')
<section class="login-form">
           <div class="container clearfix">
               <div class="left-login-image">
                   <img src="{{asset('forntend/images/login-image/login.jpg')}}" alt="">
                   <div class="trans">
                        <div class="form-heading animatable bounceInLeft">
                            <h1>Log In</h1>
                        </div>
                        <div class="form-heading-2 animatable bounceInLeft">
                            <h2>Enter Your Details</h2>
                        </div>
                        <div class="terms">privacy policy & terms of service</div>
                    </div>
                </div>
               <div class="right-form">
                   <div class="log-in-form">
                        <form method="POST"  action="{{ route('login') }}">
                        @csrf                      			
                            <div class="inputs">
                                <div class="form-inputs">
                                    <input id="email" type="email" class="form-control lgn_input @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="" required autocomplete="email" autofocus>
                                    <label for="" class="form-label"><span class="label-input">Username</span></label>
                                    @error('email')
									 	 <span class="invalid-feedback" role="alert">
									 			 <strong>{{ $message }}</strong>
									 	 </span>
									 @enderror 
                                </div>
                                <div class="form-inputs eye-button">
                                	<input id="password" type="password" id="passwordsignup" class="form-control  lgn_input @error('password') is-invalid @enderror" name="password" placeholder="" required autocomplete="current-password">                                    
                                    <label for="" class="form-label"><spna class="label-input">Password</spna></label>
                                    <i class="far fa-eye" id="togglePasswordsignup"></i>
                                    @error('password')
														<span class="invalid-feedback" role="alert">
																<strong>{{ $message }}</strong>
														</span>
												@enderror
                                </div>
                                
                                <div class="form-inputs">
                                	<input class="" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>Remember Me</span>
                                </div>
                                <div class="form-inputs">
                                	<input type="submit" value="Login">
                                	<div class="password-forgor">
										@if (Route::has('password.request'))
												<a class="btn btn-link" href="{{ route('password.request') }}">
														{{ __('Forgot Your Password?') }}
												</a>
										@endif
									</div>
                                </div>
                                	
                            </div>
                        </form>
                        
                        <div class="or">or</div>
                        <div class="login-social">
                            <span class="login-icons"><a href="{{ route('auth2','facebook') }}"><i class="fab fa-facebook-f"></i></a></span>
                            <span class="login-icons"><a href="{{ route('auth2','google') }}"><i class="fab fa-google-plus-g"></i></a></span>
                        </div>
                        <div class="sign-up">
                           <label>Not a member?<a href="#" id="signUp">Sign Up</a></label>
                        </div>
                    </div>



                <div class="sign-up-form">
                   <form method="POST" action="{{ route('register') }}">
                        @csrf
                       <div class="form-inputs">
                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                        <label for="" class="form-label"><span class="label-input">Full Name</span></label>
                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-inputs">
                             <input id="mobile" type="number" class="form-control @error('mobile') is-invalid @enderror" name="mobile" value="{{ old('mobile') }}" required autocomplete="phone Number">
                            <label for="" class="form-label"><span class="label-input">Mobile Number</span></label>
                             @error('mobile')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror 
                        </div>
                       <div class="form-inputs">
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                                @if(isset($code))
                                    <input type="hidden"  name="code" value="{{ isset($code) ? $code : old('code')}}">
                                @endif
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            <label for="" class="form-label"><span class="label-input">E-mail</span></label> 
                        </div>
                       <div class="form-inputs">
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            <label for="" class="form-label"><spna class="label-input">Password</spna></label>
                        </div>
                       <div class="form-inputs">
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            <label for="" class="form-label"><span class="label-input">Confirm Password</span></label>   
                        </div>

                       <div class="form-inputs"> <input type="submit" value="Submit"></div>
                   </form>
               </div>
               </div>
               
           </div>
       </section>

@endsection
