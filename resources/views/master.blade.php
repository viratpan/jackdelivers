@extends('layouts.app')
@section('script')
<script type="text/javascript">
	var master_id='{{ $master_id}}';
	$(document).ready(function () {
		$.ajaxSetup({
           headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
       });
		$('#filter').change(function(){
			var id=$(this).val();
      		var selectedText = $("#filter option:selected").html();
      		//$("#filter option:selected").html('Sort by : '+selectedText);;
		});
		$( "#vendorSearch" ).autocomplete({
	      source: function( request, response ) {
	        $.ajax( {
	          url: "{{route('ajax.vendor.search',$master_id)}}",
	          dataType: "json",
	          data: {
	            term: request.term
	          },
	          success: function( data ) {
	            //response( data );
	            	if(!data.length){
				        var result = [
				            {
				                label: 'No matches found', 
				                value: response.term
				            }
				        ];
				        response(result);
				    }
				    else{
				        // normal response
				        response($.map(data, function (item) {
				            return {
				                label: item.label,// + " ( Vendor )",
				                value: item.value,
				                url: item.url
				            }
				        }));
				    }
	          }
	        } );
	      },
	      minLength: 1,
	      select: function( event, ui ) {
	        // similar behavior as an HTTP redirect
			//window.location.replace(ui.item.url);

			// similar behavior as clicking on a link
			window.location.href = ui.item.url;
	      }
	    } );


	    $( "#itemSearch" ).autocomplete({
	      source: function( request, response ) {
	        $.ajax( {
	          url: "{{route('ajax.item.search',$master_id)}}",
	          dataType: "json",
	          data: {
	            term: request.term
	          },
	          success: function( data ) {
	            //response( data );
	            	if(!data.length){
				        var result = [
				            {
				                label: 'No matches found', 
				                value: response.term
				            }
				        ];
				        response(result);
				    }
				    else{
				        // normal response
				        response($.map(data, function (item) {
				            return {
				                label: item.label,//+ " ( Product )",
				                value: item.value,
				                url: item.url
				            }
				        }));
				    }
	          }
	        } );
	      },
	      minLength: 1,
	      select: function( event, ui ) {
	        //window.location.replace(ui.item.url);
	        window.location.href = ui.item.url;
	      }
	    } );




    });
    function fillterVendors(by){
    	
    	var items = $('.filterVendor');
		  items.sort(function(a, b){
		      if(by==='costof2' ||by==='time' ||by==='popular' ){
		      	return +$(a).data(by) - +$(b).data(by);
		      }else{
		      	return +$(b).data(by) - +$(a).data(by);
		      }
		      
		  });

		  items.appendTo('#vendorRow');
    }
   
</script>
@endsection
@section('content')
<link rel="stylesheet" href="{{ asset('forntend/css/vender-category.css')}}">
<style>
  .ui-autocomplete {
    max-height: 300px;
    overflow-y: auto;
    /* prevent horizontal scrollbar */
    overflow-x: hidden;
  }
  /* IE 6 doesn't support max-height
   * we use height instead, but this forces the menu to always be this tall
   */
  * html .ui-autocomplete {
    height: 300px;
  }
  .ui-menu .ui-menu-item {
    
    border: 1px solid gray;
  }
  .ui-menu .ui-menu-item-wrapper {
   
    padding: 10px;
  }
 /*///////////////////////////////////////*/
  
  </style>
<!--breadcrumb start-->
        <section class="breadcrumb">
            <div class="container">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                       
                        <li class="breadcrumb-item active" aria-current="page">{{ucfirst($master->name)}}</li>
                    </ol>
                </nav>
            </div>
        </section>
<!--breadcrumb end-->
<section class="vender-cetogoery">
            <div class="container">
            	{{-- vertical Details --}}
                <div class="categorey-head">

                    <!-- <div class="row">
                       <div class="col-lg-1 col-md-1 col-sm-12">
                            
>>>>>>> feature/16_mar_21
                        </div>
                        <div class="col-lg-11 col-md-11 col-sm-12">
                            <div class="cetogery-heading  bounceInRight">{{-- animatable
animatable --}}
                                <h1>{{ucfirst($master->name)}} @if(Session::get('address'))  in {{Session::get('address')}}@endif</h1>
                               {{--  <span class="catogery-title">190 Restaurants</span> --}}
                            </div>
                        </div>
                    </div> 
                </div> -->
                {{-- vertical Details --}}
                <div class="category-items categorey-head">
                    <div class="category-filters">
                        <div class="left-filter-dtl">
						<div class="category-img"><img
                                    src="{{ asset($master->image) }}" alt="{{ucfirst($master->name)}}">
                            </div>
                        	@if($type==1) 
                        		 <span class="rest">{{ucfirst($master->name)}}<span class="rasturant-no">{{count($vendor)}}</span>  </span>
                        	@else
                        		{{count($item)}} <span class="rest">{{ucfirst($master->name)}} Items</span>
                        	@endif  
                        </div>	
                        <div class="right-filter">
                            <ul>
                                <li><span class="filter-btn active" onclick="fillterVendors('popular');"> Relevance</span></li>
                                @if($master->id==4)
                                	<li><span class="filter-btn" onclick="fillterVendors('costof2');">Cost For Two</span></li>
                                @endif
                                {{-- <li><span class="filter-btn" onclick="fillterVendors('time');">Delivery Time</span></li> --}}
                                <li><span class="filter-btn" onclick="fillterVendors('rating');">Rating</span></li>
                                {{-- <li><span class="filter-btn">Filters</span> <span class="filter-icon"><i
                                            class="fas fa-filter"></i></span></li> --}}
                            </ul>
                        </div>
                    </div>
                    <div class="row" id="vendorRow">
                    @if($type==1)
                    	@foreach($vendor as $key => $value)
                    	<div class="col-lg-3 col-md-4 col-sm-6 col-6 filterVendor" data-rating="0" data-costof2="{{$value->costoftwo}}" data-time="0" data-popular="{{$key}}">
                            <a href="{{ route('vendor',['type' => $master->slug,'id' => $value->id, 'slug' => Str::slug($value->company_name)])}}">
	                            <div class="items">
	                                <div class="item-img  fadeIn">{{-- animatable --}}
	                                	@if($value->image)
											<img src="{{ asset($value->image) }}" alt="{{ $value->company_name}}">
										@else
											<img src="{{ asset(config('app.logo')) }}" alt="{{ $value->company_name}}">
										@endif
										@if($value->open==1)
											@if($value->ontime && $value->offtime)	
												@if(\Carbon\Carbon::now()->between(\Carbon\Carbon::parse($value->ontime),\Carbon\Carbon::parse($value->offtime) )==false)
												<div class="sv-dsicount">
													<span class="dis-dtl">Closed Open at , {{$value->ontime}}</span>
												</div>
												@endif
											@endif
										@else
										<div class="service">
											<span class="dis-dtl">Unserviceable</span>
										</div>
										@endif
	                                </div>
	                                <div class="item-name">
	                                    <h2>{{ ucfirst($value->company_name) }}</h2>
	                                    <div class="item-title">
	                                        <p>{{-- {{ Str::substr($value->address,0,50) }} --}}{{$value->city_name}}</p>
	                                    </div>
	                                </div>
	                                <div class="item-dtl">
	                                	<!-- @if(isset($value->average_rating))
	                                	@if($value->average_rating) --> <!-- @endif
	                                    @endif -->
	                                    <div class="item-rating">
	                                        <span class="item-stars"><i class="fas fa-star"></i></span>
	                                        <span class="item-rate">4.1 </span>
	                                    </div>
	                                   
	                                    <div class="item-hur">45 MINS </div>
	                                @if($value->costoftwo)
	                                    	<div class="item-price">&#8377; {{$value->costoftwo}} <span class="words">FOR TWO</span></div>
	                                @endif   
	                                </div>
	                                   
<!-- @if($value->open==1)
	@if($value->ontime && $value->offtime)	
	    @if(\Carbon\Carbon::now()->between(\Carbon\Carbon::parse($value->ontime),\Carbon\Carbon::parse($value->offtime) )==false)
	     <div class="dsicount">
	          <span class="dis-dtl">Open at , {{$value->ontime}}</span>
	      </div>
	    @endif
    @endif
@else
	<div class="dsicount">
      <span class="dis-dtl"> Currently Unserviceable</span>
  </div>
@endif -->
	                                {{--  <div class="dsicount">
	                                    <span class="dis-logo"><img src="{{ asset('images/icons-images/discount.png')}}"
	                                            alt=""></span>
	                                    <span class="dis-dtl">60% off | Use DEAL60</span>
	                                </div>  --}}
	                            </div>
                        	</a>
                        </div>
						
			            @endforeach
                    @else
                    	@foreach($item as $key => $value)
						<div class="col-md-3 col-sm-3 col-xm-6  bounceIn">{{-- animatable --}}
							<a href="{{ route('vendor',['type' => $master->slug,'id' => $value->id, 'slug' => Str::slug($value->company_name)])}}">
								<div class="items">
									<div class="item-img">
									@if($value->image)
										<img src="{{ asset($value->image) }}" alt="{{ $value->name}}">
									@else
										<img src="{{ asset(config('app.logo')) }}" alt="{{ $value->name}}">
									@endif
									</div>
									<div class="item-name">
								<h2>{{ ucfirst($value->name) }}</h2>
										<div class="item-title">
											<p>{{ ucfirst($value->vendor_company_name) }}, {{-- {{ Str::substr($value->address,0,50) }} --}}{{$value->city_name}}</p>
										</div>
									</div>
									<div class="item-dtl">
										<div class="item-rating">
											<span class="item-stars"><i class="fas fa-star"></i></span>
											<span class="item-rate">4.1</span>
										</div>
										<div class="item-hur">23 Min</div>
										{{-- <div class="item-price">&#8377; 180 <span class="words">for two</span></div> --}}
									</div>
									{{-- <div class="dsicount">
										<span class="dis-logo"><img src="images/icons-images/discount.png"
												alt=""></span>
										<span class="dis-dtl">60% off | Use DEAL60</span>
									</div> --}}
								</div>
							</a>
			            </div>
			            @endforeach
                    @endif   
                       
                    </div>
                </div>

            </div>
        </section>
@endsection