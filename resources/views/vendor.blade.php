@extends('layouts.app')

@section('style')
{{--   <link rel="stylesheet" href="{{ asset('forntend/css/vender.css')}}"> --}}
@endsection

@section('script')

<script type="text/javascript">



var cartTotal=0;
var store='{{$vendor->open}}';
var page='vendor';
var vendorIs='';
@if($vendor->open==1)
  @if($vendor->ontime && $vendor->offtime)  
      @if(\Carbon\Carbon::now()->between(\Carbon\Carbon::parse($vendor->ontime),\Carbon\Carbon::parse($vendor->offtime) )==false)

       vendorIs='Closed';storeAction();
       
      @else
        vendorIs='';
      @endif
    @endif
@else
  vendorIs='Unserviceable'; storeAction();
@endif
function storeAction(){
  //if(store==0){//alert('offline');
      $("#result .fillter").each(function(){
          var action=`<span style="background: gray;padding: 4px;color: white;">`+vendorIs+`</span>`;
          $('.add-food').html(action);
      });
    //}
}
$(document).ready(function () {
 
    @if($fillter!=null)       
      $("#fillter").val('{{ $fillter }}');
       var value = '{{ $fillter }}'.toLowerCase();
        $(".fillter").filter(function() {
          var div=$(this).attr('data-name').toLowerCase(); console.log(div);
          $(this).toggle(div.indexOf(value) > -1);
          $('.search-cat').show();
          // $(this).toggle($(this).attr('data-name').toLowerCase().indexOf(value) > -1);
        });

    @endif

    getCart();
    

    $("#fillter").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        var html='';
        if(value){
              $(".fillter").filter(function() {
              var div=$(this).attr('data-name').toLowerCase(); //console.log(div);
              $(this).toggle(div.indexOf(value) > -1);
              if($(this).toggle(div.indexOf(value) > -1)){
                html+=$(this).html();
              }
              

            });
            }else{
              $('.search-cat').html('');
              $('.fillter').hide().filter('.fillter').show();
            }
            ///////////////////////////////////////////////////////////
           checkalldivs(); 
             
        
    });
  /*filleterrr*/



});
function checkalldivs(){//alert('filter divs');
  var r=0;
   $(".food").each(function() {
    //console.log($(this).attr('data-name'));
       //   console.log($(this).find('.food-heading .food-items').html());
        var i=0; $(this).show();
         $(this).children('.fillter').each(function(){
            if($(this).is(':visible')){
              i++; r++;
             // console.log('check' + $(this).is(':visible'));
            }else{
              // console.log('check' + $(this).is(':visible'));
            }
         });
         //$(this).find('.food-heading .food-items').html(i+' Items')
         if(i==0){
           // console.log(i +' | '+$(this).find('.food-heading .food-items').html() +' | '+ $(this).attr('data-name'));
            $(this).hide();
         }else{
           // console.log(i +' | '+$(this).find('.food-heading .food-items').html() +' | '+ $(this).attr('data-name'));
            $(this).show();
         }
       });
   if(r==0){//alert('null');
     $('#menuDiv').append(`<div class="food" id="nodata"  data-id="0" data-name="No Item Found" data-category="">                             
                                <div class="food-heading" style="text-align:center;">
                                    <h1>No Item Found</h1>
                                    <span class="food-items" data-count="0">We couldn’t find any items matching your search. Please try a new keyword.</span>
                                </div>
                          </div>`);
   }else{
     //alert('not null');
      $('#menuDiv').find('#nodata').remove('');
   }
}
function checkfood(this_obj,value){
 // alert(value);
  if(value=='nonveg'){
    $('#vegcheck').prop('checked', false); // Unchecks it
  }else if(value=='veg'){
    $('#nonvegcheck').prop('checked', false); // Unchecks it
  }else{
    $('#nonvegcheck').prop('checked', false); // Unchecks it
    $('#vegcheck').prop('checked', false); // Unchecks it
  }
  if($(this_obj).prop("checked") == true){
      //  console.log("Checkbox is checked.");
        $(".fillter").filter(function() {
          var div=$(this).attr('data-name').toLowerCase(); //console.log(div);
          $(this).toggle(div.indexOf(value) > -1);
          // $(this).toggle($(this).attr('data-name').toLowerCase().indexOf(value) > -1);
        });
    }
    else{ 
      $('.fillter').hide().filter('.fillter').show();
    }
    checkalldivs();
   
}
function checknonvagfood(this_obj,value){
  //alert(value);
  if($(this_obj).prop("checked") == true){
        console.log("Checkbox is checked.");
        $(".fillter").filter(function() {
          var div=$(this).attr('data-name').toLowerCase(); //console.log(div);
          $(this).toggle(div.indexOf(value) > -1);
          // $(this).toggle($(this).attr('data-name').toLowerCase().indexOf(value) > -1);
        });
    }
    else{ /*if($(this_obj).prop("checked") == false){
        console.log("Checkbox is unchecked.");
        $(".fillter").filter(function() {
          var div=$(this).attr('data-name').toLowerCase(); //console.log(div);
          $(this).toggle(div.indexOf(value) > -1);
          // $(this).toggle($(this).attr('data-name').toLowerCase().indexOf(value) > -1);
        });
    }else{*/
      $('.fillter').hide().filter('.fillter').show();
    }
    
   
}
</script>
<script src="{{ asset('js/other/vendor_details.js')}}"></script>


@endsection
@section('content')
<!--breadcrumb start-->
  <section class="breadcrumb">
      <div class="container">
          <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ route('master',$master->slug)}}">{{  ucfirst($master->name)}}</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{ ucfirst($vendor->company_name) }}</li>
              </ol>
            </nav>
      </div>
  </section>        
<!--breadcrumb End-->


{{-- Vendor Details --}}
{{-- @if($vendor->coverimage)background-image:url({{ asset($vendor->coverimage) }})@endif --}}
<section class="vendor-section" style="">
  <div class="container">
      <div class="top-hotel-details">
          <div class="row">
              <div class="col-lg-3 co-md-3 col-sm-3 col-xs-12">
                  <div class="hotel-img">
                    @if($vendor->image)
                      <img src="{{ asset($vendor->image) }}" alt="{{ $vendor->name}}" class="scroll-img">
                    @else
                      <img src="{{ asset(config('app.logo')) }}" alt="{{ $vendor->name}}" class="scroll-img">
                    @endif
                  </div>
              </div>
              <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
              <div class="scroll-detl">
                  <div class="hotel-detail">
                          <h1>{{ ucfirst($vendor->company_name) }}</h1>
                          {{-- <div class="title"><span >Biryani</span></div> --}}
                          <div class="title"><span >{{ $vendor->address }},{{ $vendor->city_name }}</span></div>
                  </div>
                  <div class="rateing-deatils">
                      <div class="rating">
                          <div class="star-icon"><span><i class="fas fa-star"></i>--</span></div>
                          <div class="rating-title"><span>Too Few Ratings</span></div>
                      </div>
                      @if($vendor->open==1)
                        @if($vendor->ontime && $vendor->offtime)  
                            @if(\Carbon\Carbon::now()->between(\Carbon\Carbon::parse($vendor->ontime),\Carbon\Carbon::parse($vendor->offtime) )==false)
                             <div class="delevry-time">
                                  <span class="dis-dtl">Closed {{-- at , {{$vendor->ontime}} --}}</span>
                              </div>
                            @else
                              <div class="delevry-time">
                                <div class="star-icon"><span>45 min</span></div>
                                <div class="rating-title"><span>Delivery Time</span></div>
                            </div>
                            @endif
                          @else
                          <div class="delevry-time">
                                <div class="star-icon"><span>-- </span></div>
                                <div class="rating-title"><span>Delivery Time</span></div>
                            </div>
                          @endif
                      @else
                       <div class="delevry-time">
                            <span class="dis-dtl" style="border-radius: 3px;background-color: gray;padding: 6px;">Unserviceable</span><br>
                            <span>Check Back Late</span>
                        </div>
                      @endif
                      
                      @if($vendor->costoftwo)
                      <div class="cost">
                          <div class="star-icon"><span> 
                              &#8377; {{$vendor->costoftwo}}</span></div>
                          <div class="rating-title"><span>Cost for two</span></div>
                      </div>
                      @endif
                  </div>
                  <div class="soting-itmes">
                      <div class="checkbox-item">
                          <input type="text" id="fillter" placeholder="Search for Dishes...">
                          <span class="serch-icon"><i class="fas fa-search"></i></span>
                      </div>
                      <div class="checkbox-item">
                          <input type="checkbox" onclick="checkfood(this,'veg')" value="veg" id="vegcheck" name="foodtype">
                          <span class="serch-label">Veg Only</span>
                      </div>
                      <div class="checkbox-item">
                          <input type="checkbox" onclick="checkfood(this,'nonveg')" id="nonvegcheck" value="nonveg" name="foodtype">
                          <span class="serch-label">Non Veg Only</span>
                      </div>
                  </div>
              </div>
</div>
          </div>
      </div>
  </div>
</section>
{{-- end vendor details --}}

{{-- |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| --}}

    <div class="hotel-products">
            <div class="container mt-3">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs">
                  <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#orderonline">Menu</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#overview">Overview</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#reviews">Reviews</a>
                  </li>
                </ul>
              
                <!-- Tab panes -->
                <div class="tab-content">
  {{-- MENU TAB --}}                
                  <div id="orderonline" class="container vender-tab tab-pane active"><br>
                    {{-- <div class="food-serching">
                        <div class="serarch-with-icons"><input type="text" placeholder="Search for Dishes....."><span class="food-serch-icon"><i class="fas fa-search"></i></span></div>
                        
                    </div> --}}
                    <div class="row" id="result"  data-spy="scroll" data-target=".navbar" data-offset="50">
                        <div class="col-lg-2 col-md-12 ">
                            <div class="category">
                                <ul class="navbar">
                                   
                                  @foreach($itemcat as $ckey => $cvalue)                                 
                                    <li>
                                      <a href="#{{$cvalue[0]->categories_name}}" data-id="{{$cvalue[0]->categry_id}}" data-name="{{$cvalue[0]->categories_name}}">{{$cvalue[0]->categories_name}}</a>
                                    </li>
                                  @endforeach
                                </ul>
                            </div>
                        </div>
                         <div class="col-lg-6 col-md-12  border-rgt-lgt " id="menuDiv" >
                                                   
                          @foreach($itemcat as $ckey => $cvalue) 
                            <div class="food" id="{{$cvalue[0]->categry_id}}"  data-id="{{$cvalue[0]->categry_id}}" data-name="{{$cvalue[0]->categories_name}}" data-category="{{$cvalue[0]->type}} ">                             
                                <div class="food-heading" >
                                    <h1>{{$cvalue[0]->categories_name}}</h1>
                                    <span class="food-items" data-count="{{ count($cvalue)}}">{{ count($cvalue)}} Items</span>
                                </div>
                              
                    @php $thisCatItem=collect($cvalue);
                      $idata = $thisCatItem->groupBy('productsId')->toArray();
                    @endphp
                    @foreach($idata as $key => $value)
                  
                                <div class="food-detials fillter" data-type="{{$value[0]->type}}" data-name="{{ ucfirst($value[0]->slug)}}" >
                                    <div class="food-image">
                                        @if($vendor->image)
                                          <img src="{{ asset($value[0]->image) }}" alt="{{ ucfirst($value[0]->name)}}" >
                                        @else
                                          <img src="{{ asset(config('app.logo')) }}" alt="{{ ucfirst($value[0]->name)}}" >
                                        @endif                                        
                                    </div>
                                    <div class="food-name">
                                        <h2>{{ ucfirst($value[0]->name)}}</h2>
                                        <div class="amounts">{{$value[0]->product_qty.' '.$value[0]->attributes_name}}</div>
                                        <div class="price">&#8377 {{ $value[0]->product_price}}
                                        @if($value[0]->product_discount!=0)
                                           <span style="text-decoration: line-through;color:gray;">&#8377 {{$value[0]->product_cost}}</span>
                                        @endif
                                        </div>
                                    </div>
                                    <div class="add-food">
                                      @if(count($value)==1) 
                                        <button id="cart_btn_{{$value[0]->productsId}}" data-id="{{$value[0]->productsId}}" data-name="{{$value[0]->name}}" data-vendor_id="{{$value[0]->vendor_id}}" data-Pid="{{ $value[0]->product_price_id}}" data-qty="" data-price="{{$value[0]->product_price}}" onclick="addCart(this);" data-action="1" type="button">
                                          <span class="add-icon"><i class="fas fa-plus"></i></span>Add
                                        </button>

                                        <div class="counter-number" id="item_counter_{{$value[0]->productsId}}">
                                          
                                            <div class="cartUpdate">

                                                <div class="value-button" id="decrease2" onclick="updateCart(this,{{$value[0]->productsId}});" value ="-" data-id="{{$value[0]->productsId}}" data-name="{{$value[0]->name}}" data-vendor_id="{{$value[0]->vendor_id}}" data-Pid="{{ $value[0]->product_price_id}}" data-qty="" data-price="{{$value[0]->product_price}}" data-action="0">-</div>

                                                <input type="number" style="width:24px" id="qty_{{$value[0]->productsId}}_{{ $value[0]->product_price_id}}" value="0" />
                                                <div class="value-button" id="increase2" onclick="updateCart(this,{{$value[0]->productsId}});" value ="+" data-id="{{$value[0]->productsId}}" data-name="{{$value[0]->name}}" data-vendor_id="{{$value[0]->vendor_id}}" data-Pid="{{ $value[0]->product_price_id}}" data-qty="" data-price="{{$value[0]->product_price}}" data-action="1">+</div>
                                            </div>
                                        </div>
                                      @else
                                        <button data-toggle="modal" id="customize_btn_{{$value[0]->productsId}}" data-target="#customize_{{$value[0]->productsId}}"><spna class="add-icon"><i class="fas fa-plus"></i></spna>Add</button>
                                        <span class="customisable">Customisable</span>
 <!-- customize-->
    <div class="modal fade customize-model" id="customize_{{$value[0]->productsId}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content" style="background-color: #fff;padding: 1rem;text-align:left;">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <div class="customize-details">
                  <div class="custom-headind">
                      <h2>
                        <span class="custom-icons"></span>
                        Customize “{{ ucfirst($value[0]->name)}}”
                      </h2>
                      <!-- <div class="custoom-price">
                        &#8377 700
                      </div> -->
                  </div>
                  <div class="custom-items">
                      {{-- <h3><span class="star">*</span>Choose Sugar<span class="requaired">(required)</span></h3> --}}
                    @foreach($value as $vk => $v)
                      <div class="input-radio">
                        <span class="custom-icons"></span>
                        <span class="radio-title">
                            {{$v->product_qty}} {{$v->attributes_name}}</span>
                            <span>&#8377 {{$v->product_price}}</span>
                            <div class="add-food">
                              <button id="cart_btn_{{$v->productsId}}_{{ $v->product_price_id}}" data-id="{{$v->productsId}}" data-name="{{$v->name}}" data-vendor_id="{{$v->vendor_id}}" data-Pid="{{ $v->product_price_id}}" data-qty="" data-price="{{$v->product_price}}" onclick="addCart(this);" data-action="1" type="button">
                                          <span class="add-icon"><i class="fas fa-plus"></i></span>Add
                                        </button>
                               <div class="counter-number" id="item_counter_{{$v->productsId}}_{{ $v->product_price_id}}">
                                    <div class="cartUpdate">

                                        <div class="value-button" id="decrease2" onclick="updateCart(this,{{$v->productsId}});" value ="-" data-id="{{$v->productsId}}" data-name="{{$v->name}}" data-vendor_id="{{$v->vendor_id}}" data-Pid="{{ $v->product_price_id}}" data-qty="" data-price="{{$v->product_price}}" data-action="0">-</div>
                                        <input type="number" style="width:24px" id="qty_{{$v->productsId}}_{{ $v->product_price_id}}" value="0" />
                                        <div class="value-button" id="increase2" onclick="updateCart(this,{{$v->productsId}});" value ="+" data-id="{{$v->productsId}}" data-name="{{$v->name}}" data-vendor_id="{{$v->vendor_id}}" data-Pid="{{ $v->product_price_id}}" data-qty="" data-price="{{$v->product_price}}" data-action="1">+</div>

                                    </div>
                                </div>
                            </div>
                      </div>
                    @endforeach
                      
                  </div>
                 {{--  <div class="custom-steps">
                      continue
                  </div> --}}
              </div>
        </div>
      </div>
    </div>
 <!-- customize End-->
                                      @endif
                                    </div>
                                </div>
                     @endforeach

                            </div>
                    @endforeach 
                    
                        </div>
{{-- cart section --}}
                        <div class="col-lg-4 col-md-12 vender-cart" id="cartDiv">
                                    
                        </div>
{{-- cart section  END--}}
                    </div>
                  </div>
  {{-- MENU TAB End--}}  

  <div id="overview" class="container vender-tab tab-pane fade">
                        <div class="row">
                            <div class="col-lg-8 col-md-8 col-sm-12">
                                <h3>People Say This Place Is Known For</h3>
                                <p>Customizable Food, Fresh Food, Elaborate Menu, Good Quality</p>
                                <h3>Average Cost</h3>
                                <p>₹350 for two people (approx.)
                                </p>
                                <p>Exclusive of applicable taxes and
                                    charges
                                </p>
                                <p>Cash only Digital
                                    payments
                                    accepted</p>
                                <h3>More Info</h3>
                                <ul class="ov-list">
                                    <li><span class="ov-icons"><i class="fas fa-badge-check"></i></span>Home Delivery
                                    </li>
                                    <li><span class="ov-icons"><i class="fas fa-badge-check"></i></span>Outdoor Seating
                                    </li>
                                    <li><span class="ov-icons"><i class="fas fa-badge-check"></i></span>OUR SPONSORS
                                    </li>
                                </ul>
                            </div>
                        </div>

                    </div>
                    <div id="reviews" class="container vender-tab tab-pane fade"><br>
                        <div class="row">
                            <div class="col-lg-8 col-md-8 col-sm-12">
                                <div class="reviews">
                                    <h2>Touch Of Taste Reviews</h2>
                                    <div class="review-comments">
                                        <div class="rv-img"><img src="images/overview-img/person-icon-1675.png" alt="">
                                        </div>
                                        <div class="rv-dtl">
                                            <h3>Rohit Jayant</h3>
                                            <div class="rv-icons">
                                                <i class="fas fa-star"></i>
                                                <span class="rv-rate">5.0</span>
                                                <span class="rv-date">6 days ago</span>
                                            </div>
                                        </div>
                                        <div class="rv-comments">
                                            <p>simply dummy text of the printing and typesetting industry. Lorem Ipsum
                                                has been the industry's standard dummy text ever since the 1500s</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-12">
                                <div class="reviews">
                                    <h2>Touch Of Taste Reviews</h2>
                                    <div class="review-comments">
                                        <div class="rv-img"><img src="images/overview-img/person-icon-1675.png" alt="">
                                        </div>
                                        <div class="rv-dtl">
                                            <h3>Rohit Jayant</h3>
                                            <div class="rv-icons">
                                                <i class="fas fa-star"></i>
                                                <span class="rv-rate">5.0</span>
                                                <span class="rv-date">6 days ago</span>
                                            </div>
                                        </div>
                                        <div class="rv-comments">
                                            <p>simply dummy text of the printing and typesetting industry. Lorem Ipsum
                                                has been the industry's standard dummy text ever since the 1500s</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
        </div>

@endsection
