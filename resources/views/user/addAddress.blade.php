@extends('layouts.app')
@section('script')
@endsection
@section('content')
<div class="container" style="">
    <div class="main-body">
    
          <div class="row gutters-sm">
            {{-- User Nav --}}
            @include('user.nav')
            {{-- User Nav End --}}
            <div class="col-md-8 tab-content px-3 px-sm-0" id="nav-tabContent">
            	
				
				
                <div class="card mb-3">
                    <div class="card-body" style="margin:1rem;">
                    <form method="GET" id="form1" action="{{route('order.confirm')}}">
                @csrf 
                      <div class="form-row">
                        <div class="form-group col-md-6">
                                <label for="" class="">Full Name</label>
                                <input type="text" required class="form-control" name="name" value="" placeholder="Save as like Home,Office">
                        </div>                             
                       <div class="form-group col-md-6">
                            <label for="" class="">Mobile</label>
                            <input type="text" required class="form-control" name="mobile" value="" placeholder="Mobile">
                       </div>
                       <div class="form-group col-md-12">
                            <label for="" class="">Address</label>
                            <input type="text" required class="form-control" name="address" value="" placeholder="Address">
                       </div>
                       <div class="form-group col-md-6">
                            <label for="" class="">landmark</label>
                            <input type="text"  class="form-control" name="landmark" value="" placeholder="landmark">
                       </div>
                        <div class="form-group col-md-6">
                            <label for="" class="">City</label>
                            <input type="text" required class="form-control" name="city" value="" placeholder="City">
                       </div>
                       <div class="form-group col-md-6">
                            <label for="" class="">State</label>
                            <input type="text" required class="form-control" name="state" value="" placeholder="State">
                       </div>
                      </div>
                    </form>
                    </div>
                    </div>
                  </div>
        

      {{-- ///////////////////////////// --}}        
             
            </div>
          </div>
        </div>
    </div>
@endsection

@section('style')
<style type="text/css">
.nav-tabs .nav-item{
	text-align: left;
}
.nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active {
    color: #495057;
    background-color: #f8e7ddcc;
    border-color: #dee2e6 #dee2e6 #dee2e6;
    /*border: 1px solid gray;*/
    font-size: 
}
.nav-tabs .nav-link {
   
    border: 1px solid transparent;
    border-radius: .25rem; 
    /* border-top-right-radius: .25rem; */
}
.main-body {
	
    padding: 15px;
}
.card {
    box-shadow: 0 1px 3px 0 rgba(0,0,0,.1), 0 1px 2px 0 rgba(0,0,0,.06);
}

.card {
    position: relative;
    display: flex;
    flex-direction: column;
    min-width: 0;
    word-wrap: break-word;
    background-color: #fff;
    background-clip: border-box;
    border: 0 solid rgba(0,0,0,.125);
    border-radius: .25rem;
}

.card-body {
    flex: 1 1 auto;
    min-height: 1px;
    padding: 1rem;
}

.gutters-sm {
    margin-right: -8px;
    margin-left: -8px;
}

.gutters-sm>.col, .gutters-sm>[class*=col-] {
    padding-right: 8px;
    padding-left: 8px;
}
.mb-3, .my-3 {
    margin-bottom: 1rem!important;
}

.bg-gray-300 {
    background-color: #e2e8f0;
}
.h-100 {
    height: 100%!important;
}
.shadow-none {
    box-shadow: none!important;
}
</style>
@endsection