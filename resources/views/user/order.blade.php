@extends('layouts.app')
@section('script')
@endsection
@section('content')
<div class="container" style="">
    <div class="main-body">
    
          <div class="row gutters-sm">
            {{-- User Nav --}}
            @include('user.nav')
            {{-- User Nav End --}}
            <div class="col-md-8 tab-content px-3 px-sm-0" id="nav-tabContent">
            	
					
				

				<div class="card mb-3">
                		<div class="card-body" style="margin:1rem;">
                	@if(count($data)!=0)
                			@foreach($data as $key => $aval)
                			 <a href="{{ route('user.orders_details',$aval->order_id)}}" style="text-decoration-line: unset;">
		                    <div class="row text-secondary" style="padding:10px;margin-bottom: 4px;border: 1px solid gray;">
		                     
		                    	<div class="col-sm-6" >
			                      <b>{{$aval->masters_name}}   ({{$aval->vendor_company_name}})</b><br>
			                       
                            <small>ORDER #{{$aval->order_id}} | Total Item: {{$aval->items}} | {{ date('d M y', strtotime($aval->created_at)) }}{{-- Sun, Nov 11, 2018, 2:16 PM --}}</small>                     
		                      	</div>
		                      	
		                      	<div class="col-sm-2" >
			                      <b>&#8377;{{$aval->total_amount}}</b>                      
		                      	</div>
		                      	<div class="col-sm-4" >
                        @if($aval->status==0)
                            <b class="status secondary">Pending</b>
                           
                        @endif
                        @if($aval->status==11)
                            <b class="status secondary">Suggest</b>
                        @endif
                        @if($aval->status==1)
                            <b  class="status primary">Confirm</b>
                        @endif
                        @if($aval->status==2)
                            <b  class="status warning">Baken/Packing</b>
                        @endif
                        @if($aval->status==3)
                            <b  class="status warning">Dispatch</b>
                        @endif
                        @if($aval->status==4)
                            <b  class="status primary">Delivered</b>
                            <small>Your item has been delivered</small>
                        @endif
                        @if($aval->status==8)
                            <b  class="status secondary">Return</b>
                        @endif
                        @if($aval->status==9)
                            <b  class="status secondary">Exchange</b>
                        @endif
                        @if($aval->status==5)
                            <b  class="status text-danger">Cancelled</b>
                        @endif
                        @if($aval->status==6)
                            <b  class="status text-danger">Cancelled</b>
                        @endif
                        @if($aval->status==7)
                            <b  class="status text-danger">Cancelled</b>
                        @endif
			                     {{--  <b>Delivered on Oct 22, 2020</b><br>
			                      <small>Your item has been delivered</small>       --}}               
		                      	</div>
		                      
		                    </div></a>
		                    @endforeach
		                    	
		                    @else
		                    	<div align="center" style="text-align: center;margin-top: 5rem;margin-bottom: 5rem;color:black;">
	                				<b>No Orders<br>
								          You have not order any product yet!</b>
	                			</div>
		                    @endif
                			
                		</div>
                	</div>
				
				
					
        

      {{-- ///////////////////////////// --}}        
             
            </div>
          </div>
        </div>
    </div>
@endsection

@section('style')
<style type="text/css">
.nav-tabs .nav-item{
	text-align: left;
}
.nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active {
    color: #495057;
    background-color: #f8e7ddcc;
    border-color: #dee2e6 #dee2e6 #dee2e6;
    /*border: 1px solid gray;*/
    font-size: 
}
.nav-tabs .nav-link {
   
    border: 1px solid transparent;
    border-radius: .25rem; 
    /* border-top-right-radius: .25rem; */
}
.main-body {
	
    padding: 15px;
}
.card {
    box-shadow: 0 1px 3px 0 rgba(0,0,0,.1), 0 1px 2px 0 rgba(0,0,0,.06);
}

.card {
    position: relative;
    display: flex;
    flex-direction: column;
    min-width: 0;
    word-wrap: break-word;
    background-color: #fff;
    background-clip: border-box;
    border: 0 solid rgba(0,0,0,.125);
    border-radius: .25rem;
}

.card-body {
    flex: 1 1 auto;
    min-height: 1px;
    padding: 1rem;
}

.gutters-sm {
    margin-right: -8px;
    margin-left: -8px;
}

.gutters-sm>.col, .gutters-sm>[class*=col-] {
    padding-right: 8px;
    padding-left: 8px;
}
.mb-3, .my-3 {
    margin-bottom: 1rem!important;
}

.bg-gray-300 {
    background-color: #e2e8f0;
}
.h-100 {
    height: 100%!important;
}
.shadow-none {
    box-shadow: none!important;
}
</style>
@endsection