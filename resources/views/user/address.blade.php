@extends('layouts.app')
@section('script')
@endsection
@section('content')
<div class="container" style="">
    <div class="main-body">
    
          <div class="row gutters-sm">
            {{-- User Nav --}}
            @include('user.nav')
            {{-- User Nav End --}}
            <div class="col-md-8 tab-content px-3 px-sm-0" id="nav-tabContent">
            	
					<div class="card mb-3">
                		<div class="card-body" style="margin:1rem;">
                			@if($data)
                			@foreach($data as $key => $aval)
		                    <div class="row text-secondary" >
		                    	<div class="col-sm-12" style="padding:10px;margin-bottom: 4px;border: 1px solid gray;">
			                      <b>{{$aval->name}}   ({{$aval->mobile}})</b><br>
			                      {{$aval->address}},{{$aval->city}}
			                        @if($aval->states) ,{{$aval->states}} @endif
	                         		@if($aval->pincode) <b>({{$aval->pincode}})</b>@endif
			                      @if($aval->lankmark) Landmark:{{$aval->lankmark}} @endif
		                      	</div>
		                    </div>
		                    @endforeach
		                    	<div align="center" style="text-align: center;margin-top: 1rem;margin-bottom: 5rem;">
                          <button class="btn btn-md btn-info">Add New Address</button>
                        </div>
		                    @else
                        <div align="center" style="text-align: center;margin-top: 5rem;margin-bottom: 5rem;">
                          <b>No Address<br>
                           You have not add any address yet!</b>
                        </div>
		                    @endif
                		</div>
                	</div>
				

			

      {{-- ///////////////////////////// --}}        
             
            </div>
          </div>
        </div>
    </div>
@endsection

@section('style')
<style type="text/css">
.nav-tabs .nav-item{
	text-align: left;
}
.nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active {
    color: #495057;
    background-color: #f8e7ddcc;
    border-color: #dee2e6 #dee2e6 #dee2e6;
    /*border: 1px solid gray;*/
    font-size: 
}
.nav-tabs .nav-link {
   
    border: 1px solid transparent;
    border-radius: .25rem; 
    /* border-top-right-radius: .25rem; */
}
.main-body {
	
    padding: 15px;
}
.card {
    box-shadow: 0 1px 3px 0 rgba(0,0,0,.1), 0 1px 2px 0 rgba(0,0,0,.06);
}

.card {
    position: relative;
    display: flex;
    flex-direction: column;
    min-width: 0;
    word-wrap: break-word;
    background-color: #fff;
    background-clip: border-box;
    border: 0 solid rgba(0,0,0,.125);
    border-radius: .25rem;
}

.card-body {
    flex: 1 1 auto;
    min-height: 1px;
    padding: 1rem;
}

.gutters-sm {
    margin-right: -8px;
    margin-left: -8px;
}

.gutters-sm>.col, .gutters-sm>[class*=col-] {
    padding-right: 8px;
    padding-left: 8px;
}
.mb-3, .my-3 {
    margin-bottom: 1rem!important;
}

.bg-gray-300 {
    background-color: #e2e8f0;
}
.h-100 {
    height: 100%!important;
}
.shadow-none {
    box-shadow: none!important;
}
</style>
@endsection