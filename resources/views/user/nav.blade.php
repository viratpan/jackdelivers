<div class="col-md-4 mb-3">
              <div class="card">
                <div class="card-body">
                  <div class="d-flex flex-column align-items-center text-center">
                    {{-- <img src="https://bootdey.com/img/Content/avatar/avatar7.png" alt="Admin" class="rounded-circle" width="150"> --}}
                    <div class="mt-3">
                      
                      <p class="text-secondary mb-1">
                      	 <img src="https://bootdey.com/img/Content/avatar/avatar7.png" alt="Admin" class="rounded-circle" width="50">
                      	Hello, <b>{{ ucfirst(Auth::user()->name) }}</b>
                      </p>
                      {{-- <p class="text-muted font-size-sm">Bay Area, San Francisco, CA</p>
                      <button class="btn btn-primary">Follow</button>
                      <button class="btn btn-outline-primary">Message</button> --}}
                    </div>
                  </div>
                </div>
              </div>
              <div class="card mt-3">
              	<nav style="border-bottom: 1px solid #8080809e;"> 
						<div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist" style="width: 100%;display:inline-grid;">
							
							<a class="nav-item nav-link {!! (Route::is('user.profile') ? 'active' : '') !!}" id="nav-profile-tab"  href="{{ route('user.profile')}}" >Profile Information</a>

              <a class="nav-item nav-link {!! (Route::is('user.address') ? 'active' : '') !!}" id="nav-address-tab"  href="{{ route('user.address')}}" >Manage Addresses</a>

              <a class="nav-item nav-link {!! ((Route::is('user.orders') || Route::is('user.orders_details')) ? 'active' : '') !!}" id="nav-order-tab"  href="{{ route('user.orders')}}" >My Orders</a>

							<a class="nav-item nav-link {!! (Route::is('user.reviews') ? 'active' : '') !!}" id="nav-contact-tab"  href="{{ route('user.reviews')}}" >My Reviews & Ratings</a>

						{{-- 	<a class="nav-item nav-link {!! (Route::is('user.profile') ? 'active' : '') !!}" id="nav-noti-tab"  href="{{ route('user.profile')}}" >All Notifications</a> --}}

							<a class="nav-item nav-link {!! (Route::is('user.coupons') ? 'active' : '') !!}" id="nav-coupon-tab"  href="{{ route('user.coupons')}}" >My Coupons</a>
              
              <a class="nav-item nav-link {!! (Route::is('user.invite') ? 'active' : '') !!}" id="nav-invite-tab"  href="{{ route('user.invite')}}" >Invite your friends</a>
              
               <a class="nav-item nav-link" href="{{ route('logout') }}" style="text-align:center;background-color: #e30f0fbf;color: #fff; width:30%;margin-left:15px;margin-bottom:15px;margin-top :15px; border-radius:5px;border-bottom:none!important"
                  onclick="event.preventDefault();
                  document.getElementById('logout-form').submit();"><i class="uil uil-lock-alt icon__1"></i>
               {{ __('Logout') }}
               </a>
               <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                  @csrf
               </form>
                        
						</div>
					</nav>
               
              </div>
            </div>