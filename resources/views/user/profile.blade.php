@extends('layouts.app')
@section('script')
@endsection
@section('content')
<div class="container" >
    <div class="main-body">
    
          <div class="row gutters-sm">
            {{-- User Nav --}}
            @include('user.nav')
            {{-- User Nav End --}}
            <div class="col-md-8 tab-content px-3 px-sm-0" id="nav-tabContent">
            	{{-- {{ print_r($data) }} --}}
					<div class="card mb-3">
                		<div class="card-body" >
                			<div class="row">
			                    <div class="col-sm-2 col-4">
			                      <h6 class="mb-0">Full Name :</h6>
			                    </div>
			                    <div class="col-sm-10 col-8 text-secondary">
			                      {{$data->name}}
			                    </div>
			                  </div><hr>
			                  <div class="row">
			                    <div class="col-sm-2 col-4">
			                      <h6 class="mb-0">Email :</h6>
			                    </div>
			                    <div class="col-sm-9 col-8 text-secondary">
			                      {{$data->email}}
                                  @if($data->email_verified_at)<span class="float-right text-success">verified</span> @endif
			                    </div>
			                  </div><hr>
			                  <div class="row">
			                    <div class="col-sm-2 col-4">
			                      <h6 class="mb-0">Mobile</h6>
			                    </div>
			                    <div class="col-sm-10 col-8 text-secondary">
			                      {{$data->mobile}}
                                  <span class=" text-success">verified</span>
			                    </div>
			                  </div><hr>
                              <div class="row">
                                <div class="col-sm-2 col-4">
                                  <h6 class="mb-0">Wallet Ammount :</h6>
                                </div>
                                <div class="col-sm-10 col-8 text-secondary">
                                  &#8377;{{$data->ammount}}
                                  
                                </div>
                              </div><hr>
			                 @if(Session::get('address'))
			                 	<div class="row">
			                    <div class="col-sm-3 col-4">
			                      <h6 class="mb-0">Current Address :</h6>
			                    </div>
			                    <div class="col-sm-9 col-8 text-secondary">
			                      {{Session::get('address')}}
			                    </div>
			                  </div>
		                        
		                     @endif
			                  
                		</div>
                	</div>
					
					
        

      {{-- ///////////////////////////// --}}        
             
            </div>
          </div>
        </div>
    </div>
@endsection

@section('style')

@endsection