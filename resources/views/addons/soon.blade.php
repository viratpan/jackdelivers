@extends('layouts.app')
@section('script')
@endsection

@section('content')
    <section class="about-section">
            <div class="container-fluid">
                <div class="row">
                    <div class="col md-12" align="center">
                        <h1 class="about-heading">
                                <strong>{{ $master->name }}</strong>
                        </h1>                        
                        <img src="{{asset('images/coming.gif')}}" class="rounded" width="370px">
                    </div>
                </div>
            </div>
        </section>
@endsection