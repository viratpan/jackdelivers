@extends('layouts.app')
@section('script2')

  <script type="text/javascript">

    var setLocationurl="{{route('location.set')}}";
    var home="{{route('home')}}";
  </script>
 <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
 <script src="{{ asset('js/other/location.js') }}" defer></script>
 <script
      src="https://maps.googleapis.com/maps/api/js?key={{ config('app.map_key') }}&callback=initMap&libraries=places&v=weekly"
      defer
    ></script>

@endsection

@section('content')
    <div class="about">
        <div class="container">
            <div class="row" align="center">
                {{-- <p>Home ->About</p> --}}
                <div class="col-6 row" style="text-align: left;">
                    <h3>Set Location</h3>
                    <p>Order food, groceries, fruits & vegetables and medicines from favourite restaurants/shop near you.</p>
                    @if(Session::get('address')) 
                        <span>{{Session::get('address')}}</span><br>
                     @endif
                    <div class="input-group mb-3" style="margin-bottom:3rem!important;">
                     
                      <input type="text" id="autocomplete"  class="form-control" placeholder="Enter your delivery location" aria-label="Enter your delivery location" aria-describedby="basic-addon2" >
                      <div class="input-group-append" >
                       
                       <button class="btn btn-outline-secondary form-control" onclick="geolocate()" type="button">Locate Me</button>
                      </div>
                    </div>                   
                    
               </div>
                <div class="col-6 row">
                   {{--  <h3>Top Picks for You</h3>

                    @php  $master=DB::table('masters')->get(); @endphp
                    @foreach($master as $mkey => $mvalue)
                        <div class="col-md-3">
                            <img src="{{ asset($mvalue->image) }}">
                        </div>
                     @endforeach
                     --}}
                </div>
            </div>
        </div>
    </div>
    
    {{-- <div class="app-store">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <img src="{{ asset('assets/images/mobile-screenshot.png') }}" style="width:200px;height:300px;margin-left:200px;">
                    <img src="{{ asset('assets/images/app-store.png') }}">
                    <img src="{{ asset('assets/images/play-store.png') }}">
                </div>
            </div>
        </div>
    </div> --}}

@endsection
@section('style')
<style type="text/css">
#locationField,
#controls {
  position: relative;
  width: 480px;
}

/*#autocomplete {
  position: absolute;
  top: 0px;
  left: 0px;
  width: 99%;
}*/

.label {
  text-align: right;
  font-weight: bold;
  width: 100px;
  color: #303030;
  font-family: "Roboto", Arial, Helvetica, sans-serif;
}

#address {
  border: 1px solid #000090;
  background-color: #f0f9ff;
  width: 480px;
  padding-right: 2px;
}

#address td {
  font-size: 10pt;
}

.field {
  width: 99%;
}

.slimField {
  width: 80px;
}

.wideField {
  width: 200px;
}

#locationField {
  height: 20px;
  margin-bottom: 2px;
}
</style>
@endsection