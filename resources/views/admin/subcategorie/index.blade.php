@extends('layouts.admin')
@section('scripts')
 
  <script type="text/javascript">
    $(document).ready(function () {
      //$('.select2').select2();
    });
  </script>
 
  <script type="text/javascript">
    $(document).ready(function () {
      $.ajaxSetup({
           headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
       });
/*Aadhar file Upload Start*/
      $('#aadharFile').change(function() { 
        var image=$(this).val();
       
        if(image){
            var postData=new FormData();
            postData.append('file',this.files[0]);     
            var url="{{route('ajax.file.upload','subcategory')}}";     
            $.ajax({               
                async:true,
                type:"post",
                url:url,
                data:postData,
                cache:false,
                contentType: false,
                processData: false,
                success:function(data){
                  console.log(data);
                  if(data.path){
                    $('#aadharupload').val(data.path);
                    toastr.success(''+data.message, { closeButton: !0 });
                  }else{
                    toastr.error(''+data.message, { closeButton: !0 });
                  }
                },error: function(xhr, status, error) {
                  //console.log(xhr.responseJSON.errors.file);
                  var err=xhr.responseJSON.errors.file;
                   $.each(err,function(key,value){
                        toastr.error(value, { closeButton: !0 });
                      
                  });
                  
                }  
     
            });
        }
      }); 
/*Aadhar file Upload End*/


    });
  
</script>
@endsection


@section('content')


<div class="card ">
  <div class="card-header">
    <div>
        <form method="{{ isset($stateData) ? 'PUT': "POST"}}" action="{{ route('admin.subcategory') }}{{ isset($stateData) ? '/update' : "/store"}}{{ isset($stateData) ? '/'.$stateData->id : ""}}">
          @csrf
          <div class="form-row">
              <div class="form-group col-md-3">
                  <label for="inputEmail4">Sub Category Name</label>
                  <input type="text" value="{{ old('name') }}{{ isset($stateData) ? $stateData->name : ""}}" name="name" class="form-control" required placeholder="Sub-Category Name">
              </div>
               <div class="form-group col-md-3">
                  <label for="inputEmail4">Under Category</label>                 
                  <select value="{{ old('categry_id') }}" name="categry_id" class="form-control" required >
                    <option value="">Select Category</option>
                    @foreach($categry as $svalue)
                      <option value="{{ $svalue->id }}" {{ isset($stateData) ? ( $stateData->categry_id==$svalue->id ? "Selected":"" ) : ""}}>{{ $svalue->name }}</option>
                    @endforeach
                  </select>
              </div>
              <div class="form-group col-md-3">
                <label for="inputPassword4">Image</label>               
                <input type="file" accept="image/png, image/jpeg" class="form-control" id="aadharFile">
                <input type="hidden"  value="{{ isset($stateData) ? $stateData->image : ""}}" class="form-control" id="aadharupload" name="image">
              </div>
              <div class="form-group col-md-3 float-right" style="padding-top: 1.55rem;">
                <label for="inputEmail4"></label>
                  <button type="submit" class="btn btn-primary ">Submit</button>
                  @if(isset($stateData))
                  <a href="{{ route('admin.subcategory') }}" class="btn btn-info ">Cancel</a>
                  @endif

              </div>
          </div>                 
          
      </form>
    </div>

  </div>
  <div class="card-body">
    <?php //echo"<pre>",print_r($data),"</pre>";?>
    <div class="table-responsive">
        <table id="data_table" class="table">
            <thead>
                <tr>
                    <th>Sn.</th>                    
                    <th>Sub Category Name</th> 
                    <th>Under Category</th>
                    <th>Under Vertical</th>                    
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($data as $key => $uData)
                <tr>
                    <td>{{ $key+1 }}</td>
                    <td>{{ $uData->name }}
                     @if($uData->image)
                     <img  src="{{ asset('').$uData->image }}"  class="" style="height: 2rem;padding-top: 2px;">
                     @endif
                    </td>
                    <td>{{ $uData->states_name  }}</td>
                    <td>{{ $uData->master_name }}</td>
                    <td>
                        

                            <a class="btn btn-primary" href="{{ route('admin.subcategory.edit',$uData->id) }}">Edit</a>
                           
                      
                    </td>
                </tr>
            @endforeach    
                
            </tbody>
            {{-- <tfoot>
                <tr>
                  
                    <th>Name</th>
                    <th>Category</th>
                   
                </tr>
            </tfoot> --}}
        </table>

        
    </div>
  </div>
  <div class="card-footer text-muted">

  </div>
</div>
	
   @endsection
