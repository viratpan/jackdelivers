@extends('layouts.admin')
@section('scripts')
 
  <script type="text/javascript">
    $(document).ready(function () {
      //$('.select2').select2();
    });
  </script>
 
  <script type="text/javascript">
    $(document).ready(function () {
      $.ajaxSetup({
           headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
       });
/*Aadhar file Upload Start*/
      
/*Aadhar file Upload End*/


    });
  
</script>
@endsection


@section('content')


<div class="card ">
  <div class="card-header">
    <div>
        <form method="{{ isset($stateData) ? 'PUT': "POST"}}" action="{{ route('admin.commission') }}{{ isset($stateData) ? '/update' : "/store"}}{{ isset($stateData) ? '/'.$stateData->id : ""}}">
          @csrf
          <div class="form-row">
              <div class="form-group col-md-4">
                  <label for="inputEmail4">Commission Name</label>
                  <input type="text" value="{{ old('name') }}{{ isset($stateData) ? $stateData->name : ""}}" name="name" class="form-control" required placeholder="Commission Name">
              </div>
              <div class="form-group col-md-2">
                  <label for="inputEmail4">Commission (%)</label>
                  <input type="number" step=".01" value="{{ old('commission') }}{{ isset($stateData) ? $stateData->commission : ""}}" name="commission" class="form-control" required placeholder="Commission ">
              </div>
              
              <div class="form-group col-md-3 float-right" style="padding-top: 1.55rem;">
                <label for="inputEmail4"></label>
                  <button type="submit" class="btn btn-primary ">Submit</button>
                  @if(isset($stateData))
                  <a href="{{ route('admin.commission') }}" class="btn btn-info ">Cancel</a>
                  @endif
              </div>
          </div>                 
          
      </form>
    </div>

  </div>
  <div class="card-body">
    
    <div class="table-responsive">
        <table id="data_table" class="table">
            <thead>
                <tr>  <th>Sn.</th>                    
                    <th>Commission Name</th>  
                     <th>Commission (%)</th>                    
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
             @foreach ($data as $key => $uData)
                <tr>
                    <td>{{ $key+1 }}</td>
                   
                   <td>{{ $uData->name }}</td>
                    <td>{{ $uData->commission }}</td>
                    <td>  
                            <a class="btn btn-primary" href="{{ route('admin.commission.edit',$uData->id) }}">Edit</a>
                      
                    </td>
                </tr>
            @endforeach    
                
            </tbody>
           {{--  <tfoot>
                <tr>
                   <th>Id</th>
                    <th>Name</th>
                    
                    <th>Action</th>
                </tr>
            </tfoot> --}}
        </table>

        
    </div>
  </div>
  <div class="card-footer text-muted">
{{--     {!! $uData->links() !!} --}}
{{--  {{ $uData->links() }} --}}
  </div>
</div>
	
   @endsection
