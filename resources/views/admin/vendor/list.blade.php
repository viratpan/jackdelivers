@extends('layouts.admin')
@section('style')
  <style type="text/css">
      .pagination{float: right;}
  </style>
@endsection
@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    
} );
</script>
@endsection

@section('content')
<div class="card ">
  <div class="card-header">
    <h5 align="left">Vendor Detail
      <a class="btn btn-sm btn-info float-right" style="" href="{{ route('admin.add-vendor') }}"><b>+ Add Vendor</b></a>
    </h5>

  </div>
  <div class="card-body">
    <div class="table-responsive">
        <table id="data_table" class="table">
            <thead>
                <tr><th>Sn.</th>
                   
                    <th>Date</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Mobile/Whatsapp</th>
                    <th>Company</th>
                    <th>Address</th>
                    <th>City</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
              <?php foreach ($data as $key => $uData): ?>

<tr>        <td>{{ $key+1 }}</td>
                    <td>{{ date('d M y', strtotime($uData->created_at)) }}</td>
                    <td>{{ $uData->name }}</i>
                    </td>
                    <td>{{ $uData->email }}</td>
                    <td>{{ $uData->mobile }}<br><i title="Whatsapp">{{ $uData->telephone }}</i></td>
                    <td>
                      {{$uData->company_name}}<br>
                        <i title="Aadhar">{{ $uData->aadhar }}</i><br>
                        <i title="PAN">{{ $uData->pan }}</i>
                    </td>
                    <td>{{ $uData->address }}<br>{{ $uData->pin }}</td>
                  <td>{{ $uData->city_name }}  </td>
                  <td>
                    {{--  <a href="{{ route('admin.show-vendor',$uData->id)}}" class="btn btn-sm btn-primary">Show</a> --}}
                     <a href="{{ route('admin.edit-vendor',$uData->id)}}" class="btn btn-sm btn-primary">Edit</a>
                  </td>
                </tr>

            <?php endforeach; ?>

            </tbody>

        </table>


    </div>
  </div>
  <div class="card-footer text-muted">
   {{--  {{ $data->links() }} --}}
        {{-- <a class="btn btn-sm btn-info float-right" style="" href="{{ route('vendor.add-vendor') }}"><b>Click here to add Products</b></a> --}}
  </div>
</div>

   @endsection
