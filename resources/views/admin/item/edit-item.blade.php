<?php $layouts='layouts.vendor'; ?>

@if(Auth::guard('admin')->check())
@php $layouts='layouts.admin' @endphp
@endif
@if(Auth::guard('vendor')->check())
 @php  $layouts='layouts.vendor' @endphp
@endif

 @extends($layouts)




@section('content')
<style type="text/css">
  .text-red{color: red;font-weight: 700;}
</style>
<div class="card ">
  <div class="card-header">
@if(Auth::guard('admin')->check())
    <h5>Item:<b> {{$data->name}}</b> 

        <a class="btn btn-sm btn-info float-right" style="" href="{{ route('admin.item-list') }}"><b>Back</b></a>
    </h5>

@endif
@if(Auth::guard('vendor')->check())
   <h5>Item:<b> {{$data->name}}</b>
        <a class="btn btn-sm btn-info float-right" style="" href="{{ route('vendor.item-list') }}"><b>Back</b></a>
    </h5>

@endif
    
  </div>
  <div class="card-body">
    @if(Auth::guard('admin')->check())
       <form method="PUT" action="{{ route('admin.update-item',$data->id) }}" enctype="multipart/form-data">
    @endif
    @if(Auth::guard('vendor')->check())
       <form method="PUT" action="{{ route('vendor.update-item',$data->id) }}" enctype="multipart/form-data">
    @endif
   
        @csrf
        <div class="form-row">
         @if(Auth::guard('admin')->check())
          <div class="form-group col-md-3">
            <label for="inputEmail4">Vendor <span class="text-red">*</span></label>
             <select id="vendor" value="" name="vendor_id" class="form-control select2" required >
                <option value="">Please Select </option>
                @foreach($vendor as $svalue)
                  <option value="{{ $svalue->id }}" {{ isset($data) ? ( $data->vendor_id==$svalue->id ? "Selected":"" ) : ""}}>{{ $svalue->name }}
                    @if(isset($svalue->company_name)) ( {{ $svalue->company_name }} ) @endif</option>
                @endforeach
              </select>
          </div>
          <div class="form-group col-md-3">
            <label for="inputEmail4">Under Vertical <span class="text-red">*</span></label>
             <select id="master" value="" name="masters_id" class="form-control select2" required >
                <option value="">Please Select </option>
                @foreach($master as $svalue)
                  <option value="{{ $svalue->id }}" {{ isset($data) ? ( $data->masters_id==$svalue->id ? "Selected":"" ) : ""}}>{{ $svalue->name }} ( {{ $svalue->name }} )</option>
                @endforeach
              </select>
          </div>
           <hr class="col-12" style="margin: 8px;">
        @endif   
        <div class="form-group col-md-3">
          <label for="inputEmail4">Category <span class="text-red">*</span></label>
           <select id="category" value="{{ $data->categry_id }}" name="categry_id" class="form-control" required >
              <option value="">Please Select</option>
              @foreach($category as $svalue)
                <option value="{{ $svalue->id }}" {{ isset($data) ? ( $data->categry_id==$svalue->id ? "Selected":"" ) : ""}}>{{ $svalue->name }}</option>
              @endforeach
            </select>
        </div>
        <div class="form-group col-md-3">
          <label for="inputEmail4">Sub Category <span class="text-red">*</span></label>
           <select id="sub_category" value="{{ $data->sub_categry_id }}" name="sub_categry_id" class="form-control" required >
              <option value="">Please Select</option>
              @foreach($sub_category as $svalue)
                <option value="{{ $svalue->id }}" {{ isset($data) ? ( $data->sub_categry_id==$svalue->id ? "Selected":"" ) : ""}}>{{ $svalue->name }}</option>
              @endforeach
            </select>
        </div>
        

        <div class="form-group col-md-6">
          <label for="inputEmail4">Name <span class="text-red">*</span></label>
          <input type="text" id="name" value="{{ $data->name }}" name="name" class="form-control" required placeholder="Name">
        </div>      

         <div class="form-group col-md-6">
          <label for="inputEmail4"> Title <span class="text-red">*</span></label>
          <input type="text" id="title" value="{{ $data->meta_title }}" name="meta_title" class="form-control" required placeholder=" Title">

          <label for="inputEmail4"> Keywords <span class="text-red">*</span></label>
          <input type="text" value="{{ $data->meta_keywords }}" name="meta_keywords" class="form-control" required placeholder=" Keywords">

          <label for="inputEmail4"> Description <span class="text-red">*</span></label>
          <input type="text" value="{{ $data->meta_description }}" name="meta_description" class="form-control" required placeholder=" Description">
        </div>       
        

        <div class="form-group col-md-6">
          <label for="inputEmail4">Description <span class="text-red">*</span></label>
          <textarea value="{{ $data->description }}" rows="6" name="description" class="form-control" required placeholder="Description">{{ $data->description }}</textarea>
        </div>

        <div class="form-group col-md-2">
                  <label for="inputEmail4">Unit</label>                 
                  <select value="{{ old('attrubute_id') }}" name="attrubute_id" class="form-control" required >
                    <option value="">Select Category</option>
                    @foreach($attributes as $svalue)
                      <option value="{{ $svalue->id }}" {{ isset($data) ? ( $data->attrubute_id==$svalue->id ? "Selected":"" ) : ""}}>{{ $svalue->name }}</option>
                    @endforeach
                  </select>
              </div>
        <div class="form-group col-md-2">
              <label for="inputEmail4">Actual Price</label>
              <input type="number"  value="{{ $data->aprice }}" name="aprice" class="form-control" required placeholder="Actual Price">
          </div>
        <div class="form-group col-md-2">
          <label for="inputPassword4">Image</label>
         
         <input type="file" accept="image/png, image/jpeg" class="form-control" id="aadharFile">
          <input type="hidden"  value="{{ $data->image }}" class="form-control" id="aadharupload" name="image">
           @if($data->image)
           <img  src="{{ asset('').$data->image }}" class="" style="height: 4rem;padding-top: 2px;">

           @else
            <span style="border: 1px solid gray;padding: 1rem; position: inherit;top: 2rem;">
                  No Image
              </span>
            @endif
        </div>
         <div class="form-group col-md-2">
          <label for="inputPassword4">Image 2</label>
         
         <input type="file" accept="image/png, image/jpeg" class="form-control" id="aadharFile2">
          <input type="hidden"  value="{{ $data->image2 }}" class="form-control" id="aadharupload2" name="image2">
           @if($data->image2)
           <img  src="{{ asset('').$data->image2 }}" class="" style="height: 4rem;padding-top: 2px;">
           @else
              <span style="border: 1px solid gray;padding: 1rem; position: inherit;top: 2rem;">
                  No Image
              </span>
            @endif
        </div>
       {{--  @if($data->image)
          <div class="form-group col-md-2">
          <img  src="{{ asset('').$data->image }}" class="" style="height: 8rem;padding-top: 2px;">
        </div>
        @endif
        @if($data->image2)
          <div class="form-group col-md-2">
          <img  src="{{ asset('').$data->image2 }}" class="" style="height: 8rem;padding-top: 2px;">
        </div>
        @endif --}}
         <div id="restutantDiv" class="form-group col-md-4 form-row">
        
         </div>
        
        

        
  
 

        
        


      </div>
      
      <div class="form-row float-right">
          <button type="submit" class="btn btn-primary ">Submit</button>
      </div>
    </form>

  </div>
  <div class="card-footer text-muted">
    {{-- {!! $uData->links() !!} --}}
  </div>
</div>
    
@endsection

@section('scripts')
  <script src="{{  asset('js/plugins/select2.min.js') }}" ></script>
  <script type="text/javascript">
    $(document).ready(function () {
      //$('#category').triggre('change');
      restaurantshtml();
    });
  </script>
 
  <script type="text/javascript">
    $(document).ready(function () {
      $.ajaxSetup({
           headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
       });
/*Aadhar file Upload Start*/
      $('#aadharFile').change(function() { 
        var image=$(this).val();
       
        if(image){
            var postData=new FormData();
            postData.append('file',this.files[0]);     
            var url="{{route('ajax.file.upload','item')}}";     
            $.ajax({               
                async:true,
                type:"post",
                url:url,
                data:postData,
                cache:false,
                contentType: false,
                processData: false,
                success:function(data){
                  console.log(data);
                  if(data.path){
                    $('#aadharupload').val(data.path);
                    toastr.success(''+data.message, { closeButton: !0 });
                  }else{
                    toastr.error(''+data.message, { closeButton: !0 });
                  }
                },error: function(xhr, status, error) {
                  //console.log(xhr.responseJSON.errors.file);
                  var err=xhr.responseJSON.errors.file;
                   $.each(err,function(key,value){
                        toastr.error(value, { closeButton: !0 });
                      
                  });
                  
                }  
     
            });
        }
      }); 
/*Aadhar file Upload End*/
/*Aadhar2 file Upload Start*/
      $('#aadharFile2').change(function() { 
        var image=$(this).val();
       
        if(image){
            var postData=new FormData();
            postData.append('file',this.files[0]);     
            var url="{{route('ajax.file.upload','item')}}";     
            $.ajax({               
                async:true,
                type:"post",
                url:url,
                data:postData,
                cache:false,
                contentType: false,
                processData: false,
                success:function(data){
                  console.log(data);
                  if(data.path){
                    $('#aadharupload2').val(data.path);
                    toastr.success(''+data.message, { closeButton: !0 });
                  }else{
                    toastr.error(''+data.message, { closeButton: !0 });
                  }
                },error: function(xhr, status, error) {
                  //console.log(xhr.responseJSON.errors.file);
                  var err=xhr.responseJSON.errors.file;
                   $.each(err,function(key,value){
                        toastr.error(value, { closeButton: !0 });
                      
                  });
                  
                }  
     
            });
        }
      }); 
/*Aadhar file Upload End*/

    });
  </script>
  <script>
@if(Auth::guard('admin')->check())
  function restaurantshtml(){ //alert('trigger to get');
    var selectedText = $("#master option:selected").html(); 
     var restaurantshtml='<div class="form-group col-md-6">'+
        '  <label for="inputEmail4">Type <span class="text-red">*</span></label>'+
        '   <select id="type" value="{{ $data->type }}" name="type" class="form-control" required >'+
        '      <option value="">Please Select </option>'+             
        '        <option value="veg" {{ isset($data) ? ( $data->type=='veg' ? "Selected":"" ) : ""}}>Veg</option>'+
        '        <option value="nonveg" {{ isset($data) ? ( $data->type=='nonveg' ? "Selected":"" ) : ""}}>Non-Veg</option>'+
        '    </select>'+
        '</div>'+
        '<div class="form-group col-md-6">'+
        '  <label for="inputEmail4">Cost Of Two <span class="text-red">*</span></label>'+
        '  <input type="number" id="costoftwo" value="{{ $data->costoftwo }}" name="costoftwo" class="form-control" required placeholder="Cost Of Two">'+
        '</div>';
     selectedText=selectedText.toLowerCase();
     if(selectedText=='restaurants'){
        $('#restutantDiv').html(restaurantshtml);
     }else{$('#restutantDiv').html('');}
  }
@endif
@if(Auth::guard('vendor')->check())
function restaurantshtml(){ //alert('trigger to get');
    var selectedText = '{{$master->name}}'; 
     var restaurantshtml='<div class="form-group col-md-6">'+
        '  <label for="inputEmail4">Type <span class="text-red">*</span></label>'+
        '   <select id="type" value="{{ $data->type }}" name="type" class="form-control" required >'+
        '      <option value="">Please Select </option>'+             
        '        <option value="veg" {{ isset($data) ? ( $data->type=='veg' ? "Selected":"" ) : ""}}>Veg</option>'+
        '        <option value="nonveg" {{ isset($data) ? ( $data->type=='nonveg' ? "Selected":"" ) : ""}}>Non-Veg</option>'+
        '    </select>'+
        '</div>'+
        '<div class="form-group col-md-6">'+
        '  <label for="inputEmail4">Cost Of Two <span class="text-red">*</span></label>'+
        '  <input type="number" id="costoftwo" value="{{ $data->costoftwo }}" name="costoftwo" class="form-control" required placeholder="Cost Of Two">'+
        '</div>';
     selectedText=selectedText.toLowerCase();
     if(selectedText=='restaurants'){
        $('#restutantDiv').html(restaurantshtml);
     }else{$('#restutantDiv').html('');}
  }
@endif

  $('#name').change(function(e) {//alert('trigger');
    var name=$(this).val();
    $.get('{{ route('ajax.product.check_slug') }}', 
      { 'name': name }, 
      function( data ) {
        $('#slug').val(data.slug);
        $('#title').val(name);
      }
    );
  });
  /*/////////////////////////////////////////////////*/
  $('#vendor').change(function(e) {
      var id=$(this).val();
      var selectedText = $("#vendor option:selected").html();
        $.get('{{ route('ajax.vendor.options') }}', 
            { 'id': id }, 
            function( res ) {
              if(res){
                  $("#master").empty();
                  $("#category").empty();
                  $("#sub_category").empty();
                    //$("#master").append('<option value="">Please Select</option>');
                    //$("#category").append('<option value="">Please Select</option>');
                    //$("#sub_category").append('<option value="">Please Select</option>');
                  $.each(res,function(key,value){
                      $("#master").append('<option value="'+value.id+'">'+value.name+'</option>');
                  });
                 // $("#master").select2();
                 $('#master').trigger('change');
              }else{
                 $("#master").empty();
                 $("#category").empty();
                 $("#sub_category").empty();                     
              }
              
              
            }
          ); 
  });

  $('#master').change(function(e) {
      var id=$(this).val();
      var selectedText = $("#master option:selected").html();
      //var selectedText = $("#category option:selected").html(); 
       var restaurantshtml='<div class="form-group col-md-6">'+
          '  <label for="inputEmail4">Type <span class="text-red">*</span></label>'+
          '   <select id="type" value="{{ old('type') }}" name="type" class="form-control" required >'+
          '      <option value="">Please Select </option>'+             
          '        <option value="veg" >Veg</option>'+
          '        <option value="nonveg" >Non-Veg</option>'+
          '    </select>'+
          '</div>'+
          '<div class="form-group col-md-6">'+
          '  <label for="inputEmail4">Cost Of Two <span class="text-red">*</span></label>'+
          '  <input type="number" id="costoftwo" value="{{ old('costoftwo') }}" name="costoftwo" class="form-control" required placeholder="Cost Of Two">'+
          '</div>';
       selectedText=selectedText.toLowerCase();
       if(selectedText=='restaurants'){
          $('#restutantDiv').html(restaurantshtml);
       }else{$('#restutantDiv').html('');}


        $.get('{{ route('ajax.master.options') }}', 
            { 'id': id }, 
            function( res ) {
              if(res){
                  $("#category").empty();
                  $("#sub_category").empty();
                     //$("#category").append('<option value="">Please Select</option>');
                     // $("#sub_category").append('<option value="">Please Select</option>');
                  $.each(res,function(key,value){
                      $("#category").append('<option value="'+value.id+'">'+value.name+'</option>');
                  });
                  $("#category").select2();
                  $('#sub_category').trigger('change');
              }else{
                 $("#category").empty();
                 $("#sub_category").empty();
                     
              }
              
              
            }
          ); 
  });



  $('#category').change(function(e) {
     var id=$(this).val();
     var selectedText = $("#category option:selected").html(); 
     
     $.get('{{ route('ajax.category.options') }}', 
      { 'id': id }, 
      function( res ) {
        if(res){
            $("#sub_category").empty();
            $.each(res,function(key,value){
                $("#sub_category").append('<option value="'+value.id+'">'+value.name+'</option>');
            });
       
        }else{
           $("#sub_category").empty();
        }
        
        
      }
    );

  });


</script>
@endsection