<?php $layouts='layouts.vendor'; ?>

@if(Auth::guard('admin')->check())
@php $layouts='layouts.admin' @endphp
@endif
@if(Auth::guard('vendor')->check())
   @php  $layouts='layouts.vendor' @endphp
@endif

 @extends($layouts)

@section('scripts')
 <script type="text/javascript">
function calculate(this_obj) {
    
    var cost=$('#item_aprice').val();
    var discount=$('#item_discount').val();
    var price=$('#item_price').val();
    var finalp=0; console.log(" calculate| "+cost+" cost|"+discount+" discount|"+price+" price|" );
   
    
    if(cost && discount && price){
        finalp=cost-(cost*discount/100);
        $('#item_price').val(finalp.toFixed(2));
    }
 
}
</script>
@endsection

@section('content')


<div class="card ">
  <div class="card-body">
@if(Auth::guard('admin')->check())
  
    <h4>Item:<b> {{$data->name}}</b> | <small>Vendor :  </small>{{ $data->vendor_name}}</h4>{{$data->categories_name}} 
            <small>( {{$data->sub_categories_name}} )</small>
@endif
@if(Auth::guard('vendor')->check())
  <h4>Item:<b> {{$data->name}}</b></h4>{{$data->categories_name}} 
            <small>( {{$data->sub_categories_name}} )</small>

@endif
      
  </div>
  <div class="card-header">
    <div>
      @if(Auth::guard('admin')->check())
          <form method="{{ isset($pricesData) ? 'PUT': 'POST'}}" action="{{ route('admin.item.manage.store',$data->productsId) }}">
      @endif
    @if(Auth::guard('vendor')->check())
       <form method="{{ isset($pricesData) ? 'PUT': 'POST'}}" action="{{ route('vendor.item.manage.store',$data->productsId) }}">
    @endif

          @csrf
          <div class="form-row">
              <div class="form-group col-md-2">
                  <label for="inputEmail4">Quantity</label>
                  <input type="text" step=".0001" value="{{ old('qty') }}{{ isset($pricesData) ? $pricesData->qty : ''}}" name="qty" class="form-control" required placeholder="Quantity">
                  <input type="hidden" value="{{ old('id') }}{{ isset($pricesData) ? $pricesData->id : ''}}" name="id" class="form-control"  placeholder="id">
              </div>
               <div class="form-group col-md-2" style="margin-top: 1.75rem;">                                
                  {{$data->attributes_name}}
              </div>
              <div class="form-group col-md-2">
                  <label for="inputEmail4">Actual Price</label>
                  <input type="number" step=".0001" id="item_aprice" onblur="calculate(this);" value="{{ old('cost') }}{{ isset($pricesData) ? $pricesData->cost : $data->aprice}}" name="cost" class="form-control" required placeholder="Price">
                  
              </div>
              <div class="form-group col-md-2">
                  <label for="inputEmail4"> Discount % </label>
                  <input type="number" step=".0001" id="item_discount" onblur="calculate(this);"  value="{{ old('discount') }}{{ isset($pricesData) ? $pricesData->discount : 0}}" name="discount" class="form-control" required placeholder="Discount ">
              </div>
              <div class="form-group col-md-2">
                  <label for="inputEmail4">Sale Price</label>
                  <input type="number" step=".0001" id="item_price" value="{{ old('price') }}{{ isset($pricesData) ? $pricesData->price : 0}}" name="price" class="form-control" required placeholder="Price">
              </div>
              


              <div class="form-group col-md-2 float-right" style="padding-top: 1.55rem;">
                <label for="inputEmail4"></label>
                  <button type="submit" class="btn btn-primary ">Submit</button>
                  @if(isset($pricesData))
                    @if(Auth::guard('admin')->check())
                         <a href="{{ route('admin.item.manage',$data->productsId) }}" class="btn btn-info ">Cancel</a> 
                      @endif
                      @if(Auth::guard('vendor')->check())
                        <a href="{{ route('vendor.item.manage',$data->productsId) }}" class="btn btn-info ">Cancel</a> 

                      @endif 
                  
                  @endif
              </div>
          </div>                 
          
      </form>
    </div>

  </div>
  <div class="card-body">
    <div class="table-responsive">
        <table id="data_table" class="table">
            <thead>
                <tr>
                    <th>Quantity</th>
                    <th>Actual Price</th>
                    <th>Discount</th> 
                    <th>Sale Price</th>                                                           
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($prices as $uData)
                <tr>
                    <td>{{ $uData->qty }}  {{$data->attributes_name}}</td>
                    <td>{{ $uData->cost  }}</td>
                    <td>{{ $uData->discount }}</td>
                    <td>{{ $uData->price }}</td>
                    
                    <td>
                      @if(Auth::guard('admin')->check())
                         <a href="{{ route('admin.item.manage',['id' => $data->productsId, 'pid' => $uData->id]) }}" class="btn btn-sm btn-primary">Edit</a>
                      @endif
                      @if(Auth::guard('vendor')->check())
                        <a href="{{ route('vendor.item.manage',['id' => $data->productsId, 'pid' => $uData->id]) }}" class="btn btn-sm btn-primary">Edit</a>

                      @endif  
                       
                        
                    </td>
                    
                </tr>
            @endforeach    
                
            </tbody>
            
        </table>

        
    </div>
  </div>
  <div class="card-footer text-muted">
       
  </div>
</div>
    
   @endsection
