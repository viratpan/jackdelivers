<html class="no-js" lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>{{ config('app.name') }} Dashboard </title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
        ============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('images/logo.png') }}">
    <!-- Google Fonts
        ============================================ -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,400i,600,700,800,900" rel="stylesheet" />
    <!-- Scripts -->
   
    <link href="{{  asset('css/themes/lite-purple.min.css') }}" rel="stylesheet" />
</head>
<div class="auth-layout-wrap" style="background-image: url(images/photo-wide-4.jpg)">
    <div class="auth-content">
        <div class="card o-hidden">
            <div class="row">
                <div class="col-md-12">
                    <div class="p-4">
                        <div class="auth-logo text-center mb-4"><img src="images/logo.png" alt=""></div>
                        <h1 class="mb-3 text-18">Forgot Password</h1>
                        <form action="">
                            <div class="form-group">
                                <label for="email">Email address</label>
                                <input class="form-control form-control-rounded" id="email" type="email">
                            </div>
                            <button class="btn btn-primary btn-block btn-rounded mt-3">Reset Password</button>
                        </form>
                        <div class="mt-3 text-center"><a class="text-muted" href="signin.html">
                                <u>Sign in</u></a></div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>