<?php $layouts='layouts.vendor'; ?>

@if(Auth::guard('admin')->check())
@php $layouts='layouts.admin' @endphp
@endif
@if(Auth::guard('vendor')->check())
   @php  $layouts='layouts.vendor' @endphp
@endif

@extends($layouts)

@section('style')
<style type="text/css">
  .text-green{
  color: green;
  font-size: x-large;
  }.text-danger{
  color: red;
  font-size: x-large;
  }
</style>
@endsection


@section('content')


<div class="card ">
  <div class="card-header">
    <div>
      @if(Auth::guard('admin')->check())
        <form method="GET" action="{{ route('admin.item-list') }}">
      @endif
      @if(Auth::guard('vendor')->check())
       <form method="GET" action="{{ route('vendor.item-list') }}">
      @endif
          
          <div class="form-row">
              <div class="form-group col-md-3">
                  <label for="inputEmail4">Search</label>
                  <input type="text" value="{{ isset($request) ? $request->search : ""}}" name="search" class="form-control"  placeholder="Text">
              </div>
              <div class="form-group col-md-2">
                  <label for="inputEmail4">Under Vertical</label>
                  <select value="{{ old('master') }}" name="master" class="form-control select2"  >
                    <option value="">Select Please</option>
                    @foreach($master as $svalue)
                      <option value="{{ $svalue->id }}" {{ isset($request) ? ( $request->master==$svalue->id ? "Selected":"" ) : ""}}>{{ $svalue->name }}</option>
                    @endforeach
                  </select>
              </div>
              <div class="form-group col-md-2">
                  <label for="inputEmail4">Category</label>
                  <select value="{{ old('category') }}" name="category" class="form-control"  >
                    <option value="">Select Category</option>
                    @foreach($categry as $svalue)
                      <option value="{{ $svalue->id }}" {{ isset($request) ? ( $request->category==$svalue->id ? "Selected":"" ) : ""}}>{{ $svalue->name }}</option>
                    @endforeach
                  </select>
              </div>
              <div class="form-group col-md-2">
                  <label for="inputEmail4">Sub-Category</label>
                  <select value="{{ old('subcategory') }}" name="subcategory" class="form-control select2"  >
                    <option value="">Select Sub-Category</option>
                    @foreach($subcategry as $svalue)
                      <option value="{{ $svalue->id }}" {{ isset($request) ? ( $request->subcategory==$svalue->id ? "Selected":"" ) : ""}}>{{ $svalue->name }}</option>
                    @endforeach
                  </select>
              </div>
              @if(Auth::guard('admin')->check())
              <div class="form-group col-md-3">
                  <label for="inputEmail4">Vendor</label>
                  <select value="{{ old('vendor') }}" name="vendor" class="form-control select2"  >
                    <option value="">Select Vendor</option>
                    @foreach($vendor as $svalue)
                      <option value="{{ $svalue->id }}" {{ isset($request) ? ( $request->vendor==$svalue->id ? "Selected":"" ) : ""}}>{{ $svalue->name }}</option>
                    @endforeach
                  </select>
              </div>

              <div class="form-group col-md-2">
                  <label for="inputEmail4">City</label>
                  <select value="{{ old('city') }}" name="city" class="form-control select2"  >
                    <option value="">Select City</option>
                    @foreach($city as $svalue)
                      <option value="{{ $svalue->id }}" {{ isset($request) ? ( $request->city==$svalue->id ? "Selected":"" ) : ""}}>{{ $svalue->name }}</option>
                    @endforeach
                  </select>
              </div>
               @endif
              
              <div class="form-group col-md-2">
                  <label for="inputEmail4">Status</label>
                  <select value="" name="status" class="form-control"  >
                    <option value="">Select Status</option>
                    <option value="0" {{ isset($request) ? ( $request->status==0 ? "Selected":"" ) : ""}}>De-Active </option>
                    <option value="1" {{ isset($request) ? ( $request->status==1 ? "Selected":"" ) : ""}}>Instock </option>
                    <option value="2" {{ isset($request) ? ( $request->status==2 ? "Selected":"" ) : ""}}>Outofstock </option>
                    </select>
              </div>
              <div class="form-group col-md-2">
                  <label for="inputEmail4">Verify</label>
                  <select value="" name="verify" class="form-control"  >
                    <option value="">Select Please</option>
                    <option value="0" {{ isset($request) ? ( $request->verify==0 ? "Selected":"" ) : ""}}>Not Verify </option>
                    <option value="1" {{ isset($request) ? ( $request->verify==1 ? "Selected":"" ) : ""}}>Verified </option>
                    </select>
              </div>
              <div class="form-group col-md-2 float-right" style="padding-top: 1.55rem;">
                <label for="inputEmail4"></label>
                  <button type="submit" class="btn btn-primary ">Submit</button>
                     @if(Auth::guard('admin')->check())
                      <a href="{{ route('admin.item-list') }}" class="btn btn-info ">Reset</a>
                     @endif
                     @if(Auth::guard('vendor')->check())
                      <a href="{{ route('vendor.item-list') }}" class="btn btn-info ">Reset</a>
                     @endif
              </div>

              
               @if(Auth::guard('admin')->check())
               <div class="form-group col-md-4 float-right" style="padding-top: 1.55rem;">
                <a class="btn btn-sm btn-info float-right" style="" href="{{ route('admin.add-item') }}"><b>+ Add Item</b></a></div>
               @endif
               @if(Auth::guard('vendor')->check())
               <div class="form-group col-md-7 float-right" style="padding-top: 1.55rem;">
                <a class="btn btn-sm btn-info float-right" style="" href="{{ route('vendor.add-item') }}"><b>+ Add Item</b></a></div>
               @endif
                  
              


          </div>                 
          
      </form>
    </div>

  </div>
  <div class="card-body">
    <?php //echo"<pre>",print_r($data),"</pre>";?>
    <div class="table-responsive">
        <table id="" class="table vtable">
            <thead>
                <tr>
                    <th>Sn.</th> 
                 @if(Auth::guard('admin')->check())
                    <th>Date</th>
                 @endif
                    <th>Name</th>
                    <th>Image</th>
                   @if(Auth::guard('admin')->check())
                    <th>Under Vertical</th>
                   @endif
                    <th>Under Category</th>
                    <th>Sub-Category</th>
                  @if(Auth::guard('admin')->check())
                    <th>Vendor Name</th>
                    <th>Vendor Email</th>
                    <th>City</th>
                    {{-- <th>Pincode</th> --}}
                  @endif
                   {{--  <th style="width:200px;">Price</th> --}}
                    <th>Verify</th>
                    <th>Status</th>
                   <th>Action</th>
                    
                </tr>
            </thead>
            <tbody>
            @foreach ($data as $key => $uData)
                <tr>
                    <td>{{ $key+1 }}</td>
                  @if(Auth::guard('admin')->check())
                    <td>{{ date('d M y', strtotime($uData->pcreated_at)) }}</td>
                  @endif
                    <td>{{ $uData->name }}</td>
                    <td>
                      @if($uData->image)
                        <img  src="{{ asset('').$uData->image }}" class="" style="height: 3rem;">
                      @else
                        No Image
                      @endif
                    </td>
                  @if(Auth::guard('admin')->check())
                    <td>{{ $uData->masters_name }}</td>
                  @endif
                    <td>{{ $uData->categories_name }}</td>
                    <td>{{ $uData->sub_categories_name }}</td>
                    @if(Auth::guard('admin')->check())
                      <td> {{$uData->vendor_name}}</td>
                      <td>{{$uData->vendor_email}}</td>
                      <td>{{ $uData->city_name}}</td>
                     {{--  <td>{{ $uData->city_name}}</td> --}}
                    @endif
                      {{-- <td style="width:200px;">
             PRice
                 
                </td> --}}
                    <td>@if($uData->verify==1) <i class="las la-check text-green"></i>  @else  <i class="las la-times text-danger"></i>   @endif </td>
                     <td>@if($uData->featured==1)
                        <small class="featured">Featured</small>
                        @endif
                        @if($uData->status==0)
                            <small class="status secondary">Deactive</small>
                        @endif
                        @if($uData->status==1)
                            <small  class="status primary">InStock</small>
                        @endif
                        @if($uData->status==2)
                            <small  class="status warning">Out Of Stock</small>
                        @endif
                    </td>
                
                <td style="display: flex;">
                        @if(Auth::guard('admin')->check())
                            <a href="{{ route('admin.edit-item',$uData->productsId)}}" class="btn btn-sm btn-primary">Edit</a>
              {{-- verify --}} 
                        @if($uData->verify==0)
                       <a href="{{ route('admin.item.verify',['id' => $uData->productsId, 'status' => 1])}}" class="btn btn-sm btn-primary">Verify</a>
                        @else
                       <a href="{{ route('admin.item.verify',['id' => $uData->productsId, 'status' => 0])}}" class="btn btn-sm btn-light">Un-Verify</a>
                      @endif
{{-- /////////////////////////////////////// --}}
                        

                        @if($uData->status==0)
                            <a class="btn btn-success  btn-sm" href="{{ route('admin.item.status',['id' => $uData->productsId, 'status' => 1]) }}">In stock</a>
                            <a class="btn btn-warning btn-sm" href="{{ route('admin.item.status',['id' => $uData->productsId, 'status' => 2]) }}">Out of stock</a>
                        @endif
                        @if($uData->status==1)
                            <a class="btn btn-light  btn-sm" href="{{ route('admin.item.status',['id' => $uData->productsId, 'status' => 0]) }}">Deactive</a>
                            <a class="btn btn-warning btn-sm" href="{{ route('admin.item.status',['id' => $uData->productsId, 'status' => 2]) }}">Out of stock</a>
                        @endif
                        @if($uData->status==2)
                           <a class="btn btn-light  btn-sm" href="{{ route('admin.item.status',['id' => $uData->productsId, 'status' => 0]) }}">Deactive</a>
                             <a class="btn btn-success  btn-sm" href="{{ route('admin.item.status',['id' => $uData->productsId, 'status' => 1]) }}">In stock</a>
                        @endif
                            @if($uData->featured==0)
                                 <a class="btn btn-success  btn-sm" href="{{ route('admin.item.featured',['id' => $uData->productsId, 'status' => 1]) }}">featured</a>
                            @endif
                            @if($uData->featured==1)
                                 <a class="btn btn-success  btn-sm" href="{{ route('admin.item.featured',['id' => $uData->productsId, 'status' => 0]) }}">Un-featured</a>
                            @endif

                             <a target="_blank" href="{{ route('admin.item.manage',$uData->productsId)}}" class="btn btn-sm btn-info">Manage</a>
                        @endif
                        @if(Auth::guard('vendor')->check())
                        <a href="{{ route('vendor.edit-item',$uData->productsId)}}" class="btn btn-sm btn-primary">Edit</a>
                        <a target="_blank" href="{{ route('vendor.item.manage',$uData->productsId)}}" class="btn btn-sm btn-info">Manage</a> 
                         @if($uData->verify==1)
                         
                          @if($uData->status==0)
                            <a class="btn btn-success  btn-sm" href="{{ route('vendor.item.status',['id' => $uData->productsId, 'status' => 1]) }}">Instock</a>
                            <a class="btn btn-warning btn-sm" href="{{ route('vendor.item.status',['id' => $uData->productsId, 'status' => 2]) }}">Out of stock</a>
                        @endif
                        @if($uData->status==1)
                            <a class="btn btn-light  btn-sm" href="{{ route('vendor.item.status',['id' => $uData->productsId, 'status' => 0]) }}">Deactive</a>
                            <a class="btn btn-warning btn-sm" href="{{ route('vendor.item.status',['id' => $uData->productsId, 'status' => 2]) }}">Out of stock</a>
                        @endif
                        @if($uData->status==2)
                           <a class="btn btn-light  btn-sm" href="{{ route('vendor.item.status',['id' => $uData->productsId, 'status' => 0]) }}">Deactive</a>
                             <a class="btn btn-success  btn-sm" href="{{ route('vendor.item.status',['id' => $uData->productsId, 'status' => 1]) }}">In stock</a>
                        @endif
                            @if($uData->featured==0)
                                 <a class="btn btn-success  btn-sm" href="{{ route('vendor.item.featured',['id' => $uData->productsId, 'status' => 1]) }}">featured</a>
                            @endif
                            @if($uData->featured==1)
                                 <a class="btn btn-success  btn-sm" href="{{ route('vendor.item.featured',['id' => $uData->productsId, 'status' => 0]) }}">Un-featured</a>
                            @endif

                            

                            
                            @endif

                        @endif
                       

                    </td>
                  </tr>
            @endforeach    
                
            </tbody>
          
        </table>

        
    </div>
  </div>
  <div class="card-footer text-muted">
{{--     {!! $uData->links() !!} --}}
{{--  {{ $uData->links() }} --}}
  </div>
</div>
	
@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function () {
    
  });
 
</script>  

@endsection