<?php $layouts='layouts.vendor'; ?>

@if(Auth::guard('admin')->check())
@php $layouts='layouts.admin' @endphp
@endif
@if(Auth::guard('vendor')->check())
   @php  $layouts='layouts.vendor' @endphp
@endif

 @extends($layouts)
@section('scripts')
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script>
$(function() {
  $('input[name="date"]').daterangepicker({
    opens: 'right',
    showDropdowns: true,
    minYear: 1901,
    maxYear: parseInt(moment().format('YYYY'),10),
    locale: {
            format: 'DD-MM-YYYY'
        },
    ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
  }, function(start, end, label) {
    console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
  });
});

</script>

@endsection
@section('content')
<style type="text/css">
 
</style>

<div class="card ">
  <div class="card-header">
    @if(isset($assignTo))
    <div style="border-bottom: 2px solid gray;padding-left: 1rem;">
      <form method="GET" action="{{ route('admin.order-rider-assign',$assignTo->id) }}">
          <h3>Assign Rider</h3>
          <div class="form-row">
              <div class="form-group col-md-3">
                  <label for="inputEmail4">Order No</label>
                  <input type="hidden"  value="{{$assignTo->id}}" class="form-control" >
                  <input type="text" readonly="" value="OD{{$assignTo->order_id}}" class="form-control" >
              </div>
              
              <div class="form-group col-md-3">
                   <label for="inputEmail4">Rider</label>
                   <select value="" name="rider_id" class="form-control select2" required >
                    <option value="">Select Rider</option>
                    @foreach($rider_onduty as $svalue)
                      <option value="{{ $svalue->id }}" {{ isset($assignTo) ? ( $assignTo->rider_id==$svalue->id ? "Selected":"" ) : ""}}>{{ $svalue->name }} {{ $svalue->mobile }}</option>
                    @endforeach
                  </select>
              </div>
              <div class="form-group col-md-2 float-right" style="padding-top: 1.55rem;">
                <label for="inputEmail4"></label>
                  <button type="submit" class="btn btn-primary ">Assign</button>
                     @if(Auth::guard('admin')->check())
                      <a href="{{ route('admin.rider.order-list') }}" class="btn btn-info ">Reset</a>
                     @endif
                    
              </div>
          </div>
      </form>
    </div>
    @endif
    <div>
      @if(Auth::guard('admin')->check())
        <form method="GET" action="{{ route('admin.rider.order-list') }}">
      @endif
      @if(Auth::guard('vendor')->check())
      
      @endif
          
          <div class="form-row">
              <div class="form-group col-md-3">
                  <label for="inputEmail4">Date</label>
                 <input type="text" name="date" value="" class="form-control" >
              </div>
              <!-- <div class="form-group col-md-3">
                  <label for="inputEmail4">Search</label>
                  <input type="text" value="{{ isset($request) ? $request->search : ""}}" name="search" class="form-control"  placeholder="Text">
              </div> -->
             <div class="form-group col-md-2">
                  <label for="inputEmail4">Vendor</label>
                  <select value="{{ old('vendor') }}" name="vendor" class="form-control select2"  >
                    <option value="">Select Vendor</option>
                    @foreach($vendor as $svalue)
                      <option value="{{ $svalue->id }}" {{ isset($request) ? ( $request->vendor==$svalue->id ? "Selected":"" ) : ""}}>{{ $svalue->name }}</option>
                    @endforeach
                  </select>
              </div>
              <div class="form-group col-md-2">
                  <label for="inputEmail4">Order Status</label>
                  <select value="" name="status" class="form-control"  >
                    <option value="">Select Order Status</option>
                    <option value="0" {{ isset($request) ? ( $request->status==0 ? "Selected":"" ) : ""}}>Pending </option>
                    <option value="1" {{ isset($request) ? ( $request->status==1 ? "Selected":"" ) : ""}}>Confirm </option>
                    <option value="2" {{ isset($request) ? ( $request->status==2 ? "Selected":"" ) : ""}}>Dispatch </option>
                     <option value="3" {{ isset($request) ? ( $request->status==3 ? "Selected":"" ) : ""}}>Delivered </option>
                    </select>
              </div>
              <div class="form-group col-md-2">
                  <label for="inputEmail4">Rider Status</label>
                  <select value="" name="riders_status" class="form-control"  >
                    <option value="">Select Rider Confirm</option>
                    <option value="0" {{ isset($request) ? ( $request->status==0 ? "Selected":"" ) : ""}}>Picked </option>
                    <option value="1" {{ isset($request) ? ( $request->status==1 ? "Selected":"" ) : ""}}>Delivered </option>
                    
                    </select>
              </div>
               
              <div class="form-group col-md-2 float-right" style="padding-top: 1.55rem;">
                <label for="inputEmail4"></label>
                  <button type="submit" class="btn btn-primary ">Submit</button>
                     @if(Auth::guard('admin')->check())
                      <a href="{{ route('admin.rider.order-list') }}" class="btn btn-info ">Reset</a>
                     @endif
                     @if(Auth::guard('vendor')->check())
                     
                     @endif
              </div>
          </div>                 
          
      </form>
    </div>

  </div>
  <div class="card-body">
    <?php //echo"<pre>",print_r($data),"</pre>";?>
    <div class="table-responsive">
        <table  class="table vtableOrder">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Vertical</th> 
                    <th>Vendor</th>
                    <th>Order No</th>
                    <th>Items</th>
                    <th>Amount</th>
                    <th>Customer</th> 
                     <!-- <th>Email</th> -->
                      <!-- <th>Mobile</th> -->
                      <th>Rider</th>

                    {{-- <th>Status</th> --}}
                    <th>Action</th>
                    
                </tr>
            </thead>
            <tbody>
            @foreach ($data as $uData)
                
                     <td>
                      {{ date('d M y', strtotime($uData->created_at)) }}<br>
                      {{ date('H:i:s', strtotime($uData->created_at)) }}
                     </td> 
                     
                   <td>{{$uData->masters_name}} </td>
                   <td >{{ $uData->vendor_company_name }}</td>
                     <td>{{ $uData->order_id }}</td>
                     <td>{{$uData->items}}</td>                     
                     <th>{{$uData->total_amount}}</th>

                     <th>{{$uData->user_names}}<br>{{$uData->user_mobile}}</th>
                     <!-- <th>{{$uData->user_email}}</th>
                     <th>{{$uData->user_mobile}}</th> -->
                     <th>
                      @if($uData->rider_name)
                        {{$uData->rider_name}}<br>{{$uData->rider_mobile}}
                      @else
                        <b class="text-danger" style="padding-left: 1rem;">Not Assigned</b>
                      @endif</th>
                   {{-- <td>
                        @if($uData->orders_status==0)
                            <small class="status secondary">Pending</small>
                        @endif
                        @if($uData->orders_status==11)
                            <small class="status secondary">Suggest</small>
                        @endif
                        @if($uData->orders_status==1)
                            <small  class="status primary">Confirm</small>
                        @endif
                        @if($uData->orders_status==2)
                            <small  class="status warning">Dispatch</small>
                        @endif
                        @if($uData->orders_status==3)
                            <small  class="status primary">Delivered</small>
                        @endif
                        @if($uData->orders_status==4)
                            <small  class="status secondary">Return</small>
                        @endif
                        @if($uData->orders_status==5)
                            <small  class="status warning">Cancelled by Customer</small>
                        @endif
                        @if($uData->orders_status==6)
                            <small  class="status warning">Cancelled by Vendor</small>
                        @endif
                        @if($uData->orders_status==7)
                            <small  class="status warning">Cancelled by Admin</small>
                        @endif
                    </td> --}}
                    <th>
                     <a class="btn btn-primary" href="{{ route('admin.rider.assign',$uData->order_rid) }}">Assign Rider</a>
                    </th>
                </tr>
            @endforeach    
                
            </tbody>
           {{--  <tfoot>
                <tr>
                   <th>Id</th>
                    <th>Name</th>
                    
                    <th>Action</th>
                </tr>
            </tfoot> --}}
        </table>

        
    </div>
  </div>
  <div class="card-footer text-muted">
{{--     {!! $uData->links() !!} --}}
{{--  {{ $uData->links() }} --}}
  </div>
</div>
  
   @endsection
