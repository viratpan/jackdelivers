<?php $layouts='layouts.vendor'; ?>

@if(Auth::guard('admin')->check())
@php $layouts='layouts.admin' @endphp
@endif
@if(Auth::guard('vendor')->check())
   @php  $layouts='layouts.vendor' @endphp
@endif

@extends($layouts)

@section('style')
<style type="text/css">
  .text-green{
  color: green;
  font-size: x-large;
  }.text-danger{
  color: red;
  font-size: x-large;
  }
</style>
@endsection


@section('content')


<div class="card ">
  <div class="card-header">
    <div>
      @if(Auth::guard('admin')->check())
        <form method="GET" action="{{ route('admin.item-list') }}">
      @endif
      @if(Auth::guard('vendor')->check())
       <form method="GET" action="{{ route('vendor.item-discount') }}">
      @endif
          
          <div class="form-row">
            @if(Auth::guard('admin')->check())
              <div class="form-group col-md-3">
                  <label for="inputEmail4">Search</label>
                  <input type="text" value="{{ isset($request) ? $request->search : ""}}" name="search" class="form-control"  placeholder="Text">
              </div>
              <div class="form-group col-md-2">
                  <label for="inputEmail4">Master</label>
                  <select value="{{ old('master') }}" name="master" class="form-control select2"  >
                    <option value="">Select Please</option>
                    @foreach($master as $svalue)
                      <option value="{{ $svalue->id }}" {{ isset($request) ? ( $request->master==$svalue->id ? "Selected":"" ) : ""}}>{{ $svalue->name }}</option>
                    @endforeach
                  </select>
              </div>
            @endif
              <div class="form-group col-md-2">
                  <label for="inputEmail4">Category</label>
                  <select value="{{ old('category') }}" name="category" class="form-control"  >
                    <option value="">Select Category</option>
                    @foreach($categry as $svalue)
                      <option value="{{ $svalue->id }}" {{ isset($request) ? ( $request->category==$svalue->id ? "Selected":"" ) : ""}}>{{ $svalue->name }}</option>
                    @endforeach
                  </select>
              </div>
              <div class="form-group col-md-2">
                  <label for="inputEmail4">Sub-Category</label>
                  <select value="{{ old('subcategory') }}" name="subcategory" class="form-control select2"  >
                    <option value="">Select Sub-Category</option>
                    @foreach($subcategry as $svalue)
                      <option value="{{ $svalue->id }}" {{ isset($request) ? ( $request->subcategory==$svalue->id ? "Selected":"" ) : ""}}>{{ $svalue->name }}</option>
                    @endforeach
                  </select>
              </div>
              @if(Auth::guard('admin')->check())
              <div class="form-group col-md-3">
                  <label for="inputEmail4">Vendor</label>
                  <select value="{{ old('vendor') }}" name="vendor" class="form-control select2"  >
                    <option value="">Select Vendor</option>
                    @foreach($vendor as $svalue)
                      <option value="{{ $svalue->id }}" {{ isset($request) ? ( $request->vendor==$svalue->id ? "Selected":"" ) : ""}}>{{ $svalue->name }}</option>
                    @endforeach
                  </select>
              </div>

              <div class="form-group col-md-2">
                  <label for="inputEmail4">City</label>
                  <select value="{{ old('city') }}" name="city" class="form-control select2"  >
                    <option value="">Select City</option>
                    @foreach($city as $svalue)
                      <option value="{{ $svalue->id }}" {{ isset($request) ? ( $request->city==$svalue->id ? "Selected":"" ) : ""}}>{{ $svalue->name }}</option>
                    @endforeach
                  </select>
              </div>
               @endif
              
              
              <div class="form-group col-md-2 float-right" style="padding-top: 1.55rem;">
                <label for="inputEmail4"></label>
                  <button type="submit" class="btn btn-primary ">Submit</button>
                     @if(Auth::guard('admin')->check())
                      <a href="{{ route('admin.item-list') }}" class="btn btn-info ">Reset</a>
                     @endif
                     @if(Auth::guard('vendor')->check())
                      <a href="{{ route('vendor.item-discount') }}" class="btn btn-info ">Reset</a>
                     @endif
              </div>

              <div class="form-group col-md-6 float-right" style="padding-top: 1.55rem;">
               @if(Auth::guard('admin')->check())
                <a class="btn btn-sm btn-info float-right" style="" href="{{ route('admin.add-item') }}"><b>+ Add Item</b></a>
               @endif
               @if(Auth::guard('vendor')->check())
                <a class="btn btn-sm btn-info float-right" style="" href="{{ route('vendor.add-item') }}"><b>+ Add New Item</b></a>
               @endif
                  
              </div>


          </div>                 
          
      </form>
    </div>

  </div>
  <div class="card-body">
    <?php //echo"<pre>",print_r($data),"</pre>";?>
    <div class="table-responsive">
      <form method="GET" action="{{ route('vendor.item-discount-update') }}">
         <div class="form-row">
            
              <div class="form-group col-md-3">
                  <label for="inputEmail4">Discount <i style="color: red;">(%)</i></label>
                  <input type="text" step=".0001" value="" name="discount" required="" class="form-control"  placeholder="Discount">
              </div>
              <div class="form-group col-md-2 float-right" style="padding-top: 1.55rem;">
                <label for="inputEmail4"></label>
                  <button type="submit" class="btn btn-primary ">Submit</button>                    
              </div>
            
          </div>
        <table id="" class="table vtable">
            <thead>
                <tr>
                    <th width="50px"><input type="checkbox" id="allproduct"  class="form-control"></th> 
                 @if(Auth::guard('admin')->check())
                    <th>Date</th>
                 @endif
                    <th>Name</th>
                    
                   @if(Auth::guard('admin')->check())
                    <th>Under Master</th>
                   @endif
                    <th>Under Category</th>
                    <th>Sub-Category</th>
                  @if(Auth::guard('admin')->check())
                    <th>Vendor Name</th>
                    <th>Vendor Email</th>
                    <th>City</th>
                    {{-- <th>Pincode</th> --}}
                  @endif
                    <th>Qty</th>
                    <th>Cost</th>
                    <th>Discount <i style="color: red;">(%)</i></th>
                    <th>Price</th>
                    <th>Action</th>
                    
                    
                </tr>
            </thead>
            <tbody>
            @foreach ($data as $key => $uData)
                <tr>
                    <td><input type="checkbox" name="product[]" class="product form-control" value="{{ $uData->product_price_id }}"></td>
                  @if(Auth::guard('admin')->check())
                    <td>{{ date('d M y', strtotime($uData->pcreated_at)) }}</td>
                  @endif
                    <td>{{ $uData->name }}</td>
                    
                  @if(Auth::guard('admin')->check())
                    <td>{{ $uData->masters_name }}</td>
                  @endif
                    <td>{{ $uData->categories_name }}</td>
                    <td>{{ $uData->sub_categories_name }}</td>
                    @if(Auth::guard('admin')->check())
                      <td> {{$uData->vendor_name}}</td>
                      <td>{{$uData->vendor_email}}</td>
                      <td>{{ $uData->city_name}}</td>
                     {{--  <td>{{ $uData->city_name}}</td> --}}
                    @endif
                      <td>{{ $uData->product_qty}} {{ $uData->attributes_name}}</td>
                      <td>{{ $uData->product_cost}}</td>
                      <td>{{ $uData->product_discount}}</td>
                      <td>{{ $uData->product_price}}</td>
                    
                
                <td style="display: flex;">
                        
                        @if(Auth::guard('vendor')->check())
                        <a href="{{ route('vendor.edit-item',$uData->productsId)}}" class="btn btn-sm btn-primary">Edit</a>
                        <a target="_blank" href="{{ route('vendor.item.manage',$uData->productsId)}}" class="btn btn-sm btn-info">Manage</a> 
                         
                        @endif
                       

                    </td>
                  </tr>
            @endforeach    
                
            </tbody>
          
        </table>
        </form>
        
    </div>
  </div>
  <div class="card-footer text-muted">
{{--     {!! $uData->links() !!} --}}
{{--  {{ $uData->links() }} --}}
  </div>
</div>
	
@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function () {
    
  });
  $(document).ready(function(){
        $('#allproduct').click(function(){
            if($(this).prop("checked") == true){
                console.log("Checkbox is checked.");
                $('.product').each(function() { 
                  this.checked = true; 
                });
            }
            else if($(this).prop("checked") == false){
                console.log("Checkbox is unchecked.");
                $('.product').each(function() {
                  this.checked = false;
                });
            }
        });
    });
</script>  

@endsection