@extends('layouts.admin')
@section('scripts')
 
  <script type="text/javascript">
    $(document).ready(function () {
      //$('.select2').select2();
    });
  </script>
 
  <script type="text/javascript">
    $(document).ready(function () {
      $.ajaxSetup({
           headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
       });
/*Aadhar file Upload Start*/
      $('#aadharFile').change(function() { 
        var image=$(this).val();
       
        if(image){
            var postData=new FormData();
            postData.append('file',this.files[0]);     
            var url="{{route('ajax.file.upload','master')}}";     
            $.ajax({               
                async:true,
                type:"post",
                url:url,
                data:postData,
                cache:false,
                contentType: false,
                processData: false,
                success:function(data){
                  console.log(data);
                  if(data.path){
                    $('#aadharupload').val(data.path);
                    toastr.success(''+data.message, { closeButton: !0 });
                  }else{
                    toastr.error(''+data.message, { closeButton: !0 });
                  }
                },error: function(xhr, status, error) {
                  //console.log(xhr.responseJSON.errors.file);
                  var err=xhr.responseJSON.errors.file;
                   $.each(err,function(key,value){
                        toastr.error(value, { closeButton: !0 });
                      
                  });
                  
                }  
     
            });
        }
      }); 
/*Aadhar file Upload End*/


    });
  
</script>
@endsection


@section('content')


<div class="card ">
  <div class="card-header">
    <div>
        <form method="{{ isset($stateData) ? 'PUT': "POST"}}" action="{{ route('admin.master') }}{{ isset($stateData) ? '/update' : "/store"}}{{ isset($stateData) ? '/'.$stateData->id : ""}}">
          @csrf
          <div class="form-row">
              <div class="form-group col-md-3">
                  <label for="inputEmail4">Vertical Name</label>
                  <input type="text" value="{{ isset($stateData) ? $stateData->name : old('name')}}" name="name" class="form-control" required placeholder="Vertical Name">
              </div>

              <div class="form-group col-md-1">
                  <label for="inputEmail4">GST <i>%</i></label>
                  <input type="number" step=".01" value="{{ isset($stateData) ? $stateData->gst : old('gst')}}" name="gst" class="form-control" required placeholder="%">
              </div>
              <div class="form-group col-md-1">
                  <label for="inputEmail4">Commission <i>%</i></label>
                  <input type="number" step=".01" value="{{ isset($stateData) ? $stateData->commission : old('commission')}}" name="commission" class="form-control" required placeholder="%">
              </div>

              <div class="form-group col-md-1">
                  <label for="inputEmail4">Delivery Cost {{-- <i>/km</i> --}}</label>
                  <input type="number" step=".01" value="{{ isset($stateData) ? $stateData->delivery_cost : old('delivery_cost')}}" name="delivery_cost" class="form-control" required placeholder="per km">
              </div>
              <div class="form-group col-md-1">
                   <label for="inputEmail4">Cancel Till</label>
                   <select value="{{ old('cancellation_till') }}" name="cancellation_till" class="form-control" required >
                      <option value="">Select Status</option>
                      <option value="0" {{ isset($stateData) ? ( $stateData->cancellation_till==0 ? "Selected":"" ) : ""}}>Pending </option>
                      <option value="1" {{ isset($stateData) ? ( $stateData->cancellation_till==1 ? "Selected":"" ) : ""}}>Confirm </option>
                      <option value="2" {{ isset($stateData) ? ( $stateData->cancellation_till==2 ? "Selected":"" ) : ""}}>Baken/Packing </option>
                      <option value="3" {{ isset($stateData) ? ( $stateData->cancellation_till==3 ? "Selected":"" ) : ""}}>Dispatch </option>
                      <option value="4" {{ isset($stateData) ? ( $stateData->cancellation_till==4 ? "Selected":"" ) : ""}}>Delivered </option>
                      <option value="5" {{ isset($stateData) ? ( $stateData->cancellation_till==5 ? "Selected":"" ) : ""}}>Cancelled By User </option>
                      <option value="6" {{ isset($stateData) ? ( $stateData->cancellation_till==6 ? "Selected":"" ) : ""}}>Cancelled By Vendor </option>
                      <option value="7" {{ isset($stateData) ? ( $stateData->cancellation_till==7 ? "Selected":"" ) : ""}}>Cancelled By Admin </option>
                      <option value="8" {{ isset($stateData) ? ( $stateData->cancellation_till==8 ? "Selected":"" ) : ""}}>Return </option>
                      <option value="8" {{ isset($stateData) ? ( $stateData->cancellation_till==9 ? "Selected":"" ) : ""}}>Exchange </option>
                      <option value="11" {{ isset($stateData) ? ( $stateData->cancellation_till==11 ? "Selected":"" ) : ""}}>Cancelled By Admin </option>
                   
                  </select>
              </div>
              <div class="form-group col-md-1">
                  <label for="inputEmail4">Cancel Cost <i>%</i></label>
                  <input type="number" step=".01" value="{{ isset($stateData) ? $stateData->cancellation_cost : old('cancellation_cost')}}" name="cancellation_cost" class="form-control" required placeholder="Cancellation Cost">
              </div>
              <div class="form-group col-md-1">
                  <label for="inputEmail4">Returnable</label>
                  <select value="{{ old('returnable') }}" name="returnable" class="form-control" required >                    
                      <option value="0" {{ isset($stateData) ? ( $stateData->returnable==0 ? "Selected":"" ) : ""}}>No </option>
                      <option value="1" {{ isset($stateData) ? ( $stateData->returnable==1 ? "Selected":"" ) : ""}}>Yes </option>
                  </select>
              </div>

              <div class="form-group col-md-1">
                  <label for="inputEmail4">Vertical Order</label>
                  <input type="number" value="{{ isset($stateData) ? $stateData->order : old('order')}}" name="order" class="form-control" required placeholder="Vertical Order">
              </div>
              <div class="form-group col-md-3">
                  <label for="inputEmail4">Search Text</label>
                  <input type="text" value="{{ isset($stateData) ? $stateData->search_text : old('search_text')}}" name="search_text" class="form-control" required placeholder="Search Text">
              </div>
              <div class="form-group col-md-2">
                <label for="inputPassword4">Image</label>               
                <input type="file" accept="image/png, image/jpeg" class="form-control" id="aadharFile">
                <input type="hidden"  value="{{ isset($stateData) ? $stateData->image : ""}}" class="form-control" id="aadharupload" name="image">
              </div>
              <div class="form-group col-md-3 float-right" style="padding-top: 1.55rem;">
                <label for="inputEmail4"></label>
                  <button type="submit" class="btn btn-primary ">Submit</button>
                  @if(isset($stateData))
                  <a href="{{ route('admin.master') }}" class="btn btn-info ">Cancel</a>
                  @endif
              </div>
          </div>                 
          
      </form>
    </div>

  </div>
  <div class="card-body">
    
    <div class="table-responsive">
        <table id="data_table" class="table">
            <thead>
                <tr>
                  <th>Sn.</th>                    
                  <th>Vertical Name</th>
                  <th>Image</th>
                  <th>GST <i>%</i></th>
                  <th>Commision <i>%</i></th>
                  <th>Delivery Cost <i>/km</i></th>
                  <th>Cancel Till</th>
                  <th>Cancel Cost <i>%</i></th>
                  <th>Returnable</th>
                  <th>Vertical Order</th>
                  <th>Status</th>                    
                  <th>Action</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($data as $key => $uData)
                <tr>
                   <td>{{ $key+1 }}</td>
                   <td>{{ $uData->name }}</td>
                   <td>
                     @if($uData->image)
                     <img  src="{{ asset('').$uData->image }}"  class="" style="height: 2rem;padding-top: 2px;">
                     @endif
                   </td>
                   <td>{{ $uData->gst }}</td>
                   <td>{{ $uData->commission }}</td>
                   <td>{{$uData->delivery_cost}}</td>
                   <td>
                     @if($uData->cancellation_till==0)
                            <small class="status secondary">Pending</small>
                        @endif
                        @if($uData->cancellation_till==11)
                            <small class="status secondary">Suggest</small>
                        @endif
                        @if($uData->cancellation_till==1)
                            <small  class="status primary">Confirm</small>
                        @endif
                        @if($uData->cancellation_till==2)
                            <small  class="status warning">Baken/Packing</small>
                        @endif
                        @if($uData->cancellation_till==3)
                            <small  class="status primary">Dispatch</small>
                        @endif
                        @if($uData->cancellation_till==4)
                            <small  class="status secondary">Delivered</small>
                        @endif
                         @if($uData->cancellation_till==8)
                            <small  class="status secondary">Return</small>
                        @endif
                         @if($uData->cancellation_till==9)
                            <small  class="status secondary">Exchange</small>
                        @endif
                        @if($uData->cancellation_till==5)
                            <small  class="status text-danger"><b>Cancelled by Customer</b></small>
                        @endif
                        @if($uData->cancellation_till==6)
                            <small  class="status text-danger"><b>                            
                             Cancelled by Vendor                            
                            </b></small>
                        @endif
                        @if($uData->cancellation_till==7)
                            <small  class="status text-danger"><b>
                             Cancelled by Admin
                            </b></small>
                        @endif
                   </td>
                   <td>{{ $uData->cancellation_cost }} </td>
                   <td>
                      @if($uData->returnable==0)
                            <small class="status secondary">No</small>
                        @endif
                        @if($uData->returnable==1)
                            <small class="status secondary">Yes</small>
                        @endif
                   </td>
                   <td>{{ $uData->order }}</td>
                   <td>
                    @if($uData->active==1)
                        Active
                    @else
                        De-Active
                    @endif
                  </td> 
                  <td>  
                        <a class="btn btn-primary" href="{{ route('admin.master.edit',$uData->id) }}">Edit</a>
                    @if($uData->active==1)
                        <a class="btn btn-warning" href="{{ route('admin.master.status',['id' => $uData->id, 'status' => 0]) }}">De-Active</a>
                    @else
                        <a class="btn btn-info" href="{{ route('admin.master.status',['id' => $uData->id, 'status' => 1]) }}">Active</a>
                    @endif
                      
                  </td>
                </tr>
            @endforeach    
                
            </tbody>
           {{--  <tfoot>
                <tr>
                   <th>Id</th>
                    <th>Name</th>
                    
                    <th>Action</th>
                </tr>
            </tfoot> --}}
        </table>

        
    </div>
  </div>
  <div class="card-footer text-muted">
{{--     {!! $uData->links() !!} --}}
{{--  {{ $uData->links() }} --}}
  </div>
</div>
	
   @endsection
