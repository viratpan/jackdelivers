@extends('layouts.vendor')
@section('style')
  <style type="text/css">
      .pagination{float: right;}
  </style>
@endsection


@section('content')
    
    {{-- <div class="breadcrumb">
        <h1>
            <a target="_Blank" href="{{ route('vendor',['type' => $vendor->master_name,'id' => $vendor->id, 'slug' => $vendor->slug])}}">Store</a>
        </h1>
        @if(Auth::guard('vendor')->user()->open==1)
            <a href="{{route('vendor.store.status', 0) }}" class="btn btn-outline-success m-1">Online</a>
        @else
            <a href="{{route('vendor.store.status', 1)}}" class="btn btn-outline-danger m-1">Offine</a>
        @endif
        
    </div> --}}
    <div class="breadcrumb">
        <h1 class="mr-2">Store: <a target="_Blank" title="Visit Store" href="{{ route('vendor',['type' => $vendor->master_name,'id' => $vendor->id, 'slug' => $vendor->slug])}}">{{ auth()->user()->company_name}}</a></h1>
        <ul>
        @if(Auth::guard('vendor')->user()->open==1)
             <li><a href="{{route('vendor.store.status', 0) }}" title="Click To Go Offline" class="btn btn-outline-success">Online</a></li>
        @else
             <li><a href="{{route('vendor.store.status', 1)}}" title="Click To Go Online" class="btn btn-outline-danger">Offine</a></li>
        @endif
           
        </ul>
    </div>
    <div class="separator-breadcrumb border-top"></div>
    {{-- <div class="row">
        <!-- ICON BG-->
        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
                <div class="card-body text-center"><i class="i-Add-User"></i>
                    <div class="content">
                        <p class="text-muted mt-2 mb-0">New Leads</p>
                        <p class="text-primary text-24 line-height-1 mb-2">205</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
                <div class="card-body text-center"><i class="i-Financial"></i>
                    <div class="content">
                        <p class="text-muted mt-2 mb-0">Sales</p>
                        <p class="text-primary text-24 line-height-1 mb-2">$4021</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
                <div class="card-body text-center"><i class="i-Checkout-Basket"></i>
                    <div class="content">
                        <p class="text-muted mt-2 mb-0">Orders</p>
                        <p class="text-primary text-24 line-height-1 mb-2">80</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
                <div class="card-body text-center"><i class="i-Money-2"></i>
                    <div class="content">
                        <p class="text-muted mt-2 mb-0">Expense</p>
                        <p class="text-primary text-24 line-height-1 mb-2">$1200</p>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}

    <div class="row">
        <div class="col-lg-8 col-md-12">
            <div class="card mb-4">
                <div class="card-body p-0">
                    <div class="card-title border-bottom d-flex align-items-center m-0 p-3"><span>Today Orders</span><span class="flex-grow-1"></span><span class="badge badge-pill badge-warning">Updated daily</span></div>
                    @if(count($order)!=0)
                    @foreach ($order as $uData)
                    <div class="d-flex border-bottom justify-content-between p-3">
                        <div class="flex-grow-1"><span class="text-small text-muted">Time</span>
                            <h5 class="m-0">{{ date('h:i A', strtotime($uData->created_at)) }}</h5>
                        </div>
                        <div class="flex-grow-1"><span class="text-small text-muted">Order No</span>
                            <h5 class="m-0">
                              <a href="" target="_Blank">OD{{ $uData->order_id }}</a>
                            </h5>
                        </div>
                        <div class="flex-grow-1"><span class="text-small text-muted">Items</span>
                                <h5 class="m-0">{{$uData->items}}</h5>
                        </div>
                        <div class="flex-grow-1"><span class="text-small text-muted">Amount</span>
                            <h5 class="m-0">&#8377 {{$uData->total_amount}}</h5>
                        </div>
                        <div class="flex-grow-1"><span class="text-small text-muted">Payment Type</span>
                            <h5 class="m-0">@if($uData->payment_type==1)COD @else Online @endif</h5>
                        </div>
                        <div class="flex-grow-1"><span class="text-small text-muted">Status</span>
                            <h5 class="m-0" style="display: grid;">
                        @if($uData->status==0)
                            <b class="status secondary">Pending</b>                           
                        @endif
                        @if($uData->status==11)
                            <b class="status secondary">Suggest</b>
                        @endif
                        @if($uData->status==1)
                            <b  class="status primary">Confirm</b>
                        @endif
                        @if($uData->status==2)
                            <b  class="status warning">Dispatch</b>
                        @endif
                        @if($uData->status==3)
                            <b  class="status primary">Delivered</b>                            
                        @endif
                        @if($uData->status==4)
                            <b  class="status secondary">Return</b>
                        @endif
                        @if($uData->status==5)
                            <b  class="status text-danger">Cancelled</b>
                            <small>by Customer</small>
                        @endif
                        @if($uData->status==6)
                            <b  class="status text-danger">Cancelled</b>
                            <small>by me</small>
                        @endif
                        @if($uData->status==7)
                            <b  class="status text-danger">Cancelled</b>
                            <small>by Admin</small>
                        @endif</h5>
                        </div>
                        
                    </div>
                    @endforeach
                    @else
                    <div class="d-flex border-bottom justify-content-between p-3">
                      <div class="flex-grow-1"><span class="text-small text-muted"></span>
                            <h5 class="m-0 text-center">No Orders</h5>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>

         <div class="col-lg-4 col-sm-12">
            <div class="card mb-4">
                <div class="card-body">
                    <div class="card-title">Today Orders</div>
                    <div id="todaySale" style="height: 300px;"></div>
                </div>
            </div>
        </div>
    </div>
                <div class="row">
                    <div class="col-lg-8 col-md-12">
                        <div class="card mb-4">
                            <div class="card-body">
                                <div class="card-title">This Week Sales</div>
                                <div id="echartBar" style="height: 300px;"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-12">
                        <div class="card mb-4">
                            <div class="card-body">
                                <div class="card-title">This Week Sales</div>
                                <div id="echartPie" style="height: 300px;"></div>
                            </div>
                        </div>
                    </div>

                </div>




   
    
@endsection
@section('scripts')
<script type="text/javascript">
$(document).ready(function () {
/*today sales pie*/
    var todaySalePie = document.getElementById('todaySale');
  if (todaySalePie) {
    var todaySale = echarts.init(todaySalePie);
    todaySale.setOption({
      color: ['gray', 'green', '#7d6cbb', '#8877bd', '#f8f8f8', 'red'],
      tooltip: {
        show: true,
        backgroundColor: 'rgba(0, 0, 0, .8)'
      },
      series: [{
        name: 'Today Orders',
        type: 'pie',
        radius: '60%',
        center: ['50%', '50%'],
        data: [{
          value: 535,
          name: 'Pending'
        }, {
          value: 310,
          name: 'Confirm'
        }, {
          value: 234,
          name: 'Baken'
        }, {
          value: 155,
          name: 'Dispatch'
        }, {
          value: 130,
          name: 'Delivered'
        }, {
          value: 348,
          name: 'Cancelled'
        }],
        itemStyle: {
          emphasis: {
            shadowBlur: 10,
            shadowOffsetX: 0,
            shadowColor: 'rgba(0, 0, 0, 0.5)'
          }
        }
      }]
    });
    $(window).on('resize', function () {
      setTimeout(function () {
        todaySale.resize();
      }, 500);
    });
  } // Chart in Dashboard version 1
// Chart in Dashboard version 1
  var echartElemBar = document.getElementById('echartBar');

  if (echartElemBar) {
    var echartBar = echarts.init(echartElemBar);
    echartBar.setOption({
      legend: {
        borderRadius: 0,
        orient: 'horizontal',
        x: 'right',
        data: ['Online','inline','Offline']
      },
      grid: {
        left: '8px',
        right: '8px',
        bottom: '0',
        containLabel: true
      },
      tooltip: {
        show: true,
        backgroundColor: 'rgba(0, 0, 0, .8)'
      },
      xAxis: [{
        type: 'category',
        data: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'],
        axisTick: {
          alignWithLabel: true
        },
        splitLine: {
          show: false
        },
        axisLine: {
          show: true
        }
      }],
      yAxis: [{
        type: 'value',
        axisLabel: {
          formatter: '${value}'
        },
        min: 0,
        max: 100000,
        interval: 25000,
        axisLine: {
          show: false
        },
        splitLine: {
          show: true,
          interval: 'auto'
        }
      }],
      series: [{
        name: 'Online',
        data: [35000, 69000, 22500, 60000, 50000, 50000, 30000, 80000, 70000, 60000, 20000, 30005],
        label: {
          show: false,
          color: '#0168c1'
        },
        type: 'bar',
        barGap: 0,
        color: '#bcbbdd',
        smooth: true,
        itemStyle: {
          emphasis: {
            shadowBlur: 10,
            shadowOffsetX: 0,
            shadowOffsetY: -2,
            shadowColor: 'rgba(0, 0, 0, 0.3)'
          }
        }
      },{
        name: 'inline',
        data: [45000, 82000, 35000, 93000, 71000, 89000, 49000, 91000, 80200, 86000, 35000, 40050],
        label: {
          show: false,
          color: '#639'
        },
        type: 'bar',
        color: '#f44336',
        smooth: true,
        itemStyle: {
          emphasis: {
            shadowBlur: 10,
            shadowOffsetX: 0,
            shadowOffsetY: -2,
            shadowColor: 'rgba(0, 0, 0, 0.3)'
          }
        }
      }, {
        name: 'Offline',
        data: [45000, 82000, 35000, 93000, 71000, 89000, 49000, 91000, 80200, 86000, 35000, 40050],
        label: {
          show: false,
          color: '#639'
        },
        type: 'bar',
        color: '#7569b3',
        smooth: true,
        itemStyle: {
          emphasis: {
            shadowBlur: 10,
            shadowOffsetX: 0,
            shadowOffsetY: -2,
            shadowColor: 'rgba(0, 0, 0, 0.3)'
          }
        }
      }]
    });
    $(window).on('resize', function () {
      setTimeout(function () {
        echartBar.resize();
      }, 500);
    });
  } // Chart in Dashboard version 1
});
</script>
@endsection