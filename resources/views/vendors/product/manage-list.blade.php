<?php $layouts='layouts.vendor'; ?>

@if(Auth::guard('admin')->check())
@php $layouts='layouts.admin' @endphp
@endif
@if(Auth::guard('vendor')->check())
   @php  $layouts='layouts.vendor' @endphp
@endif

 @extends($layouts)



@section('content')


<div class="card ">
  <div class="card-body">
@if(Auth::guard('admin')->check())
  
    <h4>Product:<b> {{$data->name}}</b> | <small>Vendor :  </small>{{ $data->vendor_name}}</h4>{{$data->categories_name}} 
            <small>( {{$data->sub_categories_name}} )</small>
@endif
@if(Auth::guard('vendor')->check())
  <h4>Product:<b> {{$data->name}}</b></h4>{{$data->categories_name}} 
            <small>( {{$data->sub_categories_name}} )</small>

@endif
      
  </div>
  <div class="card-header">
    <div>
      @if(Auth::guard('admin')->check())
          <form method="{{ isset($pricesData) ? 'PUT': "POST"}}" action="{{ route('admin.product.manage.store',$data->productsId) }}">
      @endif
    @if(Auth::guard('vendor')->check())
       <form method="{{ isset($pricesData) ? 'PUT': "POST"}}" action="{{ route('vendor.product.manage.store',$data->productsId) }}">
    @endif

          @csrf
          <div class="form-row">
              <div class="form-group col-md-2">
                  <label for="inputEmail4">Quantity</label>
                  <input type="number" step=".0001" value="{{ old('qty') }}{{ isset($pricesData) ? $pricesData->qty : ""}}" name="qty" class="form-control" required placeholder="Quantity">
                  <input type="hidden" value="{{ old('id') }}{{ isset($pricesData) ? $pricesData->id : ""}}" name="id" class="form-control"  placeholder="id">
              </div>
               <div class="form-group col-md-2" style="margin-top: 1.75rem;">                                
                  {{$data->attributes_name}}
              </div>
              <div class="form-group col-md-2">
                  <label for="inputEmail4">Actual Price</label>
                  <input type="number" step=".0001" value="{{$data->aprice}}{{ old('cost') }}{{ isset($pricesData) ? $pricesData->cost : ""}}" name="cost" class="form-control" required placeholder="Price">
                  
              </div>
              <div class="form-group col-md-2">
                  <label for="inputEmail4">Sale Price</label>
                  <input type="number" step=".0001" value="{{ old('price') }}{{ isset($pricesData) ? $pricesData->price : ""}}" name="price" class="form-control" required placeholder="Price">
              </div>
              <div class="form-group col-md-2">
                  <label for="inputEmail4">Sales Discount % </label>
                  <input type="number" step=".0001"  value="{{ old('discount') }}{{ isset($pricesData) ? $pricesData->discount : "0"}}" name="discount" class="form-control" required placeholder="Discount ">
              </div>


              <div class="form-group col-md-2 float-right" style="padding-top: 1.55rem;">
                <label for="inputEmail4"></label>
                  <button type="submit" class="btn btn-primary ">Submit</button>
                  @if(isset($pricesData))
                  <a href="{{ route('vendor.product.manage',$data->productsId) }}" class="btn btn-info ">Cancel</a> 
                  @endif
              </div>
          </div>                 
          
      </form>
    </div>

  </div>
  <div class="card-body">
    <div class="table-responsive">
        <table id="data_table" class="table">
            <thead>
                <tr>
                    <th>Quantity</th>
                    <th>Actual Price</th>
                    <th>Sale Price</th>
                    <th>Discount</th>                                        
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($prices as $uData)
                <tr>
                    <td>{{ $uData->qty }}  {{$data->attributes_name}}</td>
                    <td>{{ $uData->price }}</td>
                    <td>{{ $uData->cost }}</td>
                    <td>{{ $uData->discount }}</td>
                    <td>
                      @if(Auth::guard('admin')->check())
                         <a href="{{ route('admin.product.manage',['id' => $data->productsId, 'pid' => $uData->id]) }}" class="btn btn-sm btn-primary">Edit</a>
                      @endif
                      @if(Auth::guard('vendor')->check())
                        <a href="{{ route('vendor.product.manage',['id' => $data->productsId, 'pid' => $uData->id]) }}" class="btn btn-sm btn-primary">Edit</a>

                      @endif  
                       
                        
                    </td>
                    
                </tr>
            @endforeach    
                
            </tbody>
            
        </table>

        
    </div>
  </div>
  <div class="card-footer text-muted">
       
  </div>
</div>
    
   @endsection
