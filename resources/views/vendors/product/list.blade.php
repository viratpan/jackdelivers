<?php $layouts='layouts.vendor'; ?>

@if(Auth::guard('admin')->check())
@php $layouts='layouts.admin' @endphp
@endif
@if(Auth::guard('vendor')->check())
  @php  $layouts='layouts.vendor' @endphp
@endif

 @extends($layouts)

@section('style')
<style type="text/css">
    .status{
        border: 0px;
        padding: 6px;
        border-radius: 8px;
    }.featured{
        background: antiquewhite;
        padding: 6px;
        border: 0px;
        border-radius: 8px;
    }
    .status.secondary {
        background: antiquewhite;
    }
    .status.primary {
        background: green;
    }
    .status.danger {
        background: red;
    }
    .status.warning {
        background: #ffc107;
    }
</style>
@endsection

@section('content')
<div class="card ">
  <div class="card-header">
    <h5 align="left">Vendor Item Mapping 
    @if(Auth::guard('admin')->check())
      <a class="btn btn-sm btn-info float-right" style="" href="{{ route('admin.add-product') }}"><b>Add</b></a>
    @endif
    @if(Auth::guard('vendor')->check())
         <a class="btn btn-sm btn-info float-right" style="" href="{{ route('vendor.add-product') }}"><b>Add</b></a>
    @endif
    </h5>

  <div>
    @if(Auth::guard('admin')->check() && isset($stateData))
        <form method="{{ isset($stateData) ? 'PUT': "POST"}}" action="{{ route('admin.product') }}{{ isset($stateData) ? '/update' : "/store"}}{{ isset($stateData) ? '/'.$stateData->id : ""}}">
   
              
              @csrf
               <div class="form-row">

               <div class="form-group col-md-4">
                  <label for="">Item</label>
                  <select  name="item_id" class="form-control" required >
                    <option value="">Select Item</option>
                    @foreach($item as $svalue)
                      <option value="{{ $svalue->id }}" {{ isset($stateData) ? ( $stateData->item_id==$svalue->id ? "Selected":"" ) : ""}}>{{ $svalue->name }}</option>
                    @endforeach
                  </select>
              </div>
              <div class="form-group col-md-4">
                  <label for="">Vendor</label>
                  <select  name="vendor_id" class="form-control" required >
                    <option value="">Select Vendor</option>
                    @foreach($vendor as $svalue)
                      <option value="{{ $svalue->id }}" {{ isset($stateData) ? ( $stateData->vendor_id==$svalue->id ? "Selected":"" ) : ""}}>{{ $svalue->name }} ( {{ $svalue->email }} )</option>
                    @endforeach
                  </select>
              </div>
              <div class="form-group col-md-3 float-right" style="padding-top: 1.55rem;">
                <label for="inputEmail4"></label>
                  <button type="submit" class="btn btn-primary ">Submit</button>
                  @if(isset($stateData))
                  <a href="{{ route('admin.product') }}" class="btn btn-info ">Cancel</a>
                  @endif

              </div>
               </div>

      </form>
              @endif
              
         
    </div>



  </div>
  <div class="card-body">
    <div class="table-responsive">
        <table id="data_table" class="table">
            <thead>
                <tr>
                    <th>Sn.</th>
                    <th>Name</th>
                    <th>Image</th>
                @if(Auth::guard('admin')->check())    <th>Vendor</th>@endif
                    <th>Category</th>
                    <th>SubCategory</th>
                    <th>Prices</th>
                    <th>Status</th>
                    
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($data as $key => $uData)
                <tr>
                    <td>{{ $key+1 }}</td>
                    <td>{{ $uData->name }}<br>
                        <small>{{ $uData->slug }}</small>
                    </td>
                    <td><img  src="{{ asset('').$uData->image }}" class="" style="height: 3rem;"></td>
                   @if(Auth::guard('admin')->check())
                   <td><span title="{{ $uData->vendor_email }}">{{ $uData->vendor_name }}</span></td>

                   @endif
                    <td>{{ $uData->categories_name }}</td>
                    <td>{{ $uData->sub_categories_name }}</td>
                    <td>@php $pp=DB::table('product_price')
                          
                          ->where('product_price.product_id',$uData->productsId)
                          ->select('product_price.qty','product_price.id','product_price.price','product_price.discount')
                          ->get();
                  //  echo"<pre>",print_r($pp)."</pre>";
                      $temp=array();
                      foreach($pp as $value){
                      $text=$value->qty.$uData->attributes_name.'( '.round($value->price).' ) on <i title="Discount">'.round($value->discount)."%</i>";
                      array_push($temp,$text);
                    } echo implode(' <br> ',$temp);
                  @endphp
                 
                </td>
                    <td>@if($uData->featured==1)
                        <small class="featured">Featured</small>
                        @endif
                        @if($uData->status==0)
                            <small class="status secondary">Deactive</small>
                        @endif
                        @if($uData->status==1)
                            <small  class="status primary">In Stock</small>
                        @endif
                        @if($uData->status==2)
                            <small  class="status warning">Out Of Stock</small>
                        @endif
                    </td>
                    <td>
                        @if(Auth::guard('admin')->check())

                           <a class="btn btn-primary btn-sm" href="{{ route('admin.product.edit',$uData->productsId) }}">Edit</a>
                        @if($uData->featured==1)
                        <small class="featured">Featured</small>
                        @endif

                        @if($uData->status==0)
                            <a class="btn btn-success  btn-sm" href="{{ route('admin.product.status',['id' => $uData->productsId, 'status' => 1]) }}">In stock</a>
                            <a class="btn btn-warning btn-sm" href="{{ route('admin.product.status',['id' => $uData->productsId, 'status' => 2]) }}">Out of stock</a>
                        @endif
                        @if($uData->status==1)
                            <a class="btn btn-light  btn-sm" href="{{ route('admin.product.status',['id' => $uData->productsId, 'status' => 0]) }}">Deactive</a>
                            <a class="btn btn-warning btn-sm" href="{{ route('admin.product.status',['id' => $uData->productsId, 'status' => 2]) }}">Out of stock</a>
                        @endif
                        @if($uData->status==2)
                           <a class="btn btn-light  btn-sm" href="{{ route('admin.product.status',['id' => $uData->productsId, 'status' => 0]) }}">Deactive</a>
                             <a class="btn btn-success  btn-sm" href="{{ route('admin.product.status',['id' => $uData->productsId, 'status' => 1]) }}">In stock</a>
                        @endif
                            @if($uData->featured==0)
                                 <a class="btn btn-success  btn-sm" href="{{ route('admin.product.featured',['id' => $uData->productsId, 'status' => 1]) }}">featured</a>
                            @endif
                            @if($uData->featured==1)
                                 <a class="btn btn-success  btn-sm" href="{{ route('admin.product.featured',['id' => $uData->productsId, 'status' => 0]) }}">Un-featured</a>
                            @endif

                             <a target="_blank" href="{{ route('admin.product.manage',$uData->productsId)}}" class="btn btn-sm btn-info">Manage</a>
                        @endif
                        @if(Auth::guard('vendor')->check())
                          @if($uData->status==0)
                            <a class="btn btn-success  btn-sm" href="{{ route('vendor.product.status',['id' => $uData->productsId, 'status' => 1]) }}">Instock</a>
                            <a class="btn btn-warning btn-sm" href="{{ route('vendor.product.status',['id' => $uData->productsId, 'status' => 2]) }}">Out of stock</a>
                        @endif
                        @if($uData->status==1)
                            <a class="btn btn-light  btn-sm" href="{{ route('vendor.product.status',['id' => $uData->productsId, 'status' => 0]) }}">Deactive</a>
                            <a class="btn btn-warning btn-sm" href="{{ route('vendor.product.status',['id' => $uData->productsId, 'status' => 2]) }}">Out of stock</a>
                        @endif
                        @if($uData->status==2)
                           <a class="btn btn-light  btn-sm" href="{{ route('vendor.product.status',['id' => $uData->productsId, 'status' => 0]) }}">Deactive</a>
                             <a class="btn btn-success  btn-sm" href="{{ route('vendor.product.status',['id' => $uData->productsId, 'status' => 1]) }}">In stock</a>
                        @endif
                            @if($uData->featured==0)
                                 <a class="btn btn-success  btn-sm" href="{{ route('vendor.product.featured',['id' => $uData->productsId, 'status' => 1]) }}">featured</a>
                            @endif
                            @if($uData->featured==1)
                                 <a class="btn btn-success  btn-sm" href="{{ route('vendor.product.featured',['id' => $uData->productsId, 'status' => 0]) }}">Un-featured</a>
                            @endif

                            <a target="_blank" href="{{ route('vendor.product.manage',$uData->productsId)}}" class="btn btn-sm btn-info">Manage</a>
                        @endif

                    </td>

                </tr>
            @endforeach

            </tbody>
            {{-- <tfoot>
                <tr>
                   <th>ID</th>
                    <th>Name</th>
                    <th>Category</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </tfoot> --}}
        </table>


    </div>
  </div>
  <div class="card-footer text-muted">

  </div>
</div>

@endsection