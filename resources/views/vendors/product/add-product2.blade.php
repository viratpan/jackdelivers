<?php $layouts='layouts.vendor'; ?>

@if(Auth::guard('admin')->check())
@php $layouts='layouts.admin' @endphp
@endif
@if(Auth::guard('vendor')->check())
   @php  $layouts='layouts.vendor' @endphp
@endif

 @extends($layouts)

@section('scripts')
  <script src="{{  asset('js/plugins/select2.min.js') }}" ></script>
  <script type="text/javascript">
    $(document).ready(function () {
      //$('.select2').select2();
    });
  </script>
 
  <script type="text/javascript">
    $(document).ready(function () {
      $.ajaxSetup({
           headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
       });
  $('#category').change(function(e) {
     var id=$(this).val();     
     $.get('{{ route('ajax.category.options') }}', 
      { 'id': id }, 
      function( res ) {
        if(res){
            $("#sub_category").empty();
            $.each(res,function(key,value){
                $("#sub_category").append('<option value="'+value.id+'">'+value.name+'</option>');
            });
            $("#sub_category").trigger('change');
        }else{
           $("#sub_category").empty();
        }
        
        
      }
    );

  });
  /*on change  Sub Category get all items*/
  $('#vendor').change(function(e) {
    $("#sub_category").trigger('change');
  });

  $('#sub_category').change(function(e) {
     var category=$('#category').val(); 
     var sub_category=$('#sub_category').val(); 
     var vendor_id=$('#vendor').val();  
     if(vendor_id==''|| sub_category=='' || category==''){
        toastr.error("Unknow Vendor !", { closeButton: !0 });
      
     }else{
       $.get('{{ route('ajax.category_sub_category.items') }}', 
        { 'category': category,'subcategory':sub_category,'vendor':vendor_id }, 
        function( res ) {
          if(res){
            console.log(res);
              $("#itemlist").html('<label for="inputEmail4">Items</label>');
              $.each(res.items,function(key,value){console.log(value.name);
                  var data= showItem(value.id,value.name,value.aprice);             
                  $("#itemlist").append(data);
              });
             
              //$("#itemlist").append('<hr class="col-md-12"><a class="btn btn-sm btn-info float-right" id="addItem1" onclick="showAllSelectedItems();" style="" >Next</a><hr class="col-md-12">');
          }else{
              toastr.error("Items Not found !", { closeButton: !0 });
          }
          
          
        }
      );
      }
  });
        $('#itemlist #addItem1').click(function(e) {
            alert('add item');
        });

  function showAllSelectedItems() {
    alert('add item');
  }
  function showItem(id,name,cost){ //console.log(name);
    var html=`<label class="checkbox checkbox-outline-primary">
                <input type="checkbox" name="item[]" class="itemlist" value="`+id+`" data-value="`+id+`" data-name="`+name+`"  /><span>`+name+`</span><span class="checkmark"></span>
            </label>`;
            return html;
  }




    });


  
  </script>
  <script>
 

</script>
@endsection


@section('content')
<style type="text/css">
  .text-red{color: red;font-weight: 700;}
</style>
<div class="card ">
  <div class="card-header">
    <h5 align="left">Add Products
@if(Auth::guard('admin')->check())
  <a class="btn btn-sm btn-info float-right" style="" href="{{ route('admin.product') }}"><b>Back</b></a>
@endif
@if(Auth::guard('vendor')->check())
     <a class="btn btn-sm btn-info float-right" style="" href="{{ route('vendor.product') }}"><b>Back</b></a>
@endif
        
    </h5>

  </div>
  <div class="card-body">
@if(Auth::guard('admin')->check())
  <form method="POST" action="{{ route('admin.product-store2') }}" enctype="multipart/form-data">
@endif
@if(Auth::guard('vendor')->check())
    
@endif
    
        @csrf
        <div class="form-row">
          <div class="form-group col-md-3">
          <label for="inputEmail4">Category <span class="text-red">*</span></label>
           <select id="category" value="{{ old('categry_id') }}"  class="form-control" required >
              <option value="">Please Select </option>
              @foreach($category as $svalue)
                <option value="{{ $svalue->id }}" >{{ $svalue->name }}</option>
              @endforeach
            </select>
        </div>
        <div class="form-group col-md-3">
          <label for="inputEmail4">Sub Category <span class="text-red">*</span></label>
           <select id="sub_category" value="{{ old('sub_categry_id') }}"  class="form-control" required >
              <option value="">Please Select Category</option>
              {{-- @foreach($sub_category as $svalue)
                <option value="{{ $svalue->id }}" >{{ $svalue->name }}</option>
              @endforeach --}}
            </select>
        </div>
        @if(Auth::guard('admin')->check())
          <div class="form-group col-md-3">
            <label for="inputEmail4">Vendor <span class="text-red">*</span></label>
             <select id="vendor" value="{{ old('vendor_id') }}" name="vendor_id" class="form-control" required >
                <option value="">Please Select </option>
                @foreach($vendor as $svalue)
                  <option value="{{ $svalue->id }}" >{{ $svalue->name }}</option>
                @endforeach
              </select>
          </div>
        
        @endif
        @if(Auth::guard('vendor')->check())
             
        @endif   
         <hr class="col-md-12" style="margin: 8px;">
        

        <div class="form-group col-md-12" id="itemlist">     
          <label for="inputEmail4">No Items. Please Select Category and Sub-Category.</label>

        </div>
        <hr class="col-md-12" style="margin: 8px;">


      </div>
      
      <div class="form-row float-right" >
          <button type="submit" class="btn btn-primary ">Save</button>
      </div>
    </form>

  </div>
  <div class="card-footer text-muted">
   
  </div>
</div>
    
@endsection
