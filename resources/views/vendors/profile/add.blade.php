<?php $layouts='layouts.vendor'; ?>

@if(Auth::guard('admin')->check())
@php $layouts='layouts.admin' @endphp
@endif
@if(Auth::guard('vendor')->check())
   @php  $layouts='layouts.vendor' @endphp
@endif

 @extends($layouts)


@section('style')
  <link href="{{  asset('css/plugins/select2.min.css') }}" rel="stylesheet" />
@endsection

@section('scripts')
  <script src="{{  asset('js/plugins/select2.min.js') }}" ></script>
  <script type="text/javascript">
    $(document).ready(function () {
      //$('.select2').select2();
    });
  </script>
 
  <script type="text/javascript">
    $(document).ready(function () {
      $.ajaxSetup({
           headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
       });
/*///// check vendor exits or not */
  $('#email').change(function(e) {
    var email=$(this).val();
    $.get('{{ route('ajax.vendor.exits') }}', 
      { 'email': email }, 
      function( data ) {
        //console.log(data);
        if(data==0){
          $('#vendorEmail').html('<i style="color:green;">Valid Email</i>');
           toastr.success('Vendor is Valid to Add.', { closeButton: !0 });
        }else{
             $('#vendorEmail').html('<i style="color:red;">Vendor allready Exits</i>');
              toastr.error("Vendor Allready Added.", { closeButton: !0 });
        }
        
      }
    );
  });
  /*Aadhar file Upload Start*/
      $('#imageFile').change(function() { 
        var image=$(this).val();
       
        if(image){
            var postData=new FormData();
            postData.append('file',this.files[0]);     
            var url="{{route('ajax.file.upload','vendor')}}";     
            $.ajax({               
                async:true,
                type:"post",
                url:url,
                data:postData,
                cache:false,
                contentType: false,
                processData: false,
                success:function(data){
                  console.log(data);
                  if(data.path){
                    $('#imageupload').val(data.path);
                    toastr.success('Image '+data.message, { closeButton: !0 });
                  }else{
                    toastr.error('Image '+data.message, { closeButton: !0 });
                  }
                },error: function(xhr, status, error) {
                  //console.log(xhr.responseJSON.errors.file);
                  var err=xhr.responseJSON.errors.file;
                   $.each(err,function(key,value){
                        toastr.error(value, { closeButton: !0 });
                      
                  });
                  
                }  
     
            });
        }
      }); 
/*Aadhar file Upload End*/
/*Aadhar file Upload Start*/
      $('#cimageFile').change(function() { 
        var image=$(this).val();
       
        if(image){
            var postData=new FormData();
            postData.append('file',this.files[0]);     
            var url="{{route('ajax.file.upload','vendor')}}";     
            $.ajax({               
                async:true,
                type:"post",
                url:url,
                data:postData,
                cache:false,
                contentType: false,
                processData: false,
                success:function(data){
                  console.log(data);
                  if(data.path){
                    $('#cimageupload').val(data.path);
                    toastr.success('Cover Image '+data.message, { closeButton: !0 });
                  }else{
                    toastr.error('Cover Image '+data.message, { closeButton: !0 });
                  }
                },error: function(xhr, status, error) {
                  //console.log(xhr.responseJSON.errors.file);
                  var err=xhr.responseJSON.errors.file;
                   $.each(err,function(key,value){
                        toastr.error(value, { closeButton: !0 });
                      
                  });
                  
                }  
     
            });
        }
      }); 
/*Aadhar file Upload Start*/
      $('#mimageFile').change(function() { 
        var image=$(this).val();
       
        if(image){
            var postData=new FormData();
            postData.append('file',this.files[0]);     
            var url="{{route('ajax.file.upload','vendor')}}";     
            $.ajax({               
                async:true,
                type:"post",
                url:url,
                data:postData,
                cache:false,
                contentType: false,
                processData: false,
                success:function(data){
                  console.log(data);
                  if(data.path){
                    $('#mimageupload').val(data.path);
                    toastr.success('Menu Image '+data.message, { closeButton: !0 });
                  }else{
                    toastr.error('Menu Image '+data.message, { closeButton: !0 });
                  }
                },error: function(xhr, status, error) {
                  //console.log(xhr.responseJSON.errors.file);
                  var err=xhr.responseJSON.errors.file;
                   $.each(err,function(key,value){
                        toastr.error(value, { closeButton: !0 });
                      
                  });
                  
                }  
     
            });
        }
      }); 
/*Aadhar file Upload End*/
/*Aadhar file Upload Start*/
      $('#aadharFile').change(function() { 
        var image=$(this).val();
       
        if(image){
            var postData=new FormData();
            postData.append('file',this.files[0]);     
            var url="{{route('ajax.file.upload','vendor')}}";     
            $.ajax({               
                async:true,
                type:"post",
                url:url,
                data:postData,
                cache:false,
                contentType: false,
                processData: false,
                success:function(data){
                  console.log(data);
                  if(data.path){
                    $('#aadharupload').val(data.path);
                    toastr.success('Aadhar '+data.message, { closeButton: !0 });
                  }else{
                    toastr.error('Aadhar '+data.message, { closeButton: !0 });
                  }
                },error: function(xhr, status, error) {
                  //console.log(xhr.responseJSON.errors.file);
                  var err=xhr.responseJSON.errors.file;
                   $.each(err,function(key,value){
                        toastr.error(value, { closeButton: !0 });
                      
                  });
                  
                }  
     
            });
        }
      }); 
/*Aadhar file Upload End*/
/*Pan file Upload Start*/

      $('#panFile').change(function() { 
        var image=$(this).val();
       
        if(image){
            var postData=new FormData();
            postData.append('file',this.files[0]);     
            var url="{{route('ajax.file.upload','vendor')}}";     
            $.ajax({               
                async:true,
                type:"post",
                url:url,
                data:postData,
                cache:false,
                contentType: false,
                processData: false,
                success:function(data){
                  console.log(data);
                  if(data.path){
                    $('#panupload').val(data.path);
                    toastr.success('PAN '+data.message, { closeButton: !0 });
                  }else{
                    toastr.error('PAN '+data.message, { closeButton: !0 });
                  }
                },error: function(xhr, status, error) {
                  //console.log(xhr.responseJSON.errors.file);
                  var err=xhr.responseJSON.errors.file;
                   $.each(err,function(key,value){
                        toastr.error(value, { closeButton: !0 });
                      
                  });
                  
                }  
     
            });
        }
      }); 
/*PAN file Upload End*/
/*GSTIN file Upload Start*/

      $('#gstinFile').change(function() { 
        var image=$(this).val();
       
        if(image){
            var postData=new FormData();
            postData.append('file',this.files[0]);     
            var url="{{route('ajax.file.upload','vendor')}}";     
            $.ajax({               
                async:true,
                type:"post",
                url:url,
                data:postData,
                cache:false,
                contentType: false,
                processData: false,
                success:function(data){
                  console.log(data);
                  if(data.path){
                    $('#gstinupload').val(data.path);
                    toastr.success('GSTIN '+data.message, { closeButton: !0 });
                  }else{
                    toastr.error('GSTIN '+data.message, { closeButton: !0 });
                  }
                },error: function(xhr, status, error) {
                  //console.log(xhr.responseJSON.errors.file);
                  var err=xhr.responseJSON.errors.file;
                   $.each(err,function(key,value){
                        toastr.error(value, { closeButton: !0 });
                      
                  });
                  
                }  
     
            });
        }
      }); 
/*GSTIN file Upload End*/
/*FSSAI file Upload Start*/

      $('#fssaiFile').change(function() { 
        var image=$(this).val();
       
        if(image){
            var postData=new FormData();
            postData.append('file',this.files[0]);     
            var url="{{route('ajax.file.upload','vendor')}}";     
            $.ajax({               
                async:true,
                type:"post",
                url:url,
                data:postData,
                cache:false,
                contentType: false,
                processData: false,
                success:function(data){
                  console.log(data);
                  if(data.path){
                    $('#fssaiupload').val(data.path);
                    toastr.success('FSSAI '+data.message, { closeButton: !0 });
                  }else{
                    toastr.error('FSSAI '+data.message, { closeButton: !0 });
                  }
                },error: function(xhr, status, error) {
                  //console.log(xhr.responseJSON.errors.file);
                  var err=xhr.responseJSON.errors.file;
                   $.each(err,function(key,value){
                        toastr.error(value, { closeButton: !0 });
                      
                  });
                  
                }  
     
            });
        }
      }); 
/*FSSAI file Upload End*/

    });
  </script>

  {{-- <script type="text/javascript" src='{{ asset('js/other/vendor_map.js')}}'></script>
    <script type="text/javascript" src='https://maps.google.com/maps/api/js?sensor=false&libraries=places&key=AIzaSyCKtoBv2YH27eVVbIK6SFv_4ldXDUCGqFE'></script> --}}
@endsection

@section('content')
<style type="text/css">
  .text-red{color: red;font-weight: 700;}
</style>
<div class="card ">
  <div class="card-header">
    @if(Auth::guard('admin')->check())
      <h5 align="left">Add Vendor 

        <a class="btn btn-sm btn-info float-right" style="" href="{{ route('admin.vendor-list') }}"><b>Back</b></a>
    </h5>
     @endif
    @if(Auth::guard('vendor')->check())
     
     @endif
    

  </div>
  <div class="card-body">
   @if(Auth::guard('admin')->check())
    <form method="POST" action="{{ route('admin.store-vendor') }}" enctype="multipart/form-data">
    @endif
    @if(Auth::guard('vendor')->check())
     
      @endif

        @csrf
      <div class="form-row">
        
        <div class="form-group col-md-2">
          <label for="inputPassword4">Email <span class="text-red">*</span></label>
         <input type="email" tabindex="0" id="email" value="{{ old('email') }}" name="email" class="form-control" required=""  placeholder="Email">
         <span id="vendorEmail"></span>
        </div>

        <div class="form-group col-md-2">
          <label for="inputEmail4">Vendor Name <span class="text-red">*</span></label>
          <input type="text" tabindex="1" value="{{ old('name') }}" name="name" class="form-control" required placeholder="Name">
        </div>
        <div class="form-group col-md-2">
          <label for="inputEmail4">Type of Organization <span class="text-red">*</span></label>
         <select value="{{ old('org_type') }}" tabindex="2" id="" name="org_type"  class="form-control " required >
              <option value="">Please Select</option>
              <option value="1">Company</option>
              <option value="2">Partnership firm/LLP</option>
              <option value="3">Sole Proprietorship</option>
              <option value="4">Independent Body</option>
                  
          </select>
        </div>
        
        <div class="form-group col-md-3">
          <label for="inputPassword4">Company Name <span class="text-red">*</span></label>
         <input type="text" tabindex="2" required value="{{ old('company_name') }}" name="company_name" class="form-control"   placeholder="Company Name">
        </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">Designation <span class="text-red">*</span></label>
          <input type="text" tabindex="3" required value="{{ old('designation') }}" class="form-control" name="designation" placeholder="Designation">
        </div>

         <div class="form-group col-md-3">
          <label for="inputPassword4">Mobile Number <span class="text-red">*</span></label>
         <input type="text" tabindex="4" required value="{{ old('mobile') }}" name="mobile" class="form-control"  placeholder="Mobile Number">
        </div>
        
        <div class="form-group col-md-3">
          <label for="inputPassword4">Whatsapp Number <span class="text-red">*</span></label>
           <input type="text" tabindex="5" required name="telephone" value="{{ old('telephone') }}" class="form-control"  placeholder="Whatsapp Number">
        </div>
        <div class="form-group col-md-6" >
          <div id="map" tabindex="1000" style="width: 98%; height: 250px;position: absolute;"></div>
        </div>

        
        
        <div class="form-group col-md-6">
          <label for="inputPassword4">Company Address <span class="text-red">*</span></label>
         <input type="text" tabindex="6" required id="address" name="address" value="{{ old('address') }}" class="form-control"  placeholder="Company  Address">
         <div class="form-row">
            <div class="form-group col-md-6">
              <label for="inputPassword4">City <span class="text-red">*</span></label>
               <select value="{{ old('city') }}" tabindex="7" id="" name="city"  class="form-control " required >
                   <option value="">Please Select</option>
                  @foreach($city as $pcKey => $svalue)
                    <option value="{{ $svalue->id }}" data-name="{{ $svalue->name }}" >{{ $svalue->name }}</option>
                  @endforeach
                </select>
            </div>
            <div class="form-group col-md-6">
              <label for="inputPassword4">PIN Code<span class="text-red">*</span></label>
             <input type="text" tabindex="8" required name="pin" value="{{ old('pin') }}" class="form-control"  placeholder="PIN Code">
            </div>
            {{-- <div class="form-group col-md-6">
              <label for="inputPassword4">latitude <span class="text-red">*</span></label>
             <input type="text"  id="latitude" name="latitude" value="{{ old('latitude') }}" class="form-control"  placeholder="latitude">
            </div>
            <div class="form-group col-md-6">
              <label for="inputPassword4">longitude <span class="text-red">*</span></label>
             <input type="text"  id="longitude" name="longitude" value="{{ old('longitude') }}" class="form-control"  placeholder="longitude">
            </div> --}}
         </div>
        </div> 
        


        <hr class="col-12" style="margin: 8px;">
        <div class="form-group col-md-1">
          <label for="inputEmail4">Cost Of Two <span class="text-red">*</span></label>
          <input type="number" id="" value="{{ old('costoftwo') }}" name="costoftwo" class="form-control" required placeholder="Cost Of Two">
        </div>

        <div class="form-group col-md-1">
          <label for="inputEmail4">Open Time<span class="text-red">*</span></label>
          <input type="time" id="" value="{{ old('ontime') }}" name="ontime" class="form-control" required placeholder="Open Time" style="padding:2px;">

        </div>

        <div class="form-group col-md-1">
          <label for="inputEmail4">Close Time<span class="text-red">*</span></label>
          <input type="time" id="" value="{{ old('offtime') }}" name="offtime" class="form-control" required placeholder="Close Time" style="padding:2px;">
        </div>

        <div class="form-group col-md-12">
          <label for="inputEmail4">Overview<span class="text-red">*</span></label>
          <textarea name="about" class="form-control" required placeholder="Overview">{{ old('about') }}</textarea>
          {{-- <input type="text" id="" value="{{ $vendor->about }}" name="about" class="form-control" required placeholder="Cost Of Two"> --}}
        </div>
        
        {{-- <div class="form-group col-md-3">
          <label for="inputPassword4">Commission Plan<span class="text-red">*</span></label>       
           <select value="{{ old('commission_id') }}" name="commission_id" class="form-control " required >
               <option value="">Please Select</option>
              @foreach($commission as $pcKey => $svalue)
                <option value="{{ $svalue->id }}" {{ isset($vendor) ? ( $vendor->commission_id==$svalue->id ? "Selected":"" ) : ""}} >{{ $svalue->name }} ( {{ $svalue->commission }}% )</option>
              @endforeach
            </select>
        </div>
          --}}
        
        <hr class="col-12" style="margin: 8px;">
        <div class="form-group col-md-3">
          <label for="inputPassword4">Image</label>       
           <input type="file" tabindex="9" class="form-control" id="imageFile">
            <input type="hidden"  value="{{ old('image') }}" class="form-control" id="imageupload" name="image">
        </div>
         <div class="form-group col-md-3">
          <label for="inputPassword4">Cover Image</label>       
           <input type="file" tabindex="10" class="form-control" id="cimageFile">
            <input type="hidden"  value="{{ old('coverimage') }}" class="form-control" id="cimageupload" name="coverimage">
        </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">Menu Image</label>       
           <input type="file" tabindex="10" class="form-control" id="mimageFile">
            <input type="hidden"  value="{{ old('menu') }}" class="form-control" id="mimageupload" name="menu">
        </div>
         

         <hr class="col-12" style="margin: 8px;">
        <div class="form-group col-md-3">
          <label for="inputPassword4">Under Vertical<span class="text-red">*</span></label>       
           <select value="{{ old('masters_id') }}" name="masters_id" class="form-control " required >
               <option value="">Please Select</option>
              @foreach($master as $pcKey => $svalue)
                <option value="{{ $svalue->id }}"  >{{ $svalue->name }}</option>
              @endforeach
            </select>
        </div>
       {{--  <div class="form-group col-md-3">
          <label for="inputPassword4">Commission Plan<span class="text-red">*</span></label>       
           <select value="{{ old('commission_id') }}" name="commission_id" class="form-control " required >
               <option value="">Please Select</option>
              @foreach($commission as $pcKey => $svalue)
                <option value="{{ $svalue->id }}"  >{{ $svalue->name }} ( {{ $svalue->commission }}% )</option>
              @endforeach
            </select>
        </div> --}}
        <hr class="col-12" style="margin: 8px;">
        <div class="form-group col-md-3">
          <label for="inputPassword4">Aadhar No. <span class="text-red">*</span></label>
         <input type="text" tabindex="13" class="form-control"  name="aadhar" required  value="{{ old('aadhar') }}" placeholder="Aadhar Card">
         <input type="file" required class="form-control" id="aadharFile">
          <input type="hidden"  value="{{ old('aadharupload') }}" class="form-control" id="aadharupload" name="aadharupload">
        </div>

        <div class="form-group col-md-3">
          <label for="inputPassword4">PAN No. <span class="text-red">*</span></label>
         <input type="text" tabindex="14" class="form-control"  name="pan" required  value="{{ old('pan') }}" placeholder="PAN Card">
         <input type="file" required class="form-control" id="panFile">
          <input type="hidden" value="{{ old('panupload') }}" class="form-control" id="panupload" name="panupload">
        </div>

        <div class="form-group col-md-3">
          <label for="inputPassword4">GSTIN <span class="text-red"></span></label>
         <input type="text" tabindex="15" class="form-control" title="Goods and Services Tax Identification Number" name="gstin"   value="{{ old('gstin') }}" placeholder="Goods and Services Tax Identification Number ">
         <input type="file" class="form-control" id="gstinFile">
          <input type="hidden" value="{{ old('gstinupload') }}" class="form-control" id="gstinupload" name="gstinupload">
        </div>

         <div class="form-group col-md-3">
          <label for="inputPassword4">FSSAI Number </label>
         <input type="text" tabindex="16" class="form-control" title="Food Safety & Standards Authority of India" value="{{ old('fssai') }}"  name="fssai" placeholder="Food Safety & Standards Authority of India">
         <input type="file" class="form-control" id="fssaiFile">
          <input type="hidden" class="form-control" id="fssaiupload" value="{{ old('fssaiupload') }}" name="fssaiupload">
        </div>
  
 

        
        


      </div>
      
      <div class="form-row float-right">
          <button type="submit" class="btn btn-primary ">Save</button>
      </div>
    </form>

  </div>
  <div class="card-footer text-muted">
    {{-- {!! $uData->links() !!} --}}
  </div>
</div>
    
@endsection
