<!DOCTYPE html>
<html>
  <head>
    <title>Welcome to the BreadButterBasket.com</title>
  </head>
  <body>
    <h2>Welcome to the BreadButterBasket.com </h2>
    Dear Vendor,<br>
    <br/>
    Your registered email-id is {{ $vendor['email'] }} , Please click on the below link to verify your email account 
    <br/>
    <a href="{{url('vendor-verify', $vendor['token'] )}}">Verify Email</a>
  </body>
</html>