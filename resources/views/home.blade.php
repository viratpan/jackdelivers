@extends('layouts.app')
@section('script')
<script>

</script>
@endsection
@section('content')

<!--about section start-->
        <section class="about-section">
            <div class="container">
                <div class="row">
                    <div class="col md-12">
                        <h1 class="about-heading">   
                            <u class="mb-2" data-delay="5000" data-words>
                                <div class="heding-text">
                                    <span >{{ config('app.name') }}</span>
                                </div>
                                <span class="anim-line"></span>
                            </u>
                        </h1>
                        <p class="about-text">Why step out when you can get everything delivered at home with the tap of a button? Living in the metropolitan city, we never have plentiful time to do all the things we want to do.
                        Jack Delivers can change the way you move things, how you shop and lets you access your city like never before. It’s never easy to make purchases or drop off packages when you get busy with work, get stuck in traffic, or you might even end up forgetting about it completely.{{-- <br>
                        {{ config('app.name') }} is the NCR’s favourite delivery app that gets you Food, Grocery, Medicine, Fruits & Vegetables, Meat & Fish, Health & Wellness, Gifts and Send Packages from one end of the city to the other. From your local Kirana stores to your favourite brands, grocery shopping to your life saving medicines, we are always on the move for you. Why worry about your chores, when you can get it all DONE !!
                        All you need to do is, Tell us where to go, what needs to be done and when. What happens next? Sit back, and let us worry about your task-at-hand.
                        You could say that we are always on the move for you. --}}</p>
                    </div>
                </div>
            </div>
        </section>
        <!--about section end-->
        <!--Vertical  section start-->
        <section class="vendor-section-home">
            <div class="container">
                <div class="row">
                    @foreach($master as $key => $value)
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 mb-3">
                        <a href="{{ route('master',$value->slug)}}" class="@if($value->active!=1) coming-soon  @endif">
                            <div class="card">
                                <img src="{{ asset($value->image) }}" class="card-img" alt="{{ $value->name}}">
                                {{-- <span><i class="far fa-arrow-alt-circle-right"></i></span> --}}
                            </div>
                            @if($value->active!=1)
                                <div class="btn btn-soon">Coming Soon</div>
                            @endif
                        </a>
                    </div>
                    @endforeach
                    
                </div>
            </div>
        </section>
        <!--Vertical  section start-->



       
@endsection
