@extends('layouts.app')
@section('script')
@endsection

@section('content')
	<section class="about-section Cancel-page">
            <div class="container">
                <div class="row">
                    <div class="col md-12">
                        <h1 class="about-heading">Cancellations and Refunds</h1>
                        <p class="text"><b>1.</b> JackDelivers shall confirm and initiate the execution of the transaction initiated by you upon receiving confirmation from you for the same. If you wish to cancel a transaction on the Platform, You shall select the cancel option on the Platform. It is to be noted that you may have to pay a cancellation fee for transactions initiated on the Platform for which work has already been commenced by the Delivery Partner or the Merchant, as the case may be. With respect to work commenced by Merchants the cancellation fee will be charged to you who will be in accordance with the cancellation and refund policies of such Merchants.</p>
                        <p class="text"><b>2.</b>JackDelivers may cancel the transaction initiated by you on the Platform, if:</p>
                        <ul>
                            <li class="text-list">The designated address to avail the JackDelivers Services provided by you is outside the service zone of JackDelivers.</li>
                            <li class="text-list">Failure to get your response via phone or any other communication channel at the time of confirmation of the order booking.</li>
                            <li class="text-list">The transaction involves supply/delivery/purchase of any material good that is illegal, offensive or violation of the Terms of Use.</li>
                            <li class="text-list">[If the transaction involves the purchase of medicines for which a medical prescription prescribed by a medical practitioner is required and for which you have not provided such medical prescription or provided an invalid medical prescription.]</li>
                            <li class="text-list">Information, instructions and authorisations provided by you is not complete or sufficient to execute the transaction initiated by you on the Platform.</li>
                            <li class="text-list">If in case of tied-up Merchants, the Tied-Up Merchant outlet is closed.</li>
                            <li class="text-list">If a Delivery Partner is not available to perform the services, as may be requested.</li>
                            <li class="text-list">If any Item for which you have initiated the transaction is not in stock with the Merchant.</li>
                            <li class="text-list">If the transaction cannot be completed for reasons not in control of JackDelivers.</li>
                        </ul>
                        <p class="text"><b>3.</b>You shall only be able to claim refunds for transactions initiated by you only if you have already pre-paid the fees with respect to such transaction. Subject to relevant Merchant’s refund policy and in accordance therein, you shall be eligible to get the refund in the following circumstances:</p>
                        <ul>
                            <li class="text-list">Your package has been tampered or damaged at the time of delivery, as determined by JackDelivers basis the parameters established by JackDelivers in its sole discretion.</li>
                            <li class="text-list">If the wrong Item has been delivered to you, which does not match with the Item for which you had initiated a transaction on the Platform.</li>
                            <li class="text-list">JackDelivers has cancelled the order because of any reason mentioned above.</li>
                            <li class="text-list">All decisions with respect to refunds will be at the sole discretion of JackDelivers and in accordance with JackDelivers’s internal refund policy (Refund Matrix) and the same shall be final and binding. All refunds initiated by JackDelivers shall be refunded to the financial source account from which, you have initiated the transaction on the Platform.</li>
                        </ul>
                    
                    </div>
                </div>
            </div>
        </section>
@endsection