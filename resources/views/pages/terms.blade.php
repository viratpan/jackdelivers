@extends('layouts.app')
@section('script')
@endsection

@section('content')
	<section class="about-section tearms-condition">
            <div class="container">
                <div class="row">
                    <div class="col md-12">
                        <h1 class="about-heading">User Terms and Conditions</h1>
                        <h4>Jack Delivers - Terms of Use – V01 – MAR 2021</h4>
                        <p>
                        This document is published in accordance with the provisions of Rule 3 (1) of the Information Technology (Intermediaries guidelines) Rules, 2011 that require publishing the rules and regulations, privacy policy and Terms of Use for access or usage of<a href="#">www.jackdelivers.com</a>  website and JackDelivers application for mobile and handheld devices.
                        </p>
                        <p class="tr-title"><b>Terms of Use</b></p>
                        <p>1.These terms of use (the "Terms of Use") govern your use of our website www. jackdelivers.com (the "Website") and our “JackDelivers" application for mobile and handheld devices (the "App"). The Website and the App are jointly referred to as the "Platform". Please read these Terms of Use carefully before you use the services. If you do not agree to these Terms of Use, you may not use the services on the Platform, and we request you to uninstall the App. By installing, downloading or even merely using the Platform, you shall be contracting with Jack Delivers and you signify your acceptance to this Terms of Use and other Jack Delivers policies (including but not limited to the Cancellation & Refund Policy, Privacy Policy and Take Down Policy) as posted on the Platform and amended from time to time, which takes effect on the date on which you download, install or use the Platform, and create a legally binding arrangement to abide by the same.</p>
                        <p>2.	The Platform is owned and operated by PIONEER VENTURES, and having its registered office at REGENT, A-239, SHIPRA SUNCITY, INDRAPURAM, Ghaziabad, Uttar Pradesh, 201014, India. For the purpose of these Terms of Use, wherever the context so requires, "you", “user”, or “User” shall mean any natural or legal person who shall transaction on the Platform by providing registration data while registering on the Platform as a registered user using any computer systems. The terms “JackDelivers", "we", "us" or "our" shall mean PIONEER VENTURES.</p>
                        <p>3.	JackDelivers enables transactions on its Platform between participating restaurants/merchants and buyers, dealing in (a) prepared food and beverages, (b) consumer goods, and (c) other products and services ("Platform Services"). The buyers ("Buyer/s") can choose and place orders ("Orders") from a variety of products and services listed and offered for sale by various merchants including but not limited to the restaurants, eateries, medicines, fruits & vegetables and grocery stores ("Merchant/s"), on the Platform. Further, the Buyer can also place Orders for undertaking certain tasks on the Platform (“Tasks”). </p>
                        <div class="download-link"><a href="{{ asset('document/tnc.pdf')}}" target="_blank"><span><i class="fas fa-cloud-download-alt"></i></span>Download User T&C</a></div>
                    </div>
                </div>
            </div>
        </section>
@endsection