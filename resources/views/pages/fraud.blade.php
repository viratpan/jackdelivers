@extends('layouts.app')
@section('script')
@endsection

@section('content')
	<section class="about-section">
            <div class="container">
                <div class="row">
                    <div class="col md-12" style="text-align: center;">
                        <h1 class="about-heading"><strong>Scam Alert!</strong></h1>
                        <h2><strong>Dont fall for phishing attempts</strong></h2>
                        <p class="col-12">
                        	<h3><strong>{{config('app.name')}} customer care will never request sensitive bank information from you <br>
                        	- such incidents are scams and should be reported immediately.</strong></h3><br><br>

						Please do not share your debit/credit card number, CVV number, OTP, UPI/ATM pin and so on with anyone claiming to be a {{config('app.name')}} representative.<br>

						{{config('app.name')}} does not have any customer care number or helpline. {{config('app.name')}} or its authorised representatives will NEVER ask you to share financial details. If you’ve encountered such an incident, please know that it is fraud, and you must report it immediately to stay safe from a phishing attempt.
						For assistance on a {{config('app.name')}} order, click on the ‘Help’ section on the Swiggy app - there is no other help channel available.<br><br>
						
						<h4 style="text-align: left!important;">Watch out for:<br></h4>
						<ul style="text-align: left!important;">
							<li>Portals outside of {{config('app.name')}} publicising fake toll-free customer care numbers.</li>
							<li>Fake websites, blogs, or suspicious posts on social media sites.</li>
							<li>Suspicious links sent to you via WhatsApp, SMS or email.</li>
							<li>Proactive calls from unauthorised numbers requesting for personal/financial information.</li>
							<li>Scamsters claiming you have a refund amount pending, in an attempt to get you to disclose sensitive information.</li>
						</ul>
						
						{{-- Portals outside of Swiggy publicising fake toll-free customer care numbers.
						Fake websites, blogs, or suspicious posts on social media sites.
						Suspicious links sent to you via WhatsApp, SMS or email.
						Proactive calls from unauthorised numbers requesting for personal/financial information.
						Scamsters claiming you have a refund amount pending, in an attempt to get you to disclose sensitive information. --}}
					</p>
                    </div>
                </div>
            </div>
        </section>
@endsection