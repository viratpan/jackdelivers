@extends('layouts.app')
@section('script')
@endsection

@section('content')
<section class="about-us-dtl">
            <div class="clearfix">
                <div class="lft-about">
                    <img src="{{ asset('forntend/images/overview-img/celebration-about.jpg')}}" alt="">
                </div>

                <div class="rgt-about">

                    <div class="comas"><img src="'{{ asset('forntend/images/overview-img/inverted-commas.png')}}" alt=""></div>
                    <p><b>About Us:</b> Why step out when you can get everything delivered at home with the tap of a
                        button? Living in the metropolitan city, we never have plentiful time to do all the things we
                        want to do.</p>
                    <p>Jack Delivers can change the way you move things, how you shop and lets you access your city like
                        never before. It’s never easy to make purchases or drop off packages when you get busy with
                        work, get stuck in traffic, or you might even end up forgetting about it completely.</p>
                    <div class="moretext">

                        <p>JackDelivers.com is the NCR’s favourite delivery app that gets you Food, Grocery, Medicine,
                            Fruits & Vegetables, Meat & Fish, Health & Wellness, Gifts and Send Packages from one end of
                            the city to the other. From your local Kirana stores to your favourite brands, grocery
                            shopping to your life saving medicines, we are always on the move for you. Why worry about
                            your chores, when you can get it all DONE !!</p>
                        <p>All you need to do is, Tell us where to go, what needs to be done and when. What happens
                            next? Sit back, and let us worry about your task-at-hand.
                            You could say that we are always on the move for you.</p>

                    </div>
                    <a href="#" class="moreless-button" data-content="toggle-text">Read More</a>
                    <div class="comas"><img src="images/overview-img/inverted-commas.png" alt=""></div>

                </div>

            </div>
        </section>
        <section class="contact-us">
            <div class="clearfix">
                <div class="row">
                    <div class="col-lg-6 col-md-7 col-sm-12">
                        <div class="lft-contact">
                            <div class="content-center">
                                <h4 class="heading_3">Get In <b>Touch</b></h4>
                                <p><b>Jack Delivers</b><br>A-142, 3nd Floor, Sector 63,<br> Noida – 201301
                                    Uttar Pradesh, India</p>
                                <div class="contact-id">
                                    <h3>Contact Us</h3>
                                    <span class="mail-icon"><i class="fas fa-envelope"></i></span>
                                    <a href="mailto:contact@jackdelivers.com"> contact@jackdelivers.com</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-4 col-sm-12">
                        <div class="rgt-map">
                            <div class="mapouter">
                                <div class="gmap_canvas"><iframe width="600" height="500" id="gmap_canvas"
                                        src="https://maps.google.com/maps?q=A-142,%202nd%20Floor,%20Sector%2063,%20Noida%20%E2%80%93%20201301%20Uttar%20Pradesh,%20India&t=&z=13&ie=UTF8&iwloc=&output=embed"
                                        frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
@endsection