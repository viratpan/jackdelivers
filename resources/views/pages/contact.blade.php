@extends('layouts.app')
@section('script')
@endsection

@section('content')
	<section class="contact-us">
            <div class="clearfix">
                <div class="row">
                    <div class="col-lg-6 col-md-7 col-sm-12">
                        <div class="lft-contact">
                            <div class="content-center">
                                <h4 class="heading_3">Get In <b>Touch</b></h4>
                                <p><b>Jack Delivers</b><br>A-142, 3rd Floor, Sector 63,<br> Noida – 201301
                                    Uttar Pradesh, India</p>
                                <div class="contact-id">
                                    <h3>Contact Us</h3>
                                    <span class="mail-icon"><i class="fas fa-envelope"></i></span>
                                    <a href="mailto:contact@jackdelivers.com"> contact@jackdelivers.com</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-4 col-sm-12">
                        <div class="rgt-map">
                            <div class="mapouter">
                                <div class="gmap_canvas"><iframe width="600" height="500" id="gmap_canvas"
                                        src="https://maps.google.com/maps?q=A-142,%202nd%20Floor,%20Sector%2063,%20Noida%20%E2%80%93%20201301%20Uttar%20Pradesh,%20India&t=&z=13&ie=UTF8&iwloc=&output=embed"
                                        frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
@endsection