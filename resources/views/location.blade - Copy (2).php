@extends('layouts.app')
@section('script')
<script
      src="https://maps.googleapis.com/maps/api/js?key={{ config('app.map_key') }}&callback=initAutocomplete&libraries=places&v=weekly"
      defer
    ></script>

 <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
 {{-- <script src="{{ asset('js/other/location.js') }}" defer></script> --}}
<script type="text/javascript">

let autocomplete;
var lat;
var lng;
var address;        //A-142 sector 63, Noida Uttarpardesh
var lc;       //like Sector 63,Noida
var city;       //like Noida/Delhi

function initAutocomplete() {
  // Create the autocomplete object, restricting the search predictions to
  // geographical location types.
  autocomplete = new google.maps.places.Autocomplete(document.getElementById("autocomplete"));
 /*   document.getElementById("autocomplete"),
    { types: ["geocode"] }
  );*/
  // Avoid paying for data that you don't need by restricting the set of
  // place fields that are returned to just the address components.
  autocomplete.setFields(["address_component","formatted_address","geometry"]);
  //on chage 
  autocomplete.addListener('place_changed', function (){
        var place = autocomplete.getPlace(); 
        console.log(place);
        if (place.geometry) 
        {
           lat = place.geometry.location.lat();
           lng = place.geometry.location.lng();
        }
        if(place.address_components){
          setTimeout(function(){
                  // set states
                    for (var i = 0; i < place.address_components.length; i++) {
                        var arr=place.address_components[i].types;

                        if(jQuery.inArray("sublocality_level_1", arr) !== -1){
                            lc=place.address_components[i].long_name;
                            console.log('Location '+place.address_components[i].long_name); 
                        }
                        if(jQuery.inArray("locality", arr) !== -1){
                            city=place.address_components[i].long_name;
                            console.log('City '+place.address_components[i].long_name); 
                        }    
                        /*if(place.address_components[i].types[0] == 'sublocality_level_1'){
                           lc=place.address_components[i].long_name;
                           console.log('Location '+place.address_components[i].long_name); 
                        }                     
                        if(place.address_components[i].types[0] == 'locality'){ 
                          city=place.address_components[i].long_name;
                          console.log('City '+place.address_components[i].long_name);               
                                                      
                        }*/
                    }
                }, 1000);
           //address=place.address_components[0].formatted_address;
           address=place.formatted_address;
           console.log('address '+place.formatted_address); 
        }
          console.log("lat: "+lat+" lng: "+lng+" address: "+address);
         
  });
  
  
}

// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(showPosition);
    
  }
}
function showPosition(position){ 
    location.latitude=position.coords.latitude;
    location.longitude=position.coords.longitude;
    //latitudeAndLongitude.innerHTML="Latitude: " + position.coords.latitude +"<br>Longitude: " + position.coords.longitude; 
    var geocoder = new google.maps.Geocoder();
    var latLng = new google.maps.LatLng(location.latitude, location.longitude);

 if (geocoder) {
    geocoder.geocode({ 'latLng': latLng}, function (place, status) {
       if (status == google.maps.GeocoderStatus.OK) {
         console.log(place);
         lat = location.latitude;
         lng = location.longitude;
         if(place[0].address_components){
          setTimeout(function(){
                  // set states
                    for (var i = 0; i < place[0].address_components.length; i++) {
                      var arr=place[0].address_components[i].types;

                      if(jQuery.inArray("sublocality_level_1", arr) !== -1){
                          lc=place[0].address_components[i].long_name;
                          console.log('Location '+place[0].address_components[i].long_name); 
                      }
                      if(jQuery.inArray("locality", arr) !== -1){
                          city=place[0].address_components[i].long_name;
                          console.log('City '+place[0].address_components[i].long_name); 
                      } 
                        /*if(place[0].address_components[i].types[0] == 'sublocality_level_1'){
                           lc=place[0].address_components[i].long_name;
                           console.log('Location '+place[0].address_components[i].long_name); 
                        }                     
                        if(place[0].address_components[i].types[0] == 'locality'){ 
                          city=place[0].address_components[i].long_name;
                          console.log('City '+place[0].address_components[i].long_name);               
                                                      
                        }*/
                    }
                }, 1000);
           //address=place.address_components[0].formatted_address;
           address=place[0].formatted_address;
           console.log('address '+place[0].formatted_address); 
        }        
         console.log("lat: "+lat+" lng: "+lng+" address: "+address);
         
       }
       else {       
        console.log("Geocoding failed: " + status);
       }
    }); //geocoder.geocode()
  }      
} //showPosition


 </script>
@endsection

@section('content')
    <div class="about">
        <div class="container">
            <div class="row" align="center">
                {{-- <p>Home ->About</p> --}}
                <div class="col-6 row" style="text-align: left;">
                    <h3>Set Location</h3>
                    <p>Order food, groceries, fruits & vegetables and medicines from favourite restaurants/shop near you.</p>
                    
                    <div class="input-group mb-3" style="margin-bottom:3rem!important;">
                      <input type="text" id="autocomplete" onFocus="initAutocomplete()" class="form-control" placeholder="Enter your delivery location" aria-label="Enter your delivery location" aria-describedby="basic-addon2" >
                      <div class="input-group-append" >
                       
                       <button class="btn btn-outline-secondary form-control" onclick="geolocate()" type="button">Locate Me</button>
                      </div>
                    </div>



                    
                    
               </div>
                <div class="col-6 row">
                    <h3>Top Picks for You</h3>

                    @php  $master=DB::table('masters')->get(); @endphp
                    @foreach($master as $mkey => $mvalue)
                        <div class="col-md-3">
                            <img src="{{ asset($mvalue->image) }}">
                        </div>
                     @endforeach
                    
                </div>
            </div>
        </div>
    </div>
    
    <div class="app-store">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <img src="{{ asset('assets/images/mobile-screenshot.png') }}" style="width:200px;height:300px;margin-left:200px;">
                    <img src="{{ asset('assets/images/app-store.png') }}">
                    <img src="{{ asset('assets/images/play-store.png') }}">
                </div>
            </div>
        </div>
    </div>

@endsection
@section('style')
<style type="text/css">
#locationField,
#controls {
  position: relative;
  width: 480px;
}

/*#autocomplete {
  position: absolute;
  top: 0px;
  left: 0px;
  width: 99%;
}*/

.label {
  text-align: right;
  font-weight: bold;
  width: 100px;
  color: #303030;
  font-family: "Roboto", Arial, Helvetica, sans-serif;
}

#address {
  border: 1px solid #000090;
  background-color: #f0f9ff;
  width: 480px;
  padding-right: 2px;
}

#address td {
  font-size: 10pt;
}

.field {
  width: 99%;
}

.slimField {
  width: 80px;
}

.wideField {
  width: 200px;
}

#locationField {
  height: 20px;
  margin-bottom: 2px;
}
</style>
@endsection