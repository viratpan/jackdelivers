<header>
        <div class="container">
            <div class="head">
                
                    <div class="burgur-menu">
                        <spna class="lines"></spna>
                        <spna class="lines"></spna>
                        <spna class="lines"></spna>
                    </div>
               
                
                
                <div class="left-logo">
                    <a href="{{route('home')}}"><img src="{{ asset(config('app.logo')) }}"  alt="{{ config('app.name') }} Logo"></a>
                </div>
                <div class="middile-nav">
                    <nav>
                        <ul>
                        @php 
                             $currentURl= Route::getFacadeRoot()->current()->uri();    
                        @endphp
                    @desktop 
                        @if(!in_array($currentURl, config('app.showMaster')))
                            <li><a href="{{route('home')}}"><span class="home-icon"><i class="fas fa-home"></i></span></a></li>
                            
                        @php  $master=DB::table('masters')->orderBy('order', 'ASC')->get(); @endphp
                        @foreach($master as $mkey => $mvalue)
                            <li style="background-image: url({{ asset($mvalue->image) }})!important;">
                                @if($mvalue->active!=1)
                                    <a href="{{ route('master',$mvalue->slug) }}"><span>coming soon</span></a>
                                @else
                                    <a href="{{ route('master',$mvalue->slug) }}" style="background-color: unset;padding: 36px 55px;"></a>
                                @endif
                            </li>
                        @endforeach                            
                        @endif
                    @elsedesktop

                        <li><a href="{{route('home')}}"><span class="home-icon"><i class="fas fa-home"></i></span></a></li>
                            
                        @php  $master=DB::table('masters')->orderBy('order', 'ASC')->get(); @endphp
                        @foreach($master as $mkey => $mvalue)
                            <li style="background-image: url({{ asset($mvalue->image) }})!important;">
                                @if($mvalue->active!=1)
                                    <a href="{{ route('master',$mvalue->slug) }}"><span>coming soon</span></a>
                                @else
                                    <a href="{{ route('master',$mvalue->slug) }}" style="background-color: unset;padding: 36px 55px;"></a>
                                @endif
                            </li>
                        @endforeach                            
                        
                    @enddesktop
                        </ul>
                    </nav>
                </div>
                <div class="right-cart">
                    <ul>
                    
                        @if(Route::is('master'))    {{-- || Route::is('vendor')) --}}
                        <li><a href="{{route('search',)}}">
                                <span data-toggle="modal" data-target="#exampleModalCenter4" title="Search"><i
                                        class="fas fa-search"></i><span class="mobile-hide">Search</span></span>
                            </a>
                        </li>
                        @endif
                        <li><a >
                             <span data-toggle="modal" data-target="#exampleModalCenter3"><i class="fas fa-map-marker-alt"  ></i> <span class="mobile-hide">Location</span></span>
                        </a></li>
                        <li><a href="{{route('cart')}}">
                            <div class="basket">
                                <i class="fas fa-shopping-basket"></i>
                                <span id="cart_count" class="cart-count">0</span>
                            </div>
                        </a></li>
                    @guest
                       {{--  <li><a href="#" data-toggle="modal" data-target="#exampleModalCenter">Sign In</a></li> --}}
                        <li><a href="{{ route('login')}}" ><i class="fas fa-user"></i><span class="mobile-hide">Sign In</span></a></li>

                    @else
                        <li>
                        @desktop
                        <div class="user">
                            <div class="user-name">{{ ucfirst(Auth::user()->name) }}<span><i class="fas fa-caret-down"></i></span></div>
                            <div class="user-details">
                                <ul>
                                    <li>
                                        <a href="{{route ('user.profile') }}">Profile</a>
                                    </li>
                                    <li>
                                        <a href="{{route ('user.orders') }}">My Orders</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('logout') }}"  class="log-out"
                                              onclick="event.preventDefault();
                                              document.getElementById('logout-form').submit();"><i class="uil uil-lock-alt icon__1"></i>
                                       {{ __('Logout') }}
                                       </a>
                                       <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                          @csrf
                                       </form>
                                       {{--  <div class="log-out">Log Out</div> --}}
                                    </li>
                                </ul>
                            </div>
                        </div>
                        @elsedesktop
                            <a href="{{route ('user.profile') }}" ><i class="fas fa-user"></i></a>
                        @enddesktop
                        </li>
                    @endif
                    </ul>
                </div>
            </div>
        </div>
           
    </header>
