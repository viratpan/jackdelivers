<footer class="footer-area">
        <div class="container">
            <div class="main-footer">
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="footer-widget">
                        <h3>Quich Links</h3>
                        <ul class="footer-list">
                            <li><a href="{{route('home')}}">Home</a></li>
                            <li><a href="{{route('about')}}">About Us</a></li>
                            {{-- <li><a href="{{route('team')}}">Team</a></li>
                            <li><a href="{{route('home')}}">Careers</a></li>
                            <li><a href="{{route('home')}}">Blog</a></li> --}}
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="footer-widget">
                        <h3>Contact</h3>
                        <ul class="footer-list">
                            <li><a href="{{route('contact')}}">Help & Support</a></li>
                            <li><a target="_blank" href="{{route('vendor.dashboard')}}">Partner with us</a></li>
                            <li><a target="_blank" href="{{route('rider')}}">Ride with us</a></li>
                        </ul>
                    </div>_blank
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="footer-widget">
                        <h3>Legal</h3>
                        <ul class="footer-list">
                            <li><a href="{{route('terms')}}">Terms & Conditions</a></li>
                            <li><a href="{{route('refund')}}">Cancellation & Refund</a></li>
                            <li><a href="{{route('privacy')}}">Privacy Policy</a></li>
                           {{--  <li><a href="{{route('home')}}">Cookie Policy</a></li> --}}
                           {{--  <li><a href="{{route('home')}}">Offer Terms</a></li>--}}
                            <li><a href="{{route('fraud')}}">Phishing & Fraud</a></li> 
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="footer-widget">
                        <ul class="footer-list">
                            @if(config('app.apple_url'))
                            <li class="appStoreicon">
                                <a href="{{config('app.apple_url')}}">
                                    <img src="{{ asset('forntend/images/download-icon/icon-AppStore_lg30tv.png')}}" alt="{{ config('app.name') }} app store">
                                </a>
                            </li>
                            @endif
                            @if(config('app.android_url'))
                            <li class="googlePlayicon">
                                <a href="{{config('app.android_url')}}">
                                    <img src="{{ asset('forntend/images/download-icon/icon-GooglePlay_1_zixjxl.png')}}" alt="{{ config('app.name') }} app store">
                                </a>
                            </li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

        <div class="copy-right-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-md-3 col-sm-3">
                        <div class="left-logo">
                            <img src="{{ asset(config('app.wlogo')) }}" alt="">
                        </div>
                    </div>
               
                <div class="col-lg-8 col-md-5 col-sm-6">
                    <div class="copy-right-text text-center">
                        <p>&copy; 2021 {{ config('app.name') }}</p>
                    </div>
                </div>
                <div class="col-lg-2 col-md-4 col-sm-3 ">
                    <div class="right-icons">
                    @if(config('app.fb_url'))
                        <span><a href=""><i class="fab fa-facebook-f"></i></a></span>
                    @endif
                    @if(config('app.twitter_url'))
                        <span><a href=""><i class="fab fa-twitter"></i></a></span>
                    @endif
                    @if(config('app.youtube_url'))
                        <span><a href=""><i class="fab fa-youtube"></i></a></span>
                    @endif
                    @if(config('app.instagram_url'))
                        <span><a href=""><i class="fab fa-instagram"></i></a></span>
                    @endif
                    
                    </div>
                </div>
            </div>
            </div>
        </div>
    </footer>
