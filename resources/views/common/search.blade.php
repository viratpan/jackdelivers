@extends('layouts.app')
@section('script')
<script type="text/javascript">
    var store='0';
    var page='search';  
    $(document).ready(function () {
        $.ajaxSetup({
           headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
       });

    // search clear btn 
      $('.clear').click(function() {
        $('.sea-search').val('');
         $('#searchResult').hide();
      });

        
        $('#search').on('keypress',function(e) {
            if(e.which == 13) {
                var val=$( "#search" ).val();
                if(val){
                   // getThisData(); 
                }
            }
            if(e.which == 8) {
                $('#searchResult').hide();
            }
        });
        {{ isset($request['term']) ? 'getThisData();' : ""}}
        $('#search').autocomplete({
           appendTo: ".searchResultDiv",
          source: function( request, response ) {
            $.ajax( {
              url: "{{route('ajax.comman.search',$master->id)}}",
              dataType: "json",
              data: {
                term: request.term
              },
              beforeSend: function(){
      $('#searchResult').hide();
   },
              success: function( data ) {
                $('#searchResult').hide();
                    if(!data.length){
                        var result = [
                            {
                                label: 'No matches found', 
                                value: response.term
                            }
                        ];
                        response(result);
                    }
                    else{
                        // normal response
                        response($.map(data, function (item) {
                            return {
                                label: item.label,// + " ( Vendor )",
                                value: item.value,
                                url: item.url,
                                title: item.title,
                                image: item.image,
                                type: item.type,


                            }
                        }));
                    }
              }
            } );
          },
          minLength: 1,
          select: function( event, ui ) {
            $( "#search" ).val( ui.item.label );
            $( "#type" ).val( ui.item.type );
           
            //$('#search').trigger('keypress');
            getThisData();
          }
        } ).autocomplete("instance")._renderItem = function(ul, item) {
                        //console.log(item.label);                           
                           var label = '';
                           if (item.image){
                              label =`<div class="search-icon"><img src="`+ item.image + `" alt=""/>
                                </div> `;
                           }
                           label +=`<div class="search-details"><span>`+ item.label+`</span>`;   
                           if (item.title){
                              //label += " <span class='ui-autocomplete-item-sub'style='float:right;margin-right:8px;'><i>" + item.title + "</i></span>";
                              label+=`<span>`+item.title +`</span>`;
                           }
                           label +=`</div>`;
                           return $("<li></li>").append( label ).appendTo(ul);
                            //return $("<li></li>").data("item.autocomplete", item).append("<a>" + label + "</a>").appendTo(ul);
                        };

/*.autocomplete( "instance" )._renderItem = function( ul, item ) {

              return $( "<li>" )
                .append()
                .append( "<div>" + item.label + "<br>" + item.desc + "</div>" )
                .appendTo( ul );
            };*/
 
    });
 function getThisData(){
    var term=$( "#search" ).val();
    var type=$( "#type" ).val();
            $.get('{{route('ajax.this.search')}}', 
                { 'term': term,
                  'type': type,
                  'master_id':'{{$master->id}}'
                   },
              //  dataType: "json",               
             function( res ) {
                
                var vendorHtml=''; var itemHtml='';
                 if(res.vendorCount==0){//alert('vendor 0');
                     /* vendorHtml =`<div class="detial-cart col-12">                                 
                                      <div class="all-food" style="display:block;">                                    
                                        <div class="waights">No data Found.</div>
                                      </div>
                                    </div>`;
                    vendorHtmlmm =`<div class="detial-cart">
                                  <div class="your-cart">
                                    <h2>Cart</h2>
                                    <span class="cart-items">Empty</span>
                                  </div>
                                  <div class="all-food" style="display:block;">
                                    <img src="`+baseUrl+`/images/empty-cart.jpg" alt="Cart Empty" width="100%">
                                    <div class="waights">Good food is always cooking! Go ahead, order some yummy items from the menu.</div>
                                  </div>
                                </div>`;*/
                    
                }
                else{
                    var i=0;
                    $.map(res.vendor, function (item) {
                        
                        var imgHtml=`<img src="{{ asset(config('app.logo')) }}" alt="`+item.name+`" >;`
                        if(item.image){
                            imgHtml=`<img src="{{ asset('`+item.image+`')}}" alt="`+item.name+`" >`;
                        }
                        var costof2=``;
                        if(item.costoftwo){
                            costof2=`<div class="item-price">&#8377; `+item.costoftwo+` <span class="words">FOR TWO</span></div>`;
                        }
                        
                        vendorHtml+=`<div class="col-lg-3 col-md-4 col-sm-6 col-6 filterVendor" data-rating="0" data-costof2="`+item.costoftwo+`" data-time="" data-popular="`+i+`">
                                         <a href="`+baseUrl+`/store/{{$master->slug}}/`+item.id+`/`+item.slug+`">
                                            <div class="items">
                                                <div class="item-img">`+imgHtml+`</div>
                                                <div class="item-name">
                                                    <h2>`+item.company_name+`</h2>
                                                    <div class="item-title">
                                                        <p>`+item.city_name+`</p>
                                                    </div>
                                                </div>
                                                <div class="item-dtl">
                                                    <div class="item-rating">
                                                        <span class="item-stars"><i class="fas fa-star"></i></span>
                                                        <span class="item-rate">4.1</span>
                                                    </div>
                                                    <div class="item-hur">23 Min</div>
                                                    `+costof2+`
                                                </div>
                                                
                                            </div>
                                        </a>
                                    </div>`;
                        //console.log(vendorHtml);
                        i++;
                    });
                    
                }
/*items maping*/
                var itemHtml=''; var j=0;
                 if(res.itemsCount==0){//alert('vendor 0');
                      itemHtml =`<div class="detial-cart">                                  
                                  <div class="all-food" style="display:block;">                                    
                                    <div class="waights">No data found</div>
                                  </div>
                                 </div>`;
                    
                }
                else{
                     $.map(res.items, function (item) {
                        var imgHtml=`<img src="{{ asset(config('app.logo')) }}" alt="`+item[0].name+`" >;`
                        if(item[0].image){
                            imgHtml=`<img src="{{ asset('`+item[0].image+`')}}" alt="`+item[0].name+`" >`;
                        }
                        var discount=``;
                        if(item[0].product_cost){
                            discount=` <span style="text-decoration: line-through;color:gray;">&#8377 `+item[0].product_cost+`</span>`;
                        }
                        var itemsHtml=``;
                        if(item.length==1){
                            itemsHtml=`<button id="cart_btn_`+item[0].productsId+`" data-id="`+item[0].productsId+`" data-name="`+item[0].name+`" data-vendor_id="`+item[0].vendor_id+`" data-Pid="`+item[0].product_price_id+`" data-qty="" data-price="`+item[0].product_price+`" onclick="addCart(this);" data-action="1" type="button">
                                          <span class="add-icon"><i class="fas fa-plus"></i></span>Add
                                        </button>
                                        <div class="counter-number" id="item_counter_`+item[0].productsId+`">
                                          
                                            <div class="cartUpdate">
                                                <div class="value-button" id="decrease2" onclick="updateCart(this,`+item[0].productsId+`);" value ="-" data-id="`+item[0].productsId+`" data-name="`+item[0].name+`" data-vendor_id="`+item[0].vendor_id+`" data-Pid="`+item[0].product_price_id+`" data-qty="" data-price="`+item[0].product_price+`" data-action="0">-</div>

                                                <input type="number" style="width:24px" id="qty_`+item[0].productsId+`_`+item[0].product_price_id+`" value="0" />
                                                <div class="value-button" id="increase2" onclick="updateCart(this,`+item[0].productsId+`);" value ="+" data-id="`+item[0].productsId+`" data-name="`+item[0].name+`" data-vendor_id="`+item[0].vendor_id+`" data-Pid="`+item[0].product_price_id+`" data-qty="" data-price="`+item[0].product_price+`" data-action="1">+</div>
                                            </div>
                                        </div>`;
                        }else{
                           var customize=``;
                           $.map(item, function (vitem) {
                                customize+=`<div class="input-radio">
                                    <span class="custom-icons"></span>
                                    <span class="radio-title">
                                        `+vitem.product_qty+` `+vitem.attributes_name+`</span>
                                        <span>&#8377 `+vitem.product_price+`</span>
                                        <div class="add-food">
                                          <button id="cart_btn_`+vitem.productsId+`_`+vitem.product_price_id+`" data-id="`+vitem.productsId+`" data-name="`+vitem.name+`" data-vendor_id="`+vitem.vendor_id+`" data-Pid="`+vitem.product_price_id+`" data-qty="" data-price="`+vitem.product_price+`" onclick="addCart(this);" data-action="1" type="button">
                                                      <span class="add-icon"><i class="fas fa-plus"></i></span>Add
                                                    </button>
                                           <div class="counter-number" id="item_counter_`+vitem.productsId+`_`+vitem.product_price_id+`">
                                                <div class="cartUpdate">

                                                    <div class="value-button" id="decrease2" onclick="updateCart(this,`+vitem.productsId+`);" value ="-" data-id="`+vitem.productsId+`" data-name="`+vitem.name+`" data-vendor_id="`+vitem.vendor_id+`" data-Pid="`+vitem.product_price_id+`" data-qty="" data-price="`+vitem.product_price+`" data-action="0">-</div>
                                                    <input type="number" style="width:24px" id="qty_`+vitem.productsId+`_`+vitem.product_price_id+`" value="0" />
                                                    <div class="value-button" id="increase2" onclick="updateCart(this,`+vitem.productsId+`);" value ="+" data-id="`+vitem.productsId+`" data-name="`+vitem.name+`" data-vendor_id="`+vitem.vendor_id+`" data-Pid="`+vitem.product_price_id+`" data-qty="" data-price="`+vitem.product_price+`" data-action="1">+</div>

                                                </div>
                                            </div>
                                        </div>
                                  </div>`;
                             });
                           /*///////////////////////////*/
                           itemsHtml=`<button data-toggle="modal" id="customize_btn_`+item[0].productsId+`" data-target="#customize_`+item[0].productsId+`"><spna class="add-icon"><i class="fas fa-plus"></i></spna>Add</button>
                                <div class="modal fade customize-model" id="customize_`+item[0].productsId+`" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                      <div class="modal-content" style="background-color: #fff;padding: 1rem;text-align:left;">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                          </button>
                                          <div class="customize-details">
                                              <div class="custom-headind">
                                                  <h2>
                                                    <span class="custom-icons"></span>
                                                    Customize “`+item[0].name+`”
                                                  </h2>
                                                  
                                              </div>
                                              <div class="custom-items">`+customize+`</div>
                                             
                                          </div>
                                    </div>
                                  </div>
                                </div>`;
                        }

                        itemHtml+=`<div class="food-detials filterItem" data-costof2="`+ item[0].product_price+`" data-rating="0" data-time="" data-popular="`+j+`">
                                    <div class="food-image">`+imgHtml+`</div>
                                    <div class="food-name">
                                        <h2>`+item[0].name+`</h2>
                                         <div class="amounts">`+item[0].product_qty+' '+item[0].attributes_name+`</div>
                                        <div class="price">&#8377 `+ item[0].product_price+discount+`
                                        </div>
                                    </div>
                                    <div class="add-food">
                                    `+itemsHtml+`
                                    </div>
                                </div>
                      `;
                      j++;
                      });
                }
                $('#restaurantsData').html(vendorHtml);
                $('#dishes').html(itemHtml);
                  getCart();
                $('#searchResult').show();
// show similar vendors
            var vendorHtml2='';
                if(type==2){
                    var i=0;
                    $.map(res.ivendor, function (item) {
                        
                        var imgHtml=`<img src="{{ asset(config('app.logo')) }}" alt="`+item[0].vendor_name+`" >;`
                        if(item[0].vendorImage){
                            imgHtml=`<img src="{{ asset('`+item[0].vendorImage+`')}}" alt="`+item[0].vendor_name+`" >`;
                        }
                        var costof2=``;
                        if(item[0].vendorCostoftwo){
                            costof2=`<div class="item-price">&#8377; `+item[0].vendorCostoftwo+` <span class="words">FOR TWO</span></div>`;
                        }
                        
                        vendorHtml2+=`<div class="col-lg-3 col-md-4 col-sm-6 col-6 filterVendor" data-rating="0" data-costof2="`+item.costoftwo+`" data-time="" data-popular="`+i+`">
                                         <a href="`+baseUrl+`/store/{{$master->slug}}/`+item[0].id+`/`+item[0].vendorSlug+`">
                                            <div class="items">
                                                <div class="item-img">`+imgHtml+`</div>
                                                <div class="item-name">
                                                    <h2>`+item[0].vendor_name+`</h2>
                                                    <div class="item-title">
                                                        <p>`+item[0].city_name+`</p>
                                                    </div>
                                                </div>
                                                <div class="item-dtl">
                                                    <div class="item-rating">
                                                        <span class="item-stars"><i class="fas fa-star"></i></span>
                                                        <span class="item-rate">4.1</span>
                                                    </div>
                                                    <div class="item-hur">23 Min</div>
                                                    `+costof2+`
                                                </div>
                                                
                                            </div>
                                        </a>
                                    </div>`;
                        //console.log(vendorHtml);
                        i++;
                    });
                }
                $('#restaurantsData').append(vendorHtml2);
  /*/////////////////////////////////////////////////////////////////////////*/
                if(type==1){
                  $('#restaurantsli').addClass('active');
                  $('#restaurants').show();
                  $('#dishes').removeClass('active');
                  $('#dishes').hide();
                }else{
                  $('#restaurantsli').removeClass('active');
                  $('#restaurants').hide();
                  $('#dishes').addClass('active');
                  $('#dishes').show();
                }
             }/*else{
                 //toastr.error("Refresh Page and Try Again..", { closeButton: !0 }); 
             }*/
            );
    
 }   
  function fillterVendors(this_obj){   
            var by=$(this_obj).val(); //alert(by); 
            if(by){
                /*vendor*/   
            var vendor = $('.filterVendor');
              vendor.sort(function(a, b){
                  if(by==='costof2' ||by==='time' ||by==='popular' ){
                    return +$(a).data(by) - +$(b).data(by);
                  }else{
                    return +$(b).data(by) - +$(a).data(by);
                  }                  
              });
              vendor.appendTo('#restaurantsData');
              /*item*/   
            var item = $('.filterItem');
              item.sort(function(a, b){
                  if(by==='costof2' ||by==='time' ||by==='popular' ){
                    return +$(a).data(by) - +$(b).data(by);
                  }else{
                    return +$(b).data(by) - +$(a).data(by);
                  }                  
              });
              item.appendTo('#dishes');
            } 
            
        }
</script>
<script src="{{ asset('js/other/vendor_details.js')}}"></script>
@endsection
@section('content')

<section class="search-items">
            <div class="container">
                <div class="search-head">
                    <div class="search-input">
                        <div class="se-input">
                            <span class="se-icons"><i class="fas fa-search"></i></span>
                            <input type="text" id="search" class="sea-search" placeholder="{{$master->search_text}}"
                                required style="border-radius: 0;" value="{{ isset($request['term']) ? $request['term'] : ""}}">
                             <input type="hidden" id="type" value="">
                            <button class="clear">Clear</button>
                        </div>
                        <div class="searchResultDiv"></div>
                    </div>
                    <div class="close-tab">
                        <a href="{{$urlPrevious}}"></a>

                    </div>
                </div>
                {{-- <div class="search-detail">
                    <div class="result-list CLEARFIX">
                        <div class="list-head">Search Results</div>
                        <div class="search-results" id="searchResult">
                            <ul>
                                <li>
                                    <div class="search-icon"><img src="images/icons-images/serch-icon.jpg" alt=""></div>
                                    <div class="search-details">
                                        <p>Saket Callipolis</p>
                                        <span></span>
                                        <p>Rainbow Drive, Kaikondrahalli, Bengaluru, Karnataka, India</p>
                                    </div>
                                </li>
                                

                            </ul>
                        </div>
                    </div>
                </div> --}}
                <div class="se-container" id="searchResult">
                    <div class="se-top-tab">
                        <ul class="nav nav-tabs">                            
                            <li class="nav-item" id="restaurantsli">
                                <a class="nav-link" data-toggle="tab" href="#restaurants">{{$master->name}}</a>
                            </li>
                            <li class="nav-item" id="dishesli">
                                <a class="nav-link" data-toggle="tab" href="#dishes">Dishes</a>
                            </li>
                        </ul>
                            {{-- <div class="sort-all">
                                <button class="nav-link" data-toggle="tab" href="#sort">
                                    <span class="sortin">Sort In :-</span>
                                    <span class="sort-rate">
                                    <select name="filter" id="filter" onchange="fillterVendors(this);">
                                        <span><option value="popular">Relevance</option></span>
                                        <spna><option value="time">Delivery Time</option></spna>
                                        <spna><option value="rating">Rating</option></spna>
                                        <spna><option value="costof2">Cost For Two</option></spna>
                                    </select></span>
                                </button>
                            </div> --}}
                    </div>
                    <div class="se-item-dtl" style="width:100%;background:unset;box-shadow:unset;">
                        <div class="tab-content">
                            <div id="dishes" class="container tab-contents fade "><br>
                               
                            </div>
                            <div id="restaurants" class="container tab-contents tab-pane "><br>
                                <div class="row" id="restaurantsData">
                                    
                                </div>
                            </div>
                            
                        </div>
                        
                    </div>
                </div>
            </div>
        </section>

               
@endsection