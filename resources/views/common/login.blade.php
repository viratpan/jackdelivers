
 <!-- sign in form modal-->
        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  <div class="form">
                <div class="user-icon">
                    <i class="fas fa-user"></i>
                </div>
                <div class="user-form">
                    <form action="">
                        <input type="email" placeholder="Username">
                        <div class="eye-button">
                            <input type="password" placeholder="Password" name="password" id="password">
                            <i class="far fa-eye" id="togglePassword"></i>
                        </div>
                        
                        <div class="remember clearfix">
                            <input type="checkbox" value="" >
                        <label for=""> Remember Me</label>
                        </div>
                        <input type="submit" value="Log In">
                    </form>
                </div>
                <div class="sign-up">
                    <label>Not a member?<a href="#">Sign Up</a></label>
                </div>
               
              </div>
            </div>
          </div>
        </div>
<!-- sign in form modal end-->