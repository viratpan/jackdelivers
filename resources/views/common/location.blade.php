<!--map model start-->
  <script type="text/javascript">

    var setLocationurl="{{route('location.set')}}";
    var home="{{route('home')}}";
  </script>
 <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
 <script src="{{ asset('js/other/location.js') }}" defer></script>
 <script
      src="https://maps.googleapis.com/maps/api/js?key={{ config('app.map_key') }}&callback=initMap&libraries=places&v=weekly"
      defer
    ></script>
        <section class="map-model">
            <div class="modal fade" id="exampleModalCenter3" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-keyboard="false" data-backdrop="static">
                <div class="modal-dialog modal-dialog-centered" role="document">
                  <div class="modal-content">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <div class="search-heading">
                          <h1 >Add your Location</h1>
                          @if(Session::get('address')) 
                            <span>{{Session::get('address')}}</span><br>
                          @endif
                      </div>
                    <div class="search-location">
                        <span class="search-input"><input type="text" id="autocomplete" placeholder="Search for area, street name.."></span>
                        <span class="search-icon"><i class="fas fa-search-location"></i></span>
                    </div>
                    <div class="gps-location">
                        <span class="location" onclick="geolocate()"><i class="fas fa-crosshairs crosshairs-active"></i>Use Current Loctaion</span>
                    </div>
                    <div class="result-list CLEARFIX">
                        <div class="list-head">Search Results</div>
                        <div class="search-results">
                            <ul>
                                <li>
                                    <div class="search-icon"><i class="fas fa-map-marker"></i></div>
                                    <div class="search-details">
                                        <p>Saket Callipolis</p>
                                        <p>Rainbow Drive, Kaikondrahalli, Bengaluru, Karnataka, India</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="search-icon"><i class="fas fa-map-marker"></i></div>
                                    <div class="search-details">
                                        <p>Saket Callipolis</p>
                                        <p>Rainbow Drive, Kaikondrahalli, Bengaluru, Karnataka, India</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="search-icon"><i class="fas fa-map-marker"></i></div>
                                    <div class="search-details">
                                        <p>Saket Callipolis</p>
                                        <p>Rainbow Drive, Kaikondrahalli, Bengaluru, Karnataka, India</p>
                                    </div>

                                </li>
                                <li>
                                    <div class="search-icon"><i class="fas fa-map-marker"></i></div>
                                    <div class="search-details">
                                        <p>Saket Callipolis</p>
                                        <p>Rainbow Drive, Kaikondrahalli, Bengaluru, Karnataka, India</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="search-icon"><i class="fas fa-map-marker"></i></div>
                                    <div class="search-details">
                                        <p>Saket Callipolis</p>
                                        <p>Rainbow Drive, Kaikondrahalli, Bengaluru, Karnataka, India</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
              </div>
            </div>

        </section>
          <!--map model end-->