@extends('layouts.app')
@section('script')
<script type="text/javascript">
    var newAddress=`<div class="form-row">
                        <div class="form-group col-md-6">
                                <label for="" class="">Full Name</label>
                                <input type="text" required class="form-control" name="name" value="" placeholder="Save as like Home,Office">
                        </div>                             
                       <div class="form-group col-md-6">
                            <label for="" class="">Mobile</label>
                            <input type="text" required class="form-control" name="mobile" value="" placeholder="Mobile">
                       </div>
                       <div class="form-group col-md-12">
                            <label for="" class="">Address</label>
                            <input type="text" required class="form-control" name="address" value="" placeholder="Address">
                       </div>
                       <div class="form-group col-md-6">
                            <label for="" class="">landmark</label>
                            <input type="text"  class="form-control" name="landmark" value="" placeholder="landmark">
                       </div>
                        <div class="form-group col-md-6">
                            <label for="" class="">City</label>
                            <input type="text" required class="form-control" name="city" value="" placeholder="City">
                       </div>
                       <div class="form-group col-md-6">
                            <label for="" class="">State</label>
                            <input type="text" required class="form-control" name="state" value="" placeholder="State">
                       </div>

                    </div>`;
	$(document).ready(function () {
        //$('input[name="user_address"]:radio').trigger('click');
        getAddress();
        
		$( 'input[name="user_address"]:radio' ).change(function(){
            var val=$(this).val();
           // alert(val);
            if(val=='new'){
                $('#newAddress').html(newAddress);
                $('#newAddress').css('border',' 1px solid gray');
            }else{
                $('#newAddress').html('');
                $('#newAddress').css('border','0px');
            }
        });
        function getAddress() {
            var val=$('input[name="user_address"]:radio').val();
           // alert(val);
            if(val=='new'){//alert('set new address');
                $('#newAddress').html(newAddress);
                $('#newAddress').css('border',' 1px solid gray');
            }else{//alert('remove add');
                $('#newAddress').html('');
                $('#newAddress').css('border','0px');
            }
        }
    });
    function addAddress(){
        $('#addAddressDiv').hide();
        $('#addressFormDiv').show();
        $('#addressFormDiv').css('display','block!important');

        var order=`<a href="{{ route('order.confirm')}}" style="border:3px solid red;border-radius: 18px;color:red;" class="btn btn-md btn-dangar">Continue</a>`;
        /*$.get('{{ route('ajax.getUserAddress') }}',
          function(data){
             //$('#addressFormDiv').html(data);
              $('#addressFormDiv').append('wfhweehfruewhruehufhuqwehfuerhyfu98fu9qfuruqr');
            if(data==null){
                $('#addressFormDiv').append('no address founds');
            }
            $('#addressFormDiv').append(order);
            $('#addressFormDiv').css('margin-bottom','3rem');

          }
        );*/
    }

    $(document).ready(function(){
	$("#exampleRadios2").click(function(){
	  $("#form2").show();
	});
	$("#exampleRadios1").click(function(){
		$("#form2").hide();
	  });
	  $("#exampleRadios3").click(function(){
		$("#form2").hide();
	  });
  });
</script>
@endsection
@section('content')
<section class="steps-progress sign-up-form">
            <div class="container">
            <form method="GET" id="form1" action="{{route('order.confirm')}}">
                @csrf  
                <div class="row"> 
                    
                    <div class="col-lg-8 col-md-12 col-sm-12"> 
                        <div class="add-head"><h2>Address</h2></div>
                            <div class="all-address ">
                               {{--  <form method="GET" id="form1" action="{{route('order.confirm')}}">
                                    @csrf  --}}                               
                                    @if(isset($address))
            
                                      @foreach($address as $key => $value)
                                        <div class="form-check">
                                            <input type="radio" checked class="form-check-input" id="" name="user_address" value="{{$value->id}}">
                                            <label class="form-check-label" for="">{{$value->name}} <i>( {{$value->mobile}} )</i><br>
                                                {{$value->address}},{{$value->city}}
                                                @if($value->states) ,{{$value->states}} @endif
                                                 @if($value->pincode) <i>({{$value->pincode}})</i>@endif
                                            </label>
                                        </div>
                                                                   
                                      @endforeach 
                                    @endif
                                   
                                      <div class="form-check">
                                        <input class="form-check-input" @if(count($address)==0)  checked  @endif type="radio" name="user_address" id="exampleRadios2" value="new">
                                        <label class="form-check-label" for="exampleRadios2">
                                          Add Address
                                        </label>
                                      </div>
                               {{--  </form> --}}
                            </div>
                            <div class="row form-row-2" id="newAddress" >
              
                            </div>
                           
                            <div class="add-head"><h2>Payment</h2></div>
                            
                                
                                <div class="form-check">
                                    <input type="radio" class="form-check-input"  id="" name="payment" value="1">
                                    <label class="form-check-label" for="exampleRadios2">
                                     Cash on Delivery
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input type="radio" checked class="form-check-input" id="" name="payment" value="2">
                                    <label class="form-check-label" for="exampleRadios2">
                                     Payment Online
                                    </label>
                                </div>
                            
                            <div class="btn-sub">
                                <button type="submit">Continue</button>
                            </div>
                        </div> 
                        <div class="col-lg-4 col-md-12 col-sm-12">
                            <div class="all-cart-products clearfix">
                                <div class="tab-content" id="faq-tab-content">
                                    <div class="tab-pane show all-cart-products active" id="tab1" role="tabpanel" aria-labelledby="tab1">
                                        <div class="accordion food" id="accordion-tab-1">
                                           
 @php
        $total=0;
        $p_arr=array(); $p_temp=0;     // all items Price array
        $d_arr=array(); $d_temp=0;    // all items Delivery array
        $g_arr=array(); $g_temp=0;    // all items GST array
        foreach ($idata as $key => $mvalue) {
            $masters_id='';$masters_name=''; $price=0; $delivery=0; $gst=0;  $m_items=0;
            if(isset($mvalue[0])){
                $m_items=count($mvalue);
                $masters_id=$mvalue[0]['masters_id'];
                $masters_name=$mvalue[0]['masters_name'];
                $master=null;
                if($m_items!=0){
                    $master=DB::table('masters')->where('id', $masters_id)->first();
                    //var_dump($master);
                }
                @endphp
    {{-- main div start --}}
            <div class="card order-category">
                    <div class="pd-heading">
                        <h2> Order Summary <small>( {{$count}} items)</small></h2>
                    </div>
                <div class="card-header" id="accordion-tab-1-heading-{{$masters_id}}">
                    <div class="food-heading" id="checkout-fd" data-toggle="collapse" data-target="#accordion-tab-1-content-{{$masters_id}}" aria-expanded="false" aria-controls="accordion-tab-1-content-{{$masters_id}}">
                        <h1>{{ ucfirst($masters_name) }}</h1>
                        <span class="food-items">({{$m_items}} Items)</span>
                        <span class="fd-icons"><i class="fas fa-caret-down"></i></span>
                    </div>
                </div>
            {{-- collapse section --}}
                <div class="collapse show" id="accordion-tab-1-content-{{$masters_id}}" aria-labelledby="accordion-tab-1-heading-{{$masters_id}}" data-parent="#accordion-tab-1">
                {{-- card start --}}
                    <div class="card-body food">
                @php
               // echo "<hr>";
                $temp=collect($mvalue);
                $vitems = $temp->groupBy('vendor_id')->toArray();
                foreach ($vitems as $key => $value) {
                    $vendor_id='';$vendor_name='';$vendor_address='';$items=0; $v_price=0; $v_delivery=0; $distance=3.8;
                    if(isset($value[0])){
                        $items=count($value); 
                        
                        $vendor_id=$value[0]['vendor_id'];
                        $vendor_name=$value[0]['vendor_name'];
                        $vendor_address=$value[0]['vendor_address'];
                        @endphp
                         {{-- vendor show tab --}} 
                         <div class="card order-category">
                            <div class="card-header" id="accordion-tab-{{$vendor_id}}-heading-{{$vendor_id}}">
                                <div class="food-heading" id="checkout-fd" data-toggle="collapse" data-target="#accordion-tab-{{$vendor_id}}-content-{{$vendor_id}}" aria-expanded="false" aria-controls="accordion-tab-{{$vendor_id}}-content-{{$vendor_id}}">
                                    <h1>{{ ucfirst($vendor_name) }}</h1>
                                    <span class="food-items">({{$items}} Items)</span>
                                    <span class="fd-icons"><i class="fas fa-caret-down"></i></span>
                                </div>
                            </div>
                            <div class="collapse" id="accordion-tab-{{$vendor_id}}-content-{{$vendor_id}}" aria-labelledby="accordion-tab-{{$vendor_id}}-heading-{{$vendor_id}}" data-parent="#accordion-tab-{{$vendor_id}}">
                                    <div class="card-body food">

                        @php
                        foreach ($value as $key => $product) {
                           // echo $product['name'];echo "<br>";
                            $t_price=$product['qty']*$product['price'];
                             $price+=$t_price;
                                $v_price+=$t_price;
                        @endphp
                        {{--     --}}
                        <div class="food-detials"> 
                             <div class="food-image">
                                <img src="{{asset( $product['image'])}}" alt="{{ ucfirst($product['name']) }}"/>
                             </div> 
                            <div class="food-name">
                                {{-- <h2>{{ ucfirst($vendor_name) }}</h3></h2> --}}
                                <div class="waights">{{ ucfirst($product['name']) }}</div>
                                <small>{{ ucfirst($vendor_name) }}</small>
                                <div class="price"><small>{{ $product['attributes']}}</small> &#8377 {{ $product['price']}}</div>
                            </div>
                        </div>
                        @php
                        }
                        if($items!=0){
                            $v_delivery=$master->delivery_cost*$distance;
                            /* all item delivery_cost*/
                            $delivery+=$v_delivery;
                            /*this vendor all item price + this master delivery_cost*/
                                $v_price+=$v_delivery;
                            if($masters_id==4){ $gst+=($v_price*$master->gst)/100;}
                            array_push($p_arr,$price);
                            array_push($d_arr,$delivery);
                            array_push($g_arr,$gst);
                             $p_temp=$price;
                             $d_temp=$delivery;
                             $g_temp=$gst; 

                        }
                        @endphp
                                </div>
                            </div>
                        </div>
                    @php
                    }
                    
                }
            @endphp
              </div>  {{-- card section end --}}
              </div>{{-- collasp end section --}}
            </div>{{-- main div end --}}
            @php
            }
            $total=$p_temp+$d_temp+$g_temp;
            
        }
        /*echo "<hr>";*/
        //echo "item: <pre>",print_r($idata),"</pre></hr>";die();
    @endphp
                                                

                                                 



                                            

                                        </div>
                                    </div>
                                </div>
                                <div class="apply-cp">
                                    <label for="">Gift/Discount code</label>
                                    <input type="text" placeholder="">
                                    <button type="button">Apply</button>
                                </div>
                                <div class="total-check-out">
                                    <table class="t-check-out">
                                        <tbody>
                                            <tr>
                                                <td>Price:</td>
                                                <td>&#8377 {{number_format($p_temp, 2) }}</td>
                                              </tr>
                                              <tr>
                                                <td>Delivery :</td>
                                                <td>&#8377 {{number_format($d_temp, 2) }}</td>
                                              </tr>
                                              <tr>
                                                <td>Total:</td>
                                                <td>&#8377 {{number_format($d_temp+$p_temp, 2) }}</td>
                                              </tr>
                                              
                                              <tr>
                                                <td>GST( 5% Only in Resturant):</td>
                                                <td>&#8377 {{number_format($g_temp, 2) }}</td>
                                              </tr>
                                              <tr>
                                                <td>Total Amount:</td>
                                                <td>&#8377 {{number_format(round($total),2)}}</td>
                                              </tr>
                                             
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                   </form>
                </div> 
            </div> 
        </section>

@endsection

