<!-- @extends('layouts.app') -->

@section('content')
<br>
	     <div class="product-section section mt-30" style="margin-top: 10rem;" >
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-5">
					<div class="sign-form">
						<div class="sign-inner">

							<div class="form-dt">
								<div class="form-inpts checout-address-step">

									<div class="login">
										<h3>Login to your account</h3>
                    <!-- Login Form -->
                    					<div class="row" align="center">
                    						
												  <div class="col-md-5">
												    <a class="btn btn-outline-dark" href="{{ route('auth2','google') }}" role="button" style="text-transform:none">
												      <img width="20px" style="margin-bottom:3px; margin-right:5px" alt="Google sign-in" src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Google_%22G%22_Logo.svg/512px-Google_%22G%22_Logo.svg.png" />
												      Login with Google
												    </a>
												  </div>
												  <div class="col-md-5">
												    <a class="btn btn-outline-dark" href="{{ route('auth2','facebook') }}" role="button" style="text-transform:none">
												      <img width="20px" style="margin-bottom:3px; margin-right:5px" alt="Google sign-in" src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/51/Facebook_f_logo_%282019%29.svg/500px-Facebook_f_logo_%282019%29.svg.png" />
												      Login with Facebook
												    </a>
												  </div>
										</div>
										<hr>
										<form method="POST" class="my-4" action="{{ route('login') }}">
												 @csrf
                      						<div class="form-group">
												<input id="email" type="email" class="form-control lgn_input @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="Email" required autocomplete="email" autofocus>
											 <i class="uil uil-mobile-android-alt lgn_icon"></i>
											 @error('email')
											 	 <span class="invalid-feedback" role="alert">
											 			 <strong>{{ $message }}</strong>
											 	 </span>
											 @enderror
					                      </div>
					                      <div class="form-group">
												<input id="password" type="password"  class="form-control  lgn_input @error('password') is-invalid @enderror" name="password" placeholder="password" required autocomplete="current-password">

												<i class="uil uil-padlock lgn_icon"></i>
												@error('password')
														<span class="invalid-feedback" role="alert">
																<strong>{{ $message }}</strong>
														</span>
												@enderror
					                      </div>

					                      <button type="submit" class="btn mt-10 btn-primary">Login</button>
										</form>
											<div class="password-forgor">
												@if (Route::has('password.request'))
														<a class="btn btn-link" href="{{ route('password.request') }}">
																{{ __('Forgot Your Password?') }}
														</a>
												@endif
											</div>
											<div class="signup-link">
												{{-- <a href="" class="btn btn-facebook"> Facebook</a>
												<a href="{{ route('auth2','google') }}" class="btn btn-facebook"> google</a> --}}

												<p>Don't have an account? - <a href="{{route('register')}}">Sign Up Now</a></p>
											</div>


                </div>

								</div>

							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
@endsection
