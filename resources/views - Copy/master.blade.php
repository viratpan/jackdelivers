@extends('layouts.app')
@section('script')
<script type="text/javascript">
	var master_id='{{ $master_id}}';
	$(document).ready(function () {
		$.ajaxSetup({
           headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
       });
		$('#filter').change(function(){
			var id=$(this).val();
      		var selectedText = $("#filter option:selected").html();
      		//$("#filter option:selected").html('Sort by : '+selectedText);;
		});
		$( "#vendorSearch" ).autocomplete({
	      source: function( request, response ) {
	        $.ajax( {
	          url: "{{route('ajax.vendor.search',$master_id)}}",
	          dataType: "json",
	          data: {
	            term: request.term
	          },
	          success: function( data ) {
	            //response( data );
	            	if(!data.length){
				        var result = [
				            {
				                label: 'No matches found', 
				                value: response.term
				            }
				        ];
				        response(result);
				    }
				    else{
				        // normal response
				        response($.map(data, function (item) {
				            return {
				                label: item.label,// + " ( Vendor )",
				                value: item.value,
				                url: item.url
				            }
				        }));
				    }
	          }
	        } );
	      },
	      minLength: 1,
	      select: function( event, ui ) {
	        // similar behavior as an HTTP redirect
			//window.location.replace(ui.item.url);

			// similar behavior as clicking on a link
			window.location.href = ui.item.url;
	      }
	    } );


	    $( "#itemSearch" ).autocomplete({
	      source: function( request, response ) {
	        $.ajax( {
	          url: "{{route('ajax.item.search',$master_id)}}",
	          dataType: "json",
	          data: {
	            term: request.term
	          },
	          success: function( data ) {
	            //response( data );
	            	if(!data.length){
				        var result = [
				            {
				                label: 'No matches found', 
				                value: response.term
				            }
				        ];
				        response(result);
				    }
				    else{
				        // normal response
				        response($.map(data, function (item) {
				            return {
				                label: item.label,//+ " ( Product )",
				                value: item.value,
				                url: item.url
				            }
				        }));
				    }
	          }
	        } );
	      },
	      minLength: 1,
	      select: function( event, ui ) {
	        //window.location.replace(ui.item.url);
	        window.location.href = ui.item.url;
	      }
	    } );




    });
    
   
</script>
@endsection
@section('content')
<style>
  .ui-autocomplete {
    max-height: 300px;
    overflow-y: auto;
    /* prevent horizontal scrollbar */
    overflow-x: hidden;
  }
  /* IE 6 doesn't support max-height
   * we use height instead, but this forces the menu to always be this tall
   */
  * html .ui-autocomplete {
    height: 300px;
  }
  .ui-menu .ui-menu-item {
    
    border: 1px solid gray;
  }
  .ui-menu .ui-menu-item-wrapper {
   
    padding: 10px;
  }
 /*///////////////////////////////////////*/
  /*qty button*/
.quantity-button {
   border: 2px solid #green;
    border-radius: 10px;
    width: 86px;
   /* margin-bottom: 10px;*/
    position: absolute;
    right: 1rem;
    padding: 2.5px;
    display: inline-flex;
}
.quantity-button .inc-dec {
	/*border: 2px solid #green;*/
	background-color: white;
	font-size:20px;
	font-weight:600;
	border-radius: 10px;
}
.quantity-button .inc-dec:focus {
	/*border:0px solid #fff!important;*/
	background-color: green;
	color:white;
	border-radius: 10px;
}
.quantity-button .number-btn {
	
	width:20px;
	border:none;
}
  </style>
<div class="store" style="height: Auto!important;padding-bottom: 1rem;padding-top: 2.5rem;">
    <div class="container">
	    <div class="row">
	        <ul class="col-6">
	            <li>
		        	<img src="{{ asset($master->image) }}" alt="{{ $master->name }}">
		        </li>
		        <li><h1>{{ $master->name }}</h1></li>
	    	</ul>
	    	{{-- <div class="col-6 " style="margin-top: 8rem;display: inline-flex;">
	    		<select id="search_type" onchange="filterData(this,{{ $master->id }});" class="form-control " style="width: 100px;background-color: #f16a3b;color:#fff;">
	    			<option value="1">Vendor</option>
	    			<option value="2">item</option>
	    		</select>
	    		<input type="text" name="term" class="form-control ">
	    	</div> --}}

	    </div>
	    <div class="row">
	        
	    	<div class="col-5 " style="margin-top:;display: inline-flex;">
	    		<form method="GET" action="{{ route('master',$master->slug) }}" style="display: -webkit-inline-box;">
		    		<select id="search_type" name="type" onchange="vendorData(this,{{ $master->id }});" class="form-control " style="width:Auto;background-color: #f16a3b;color:#fff;">
		    			<option value="1">Select By Vendor</option>
		    			
		    		</select>
		    		<input  type="text" id="vendorSearch" style="margin: 0px 4px;" name="search" class="col-8 form-control ">
		    		<button type="submit" class="btn btn-sm btn-info" style="padding: 7px;">Submit</button>
		    	</form>
	    	</div>
	    	<div class="col-5 " style="margin-top:;display: inline-flex;padding-left: 33px;">
	    		<form method="GET" action="{{ route('master',$master->slug) }}" style="display: -webkit-inline-box;">
		    		<select id="search_type2" name="type" onchange="itemData(this,{{ $master->id }});" class="form-control " style="width:Auto;background-color: #f16a3b;color:#fff;">
		    			
		    			<option value="2">Select By Item</option>
		    		</select>
		    		<input type="text" id="itemSearch" style="margin: 0px 4px;" name="search" class="col-8 form-control ">
		    		<button type="submit" class="btn btn-sm btn-info " style="padding: 7px;">Submit</button>
		    	</form>
	    	</div>
	    	<div class="col-2" style="padding-left: 25px;">
	    		<select id="filter" {{-- onchange="filterData(this,{{ $master->id }});" --}} class="form-control " style="width: ;font-size:14px;background-color: #f16a3b;color:#fff;">
	    			{{-- <option value="1">Sort By</option> --}}
	    			<option value="1">Sort by : Popularity</option>
	    			<option value="2">Sort by : Top Rated</option>
	    			<option value="3">Sort by : Top Discount</option>
	    		</select>
	    	</div>
	    </div>
	</div>
</div>

<div class="vendor">
    <div class="container">
	    <div class="row" id="result">
	@if($type==1)
		<div class="col-md-12" style="text-align: center;"><h2>Vendors</h2></div>
	    @foreach($vendor as $key => $value)
	        <div class="col-md-6">
	          <a href="{{ route('vendor',['type' => $master->slug,'id' => $value->id, 'slug' => Str::slug($value->company_name)])}}">
	            <div class="left-store">
	                
	                @if($value->image)
	                <img src="{{ asset($value->image) }}" alt="{{ $value->name}}" style="width:125px;height:125px;">	                @else
	                <img src="{{ asset(config('app.logo')) }}" alt="{{ $value->name}}" style="width:125px;height:100px;">	                @endif
	                <div class= "store-details">
	                <h5>{{ ucfirst($value->company_name) }}</h5><br><p>  </p>
	                {{-- <p> {{$value->distance}}Km <br> --}}
	                {{-- {{ Str::substr($value->address,0,50) }} --}}{{$value->city_name}} </p><br>
	                 <!-- <p style="color:black;">Exclusive Offers on selected item!</p> -->
	            </div>
	            </div>
	          </a>
	        </div>
	    @endforeach    	     
	@else
		<div class="col-md-12" style="text-align: center;"><h2>Items</h2></div>
		@foreach($item as $key => $value)
	        <div class="col-md-6">
	          <a href="{{ route('vendor',['type' => $master->slug,'id' => $value->vendor_id, 'slug' => Str::slug($value->vendor_company_name),'item'=>$value->slug])}}">
	            <div class="left-store">
	                @if($value->image)
	                	<img src="{{ asset($value->image) }}" alt="{{ $value->name}}" style="width:125px;height:125px;">
	                @else
	                	<img src="{{ asset(config('app.logo')) }}" alt="{{ $value->name}}" style="width:125px;height:100px;">
	                @endif
	                <div class= "store-details">
	                <h5>{{ ucfirst($value->name) }}</h5><br>
	                <p>{{ ucfirst($value->vendor_company_name) }} <br>
	                	Rs. {{ $value->product_price }}</p>
	                	@if($value->product_discount!=0)
                            <span style="text-decoration: line-through;color:gray;">Rs. {{$value->product_cost}}</span>
                            <b style="color:tomato">({{$value->product_discount}}%)</b>
                          @endif
	                
	                <p><i>( {{ $value->product_qty.' '.$value->attributes_name }} )</i></p>
	                 
	            </div>
	            </div>
	          </a>
	        </div>
	    @endforeach
	@endif    
		</div>
	</div>
</div>
@endsection