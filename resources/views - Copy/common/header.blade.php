<div class="navbar-area">
        <div class="mobile-nav">
            <a href="index.html" class="logo">
                <img src="assets/images/logo.png" alt="Logo">
            </a>
        </div>

        <div class="main-nav">
            <div class="container-fluid">
                <nav class="navbar navbar-expand-md navbar-light ">
                    <a class="navbar-brand" href="index.html">
                        <img src="assets/images/logo.png" alt="Logo">
                    </a>
                    <div class="collapse navbar-collapse mean-menu" id="navbarSupportedContent">
                        <ul class="navbar-nav m-auto">
                            {{-- <li class="nav-item">
                                <a href="{{route('home')}}" class="nav-link active"><i class="fas fa-home"></i> Home
                                </a>
                            </li> --}}

							@php 
							     $currentURl= Route::getFacadeRoot()->current()->uri();    
							@endphp
							@if(!in_array($currentURl, config('app.showMaster')))
							    <li class="nav-item">
	                                <a href="{{route('home')}}" class="nav-link active"><i class="fas fa-home"></i> Home
	                                </a>
	                            </li>
							        
							    @php  $master=DB::table('masters')->orderBy('order', 'ASC')->get(); @endphp
							       @foreach($master as $mkey => $mvalue)
							       	<li class="nav-item">
		                                <a href="{{ route('master',$mvalue->slug) }}" class="nav-link @if($mvalue->active!=1) soon  @endif">
		                                    <figure class="figure">
		                                        <img src="{{ asset($mvalue->image) }}" class="figure-img" alt="{{ $mvalue->name }}">
		                                        @if($mvalue->active!=1)
		                                        	<figcaption class="figure-caption">Coming Soon</figcaption>
		                                        @endif
		                                    </figure>
		                                </a>
		                            </li>
							        
							       @endforeach
							    
							@endif 
                            
                        </ul>
                        
                        <div class="others-options d-flex align-items-center">
                            <div class="option-item">
                                <div class="location-btn">
                                    <a href="{{ route('location') }}" class="location-btn-icon">
                                        <i class="fas fa-map-marker-alt"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="option-item">
                                <div class="add-cart-btn">
                                    <a href="{{route('cart')}}" class="cart-btn-icon">
                                        <i class="fas fa-shopping-cart"></i>
                                        <span>0</span>
                                    </a>
                                </div>
                            </div>
                        @guest
                            <div class="option-item">
                                <div class="login-btn">
                                    <a href="#" class="login-btn-icon">
                                        <span class="ls-icon"><i class="fas fa-sign-in-alt"></i></span> <span class="ls-text">Log In/Sign Up</span>
                                    </a>
                                </div>
                            </div>
                        @else

                        @endif
                        </div>
                    </div>
                </nav>
            </div>
        </div>
        <div class="side-nav-responsive">
            <div class="container-fluid">
                <div class="dot-menu">
                    <div class="circle-inner">
                        <div class="option-item">
                            <div class="location-btn">
                                <a href="{{ route('location') }}" class="location-btn-icon">
                                    <i class="fas fa-map-marker-alt"></i>
                                </a>
                            </div>
                        </div>
                        <div class="option-item">
                            <div class="add-cart-btn">
                                <a href="{{ route('cart') }}" class="cart-btn-icon">
                                    <i class="fas fa-shopping-cart"></i>
                                    <span>0</span>
                                </a>
                            </div>
                        </div>
                        <div class="option-item">
                            <div class="login-btn">
                                <a href="#" class="login-btn-icon">
                                    <span class="ls-icon"><i class="fas fa-sign-in-alt"></i></span> <span class="ls-text">Log In/Sign Up</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

