 <!-- Navigation -->
 <style type="text/css">
   .comingSonehead{
       /* position: relative;*/
       /* left:-100px; /*2%;*/*/
        top:0px; /*-52px;*/
        background: snow;
       /* padding: 5px;*/
        color: tomato;
   }
 </style>
  <nav class="navbar navbar-expand-lg navbar-dark bg-white fixed-top">
    <div class="container">
      <a class="navbar-brand" href="{{route('home')}}">
        <img src="{{ asset(config('app.logo')) }}" style="width:135px;" alt="{{config('app.name')}}">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
@php 
     $currentURl= Route::getFacadeRoot()->current()->uri();
    
     
@endphp
    @if(!in_array($currentURl, config('app.showMaster')))
       <ul class="navbar-nav ml-auto" style="">
            <a class="nav-link" href="{{route('home')}}" style="text-align:center;color:red;font-weight:600;font-size: 15px;background-color: #fff;border-radius: 20px;padding:10px;border:1px solid transparent;margin-top: 1rem;">Home</a>
          @php  $master=DB::table('masters')->orderBy('order', 'ASC')->get(); @endphp
           @foreach($master as $mkey => $mvalue)
            @if($mvalue->id==4)
               <li class="nav-item">
                <a class="nav-link" href="{{ route('master',$mvalue->slug) }}">
                  <img src="{{ asset($mvalue->image) }}" style="width:120px;height:100px;">
                  <span class="sr-only">(current)</span>
                </a>
              </li>
            @else
              <li class="nav-item" style="height: 100px;width:120px;">
                <a class="nav-link" >
                  <img src="{{ asset($mvalue->image) }}" style="width:100px;height:100px;">
                  <span class="sr-only">(current)</span>
                  <span class="comingSonehead">Coming Soon</span>
                </a>
              </li>
             @endif
           @endforeach
        </ul>
     @endif

        
        <ul class="navbar-nav ml-auto" style="float:right;">
          <li class="nav-item">
            <a class="nav-link" href="{{route('location')}}">
              <img src="{{ asset('assets/images/location.png') }}" style="width:60px;height:60px;">
              <span class="sr-only">(current)</span>
            </a>
          </li>
          {{-- <li class="nav-item" >
            
             <a class="nav-link" href="{{route('vendor.dashboard')}}">
              <img src="{{ asset('assets/images/vendor.png') }}" style="margin-top:11px;"></a>
          </li> --}}
          <li class="nav-item">
            <a class="nav-link" href="{{ route('cart')}}">
              <img src="{{ asset('assets/images/ecom.png') }}" style="width:60px; height: 60px;">
              <span id="cart_count" style="position: fixed;" class="@if(Session::get('cart')!==null) cart_count @endif">@if(Session::get('cart')!==null) {{ count(Session::get('cart', array())) }}  @endif</span>
            </a>
          </li>
          @guest
              <li class="nav-item">
                <a class="nav-link" href="{{ url('/login') }}">
                  <img src="{{ asset('assets/images/customer-login.png') }}" style="margin-top:11px;"></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{ url('/register') }}">
                  <img src="{{ asset('assets/images/customer-register.png') }}" style="margin-top:11px;"></a>
              </li>
               
          @else
                  <li class="nav-item dropdown pr-2" style="margin-top: 1.5rem;">
                     <span class="dropdown-toggle" id="dropdownMenu12"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                     <i class="fa fa-user"></i> {{ ucfirst(Auth::user()->name) }}<span class="caret"></span> </span>
                     <ul class="dropdown-menu" id="dropdownMenu12ul"  aria-labelledby="dropdownMenu125213" style="text-align: center;top: 1.4rem;padding:1px;">
                        <!-- <li><a href="#">My Account</a></li> -->
                        <li style="border-bottom: 1px solid rgba(0,0,0,.15);">
                          <a href="{{route ('user.profile') }}" style="text-decoration:none;color: black;">My Profile</a>
                        </li>
                        {{-- <li style="border-bottom: 1px solid rgba(0,0,0,.15);">
                          <a href="{{route ('user.orders') }}" style="text-decoration:none;color: black;">My Orders</a>
                        </li>

                        <li style="border-bottom: 1px solid rgba(0,0,0,.15);">
                          <a href="{{route ('user.address') }}" style="text-decoration:none;color: black;">My Adddress</a>
                        </li>

                        
                       --}}
                        <li style="border-bottom: 1px solid rgba(0,0,0,.15);">
                           <a href="{{ route('logout') }}" style="text-decoration:none;color: black;"
                              onclick="event.preventDefault();
                              document.getElementById('logout-form').submit();"><i class="uil uil-lock-alt icon__1"></i>
                           {{ __('Logout') }}
                           </a>
                           <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                              @csrf
                           </form>
                        </li>
                     </ul>
                  </li>


          @endif
          
        </ul>
      </div>
    </div>
  </nav>