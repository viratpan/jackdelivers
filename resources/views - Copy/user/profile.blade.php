@extends('layouts.app')
@section('script')
@endsection
@section('content')
<div class="container" style="margin-top: 120px;">
    <div class="main-body">
    
          <!-- Breadcrumb -->
          {{-- <nav aria-label="breadcrumb" class="main-breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="index.html">Home</a></li>
              <li class="breadcrumb-item"><a href="javascript:void(0)">data</a></li>
              <li class="breadcrumb-item active" aria-current="page">data Profile</li>
            </ol>
          </nav> --}}
          <!-- /Breadcrumb -->
    
          <div class="row gutters-sm">
            <div class="col-md-4 mb-3">
              <div class="card">
                <div class="card-body">
                  <div class="d-flex flex-column align-items-center text-center">
                    {{-- <img src="https://bootdey.com/img/Content/avatar/avatar7.png" alt="Admin" class="rounded-circle" width="150"> --}}
                    <div class="mt-3">
                      
                      <p class="text-secondary mb-1">
                      	 <img src="https://bootdey.com/img/Content/avatar/avatar7.png" alt="Admin" class="rounded-circle" width="50">
                      	Hello, <b>{{ucfirst($data->name)}}</b>
                      </p>
                      {{-- <p class="text-muted font-size-sm">Bay Area, San Francisco, CA</p>
                      <button class="btn btn-primary">Follow</button>
                      <button class="btn btn-outline-primary">Message</button> --}}
                    </div>
                  </div>
                </div>
              </div>
              <div class="card mt-3">
              	<nav style="border-bottom: 1px solid #8080809e;"> 
						<div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist" style="width: 100%;display:inline-grid;">
							
							<a class="nav-item nav-link active" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Profile Information</a>
              				<a class="nav-item nav-link " id="nav-address-tab" data-toggle="tab" href="#nav-address" role="tab" aria-controls="nav-address" aria-selected="true">Manage Addresses</a>
              				<a class="nav-item nav-link" id="nav-order-tab" data-toggle="tab" href="#nav-order" role="tab" aria-controls="nav-order" aria-selected="false">My Orders</a>
							<a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">My Reviews & Ratings</a>
							<a class="nav-item nav-link" id="nav-noti-tab" data-toggle="tab" href="#nav-noti" role="tab" aria-controls="nav-noti" aria-selected="false">All Notifications</a>
							<a class="nav-item nav-link" id="nav-coupon-tab" data-toggle="tab" href="#nav-coupon" role="tab" aria-controls="nav-coupon" aria-selected="false">My Coupons</a>
              <a class="nav-item nav-link" id="nav-invite-tab" data-toggle="tab" href="#nav-invite" role="tab" aria-controls="nav-invite" aria-selected="false">Invite your friends</a>
              
               <a class="nav-item nav-link" href="{{ route('logout') }}" style="text-align:center;background-color: #e30f0fbf;color: #fff;"
                  onclick="event.preventDefault();
                  document.getElementById('logout-form').submit();"><i class="uil uil-lock-alt icon__1"></i>
               {{ __('Logout') }}
               </a>
               <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                  @csrf
               </form>
                        
						</div>
					</nav>
               
              </div>
            </div>
            <div class="col-md-8 tab-content px-3 px-sm-0" id="nav-tabContent">
            	<div class="tab-pane fade show active" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
					<div class="card mb-3">
                		<div class="card-body" style="margin:1rem;">
                			<div class="row">
			                    <div class="col-sm-3">
			                      <h6 class="mb-0">Full Name</h6>
			                    </div>
			                    <div class="col-sm-9 text-secondary">
			                      {{$data->name}}
			                    </div>
			                  </div><hr>
			                  <div class="row">
			                    <div class="col-sm-3">
			                      <h6 class="mb-0">Email</h6>
			                    </div>
			                    <div class="col-sm-9 text-secondary">
			                      {{$data->email}}
			                    </div>
			                  </div><hr>
			                  <div class="row">
			                    <div class="col-sm-3">
			                      <h6 class="mb-0">Mobile</h6>
			                    </div>
			                    <div class="col-sm-9 text-secondary">
			                      {{$data->mobile}}
			                    </div>
			                  </div><hr>
			                 @if(Session::get('address'))
			                 	<div class="row">
			                    <div class="col-sm-3">
			                      <h6 class="mb-0">Current Address</h6>
			                    </div>
			                    <div class="col-sm-9 text-secondary">
			                      {{Session::get('address')}}
			                    </div>
			                  </div>
		                        
		                     @endif
			                  
                		</div>
                	</div>
				</div>
				<div class="tab-pane fade " id="nav-address" role="tabpanel" aria-labelledby="nav-address-tab">
					<div class="card mb-3">
                		<div class="card-body" style="margin:1rem;">
                			@if($address)
                			@foreach($address as $key => $aval)
		                    <div class="row text-secondary" >
		                    	<div class="col-sm-12" style="padding:10px;margin-bottom: 4px;border: 1px solid gray;">
			                      <b>{{$aval->name}}   ({{$aval->mobile}})</b><br>
			                      {{$aval->address}},{{$aval->city}}
			                        @if($aval->states) ,{{$aval->states}} @endif
	                         		@if($aval->pincode) <b>({{$aval->pincode}})</b>@endif
			                      @if($aval->lankmark) Landmark:{{$aval->lankmark}} @endif
		                      	</div>
		                    </div>
		                    @endforeach
		                    	<div align="center" style="text-align: center;margin-top: 1rem;margin-bottom: 5rem;">
                          <button class="btn btn-md btn-info">Add New Address</button>
                        </div>
		                    @else
                        <div align="center" style="text-align: center;margin-top: 5rem;margin-bottom: 5rem;">
                          <b>No Address<br>
                           You have not add any address yet!</b>
                        </div>
		                    @endif
                		</div>
                	</div>
				</div>
				<div class="tab-pane fade " id="nav-order" role="tabpanel" aria-labelledby="nav-order-tab">
					<div class="card mb-3">
                		<div class="card-body" style="margin:1rem;">
                			@if($order)
                			@foreach($order as $key => $aval)
                			 <a href="" style="text-decoration-line: unset;">
		                    <div class="row text-secondary" style="padding:10px;margin-bottom: 4px;border: 1px solid gray;">
		                     
		                    	<div class="col-sm-6" >
			                      <b>{{$aval->masters_name}}   ({{$aval->vendor_company_name}})</b><br>
			                      <small>Total Item: {{$aval->items}}</small>                     
		                      	</div>
		                      	
		                      	<div class="col-sm-2" >
			                      <b>Rs.{{$aval->total_amount}}</b>                      
		                      	</div>
		                      	<div class="col-sm-4" >
			                      <b>Delivered on Oct 22, 2020</b><br>
			                      <small>Your item has been delivered</small>                     
		                      	</div>
		                      
		                    </div></a>
		                    @endforeach
		                    	
		                    @else
		                    	<div align="center" style="text-align: center;margin-top: 5rem;margin-bottom: 5rem;">
	                				<b>No Orders<br>
								          You have not order any product yet!</b>
	                			</div>
		                    @endif
                			
                		</div>
                	</div>
				</div>
				<div class="tab-pane fade " id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
					<div class="card mb-3">
                		<div class="card-body" style="margin:1rem;">
                			<div align="center" style="text-align: center;margin-top: 5rem;margin-bottom: 5rem;">
                				<b>No Reviews & Ratings<br>
								You have not rated or reviewed any product yet!</b>
                			</div>
                		</div>
                	</div>
				</div>
				<div class="tab-pane fade " id="nav-noti" role="tabpanel" aria-labelledby="nav-noti-tab">
					<div class="card mb-3" >
                		<div class="card-body" style="margin:1rem;">
                			<div align="center" style="text-align: center;margin-top: 5rem;margin-bottom: 5rem;">
                				<b>No Notification yet!</b>
                			</div>
                		</div>
                	</div>
				</div>
				<div class="tab-pane fade " id="nav-coupon" role="tabpanel" aria-labelledby="nav-coupon-tab">
					<div class="card mb-3">
                		<div class="card-body" style="margin:1rem;">
                			<div align="center" style="text-align: center;margin-top: 5rem;margin-bottom: 5rem;">
                				<b>No Coupons yet!</b>
                			</div>
                		</div>
                	</div>
				</div>
        <div class="tab-pane fade " id="nav-invite" role="tabpanel" aria-labelledby="nav-invite-tab">
          <div class="card mb-3">
                    <div class="card-body" style="margin:1rem;">
                      <div align="center" style="text-align: center;margin-top: 5rem;margin-bottom: 5rem;">
                        <b><span>{{route ('user.invite',Auth::user()->referrer_code) }}</span></b>
                        <p>Share code and Earn ...</p>
                      </div>
                    </div>
                  </div>
        </div>

      {{-- ///////////////////////////// --}}        
             
            </div>
          </div>
        </div>
    </div>
@endsection

@section('style')
<style type="text/css">
.nav-tabs .nav-item{
	text-align: left;
}
.nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active {
    color: #495057;
    background-color: #f8e7ddcc;
    border-color: #dee2e6 #dee2e6 #dee2e6;
    /*border: 1px solid gray;*/
    font-size: 
}
.nav-tabs .nav-link {
   
    border: 1px solid transparent;
    border-radius: .25rem; 
    /* border-top-right-radius: .25rem; */
}
.main-body {
	
    padding: 15px;
}
.card {
    box-shadow: 0 1px 3px 0 rgba(0,0,0,.1), 0 1px 2px 0 rgba(0,0,0,.06);
}

.card {
    position: relative;
    display: flex;
    flex-direction: column;
    min-width: 0;
    word-wrap: break-word;
    background-color: #fff;
    background-clip: border-box;
    border: 0 solid rgba(0,0,0,.125);
    border-radius: .25rem;
}

.card-body {
    flex: 1 1 auto;
    min-height: 1px;
    padding: 1rem;
}

.gutters-sm {
    margin-right: -8px;
    margin-left: -8px;
}

.gutters-sm>.col, .gutters-sm>[class*=col-] {
    padding-right: 8px;
    padding-left: 8px;
}
.mb-3, .my-3 {
    margin-bottom: 1rem!important;
}

.bg-gray-300 {
    background-color: #e2e8f0;
}
.h-100 {
    height: 100%!important;
}
.shadow-none {
    box-shadow: none!important;
}
</style>
@endsection