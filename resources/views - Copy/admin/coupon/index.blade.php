@extends('layouts.admin')
@section('scripts')
 
  <script type="text/javascript">
    $(document).ready(function () {
      //$('.select2').select2();
    });
  </script>
 
  <script type="text/javascript">
    $(document).ready(function () {
      $.ajaxSetup({
           headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
       });

      $('#type').change(function(e) {
         var id=$(this).val();
         if(id){  //alert('type');
            if(id==1){
              var html=`<label for="inputEmail4">Discount %</label>
                      <input type="text" value="{{ old('discount') }}{{ isset($stateData) ? $stateData->discount : ""}}" name="discount" class="form-control" required placeholder="Discount">`;
                $('#divType').html(html);
             }else if(id==2){
              var html=`<label for="inputEmail4">Wallet Amount</label>
                      <input type="text" value="{{ old('wallet_amount') }}{{ isset($stateData) ? $stateData->wallet_amount : ""}}" name="wallet_amount" class="form-control" required placeholder="Wallet Amount">`;
                $('#divType').html(html);
             }else if(id==3){
              var html=`<label for="inputEmail4">Order Min Price</label>
                      <input type="text" value="{{ old('min_price') }}{{ isset($stateData) ? $stateData->min_price : ""}}" name="min_price" class="form-control" required placeholder="Order Min Price">`;
                $('#divType').html(html);
             }
         }
         

      });

      $('#for').change(function(e) {
         var id=$(this).val();
         if(id){  //alert('for');
            if(id==1){
              var html=`<label for="inputEmail4">For Vertical</label>                 
                  <select value="{{ old('masters_id') }}" name="masters_id" class="form-control select2" required >
                    <option value="">Select Vertical</option>
                    @foreach($master as $svalue)
                      <option value="{{ $svalue->id }}" {{ isset($stateData) ? ( $stateData->masters_id==$svalue->id ? "Selected":"" ) : ""}}>{{ $svalue->name }}</option>
                    @endforeach
                  </select>`;
                $('#divFor').html(html);
             }else if(id==2){
              var html=`<label for="inputEmail4">For Category</label>                 
                  <select value="{{ old('categry_id') }}" name="categry_id" class="form-control select2" required >
                    <option value="">Select Category</option>
                    @foreach($category as $svalue)
                      <option value="{{ $svalue->id }}" {{ isset($stateData) ? ( $stateData->categry_id==$svalue->id ? "Selected":"" ) : ""}}>{{ $svalue->name }}</option>
                    @endforeach
                  </select>`;
                $('#divFor').html(html);
             }else if(id==3){
              var html=`<label for="inputEmail4">For Sub-Category</label>                 
                  <select value="{{ old('sub_categry_id') }}" name="sub_categry_id" class="form-control select2" required >
                    <option value="">Select Sub-Category</option>
                    @foreach($sub_category as $svalue)
                      <option value="{{ $svalue->id }}" {{ isset($stateData) ? ( $stateData->sub_categry_id==$svalue->id ? "Selected":"" ) : ""}}>{{ $svalue->name }}</option>
                    @endforeach
                  </select>`;
                $('#divFor').html(html);
             }else if(id==4){
              var html=`<label for="inputEmail4">For Vendor</label>                 
                  <select value="{{ old('vendor_id') }}" name="vendor_id" class="form-control select2" required >
                    <option value="">Select Vendor</option>
                    @foreach($vendor as $svalue)
                      <option value="{{ $svalue->id }}" {{ isset($stateData) ? ( $stateData->vendor_id==$svalue->id ? "Selected":"" ) : ""}}>{{ $svalue->company_name }}</option>
                    @endforeach
                  </select>`;
                $('#divFor').html(html);
             }else if(id==5){
              var html=`<label for="inputEmail4">For User</label>                 
                  <select value="{{ old('user_id') }}" name="user_id" class="form-control select2" required >
                    <option value="">Select User</option>
                    @foreach($user as $svalue)
                      <option value="{{ $svalue->id }}" {{ isset($stateData) ? ( $stateData->user_id==$svalue->id ? "Selected":"" ) : ""}}>{{ $svalue->name }}</option>
                    @endforeach
                  </select>`;
                $('#divFor').html(html);
             }else if(id==6){
              var html=`<label for="inputEmail4">For Product</label>                 
                  <select value="{{ old('product_id') }}" name="product_id" class="form-control select2" required >
                    <option value="">Select Product</option>
                    @foreach($product as $svalue)
                      <option value="{{ $svalue->id }}" {{ isset($stateData) ? ( $stateData->product_id==$svalue->id ? "Selected":"" ) : ""}}>{{ $svalue->name }} <i>( {{ $svalue->company_name }} )</i></option>
                    @endforeach
                  </select>`;
                $('#divFor').html(html);
             }else{
              var html=`<label for="inputEmail4">Flat Discount</label> 
                        <select value="{{ old('all') }}" name="all" class="form-control select2" required >
                    <option value="1" selected >All Products</option>
                   
                  </select>`;
              $('#divFor').html(html);
             }
         }
         

      });

      //////////////////////////
      $('#type').trigger('change');
      $('#for').trigger('change');
        //$('.select2').select2();
    });
  
</script>
@endsection


@section('content')


<div class="card ">
  <div class="card-header">
    <div>
        <form method="{{ isset($stateData) ? 'PUT': "POST"}}" action="{{ route('admin.coupon') }}{{ isset($stateData) ? '/update' : "/store"}}{{ isset($stateData) ? '/'.$stateData->id : ""}}">
          @csrf
          <div class="form-row">
              <div class="form-group col-md-2">
                  <label for="inputEmail4">Name</label>
                  <input type="text" value="{{ old('name') }}{{ isset($stateData) ? $stateData->name : ""}}" name="name" class="form-control" required placeholder="Name">
              </div>
              <div class="form-group col-md-2">
                  <label for="inputEmail4">Code</label>
                  <input type="text" value="{{ old('code') }}{{ isset($stateData) ? $stateData->code : ""}}" name="code" class="form-control" required placeholder="Code">
              </div>
              <div class="form-group col-md-2">
                  <label for="inputEmail4">Start</label>
                  <input type="date" value="{{ old('start_at') }}{{ isset($stateData) ? $stateData->start_at : ""}}" name="start_at" class="form-control" required placeholder="Name">
              </div>
              <div class="form-group col-md-2">
                  <label for="inputEmail4">End</label>
                  <input type="date" value="{{ old('end_at') }}{{ isset($stateData) ? $stateData->end_at : ""}}" name="end_at" class="form-control" required placeholder="Code">
              </div>
              <div class="form-group col-md-2">
                  <label for="inputEmail4">Type</label>                 
                  <select id="type" value="{{ old('type') }}" name="type" class="form-control" required >
                    <option value="">Select Type</option>                   
                    <option value="1" {{ isset($stateData) ? ( $stateData->type==1 ? "Selected":"" ) : ""}}>Order Discount</option>
                    <option value="2" {{ isset($stateData) ? ( $stateData->type==2 ? "Selected":"" ) : ""}}>Wallet</option>
                    <option value="3" {{ isset($stateData) ? ( $stateData->type==3 ? "Selected":"" ) : ""}}>Order Amount</option>
                  </select>
              </div>
              <div class="form-group col-md-2">
                  <label for="inputEmail4">For</label>                 
                  <select id="for" value="{{ old('for') }}" name="for" class="form-control" required >
                    <option value="0">All</option>                   
                    <option value="1" {{ isset($stateData) ? ( $stateData->for==1 ? "Selected":"" ) : ""}}>Under Vertical</option>
                    <option value="2" {{ isset($stateData) ? ( $stateData->for==2 ? "Selected":"" ) : ""}}>Category</option>
                    <option value="3" {{ isset($stateData) ? ( $stateData->for==3 ? "Selected":"" ) : ""}}>Sub-Category</option>
                    <option value="4" {{ isset($stateData) ? ( $stateData->for==4 ? "Selected":"" ) : ""}}>Vendor</option>
                    <option value="5" {{ isset($stateData) ? ( $stateData->for==5 ? "Selected":"" ) : ""}}>User</option>
                    <option value="6" {{ isset($stateData) ? ( $stateData->for==6 ? "Selected":"" ) : ""}}>Product</option>
                  </select>
              </div>
              <div class="form-group col-md-2" id="divType">
                  <label for="inputEmail4" style="color: red;">Please Select Type</label> 
                  <select class="form-control"></select>
              </div>
              

              <div class="form-group col-md-2" id="divFor" >
                   <label for="inputEmail4" style="color: red;">Please Select For</label> 
                   <select class="form-control"></select>
              </div>
              <div class="form-group col-md-5"  >
                   <label for="inputEmail4">Description</label>
                  <input type="text" value="{{ old('description') }}{{ isset($stateData) ? $stateData->description : ""}}" name="description" class="form-control" required placeholder="Description">
              </div>
                            
              <div class="form-group col-md-3 float-right" style="padding-top: 1.55rem;">
                <label for="inputEmail4"></label>
                  <button type="submit" class="btn btn-primary ">Submit</button>
                  @if(isset($stateData))
                  <a href="{{ route('admin.coupon') }}" class="btn btn-info ">Cancel</a>
                  @endif

              </div>
          </div>                 
          
      </form>
    </div>

  </div>
  <div class="card-body">
    <?php //echo"<pre>",print_r($data),"</pre>";?>
    <div class="table-responsive">
        <table id="data_table" class="table">
            <thead>
                <tr>
                    <th>Sn.</th>                    
                    <th>Name</th> 
                    <th>Code</th>
                    <th>Type</th>
                    <th>For</th>                    
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($data as $key => $uData)
                <tr>
                    <td>{{ $key+1 }}</td>
                    <td>{{ $uData->name }}</td>
                    <td>{{ $uData->code }}</td>
                    <td>
                      @if($uData->type==1)
                       <b> Discount:</b> {{$uData->discount}} %
                      @elseif($uData->type==2)
                       <b> Wallet :</b> Rs. {{$uData->wallet_amount}}
                      @elseif($uData->type==3)
                       <b> Order Price :</b> Rs. {{$uData->min_price}}
                      @endif

                    </td>
                    <td>
                      @if($uData->for==1)
                       <b> Under Vertical:</b> {{$uData->master_name}}
                      @elseif($uData->for==2)
                       <b> Category :</b> {{$uData->cat_name}}
                      @elseif($uData->for==3)
                       <b> Sub-Category :</b> {{$uData->sub_cat_name}}
                      @elseif($uData->for==4)
                       <b> Vendor:</b> {{$uData->vendors_name}}
                      @elseif($uData->for==5)
                       <b> User :</b> {{$uData->user_name}}
                      @elseif($uData->for==6)
                       <b> Product :</b> {{$uData->products_name}} <i>( {{$uData->vendors_name}} )</i>
                      @endif

                    </td>
                    <td>
                        <a class="btn btn-primary" href="{{ route('admin.coupon.edit',$uData->id) }}">Edit</a>
                      
                    </td>
                </tr>
            @endforeach    
                
            </tbody>
            {{-- <tfoot>
                <tr>
                  
                    <th>Name</th>
                    <th>Category</th>
                   
                </tr>
            </tfoot> --}}
        </table>

        
    </div>
  </div>
  <div class="card-footer text-muted">

  </div>
</div>
	
   @endsection
