@extends('layouts.admin')
@section('scripts')
 
  <script type="text/javascript">
    $(document).ready(function () {
      //$('.select2').select2();
    });
  </script>
 
  <script type="text/javascript">
    $(document).ready(function () {
      $.ajaxSetup({
           headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
       });
/*Aadhar file Upload Start*/
      $('#aadharFile').change(function() { 
        var image=$(this).val();
       
        if(image){
            var postData=new FormData();
            postData.append('file',this.files[0]);     
            var url="{{route('ajax.file.upload','master')}}";     
            $.ajax({               
                async:true,
                type:"post",
                url:url,
                data:postData,
                cache:false,
                contentType: false,
                processData: false,
                success:function(data){
                  console.log(data);
                  if(data.path){
                    $('#aadharupload').val(data.path);
                    toastr.success(''+data.message, { closeButton: !0 });
                  }else{
                    toastr.error(''+data.message, { closeButton: !0 });
                  }
                },error: function(xhr, status, error) {
                  //console.log(xhr.responseJSON.errors.file);
                  var err=xhr.responseJSON.errors.file;
                   $.each(err,function(key,value){
                        toastr.error(value, { closeButton: !0 });
                      
                  });
                  
                }  
     
            });
        }
      }); 
/*Aadhar file Upload End*/


    });
  
</script>
@endsection


@section('content')


<div class="card ">
  <div class="card-header">
    <div>
        <form method="{{ isset($stateData) ? 'PUT': "POST"}}" action="{{ route('admin.master') }}{{ isset($stateData) ? '/update' : "/store"}}{{ isset($stateData) ? '/'.$stateData->id : ""}}">
          @csrf
          <div class="form-row">
              <div class="form-group col-md-4">
                  <label for="inputEmail4">Vertical Name</label>
                  <input type="text" value="{{ old('name') }}{{ isset($stateData) ? $stateData->name : ""}}" name="name" class="form-control" required placeholder="Vertical Name">
              </div>
              <div class="form-group col-md-2">
                  <label for="inputEmail4">Vertical Order</label>
                  <input type="number" value="{{ old('order') }}{{ isset($stateData) ? $stateData->order : ""}}" name="order" class="form-control" required placeholder="Vertical Order">
              </div>
              <div class="form-group col-md-3">
                <label for="inputPassword4">Image</label>               
                <input type="file" accept="image/png, image/jpeg" class="form-control" id="aadharFile">
                <input type="hidden"  value="{{ isset($stateData) ? $stateData->image : ""}}" class="form-control" id="aadharupload" name="image">
              </div>
              <div class="form-group col-md-3 float-right" style="padding-top: 1.55rem;">
                <label for="inputEmail4"></label>
                  <button type="submit" class="btn btn-primary ">Submit</button>
                  @if(isset($stateData))
                  <a href="{{ route('admin.master') }}" class="btn btn-info ">Cancel</a>
                  @endif
              </div>
          </div>                 
          
      </form>
    </div>

  </div>
  <div class="card-body">
    
    <div class="table-responsive">
        <table id="data_table" class="table">
            <thead>
                <tr>  <th>Sn.</th>                    
                    <th>Vertical Name</th>  
                     <th>Vertical Order</th>
                     <th>Status</th>                    
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
             @foreach ($data as $key => $uData)
                <tr>
                    <td>{{ $key+1 }}</td>
                    <td>
                     {{ $uData->name }}
                     @if($uData->image)
                     <img  src="{{ asset('').$uData->image }}"  class="" style="height: 2rem;padding-top: 2px;">
                     @endif
                   </td>
                   <td>{{ $uData->order }}</td>
                   <td>
                    @if($uData->active==1)
                        Active
                    @else
                        De-Active
                    @endif
                  </td> 
                    <td>  
                        <a class="btn btn-primary" href="{{ route('admin.master.edit',$uData->id) }}">Edit</a>
                    @if($uData->active==1)
                        <a class="btn btn-warning" href="{{ route('admin.master.status',['id' => $uData->id, 'status' => 0]) }}">De-Active</a>
                    @else
                        <a class="btn btn-info" href="{{ route('admin.master.status',['id' => $uData->id, 'status' => 1]) }}">Active</a>
                    @endif
                      
                    </td>
                </tr>
            @endforeach    
                
            </tbody>
           {{--  <tfoot>
                <tr>
                   <th>Id</th>
                    <th>Name</th>
                    
                    <th>Action</th>
                </tr>
            </tfoot> --}}
        </table>

        
    </div>
  </div>
  <div class="card-footer text-muted">
{{--     {!! $uData->links() !!} --}}
{{--  {{ $uData->links() }} --}}
  </div>
</div>
	
   @endsection
