<html class="no-js" lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>{{ config('app.name') }} Sing In {{ isset($url) ? ucfirst($url) : ""}}</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
        ============================================-->
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset(config('app.icon')) }}">
    <!-- Google Fonts
        ============================================ -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,400i,600,700,800,900" rel="stylesheet" />
    <!-- Scripts -->
   
    <link href="{{  asset('css/themes/lite-purple.min.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="{{  asset('css/plugins/toastr.css') }}" />
</head>
<div class="auth-layout-wrap" style="background-image: url()">
    <div class="auth-content">
        <div class="card o-hidden">
            <div class="row">
                <div class="col-md-12">
                    <div class="p-4">
                        <div class="auth-logo text-center mb-4">
                            <img src="{{ asset(config('app.logo')) }}" style="width: Auto!important;" alt="{{ config('app.name') }}"></div>
                        <h1 class="mb-3 text-18 text-center">Sign In {{ isset($url) ? ucfirst($url) : ""}}</h1>
                        <form method="POST" action="{{ route('login') }}{{ isset($url) ? '/'.$url : ""}}">
                        @csrf
                            <div class="form-group">
                                <label for="email">Email address</label>
                                <input class="form-control form-control-rounded @error('email') is-invalid @enderror" id="email" type="email" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input class="form-control form-control-rounded @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" id="password" type="password">
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group text-center">
                            
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember" style="padding-top: 0.2rem;">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            
                            </div>
                            <button class="btn btn-rounded btn-primary btn-block mt-2">Sign In</button>
                        </form>
                        <div class="mt-3 mb-3 text-center">
                            @if(isset($url) )
                                @if($url=='vendor')
                                    <a class="text-muted" style="" href="{{ route('forget.vendor') }}">                           
                                        <u>Forgot Password?</u>
                                    </a>
                                @endif
                                @if($url=='admin')
                                    <a class="text-muted" style="" href="{{ route('forget.admin') }}">                           
                                        <u>Forgot Password?</u>
                                    </a>
                                @endif
                            @endif
                            @if(isset($url) && $url=='vendor')
                                <a class="text-muted float-right" style="" href="{{ route('register') }}{{ isset($url) ? '/'.$url : ""}}">
                                    Sign Up
                                </a>
                            @endif
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    
</div>

<script src="{{ asset('js/plugins/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('js/plugins/toastr.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function () {
       
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                toastr.error("{{ $error }}", { closeButton: !0 });
            @endforeach  
        @endif
        @if (session('success'))
            toastr.success("{{ session('success') }}", { closeButton: !0 });
        @endif
        @if (session('error'))
            toastr.error("{{ session('error') }}", { closeButton: !0 });
        @endif
        @if (session('warning'))
            toastr.warning("{{ session('warning') }}", { closeButton: !0 });
        @endif

       
    });
</script>