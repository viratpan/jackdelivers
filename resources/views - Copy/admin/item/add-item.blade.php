<?php $layouts='layouts.vendor'; ?>

@if(Auth::guard('admin')->check())
@php $layouts='layouts.admin' @endphp
@endif
@if(Auth::guard('vendor')->check())
   @php  $layouts='layouts.vendor' @endphp
@endif

 @extends($layouts)

@section('scripts')
  <script src="{{  asset('js/plugins/select2.min.js') }}" ></script>
  <script type="text/javascript">
    $(document).ready(function () {
      //$('.select2').select2();
    });
  </script>
 
  <script type="text/javascript">
    var vendor_lat='';
    var vendor_long='';
    var vendor_draggable=1;
    $(document).ready(function () {
      $.ajaxSetup({
           headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
       });
/*Aadhar file Upload Start*/
      $('#aadharFile').change(function() { 
        var image=$(this).val();
       
        if(image){
            var postData=new FormData();
            postData.append('file',this.files[0]);     
            var url="{{route('ajax.file.upload','item')}}";     
            $.ajax({               
                async:true,
                type:"post",
                url:url,
                data:postData,
                cache:false,
                contentType: false,
                processData: false,
                success:function(data){
                  console.log(data);
                  if(data.path){
                    $('#aadharupload').val(data.path);
                    toastr.success(''+data.message, { closeButton: !0 });
                  }else{
                    toastr.error(''+data.message, { closeButton: !0 });
                  }
                },error: function(xhr, status, error) {
                  //console.log(xhr.responseJSON.errors.file);
                  var err=xhr.responseJSON.errors.file;
                   $.each(err,function(key,value){
                        toastr.error(value, { closeButton: !0 });
                      
                  });
                  
                }  
     
            });
        }
      }); 
/*Aadhar file Upload End*/
/*Aadhar2 file Upload Start*/
      $('#aadharFile2').change(function() { 
        var image=$(this).val();
       
        if(image){
            var postData=new FormData();
            postData.append('file',this.files[0]);     
            var url="{{route('ajax.file.upload','item')}}";     
            $.ajax({               
                async:true,
                type:"post",
                url:url,
                data:postData,
                cache:false,
                contentType: false,
                processData: false,
                success:function(data){
                  console.log(data);
                  if(data.path){
                    $('#aadharupload2').val(data.path);
                    toastr.success(''+data.message, { closeButton: !0 });
                  }else{
                    toastr.error(''+data.message, { closeButton: !0 });
                  }
                },error: function(xhr, status, error) {
                  //console.log(xhr.responseJSON.errors.file);
                  var err=xhr.responseJSON.errors.file;
                   $.each(err,function(key,value){
                        toastr.error(value, { closeButton: !0 });
                      
                  });
                  
                }  
     
            });
        }
      }); 
/*Aadhar file Upload End*/

    });
  </script>
  <script>
  $('#name').change(function(e) {
    var name=$(this).val();
    $.get('{{ route('ajax.item.check_slug') }}', 
      { 'name': name }, 
      function( data ) {
        $('#slug').val(data.slug);
        $('#title').val(name);
      }
    );
  });
@if(Auth::guard('admin')->check())
  /*/////////////*/
  $('#vendor').trigger('change');
  /*//////////////////////////////////////////////////////////////////////////////*/
  $('#vendor').change(function(e) {
      var id=$(this).val();
      var selectedText = $("#vendor option:selected").html();
        $.get('{{ route('ajax.vendor.options') }}', 
            { 'id': id }, 
            function( res ) {
              if(res){
                  $("#master").empty();
                  $("#category").empty();
                  $("#sub_category").empty();
                    //$("#master").append('<option value="">Please Select</option>');
                    //$("#category").append('<option value="">Please Select</option>');
                    //$("#sub_category").append('<option value="">Please Select</option>');
                  $.each(res,function(key,value){
                      $("#master").append('<option value="'+value.id+'">'+value.name+'</option>');
                  });
                 // $("#master").select2();
                 $('#master').trigger('change');
              }else{
                 $("#master").empty();
                 $("#category").empty();
                 $("#sub_category").empty();                     
              }
              
              
            }
          ); 
  });

  $('#master').change(function(e) {
      var id=$(this).val();
      var selectedText = $("#master option:selected").html();
      //var selectedText = $("#category option:selected").html(); 
       var restaurantshtml='<div class="form-group col-md-6">'+
          '  <label for="inputEmail4">Type <span class="text-red">*</span></label>'+
          '   <select id="type" value="{{ old('type') }}" name="type" class="form-control" required >'+
          '      <option value="">Please Select </option>'+             
          '        <option value="veg" >Veg</option>'+
          '        <option value="nonveg" >Non-Veg</option>'+
          '    </select>'+
          '</div>'+
          '<div class="form-group col-md-6">'+
          '  <label for="inputEmail4">Cost Of Two <span class="text-red">*</span></label>'+
          '  <input type="number" id="costoftwo" value="{{ old('costoftwo') }}" name="costoftwo" class="form-control" required placeholder="Cost Of Two">'+
          '</div>';
       selectedText=selectedText.toLowerCase();
       if(selectedText=='restaurants'){
          $('#restutantDiv').html(restaurantshtml);
       }else{$('#restutantDiv').html('');}


        $.get('{{ route('ajax.master.options') }}', 
            { 'id': id }, 
            function( res ) {
              if(res){
                  $("#category").empty();
                  $("#sub_category").empty();
                     //$("#category").append('<option value="">Please Select</option>');
                     // $("#sub_category").append('<option value="">Please Select</option>');
                  $.each(res,function(key,value){
                      $("#category").append('<option value="'+value.id+'">'+value.name+'</option>');
                  });
                  $("#category").select2();
                  $('#sub_category').trigger('change');
              }else{
                 $("#category").empty();
                 $("#sub_category").empty();
                     
              }
              
              
            }
          ); 
  });
@endif
  /*///////////////////////////////////*/
  $('#category').change(function(e) {
     var id=$(this).val();
     @if(Auth::guard('vendor')->check())

     var selectedText = '{{$master->name}}'; 
     var restaurantshtml='<div class="form-group col-md-6">'+
        '  <label for="inputEmail4">Type <span class="text-red">*</span></label>'+
        '   <select id="type" value="{{ old('type') }}" name="type" class="form-control" required >'+
        '      <option value="">Please Select </option>'+             
        '        <option value="veg" >Veg</option>'+
        '        <option value="nonveg" >Non-Veg</option>'+
        '    </select>'+
        '</div>'+
        '<div class="form-group col-md-6">'+
        '  <label for="inputEmail4">Cost Of Two <span class="text-red">*</span></label>'+
        '  <input type="number" id="costoftwo" value="{{ old('costoftwo') }}" name="costoftwo" class="form-control" required placeholder="Cost Of Two">'+
        '</div>';
     selectedText=selectedText.toLowerCase();
     if(selectedText=='restaurants'){
        $('#restutantDiv').html(restaurantshtml);
     }else{$('#restutantDiv').html('');}
@endif

     $.get('{{ route('ajax.category.options') }}', 
      { 'id': id }, 
      function( res ) {
        if(res){
            $("#sub_category").empty();
             // $("#sub_category").append('<option value="">Please Select</option>');
            $.each(res,function(key,value){
                $("#sub_category").append('<option value="'+value.id+'">'+value.name+'</option>');
            });
             $("#sub_category").select2();
       
        }else{
           $("#sub_category").empty();
        }
        
        
      }
    );

  });


</script>
@endsection


@section('content')
<style type="text/css">
  .text-red{color: red;font-weight: 700;}
</style>
<div class="card ">
  <div class="card-header">
    <h5 align="left">Add Item
@if(Auth::guard('admin')->check())
  <a class="btn btn-sm btn-info float-right" style="" href="{{ route('admin.item-list') }}"><b>Back</b></a>
@endif
@if(Auth::guard('vendor')->check())
     
@endif
        
    </h5>

  </div>
  <div class="card-body">
@if(Auth::guard('admin')->check())
  <form method="POST" action="{{ route('admin.store-item') }}" enctype="multipart/form-data">
@endif
@if(Auth::guard('vendor')->check())
   <form method="POST" action="{{ route('vendor.store-item') }}" enctype="multipart/form-data">
@endif
    
        @csrf
        <div class="form-row">
        @if(Auth::guard('admin')->check())
          <div class="form-group col-md-3">
            <label for="inputEmail4">Vendor <span class="text-red">*</span></label>
             <select id="vendor" value="" name="vendor_id" class="form-control select2" required >
                <option value="">Please Select </option>
                @foreach($vendor as $svalue)
                  <option data-masters="" value="{{ $svalue->id }}" >{{ $svalue->name }}
                   @if(isset($svalue->company_name)) ( {{ $svalue->company_name }} ) @endif
                 </option>
                @endforeach
              </select>
          </div>
          <div class="form-group col-md-3">
            <label for="inputEmail4">Under Vertical <span class="text-red">*</span></label>
             <select id="master" value="" name="masters_id" class="form-control select2" required >
                <option value="">Please Select </option>
                {{-- @foreach($master as $svalue)
                  <option value="{{ $svalue->id }}" >{{ $svalue->name }} ( {{ $svalue->name }} )</option>
                @endforeach --}}
              </select>
          </div>
           <hr class="col-12" style="margin: 8px;">
          
        <div class="form-group col-md-3">
          <label for="inputEmail4">Category <span class="text-red">*</span></label>
           <select id="category" value="" name="categry_id" class="form-control" required >
              <option value="">Please Select </option>
              {{-- @foreach($category as $svalue)
                <option value="{{ $svalue->id }}" >{{ $svalue->name }}</option>
              @endforeach --}}
            </select>
        </div>
        <div class="form-group col-md-3">
          <label for="inputEmail4">Sub Category <span class="text-red">*</span></label>
           <select id="sub_category" value="" name="sub_categry_id" class="form-control" required >
              <option value="">Please Select Category</option>
              {{-- @foreach($sub_category as $svalue)
                <option value="{{ $svalue->id }}" >{{ $svalue->name }}</option>
              @endforeach --}}
            </select>
        </div>
      @endif
      @if(Auth::guard('vendor')->check())
        <div class="form-group col-md-3">
          <label for="inputEmail4">Category <span class="text-red">*</span></label>
           <select id="category" value="" name="categry_id" class="form-control" required >
              <option value="">Please Select </option>
              @foreach($category as $svalue)
                <option value="{{ $svalue->id }}" >{{ $svalue->name }}</option>
              @endforeach
            </select>
        </div>
        <div class="form-group col-md-3">
          <label for="inputEmail4">Sub Category <span class="text-red">*</span></label>
           <select id="sub_category" value="" name="sub_categry_id" class="form-control" required >
              <option value="">Please Select Category</option>
              {{-- @foreach($sub_category as $svalue)
                <option value="{{ $svalue->id }}" >{{ $svalue->name }}</option>
              @endforeach --}}
            </select>
        </div>
       @endif  

        <div class="form-group col-md-6">
          <label for="inputEmail4">Name <span class="text-red">*</span></label>
          <input type="text" id="name" value="{{ old('name') }}" name="name" class="form-control" required placeholder="Name">

          <input type="text" id="slug" value="{{ old('slug') }}" name="slug" class="form-control" required placeholder="Slug">

        </div>
        

         <div class="form-group col-md-6">
          <label for="inputEmail4">Title <span class="text-red">*</span></label>
          <input type="text" id="title" value="{{ old('meta_title') }}" name="meta_title" class="form-control" required placeholder="Title">

          <label for="inputEmail4">Keywords <span class="text-red">*</span></label>
          <input type="text" value="{{ old('meta_keywords') }}" name="meta_keywords" class="form-control" required placeholder="Keywords">

          <label for="inputEmail4">Description <span class="text-red">*</span></label>
          <input type="text" value="{{ old('meta_description') }}" name="meta_description" class="form-control" required placeholder="Description">
        </div>

        <div class="form-group col-md-6">
          <label for="inputEmail4"> Description <span class="text-red">*</span></label>
          <textarea value="{{ old('description') }}" rows="6" name="description" class="form-control" required placeholder="Description">{{ old('description') }}</textarea>
        </div>
        
         <div class="form-group col-md-2">
                  <label for="inputEmail4">Unit</label>                 
                  <select value="{{ old('attrubute_id') }}" name="attrubute_id" class="form-control" required >
                    <option value="">Select Category</option>
                    @foreach($attributes as $svalue)
                      <option value="{{ $svalue->id }}" >{{ $svalue->name }}</option>
                    @endforeach
                  </select>
              </div>
         <div class="form-group col-md-2">
              <label for="inputEmail4">Actual Price</label>
              <input type="number"  value="{{ old('aprice') }}" name="aprice" class="form-control" required placeholder="Actual Price">
          </div>
        <div class="form-group col-md-2">
          <label for="inputPassword4">Image</label>
         
         <input type="file" accept="image/png, image/jpeg" class="form-control" id="aadharFile">
          <input type="hidden"  value="" class="form-control" id="aadharupload" name="image">
        </div>
        <div class="form-group col-md-2">
          <label for="inputPassword4">Image 2</label>
         
         <input type="file" accept="image/png, image/jpeg" class="form-control" id="aadharFile2">
          <input type="hidden"  value="" class="form-control" id="aadharupload2" name="image2">
        </div>
       <div id="restutantDiv" class="form-group col-md-4 form-row">
        
       </div>

        
  
 

        
        


      </div>
      
      <div class="form-row float-right">
          <button type="submit" class="btn btn-primary ">Submit</button>
      </div>
    </form>

  </div>
  <div class="card-footer text-muted">
    {{-- {!! $uData->links() !!} --}}
  </div>
</div>
    
@endsection
