@extends('layouts.admin')



@section('content')


<div class="card ">
  <div class="card-header">
    <div>
        <form method="GET" action="{{ route('admin.customer-list') }}">
     
          <div class="form-row">
              <div class="form-group col-md-3">
                  <label for="inputEmail4">Search</label>
                  <input type="text" value="{{ isset($request) ? $request->search : ""}}" name="search" class="form-control"  placeholder="Text">
              </div>
              
               
              <div class="form-group col-md-2 float-right" style="padding-top: 1.55rem;">
                <label for="inputEmail4"></label>
                  <button type="submit" class="btn btn-primary ">Submit</button>
                 
                    <a href="{{ route('admin.customer-list') }}" class="btn btn-info ">Reset</a>
                  
              </div>
          </div>                 
          
      </form>
    </div>

  </div>
  <div class="card-body">
    <?php //echo"<pre>",print_r($data),"</pre>";?>
    <div class="table-responsive">
        <table id="" class="table vtable">
            <thead>
                <tr><th>Sn.</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Mobile</th>
                    <th>Wallet Ammount</th>
                  <th>Date</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($data as $key => $uData)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{ $uData->name }}</td>
                    <td>{{ $uData->email  }}</td>
                    <td>{{ $uData->mobile  }}</td>
                    <td>{{ $uData->ammount  }}</td>
                    <td><?php echo date('d/M/y', strtotime($uData->created_at)) ; ?></td>
                </tr>
            @endforeach    
                
            </tbody>
           {{--  <tfoot>
                <tr>
                   <th>Id</th>
                    <th>Name</th>
                    
                    <th>Action</th>
                </tr>
            </tfoot> --}}
        </table>

        
    </div>
  </div>
  <div class="card-footer text-muted">
{{--     {!! $uData->links() !!} --}}
{{--  {{ $uData->links() }} --}}
  </div>
</div>
	
   @endsection
