<?php $layouts='layouts.vendor'; ?>

@if(Auth::guard('admin')->check())
@php $layouts='layouts.admin' @endphp
@endif
@if(Auth::guard('vendor')->check())
   @php  $layouts='layouts.vendor' @endphp
@endif

 @extends($layouts)
@section('scripts')
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script>
$(function() {
  $('input[name="date"]').daterangepicker({
    opens: 'right',
    showDropdowns: true,
    minYear: 1901,
    maxYear: parseInt(moment().format('YYYY'),10),
    locale: {
            format: 'DD-MM-YYYY'
        },
    ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
  }, function(start, end, label) {
    console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
  });
});
 var groupColumn = 0;
$(".vtableOrder").DataTable({
        //"lengthMenu": [[-1, 50, 100, 150], ["All",50, 100, 150]],
            dom: "<'row'<'col-sm-12 col-md-4 w-100 datasearch'f><'col-sm-12 col-md-4 data-show-hide'C><'col-sm-12 col-md-3 data-buttons'B>>" +
                "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
                    "lengthMenu": [[-1,50, 100, 150], ["All",50, 100, 150]],
                    "scrollY": 200,
                    //"scrollX": true,
                "columnDefs": [
                    { "visible": false, "targets": groupColumn }
                ],
                "drawCallback": function ( settings ) {
                      var api = this.api();
                      var rows = api.rows( {page:'current'} ).nodes();
                      //console.log(api);
                      var last=null;
           
                      api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                          if ( last !== group ) {console.log(i);

                              $(rows).eq( i ).before(
                                  '<tr class="group"><td colspan="17" style="padding:6px;text-align:center;">'+group+'</td></tr>'
                              );           
                              last = group;
                          }
                      } );
                  },
                buttons: [
                    'pageLength','excel', 'pdf','print'
                ],
                language: { search: '', searchPlaceholder: "Search..." }
        });
 $('#vendor').change(function(e) {
    //$("#sub_category").trigger('change');
  });
 function itemSuggest(this_obj){
  alert(this_obj);
 }
</script>

@endsection
@section('content')
<style type="text/css">
 
</style>

<div class="card ">
  <div class="card-header">
    <div>
      @if(Auth::guard('admin')->check())
        <form method="GET" action="{{ route('admin.order-list') }}">
      @endif
      @if(Auth::guard('vendor')->check())
       <form method="GET" action="{{ route('vendor.order-list') }}">
      @endif
          
          <div class="form-row">
              <div class="form-group col-md-3">
                  <label for="inputEmail4">Date</label>
                 <input type="text" name="date" value="" class="form-control" >
              </div>
              <div class="form-group col-md-3">
                  <label for="inputEmail4">Search</label>
                  <input type="text" value="{{ isset($request) ? $request->search : ""}}" name="search" class="form-control"  placeholder="Text">
              </div>
             
              <div class="form-group col-md-2">
                  <label for="inputEmail4">Status</label>
                  <select value="" name="status" class="form-control"  >
                    <option value="">Select Status</option>
                    <option value="0" {{ isset($request) ? ( $request->status==0 ? "Selected":"" ) : ""}}>Pending </option>
                    <option value="1" {{ isset($request) ? ( $request->status==1 ? "Selected":"" ) : ""}}>Confirm </option>
                    <option value="2" {{ isset($request) ? ( $request->status==2 ? "Selected":"" ) : ""}}>Dispatch </option>
                     <option value="3" {{ isset($request) ? ( $request->status==3 ? "Selected":"" ) : ""}}>Delivered </option>
                    </select>
              </div>
              <div class="form-group col-md-2">
                  <label for="inputEmail4">Confirm</label>
                  <select value="" name="confirmed" class="form-control"  >
                    <option value="">Select Status</option>
                    <option value="0" {{ isset($request) ? ( $request->status==0 ? "Selected":"" ) : ""}}>Pending </option>
                    <option value="1" {{ isset($request) ? ( $request->status==1 ? "Selected":"" ) : ""}}>Confirm </option>
                    
                    </select>
              </div>
               
              <div class="form-group col-md-2 float-right" style="padding-top: 1.55rem;">
                <label for="inputEmail4"></label>
                  <button type="submit" class="btn btn-primary ">Submit</button>
                     @if(Auth::guard('admin')->check())
                      <a href="{{ route('admin.order-list') }}" class="btn btn-info ">Reset</a>
                     @endif
                     @if(Auth::guard('vendor')->check())
                      <a href="{{ route('vendor.order-list') }}" class="btn btn-info ">Reset</a>
                     @endif
              </div>
          </div>                 
          
      </form>
    </div>

  </div>
  <div class="card-body">
    <?php //echo"<pre>",print_r($data),"</pre>";?>
    <div class="table-responsive">
        <table  class="table vtableOrder">
            <thead>
                <tr><th>Order Group</th>
                    {{-- <th>Date</th> --}}
                    <th>Name</th> 
                    <th>Cost</th>
                    <th>Discount</th>
                    <th>Price</th>
                    <th>Qty</th>
                    <th>Paid Amount</th>
                    <th>Status</th>
                    <th>Action</th>
                    
                </tr>
            </thead>
            <tbody>
            @foreach ($data as $uData)
              <tr>
                <td data-date="{{ date('d M y', strtotime($uData->created_at)) }}"
                 data-group="{{ $uData->order_id }}">
                  @if(Auth::guard('admin')->check())
                      <span class="text-info">{{$uData->masters_name}} : {{ $uData->vendor_company_name }}</span> | 
                  @endif
                  <span class="">OD{{ $uData->order_id }} | Items: {{$uData->items}} | Discount: {{$uData->discount}} | Delivery : {{$uData->shipping}} | Price: {{$uData->price}} | GST: {{$uData->tax}} </span> | 
                  <b class="text-danger">Final Amount: {{$uData->total_amount}}</b> |
                  <b class="">Payment Type: COD</b>
                  <hr style="margin: 2px;border:1px solid;">
                  Name: {{ $uData->user_names}} | Email: {{ $uData->user_email}} | Mobile: {{ $uData->user_mobile}} | Address: {{ $uData->address}}, {{ $uData->city}}, {{ $uData->state}}
                @if($uData->rider_name) 
                 <span style="margin-left: 1rem;"> Rider Name: {{ $uData->rider_name}} | Rider Mobile: {{ $uData->rider_mobile}}</span>
                 @else
                    @if(Auth::guard('vendor')->check())
                      <b class="text-danger" style="padding-left: 1rem;">Rider Not Assigned</b>
                    @else
                     <a class="btn btn-success btn-sm" target="_Blanks" href="{{ route('admin.rider.assign',$uData->order_rid) }}">Assign Rider</a>
                    @endif
                 
                 @endif
{{-- ////////////////////////////////////////////////////// --}}
             @if(Auth::guard('vendor')->check())  
                 @if($uData->item_status==0)
                      <a class="btn btn-success  btn-sm" href="{{ route('vendor.order.status-all',['id' => $uData->order_rid,'ostatus' => 0, 'status' => 1]) }}">Confirm All</a>                           
                      <a class="btn btn-danger btn-sm" href="{{ route('vendor.order.status-all',['id' => $uData->order_rid,'ostatus' => 0, 'status' => 6]) }}">Cancel All</a>
                 @elseif($uData->item_status==1)
                      <a class="btn btn-warning  btn-sm" href="{{ route('vendor.order.status-all',['id' => $uData->order_rid,'ostatus' => 1, 'status' => 2]) }}">Dispatch All</a>
                     
                 @endif
            @endif    
{{-- ////////////////////////////////////////////////////// --}}
                
                 
                  
                </td>
                    {{-- <td>{{ date('d M y', strtotime($uData->created_at)) }}</td> --}}
                     <td data-id="{{ $uData->order_rid }}">{{ $uData->product_name }}&nbsp;<i>( {{ $uData->product_qty.' '.$uData->attributes_name}} )</i>
                     </td>
                   
                   
                      <td>{{ $uData->item_cost}}</td>
                      <td>{{ $uData->item_discount}}</td>
                       <td>{{ $uData->item_price}}</td>
                      <td>{{ $uData->item_qty}}</td>
                     
                      <td>{{ $uData->item_amount}}</td>
                   <td>
                        @if($uData->item_status==0)
                            <small class="status secondary">Pending</small>
                        @endif
                        @if($uData->item_status==11)
                            <small class="status secondary">Suggest</small>
                        @endif
                        @if($uData->item_status==1)
                            <small  class="status primary">Confirm</small>
                        @endif
                        @if($uData->item_status==2)
                            <small  class="status warning">Dispatch</small>
                        @endif
                        @if($uData->item_status==3)
                            <small  class="status primary">Delivered</small>
                        @endif
                        @if($uData->item_status==4)
                            <small  class="status secondary">Return</small>
                        @endif
                        @if($uData->item_status==5)
                            <small  class="status text-danger"><b>Cancelled by Customer</b></small>
                        @endif
                        @if($uData->item_status==6)
                            <small  class="status text-danger"><b>
                            @if(Auth::guard('vendor')->check())
                              Cancelled by Me
                            @else
                             Cancelled by Vendor
                            @endif
                            </b></small>
                        @endif
                        @if($uData->item_status==7)
                            <small  class="status text-danger"><b>
                            @if(Auth::guard('admin')->check())
                              Cancelled by Me
                            @else
                             Cancelled by Admin
                            @endif
                            </b></small>
                        @endif
                    </td>
                    <th>
     @if(Auth::guard('admin')->check())
                       @if($uData->item_status==0)
                            <a class="btn btn-success  btn-sm" href="{{ route('admin.order.status',['id' => $uData->item_id, 'status' => 1]) }}">Confirm</a>
                            <button data-order="{{ $uData->item_id}}" data-status="11" type="button" onclick="itemSuggest(this);" class="btn btn-warning btn-sm" >Suggest</button>
                            <a class="btn btn-danger btn-sm" href="{{ route('admin.order.status',['id' => $uData->item_id, 'status' => 7]) }}">Cancel</a>
                        @endif
                        @if($uData->item_status==11)
                           {{--  <small class="status secondary">Suggest</small> --}}
                        @endif
                        @if($uData->item_status==1)
                             <a class="btn btn-warning btn-sm" href="{{ route('admin.order.status',['id' => $uData->item_id, 'status' => 2]) }}">Dispatch</a>
                        @endif
                        @if($uData->item_status==2)
                            <a class="btn btn-warning btn-sm" href="{{ route('admin.order.status',['id' => $uData->item_id, 'status' => 3]) }}">Delivered</a>
                        @endif
                        @if($uData->item_status==3)
                            <a class="btn btn-warning btn-sm" href="{{ route('admin.order.status',['id' => $uData->item_id, 'status' => 4]) }}">Return</a>
                            <a class="btn btn-info btn-sm" href="{{ route('admin.order.status',['id' => $uData->item_id, 'status' => 4]) }}">Check Payment</a>
                        @endif
                        @if($uData->item_status==4)
                            <a class="btn btn-warning btn-sm" href="{{ route('admin.order.status',['id' => $uData->item_id, 'status' => 11]) }}">Suggest</a>
                        @endif
                        @if($uData->item_status==5)
                            
                        @endif
                        @if($uData->item_status==6)
                            
                        @endif
       @endif
      @if(Auth::guard('vendor')->check())
                        @if($uData->item_status==0)
                            <a class="btn btn-success  btn-sm" href="{{ route('vendor.order.status',['id' => $uData->item_id, 'status' => 1]) }}">Confirm</a>
                            <button data-order="{{ $uData->item_id}}" data-status="11" type="button" onclick="itemSuggest(this);" class="btn btn-warning btn-sm" >Suggest</button>
                            <a class="btn btn-danger btn-sm" href="{{ route('vendor.order.status',['id' => $uData->item_id, 'status' => 6]) }}">Cancel</a>
                        @endif
                        @if($uData->item_status==11)
                           {{--  <small class="status secondary">Suggest</small> --}}
                        @endif
                        @if($uData->item_status==1)
                             <a class="btn btn-warning btn-sm" href="{{ route('vendor.order.status',['id' => $uData->item_id, 'status' => 2]) }}">Dispatch</a>
                        @endif
                        @if($uData->item_status==2)
                            {{-- <a class="btn btn-warning btn-sm" href="{{ route('vendor.order.status',['id' => $uData->item_id, 'status' => 3]) }}">Delivered</a> --}}
                            waiting for Delivery
                        @endif
                        @if($uData->item_status==3)
                          <b>Delivered</b>
                            {{-- <a class="btn btn-warning btn-sm" href="{{ route('vendor.order.status',['id' => $uData->item_id, 'status' => 4]) }}">Return</a> --}}
                            {{-- <a class="btn btn-info btn-sm" href="{{ route('vendor.order.status',['id' => $uData->item_id, 'status' => 4]) }}">Check Payment</a> --}}

                        @endif
                        @if($uData->item_status==4)
                            {{-- <a class="btn btn-warning btn-sm" href="{{ route('vendor.order.status',['id' => $uData->item_id, 'status' => 11]) }}">Suggest</a> --}}
                        @endif
                        @if($uData->item_status==5)
                            
                        @endif
                        @if($uData->item_status==6)
                            
                        @endif
      @endif
                    </th>
                </tr>
            @endforeach    
                
            </tbody>
           {{--  <tfoot>
                <tr>
                   <th>Id</th>
                    <th>Name</th>
                    
                    <th>Action</th>
                </tr>
            </tfoot> --}}
        </table>

        
    </div>
  </div>
  <div class="card-footer text-muted">
{{--     {!! $uData->links() !!} --}}
{{--  {{ $uData->links() }} --}}
  </div>
</div>
  
   @endsection
