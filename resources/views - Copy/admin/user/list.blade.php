@extends('layouts.admin')
@section('style')
  <style type="text/css">
      .pagination{float: right;}
  </style>
@endsection


@section('content')
<div class="card ">
  <div class="card-header">
    <h5 align="left">User Detail

    </h5>

  </div>
  <div class="card-body">
    <div class="table-responsive">
        <table id="data_table" class="table">
            <thead>
                <tr>
                    <th>Sn.</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Mobile</th>
                    <th>Wallet Ammount</th>
                  <th>Date</th>
                   {{--  <th>Action</th> --}}
                </tr>
            </thead>
            <tbody>
             
              <?php foreach ($data as $key => $uData): ?>

                  <tr>
                    <td>{{$key+1}}</td>
                    <td>{{ $uData->name }}</td>
                    <td>{{ $uData->email  }}</td>
                    <td>{{ $uData->mobile  }}</td>
                    <td>{{ $uData->ammount  }}</td>
                    <td><?php echo date('d/M/y', strtotime($uData->created_at)) ; ?></td>


                </tr>

            <?php endforeach; ?>

            </tbody>
            <tfoot>
                <tr>
                   <th>ID</th>
                    <th>Name</th>
                    <th>Email</th>
                      <th>Create Date</th>

                </tr>
            </tfoot>
        </table>


    </div>
  </div>
  <div class="card-footer text-muted">
    {{ $data->links() }}
        {{-- <a class="btn btn-sm btn-info float-right" style="" href="{{ route('vendor.add-vendor') }}"><b>Click here to add Products</b></a> --}}
  </div>
</div>

   @endsection
