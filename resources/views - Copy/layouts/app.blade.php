
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>{{ config('app.name') }}</title>

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
    ============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset(config('app.icon')) }}">
     <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <script src="{{ asset('js/app.js') }}"></script>
    
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" />
    <link rel="stylesheet" href="{{ asset('assets/css/meanmenu.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/responsive.css') }}">

    <script src="{{ asset('assets/js/jquery-3.5.1.slim.min.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/js/meanmenu.js') }}"></script>
    <script src="{{ asset('assets/js/custom.js') }}"></script>    

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="{{  asset('css/plugins/toastr.css') }}" />
    <script src="{{ asset('js/plugins/toastr.min.js') }}"></script>
        

     @yield('style')
     <style type="text/css">
      .preloader {
        position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
       /* background: #f2f2f2 url("{{ asset('images/spinner.gif') }}") center no-repeat;*/
      }
    </style>
</head>
<body>
   <div class="preloader"></div>
       @include('common.header')
       {{-- Main Section --}}
       
        @yield('content')
       {{-- End MAin Section --}}
       
       @include('common.footer')
      <script type="text/javascript">
        $(document).ready(function() { 
            $('.preloader').hide();
            $.ajaxSetup({
               headers: {
                   'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $("#dropdownMenu12").click(function(){
              $("#dropdownMenu12ul").toggle();
            });
            @if ($errors->any())
                @foreach ($errors->all() as $error)
                    toastr.error("{{ str_replace('v ', '', $error) }}", { closeButton: !0 });
                @endforeach
            @endif
            @if (session('success'))
                toastr.success("<span style='color:black;'>{{ session('success') }}</span>", { closeButton: !0 });
            @endif
            @if (session('error'))
                toastr.error("<span style='color:black;'>{{ session('error') }}</span>", { closeButton: !0 });
            @endif
            @if (session('warning'))
                toastr.warning("<span style='color:black;'>{{ session('warning') }}</span>", { closeButton: !0 });
            @endif
         });
      </script>
      <script type="text/javascript">
        $(document).ready(function () {
          updateCartCount();
          
        });
        function updateCartCount() {
          $.get('{{route('ajax.getCart')}}',
             function( data ) {
                if(data.count==0){
                  var html ='Your cart is empty<br>Add items to get started';
                  $('#cart_count').hide(); 
                }else{
                  $('#cart_count').show();
                    $('#cart_count').show();
                 // $('#cart_count').css('display','block!important'); 
                  $('#cart_count').html(data.count); 
                }
             }
            );
        }
        /*function showuserdd(){
           //$("dropdownMenu125213in").toggleClass("show");
        }*/
      </script>



       @yield('script')
       @yield('scripts')
{{-- push notification --}}
<link href="https://res.cloudinary.com/dxfq3iotg/raw/upload/v1557232134/toasty.css" rel="stylesheet" />
<script src="https://res.cloudinary.com/dxfq3iotg/raw/upload/v1557232134/toasty.js"></script>
<script>
          /*Echo.channel('events')
              .listen('RealTimeMessage', (e) => console.log('RealTimeMessage: ' + e.message));*/
         /* Echo.private('events')
              .listen('RealTimeMessage', (e) => console.log('Private RealTimeMessage: ' + e.message.body));*/
      var options = {
        autoClose: true,
        progressBar: true,
        enableSounds: true,
         duration: 4000,// .... Default value is set to 4000 (4 seconds). 

        transition: "slideUpDownFade",
        sounds: {
          info: "{{ asset('images/noti.mp3') }}",
          //info: "https://res.cloudinary.com/dxfq3iotg/video/upload/v1557233294/info.mp3",
          // path to sound for successfull message:
          success: "https://res.cloudinary.com/dxfq3iotg/video/upload/v1557233524/success.mp3",
          // path to sound for warn message:
          warning: "https://res.cloudinary.com/dxfq3iotg/video/upload/v1557233563/warning.mp3",
          // path to sound for error message:
          error: "https://res.cloudinary.com/dxfq3iotg/video/upload/v1557233574/error.mp3",
        },
      };
    window.Laravel = {
        csrfToken: "{{ csrf_token() }}"
    };
    var toast = new Toasty(options);
    toast.configure(options);
@if(isset(Auth::user()->id))

    /*Echo.private('App.User.{{Auth::user()->id }}')
    .notification((notification) => {
       console.log(notification.message.user_type +' users only');

      //if(notification.message.user_type=='user'){
          toast.success(notification.message.body);
      // }
        
    });*/


@endif
</script>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '426666732115737',
      cookie     : true,
      xfbml      : true,
      version    : '1.0'
    });
      
    FB.AppEvents.logPageView();   
      
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/602b60d19c4f165d47c3a7b4/1eukn4dcg';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>

    

</body>
</html>