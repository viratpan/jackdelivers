@extends('layouts.vendor')
@section('style')
  <style type="text/css">
      .pagination{float: right;}
  </style>
@endsection


@section('content')
    
    <div class="breadcrumb">
        <h1>Dashboard</h1>
        {{-- <ul>
            <li><a href="href.html">Form</a></li>
            <li>Basic</li>
        </ul> --}}
    </div>
    <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Vendor Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in! as Vendor') }}
                </div>
            </div>
        </div>
    </div>
</div>
    
@endsection
