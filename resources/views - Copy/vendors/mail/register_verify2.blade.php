<td class="esd-stripe" align="center" esd-custom-block-id="233560">
    <table class="es-footer-body" align="center" cellpadding="0" cellspacing="0" width="600" style="background-color: transparent;">
        <tbody>
            <tr>
                <td class="esd-structure es-p10t es-p20r es-p20l" align="left">
                    <!--[if mso]><table width="560" cellpadding="0" cellspacing="0"><tr><td width="194" valign="top"><![endif]-->
                    <table cellpadding="0" cellspacing="0" class="es-left" align="left">
                        <tbody>
                            <tr>
                                <td width="174" class="es-m-p0r es-m-p20b esd-container-frame" align="center">
                                    <table cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>
                                            <tr>
                                                <td align="left" class="esd-block-text es-p10t es-p10b" esd-links-underline="none">
                                                    <p style="font-size: 18px;"><strong>Info</strong></p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" class="esd-block-text" esd-links-underline="none">
                                                    <p><a target="_blank" style="text-decoration: none;" href="https://viewstripo.email">About</a><br><a target="_blank" style="text-decoration: none;" href="https://viewstripo.email">Gallery</a><br><a target="_blank" style="text-decoration: none;" href="https://viewstripo.email">Blog</a></p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td class="es-hidden" width="20"></td>
                            </tr>
                        </tbody>
                    </table>
                    <!--[if mso]></td><td width="173" valign="top"><![endif]-->
                    <table cellpadding="0" cellspacing="0" class="es-left" align="left">
                        <tbody>
                            <tr>
                                <td width="173" class="es-m-p20b esd-container-frame" align="center">
                                    <table cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>
                                            <tr>
                                                <td align="left" class="esd-block-text es-p10t es-p10b" esd-links-underline="none">
                                                    <p style="font-size: 18px;"><strong>Contact us</strong></p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" class="esd-block-text" esd-links-underline="none">
                                                    <p><a target="_blank" href="https://viewstripo.email" style="text-decoration: none;">► Your addres will be here<br></a><a target="_blank" href="tel:" style="text-decoration: none;">► (000) 1234 5678 99</a><br><a target="_blank" href="mailto:" style="text-decoration: none;">► company@gmail.com</a></p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <!--[if mso]></td><td width="20"></td><td width="173" valign="top"><![endif]-->
                    <table cellpadding="0" cellspacing="0" class="es-right" align="right">
                        <tbody>
                            <tr>
                                <td width="173" align="center" class="esd-container-frame">
                                    <table cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>
                                            <tr>
                                                <td align="left" class="esd-block-text es-p10t es-p10b" esd-links-underline="none">
                                                    <p style="font-size: 18px;"><strong>Locate us</strong></p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="esd-block-image" style="font-size: 0px;"><a target="_blank" href="https://viewstripo.email"><img class="adapt-img" src="https://tlr.stripocdn.email/content/guids/CABINET_000f7632d9ef1f3e261fd7b4ea3d60f0/images/69201609329712463.png" alt="Map" style="display: block;" width="173" title="Map"></a></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <!--[if mso]></td></tr></table><![endif]-->
                </td>
            </tr>
            <tr>
                <td class="esd-structure es-p20" align="left">
                    <!--[if mso]><table width="560" cellpadding="0" cellspacing="0"><tr><td width="270" valign="top"><![endif]-->
                    <table cellpadding="0" cellspacing="0" align="left" class="es-left">
                        <tbody>
                            <tr>
                                <td width="270" class="esd-container-frame es-m-p20b" align="center" valign="top">
                                    <table cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>
                                            <tr>
                                                <td align="left" class="esd-block-social es-p15t" style="font-size:0">
                                                    <table cellpadding="0" cellspacing="0" class="es-table-not-adapt es-social">
                                                        <tbody>
                                                            <tr>
                                                                <td align="center" valign="top" class="es-p10r"><a target="_blank" href="https://viewstripo.email."><img title="Facebook" src="https://tlr.stripocdn.email/content/assets/img/social-icons/rounded-black-bordered/facebook-rounded-black-bordered.png" alt="Fb" width="32" height="32"></a></td>
                                                                <td align="center" valign="top" class="es-p10r"><a target="_blank" href="https://viewstripo.email."><img title="Twitter" src="https://tlr.stripocdn.email/content/assets/img/social-icons/rounded-black-bordered/twitter-rounded-black-bordered.png" alt="Tw" width="32" height="32"></a></td>
                                                                <td align="center" valign="top" class="es-p10r"><a target="_blank" href="https://viewstripo.email."><img title="Instagram" src="https://tlr.stripocdn.email/content/assets/img/social-icons/rounded-black-bordered/instagram-rounded-black-bordered.png" alt="Inst" width="32" height="32"></a></td>
                                                                <td align="center" valign="top"><a target="_blank" href="https://viewstripo.email."><img title="Youtube" src="https://tlr.stripocdn.email/content/assets/img/social-icons/rounded-black-bordered/youtube-rounded-black-bordered.png" alt="Yt" width="32" height="32"></a></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <!--[if mso]></td><td width="20"></td><td width="270" valign="top"><![endif]-->
                    <table cellpadding="0" cellspacing="0" class="es-right" align="right">
                        <tbody>
                            <tr>
                                <td width="270" align="left" class="esd-container-frame">
                                    <table cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>
                                            <tr>
                                                <td align="right" class="esd-block-text es-m-txt-l">
                                                    <p><a target="_blank">Unsubscribe</a><br><a target="_blank" href="https://viewstripo.email">View in browser<br></a>Illustration by&nbsp;<a target="_blank" href="https://www.freepik.com/">freepik</a><a target="_blank" href="https://viewstripo.email"></a><br></p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <!--[if mso]></td></tr></table><![endif]-->
                </td>
            </tr>
        </tbody>
    </table>
</td>