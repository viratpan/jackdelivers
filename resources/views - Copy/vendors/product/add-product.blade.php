<?php $layouts='layouts.vendor'; ?>

@if(Auth::guard('admin')->check())
@php $layouts='layouts.admin' @endphp
@endif
@if(Auth::guard('vendor')->check())
   @php  $layouts='layouts.vendor' @endphp
@endif

 @extends($layouts)

@section('scripts')
  <script src="{{  asset('js/plugins/select2.min.js') }}" ></script>
  <script type="text/javascript">
    $(document).ready(function () {
      //$('.select2').select2();
      $('#fastDiscountDiv').hide();
    });
  </script>
 
  <script type="text/javascript">
    $(document).ready(function () {
      $.ajaxSetup({
           headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
       });
  $('#category').change(function(e) {
     var id=$(this).val();     
     $.get('{{ route('ajax.category.options') }}', 
      { 'id': id }, 
      function( res ) {
        if(res){
            $("#sub_category").empty();
            $.each(res,function(key,value){
                $("#sub_category").append('<option value="'+value.id+'">'+value.name+'</option>');
            });
            $("#sub_category").trigger('change');
        }else{
           $("#sub_category").empty();
        }
        
        
      }
    );

  });
  /*on change  Sub Category get all items*/
  $('#vendor').change(function(e) {
    //$("#sub_category").trigger('change');
  });
  $('#fastDiscount').blur(function(e) { //alert('fastDiscount');
    var value=$('#fastDiscount').val(); console.log(value);
    //var discount=$(this_obj).parents("#item_"+id).find('#item_discount_'+id).val();
    $('#itemselectedlist .form-row ').find('input.DiscountInput').each(function() {
      console.log($(this).html());//.find("input[name='discount']")
      $(this).val(value);
      $(this).trigger('onblur');
    });
  });

  $('#sub_category').change(function(e) {
     var category=$('#category').val(); 
     var sub_category=$('#sub_category').val(); 
     var vendor_id=$('#vendor').val();  
     if(vendor_id==''|| sub_category=='' || category==''){
      toastr.error("Select Vendor !", { closeButton: !0 });
      
    }else{
       $.get('{{ route('ajax.category_sub_category.items') }}', 
        { 'category': category,'subcategory':sub_category,'vendor':vendor_id }, 
        function( res ) {
          if(res){
            console.log(res.items.length);
            if(res.items.length==0){
             // $('#fastDiscountDiv').hide();
                 $("#itemlist").html('<label for="inputEmail4"><b>No Items Match</b></label>');
            }else{
              $('#fastDiscountDiv').show();
               $("#itemlist").html('<label for="inputEmail4">Items</label> : &nbsp;&nbsp;');
              $.each(res.items,function(key,value){//console.log(value.name);
                  var checked=''; console.log(value.id+' subcategory reload ['+ items+'] | '+$.inArray(value.id,items));
                  if($.inArray(value.id,items)!=-1){
                    checked='checked'; alert('checked');
                  } 
                  var data= showItem(value.id,value.name,value.aprice,value.attributes_name,checked);             
                  $("#itemlist").append(data);
              });
            }
             
             
              //$("#itemlist").append('<hr class="col-md-12"><a class="btn btn-sm btn-info float-right" id="addItem1" onclick="showAllSelectedItems();" style="" >Next</a><hr class="col-md-12">');
          }else{
              toastr.error("Items Not found !", { closeButton: !0 });
              $('#fastDiscountDiv').hide();
          }
          
          
        }
      );
      }
  });
        $('#itemlist #addItem1').click(function(e) {
            alert('add item');
        });

  function showAllSelectedItems() {
    alert('add item');
  }
  function showItem(id,name,cost,unit,checked){ console.log(name);
    
   var html=`<label class="checkbox checkbox-outline-primary" style="padding-right: 4px;">
                <input type="checkbox" onclick="addItem(this);" id="sItem_`+id+`"  class="itemlist" value="`+id+`" data-value="`+id+`" data-name="`+name+`" data-div="item_`+id+`" `+checked+` data-unit="`+unit+`" data-cost="`+cost+`"  /><span>`+name+`</span><span class="checkmark"></span>
             </label>`;
            return html;
  }




    });
var items=[];
function addItem(this_obj){
  //console.log(this_obj);
  var name=$(this_obj).attr('data-name');
  var unit=$(this_obj).attr('data-unit');
  var id=$(this_obj).attr('data-value');
  var cost=$(this_obj).attr('data-cost');
  var vendor_id=$('#vendor').val(); 
  var submitBtn=` <button type="submit" class="btn btn-primary ">Save</button>`;
  

  if($.inArray(id,items)==-1){
      items.push(id); console.log(items);
  
      var html=`<div class="form-row" id="item_`+id+`">        
                <div class="form-group col-md-2" style="margin-top:1.55rem;">                  
                      <b>`+name+`</b>                  
                </div>
                <div class="form-group col-md-1">
                  <label for="inputEmail4">Quantity</label>
                  <input type="number" step=".0001" value="1" name="qty[`+id+`]" class="form-control" required placeholder="Quantity">
                  <input type="hidden" value="`+id+`" data-value="`+id+`" name="item[]" class="form-control" required placeholder="Quantity">
                  
              </div>
                 <div class="form-group col-md-1" style="margin-top: 1.75rem;">`+unit+`</div>
                <div class="form-group col-md-2">
                  <label for="inputEmail4">Actual Price<span class="text-red">*</span></label>
                  <input type="number" step=".0001" onblur="calculate(this);" id="item_aprice_`+id+`" data-value="`+id+`" value="`+cost+`" name="aprice[`+id+`]" class="form-control" required placeholder="Acctual Cost">
                </div>
                <div class="form-group col-md-2">
                  <label for="inputEmail4">Discount % <span class="text-red">*</span></label>
                  <input type="number" step=".0001" onblur="calculate(this);"  id="item_discount_`+id+`" data-value="`+id+`" value="0" name="discount[`+id+`]" class="form-control DiscountInput" required placeholder="Discount">
                </div>
                <div class="form-group col-md-2">
                  <label for="inputEmail4">Sales Price <span class="text-red">*</span></label>
                  <input type="number" step=".0001"   id="item_price_`+id+`" data-value="`+id+`" value="`+cost+`" name="price[`+id+`]" class="form-control" required placeholder="Price">
                </div>
                
              </div>`;
      $('#itemselectedlist').append(html);
  }else{
      
      remove(this_obj);
     // toastr.error("Items Already Selected.", { closeButton: !0 });
  }
  /*show submit Btn*/
  //alert(items.length);
  if(items.length!=0){
    $('#formSubmit').html(submitBtn);
  }else{
    $('#formSubmit').html('');
  }
}
function remove(this_obj){
  var id=$(this_obj).attr('data-value');
  var div=$(this_obj).attr('data-div');
   //$(this_obj).parents("#"+div).remove();
   $('#itemselectedlist #'+div).remove();

  items = jQuery.grep(items, function(value) {
        return value != id;
      });
  
  //console.log(items);

}
function calculate(this_obj) {
    var id=$(this_obj).attr('data-value');

    var cost=$(this_obj).parents("#item_"+id).find('#item_aprice_'+id).val();
    var discount=$(this_obj).parents("#item_"+id).find('#item_discount_'+id).val();
    var price=$(this_obj).parents("#item_"+id).find('#item_price_'+id).val();
    var finalp=0; console.log(id+" calculate| "+cost+" cost|"+discount+" discount|"+price+" price|" );
   
    
    if(cost && discount && price){
        finalp=cost-(cost*discount/100);
        $(this_obj).parents("#item_"+id).find('#item_price_'+id).val(finalp);
    }
 
}
  
  </script>
  <script>
 

</script>
@endsection


@section('content')
<style type="text/css">
  .text-red{color: red;font-weight: 700;}
</style>
<div class="card ">
  <div class="card-header">
    <h5 align="left">Vendor Item Mapping
@if(Auth::guard('admin')->check())
  <a class="btn btn-sm btn-info float-right" style="" href="{{ route('admin.product') }}"><b>Back</b></a>
@endif
@if(Auth::guard('vendor')->check())
     <a class="btn btn-sm btn-info float-right" style="" href="{{ route('vendor.product') }}"><b>Back</b></a>
@endif
        
    </h5>

  </div>
  <div class="card-body">
@if(Auth::guard('admin')->check())
  <form method="POST" action="{{ route('admin.product-store2') }}" enctype="multipart/form-data">
@endif
@if(Auth::guard('vendor')->check())
     <form method="POST" action="{{ route('vendor.product-store2') }}" enctype="multipart/form-data">
@endif
    
        @csrf
        <div class="form-row">
          @if(Auth::guard('admin')->check())
           <div class="form-group col-md-3">
            <label for="inputEmail4">Vendor <span class="text-red">*</span></label>
             <select id="vendor" value="{{ old('vendor_id') }}" name="vendor_id" class="form-control select2" required >
                <option value="">Please Select </option>
                @foreach($vendor as $svalue)
                  <option value="{{ $svalue->id }}" >{{ $svalue->name }}</option>
                @endforeach
              </select>
          </div>
        
        @endif
         
          <div class="form-group col-md-3">
          <label for="inputEmail4">Category <span class="text-red">*</span></label>
           <select id="category" value="{{ old('categry_id') }}"  class="form-control" required >
              <option value="">Please Select </option>
              @foreach($category as $svalue)
                <option value="{{ $svalue->id }}" >{{ $svalue->name }}</option>
              @endforeach
            </select>
             @if(Auth::guard('vendor')->check())      
                <input type="hidden" name="vendor" id="vendor" value="{{ Auth::guard('vendor')->user()->id }}"> 
            @endif 
        </div>
        <div class="form-group col-md-3">
          <label for="inputEmail4">Sub Category <span class="text-red">*</span></label>
           <select id="sub_category" value="{{ old('sub_categry_id') }}"  class="form-control" required >
              <option value="">Please Select Category</option>
              {{-- @foreach($sub_category as $svalue)
                <option value="{{ $svalue->id }}" >{{ $svalue->name }}</option>
              @endforeach --}}
            </select>
        </div>

        
        
         <hr class="col-md-12" style="margin: 8px;">
        

        <div class="form-group col-md-12" id="itemlist" style="display: flex;">     
          <label for="">No Items. Please Select Category and Sub-Category.</label>

        </div>
        <hr class="col-md-12" style="margin: 8px;">
        <div class="form-group col-md-3" id="fastDiscountDiv">
          <label for="">Bulk Discount <span class="text-red"></span></label>
           <input type="number" step=".0001"  id="fastDiscount" class="form-control">
        </div>

          <div class="form-group col-md-12" id="itemselectedlist"></div>

      </div>
      
      <div class="form-row float-right" id="formSubmit">
         
      </div>
    </form>

  </div>
  <div class="card-footer text-muted">
   
  </div>
</div>
    
@endsection
