<?php $layouts='layouts.vendor'; ?>

@if(Auth::guard('admin')->check())
@php $layouts='layouts.admin' @endphp
@endif
@if(Auth::guard('vendor')->check())
 @php  $layouts='layouts.vendor' @endphp
@endif

 @extends($layouts)




@section('content')
<style type="text/css">
  .text-red{color: red;font-weight: 700;}
</style>
<div class="card ">
  <div class="card-header">
@if(Auth::guard('admin')->check())
    <h5>Product:<b> {{$data->name}}</b> | <small>Vendor :  </small>{{ $data->vendor_name}}

        <a class="btn btn-sm btn-info float-right" style="" href="{{ route('admin.product') }}"><b>Back</b></a>
    </h5>

@endif
@if(Auth::guard('vendor')->check())
  <h5 align="left">Edit Product : {{ $data->name}}

        <a class="btn btn-sm btn-info float-right" style="" href="{{ route('vendor.product') }}"><b>Back</b></a>
    </h5>

@endif
    
  </div>
  <div class="card-body">
    @if(Auth::guard('admin')->check())
       <form method="PUT" action="{{ route('admin.update-product',$data->id) }}" enctype="multipart/form-data">
    @endif
    @if(Auth::guard('vendor')->check())
       <form method="PUT" action="{{ route('vendor.update-product',$data->id) }}" enctype="multipart/form-data">
    @endif
   
        @csrf
        <div class="form-row">
        
        <div class="form-group col-md-3">
          <label for="inputEmail4">Category <span class="text-red">*</span></label>
           <select id="category" value="{{ $data->categry_id }}" name="categry_id" class="form-control" required >
              <option value="">Please Select</option>
              @foreach($category as $svalue)
                <option value="{{ $svalue->id }}" {{ isset($data) ? ( $data->categry_id==$svalue->id ? "Selected":"" ) : ""}}>{{ $svalue->name }}</option>
              @endforeach
            </select>
        </div>
        <div class="form-group col-md-3">
          <label for="inputEmail4">Sub Category <span class="text-red">*</span></label>
           <select id="sub_category" value="{{ $data->sub_categry_id }}" name="sub_categry_id" class="form-control" required >
              <option value="">Please Select</option>
              @foreach($sub_category as $svalue)
                <option value="{{ $svalue->id }}" {{ isset($data) ? ( $data->sub_categry_id==$svalue->id ? "Selected":"" ) : ""}}>{{ $svalue->name }}</option>
              @endforeach
            </select>
        </div>
        

        <div class="form-group col-md-6">
          <label for="inputEmail4">Name <span class="text-red">*</span></label>
          <input type="text" id="name" value="{{ $data->name }}" name="name" class="form-control" required placeholder="Name">
        </div>      

         <div class="form-group col-md-6">
          <label for="inputEmail4"> Title <span class="text-red">*</span></label>
          <input type="text" id="title" value="{{ $data->meta_title }}" name="meta_title" class="form-control" required placeholder=" Title">

          <label for="inputEmail4"> Keywords <span class="text-red">*</span></label>
          <input type="text" value="{{ $data->meta_keywords }}" name="meta_keywords" class="form-control" required placeholder=" Keywords">

          <label for="inputEmail4"> Description <span class="text-red">*</span></label>
          <input type="text" value="{{ $data->meta_description }}" name="meta_description" class="form-control" required placeholder=" Description">
        </div>       
        

        <div class="form-group col-md-6">
          <label for="inputEmail4">Description <span class="text-red">*</span></label>
          <textarea value="{{ $data->description }}" rows="6" name="description" class="form-control" required placeholder="Description">{{ $data->description }}</textarea>
        </div>

        <div class="form-group col-md-2">
          <label for="inputPassword4">Image</label>
         
         <input type="file" accept="image/png, image/jpeg" class="form-control" id="aadharFile">
          <input type="hidden"  value="{{ $data->image }}" class="form-control" id="aadharupload" name="image">
        </div>
        @if($data->image)
          <div class="form-group col-md-2">
          <img  src="{{ asset('').$data->image }}" class="" style="height: 8rem;padding-top: 2px;">
        </div>
        @endif
         <div id="restutantDiv" class="form-group col-md-4 form-row">
        
         </div>
        
        

        
  
 

        
        


      </div>
      
      <div class="form-row float-right">
          <button type="submit" class="btn btn-primary ">Save</button>
      </div>
    </form>

  </div>
  <div class="card-footer text-muted">
    {{-- {!! $uData->links() !!} --}}
  </div>
</div>
    
@endsection

@section('scripts')
  <script src="{{  asset('js/plugins/select2.min.js') }}" ></script>
  <script type="text/javascript">
    $(document).ready(function () {
      //$('#category').triggre('change');
      restaurantshtml();
    });
  </script>
 
  <script type="text/javascript">
    $(document).ready(function () {
      $.ajaxSetup({
           headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
       });
/*Aadhar file Upload Start*/
      $('#aadharFile').change(function() { 
        var image=$(this).val();
       
        if(image){
            var postData=new FormData();
            postData.append('file',this.files[0]);     
            var url="{{route('ajax.file.upload')}}";     
            $.ajax({               
                async:true,
                type:"post",
                url:url,
                data:postData,
                cache:false,
                contentType: false,
                processData: false,
                success:function(data){
                  console.log(data);
                  if(data.path){
                    $('#aadharupload').val(data.path);
                    toastr.success(''+data.message, { closeButton: !0 });
                  }else{
                    toastr.error(''+data.message, { closeButton: !0 });
                  }
                },error: function(xhr, status, error) {
                  //console.log(xhr.responseJSON.errors.file);
                  var err=xhr.responseJSON.errors.file;
                   $.each(err,function(key,value){
                        toastr.error(value, { closeButton: !0 });
                      
                  });
                  
                }  
     
            });
        }
      }); 
/*Aadhar file Upload End*/


    });
  </script>
  <script>
  function restaurantshtml(){ //alert('trigger to get');
    var selectedText = $("#category option:selected").html(); 
     var restaurantshtml='<div class="form-group col-md-6">'+
        '  <label for="inputEmail4">Type <span class="text-red">*</span></label>'+
        '   <select id="type" value="{{ $data->type }}" name="type" class="form-control" required >'+
        '      <option value="">Please Select </option>'+             
        '        <option value="veg" {{ isset($data) ? ( $data->type=='veg' ? "Selected":"" ) : ""}}>Veg</option>'+
        '        <option value="nonveg" {{ isset($data) ? ( $data->type=='nonveg' ? "Selected":"" ) : ""}}>Non-Veg</option>'+
        '    </select>'+
        '</div>'+
        '<div class="form-group col-md-6">'+
        '  <label for="inputEmail4">Cost Of Two <span class="text-red">*</span></label>'+
        '  <input type="text" id="costoftwo" value="{{ $data->costoftwo }}" name="costoftwo" class="form-control" required placeholder="Cost Of Two">'+
        '</div>';
     selectedText=selectedText.toLowerCase();
     if(selectedText=='restaurants'){
        $('#restutantDiv').html(restaurantshtml);
     }else{$('#restutantDiv').html('');}
  }
  $('#name').change(function(e) {//alert('trigger');
    var name=$(this).val();
    $.get('{{ route('ajax.product.check_slug') }}', 
      { 'name': name }, 
      function( data ) {
        $('#slug').val(data.slug);
        $('#title').val(name);
      }
    );
  });
  
  $('#category').change(function(e) {
     var id=$(this).val();
     var selectedText = $("#category option:selected").html(); 
     var restaurantshtml='<div class="form-group col-md-6">'+
        '  <label for="inputEmail4">Type <span class="text-red">*</span></label>'+
        '   <select id="type" value="{{ $data->type }}" name="type" class="form-control" required >'+
        '      <option value="">Please Select </option>'+             
        '        <option value="veg" {{ isset($data) ? ( $data->type=='veg' ? "Selected":"" ) : ""}}>Veg</option>'+
        '        <option value="nonveg" {{ isset($data) ? ( $data->type=='nonveg' ? "Selected":"" ) : ""}}>Non-Veg</option>'+
        '    </select>'+
        '</div>'+
        '<div class="form-group col-md-6">'+
        '  <label for="inputEmail4">Cost Of Two <span class="text-red">*</span></label>'+
        '  <input type="text" id="costoftwo" value="{{ $data->costoftwo }}" name="costoftwo" class="form-control" required placeholder="Cost Of Two">'+
        '</div>';
     selectedText=selectedText.toLowerCase();
     if(selectedText=='restaurants'){
        $('#restutantDiv').html(restaurantshtml);
     }else{$('#restutantDiv').html('');}
     $.get('{{ route('ajax.category.options') }}', 
      { 'id': id }, 
      function( res ) {
        if(res){
            $("#sub_category").empty();
            $.each(res,function(key,value){
                $("#sub_category").append('<option value="'+value.id+'">'+value.name+'</option>');
            });
       
        }else{
           $("#sub_category").empty();
        }
        
        
      }
    );

  });


</script>
@endsection