<style type="text/css">
  .text-red{color: red;font-weight: 700;}
</style>
<script type="text/javascript">
    var vendor_lat='{{ $vendor->latitude }}';
    var vendor_long='{{ $vendor->longitude }}';
    var vendor_draggable=1;
</script>
<div class="card ">
  <div class="card-header">
    @if(Auth::guard('admin')->check())
      <h5 align="left">Edit Vendor : {{ $vendor->company_name }}

        <a class="btn btn-sm btn-info float-right" style="" href="{{ route('admin.vendor-list') }}"><b>Back</b></a>
    </h5>
    
    @elseif(Auth::guard('vendor')->check())
      <h5 align="left">Edit Profile

        <a class="btn btn-sm btn-info float-right" style="" href="{{ route('vendor.dashboard') }}"><b>Back</b></a>
    </h5>
    @else
    <h5 align="left">Edit Profile <b>{{ $vendor->company_name }}</b></h5>
     @endif
    

  </div>
  <div class="card-body">
   @if(Auth::guard('admin')->check())
    <form method="PUT" action="{{ route('admin.vendor.update',$vendor->id) }}" enctype="multipart/form-data">    
    @elseif(Auth::guard('vendor')->check())
    <form method="PUT" action="{{ route('vendor.profile.update') }}" enctype="multipart/form-data"> 
    @else
    <form method="PUT" action="{{ route('vendor-profile-update',$vendor->id) }}" enctype="multipart/form-data"> 
    @endif
        @csrf
      <div class="form-row">
        
        <div class="form-group col-md-2">
          <label for="inputPassword4">Email <span class="text-red">*</span></label>
         <input type="email" value="{{ $vendor->email }}" name="email" class="form-control" required=""  placeholder="Email">
        </div>

        <div class="form-group col-md-2">
          <label for="inputEmail4">Vendor Name <span class="text-red">*</span></label>
          <input type="text" value="{{ $vendor->name }}" name="name" class="form-control" required placeholder="Name">
        </div>
        <div class="form-group col-md-2">
          <label for="inputEmail4">Type of Organization <span class="text-red">*</span></label>
         <select value="{{ old('org_type') }}" tabindex="2" id="" name="org_type"  class="form-control " required >
              <option value="">Please Select</option>
              <option value="1" {{ isset($vendor) ? ( $vendor->org_type==1 ? "Selected":"" ) : ""}} >Company</option>
              <option value="2" {{ isset($vendor) ? ( $vendor->org_type==2 ? "Selected":"" ) : ""}} >Partnership firm/LLP</option>
              <option value="3" {{ isset($vendor) ? ( $vendor->org_type==3 ? "Selected":"" ) : ""}} >Sole Proprietorship</option>
              <option value="4" {{ isset($vendor) ? ( $vendor->org_type==4 ? "Selected":"" ) : ""}} >Independent Body</option>
                  
          </select>
        </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">Company Name <span class="text-red">*</span></label>
         <input type="text" required value="{{ $vendor->company_name }}" name="company_name" class="form-control"   placeholder="Company Name">
        </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">Designation <span class="text-red">*</span></label>
          <input type="text" required value="{{ $vendor->designation }}" class="form-control" name="designation" placeholder="Designation">
        </div>

         <div class="form-group col-md-3">
          <label for="inputPassword4">Mobile Number <span class="text-red">*</span></label>
         <input type="text" required value="{{ $vendor->mobile }}" name="mobile" class="form-control"  placeholder="Mobile Number">
        </div>
        
        <div class="form-group col-md-3">
          <label for="inputPassword4">Whatsapp Number <span class="text-red">*</span></label>
           <input type="text" required name="telephone" value="{{ $vendor->telephone }}" class="form-control"  placeholder="Whatsapp Number">
        </div>
        <div class="form-group col-md-6" >
          <div id="map" style="width: 98%; height: 250px;position: absolute;">
            <img src="{{ asset('images/map.jpg') }}" style="width: 100%;">
          </div>
        </div>

        <div class="form-group col-md-6">
          <label for="inputPassword4">Company Address <span class="text-red">*</span></label>
         <input type="text" id="address" name="address"  required name="address" value="{{ $vendor->address }}" class="form-control"  placeholder="Company  Address">
         <div class="form-row">
            <div class="form-group col-md-6">
              <label for="">City <span class="text-red">*</span></label>
               <select value="{{ $vendor->city }}"  name="city"  class="form-control " required >
               <option value="">Please Select</option>
              @foreach($city as $pcKey => $svalue)
                <option value="{{ $svalue->id }}" {{ isset($vendor) ? ( $vendor->city==$svalue->id ? "Selected":"" ) : ""}}>{{ $svalue->name }}</option>
              @endforeach
            </select>
            </div>
            <div class="form-group col-md-6">
              <label for="inputPassword4">PIN Code<span class="text-red">*</span></label>
             <input type="text" required name="pin" value="{{ $vendor->pin }}" class="form-control"  placeholder="PIN Code">
            </div>
            {{-- <div class="form-group col-md-6">
              <label for="inputPassword4">latitude <span class="text-red">*</span></label>
             <input type="text"  id="latitude" name="latitude" value="{{ $vendor->latitude }}" class="form-control"  placeholder="latitude">
            </div>
            <div class="form-group col-md-6">
              <label for="inputPassword4">longitude <span class="text-red">*</span></label>
             <input type="text"  id="longitude" name="longitude" value="{{ $vendor->longitude }}" class="form-control"  placeholder="longitude">
            </div> --}}
         </div>
        </div>

        {{-- 
         <div class="form-group col-md-3">
          <label for="inputPassword4">City <span class="text-red">*</span></label>
           <select value="{{ $vendor->city }}" id="" name="city"  class="form-control " required >
               <option value="">Please Select</option>
              @foreach($city as $pcKey => $svalue)
                <option value="{{ $svalue->id }}" {{ isset($vendor) ? ( $vendor->city==$svalue->id ? "Selected":"" ) : ""}}>{{ $svalue->name }}</option>
              @endforeach
            </select>
        </div>
        <div class="form-group col-md-6">
          <label for="inputPassword4">Company Address <span class="text-red">*</span></label>
         <input type="text" id="address" name="address"  required name="address" value="{{ $vendor->address }}" class="form-control"  placeholder="Company  Address">
        </div>
        <div class="form-group col-md-2">
          <label for="inputPassword4">Company Address PIN<span class="text-red">*</span></label>
         <input type="text" required name="pin" value="{{ $vendor->pin }}" class="form-control"  placeholder="Company Address PIN">
        </div> --}}

        <hr class="col-12" style="margin: 8px;">
        <div class="form-group col-md-3">
          <label for="inputPassword4">Under Vertical<span class="text-red">*</span></label>       
           <select value="{{ old('masters_id') }}" name="masters_id" class="form-control " required >
               <option value="">Please Select</option>
              @foreach($master as $pcKey => $svalue)
                <option value="{{ $svalue->id }}" {{ isset($vendor) ? ( $vendor->masters_id==$svalue->id ? "Selected":"" ) : ""}} >{{ $svalue->name }}</option>
              @endforeach
            </select>
        </div>
        {{-- <div class="form-group col-md-3">
          <label for="inputPassword4">Commission Plan<span class="text-red">*</span></label>       
           <select value="{{ old('commission_id') }}" name="commission_id" class="form-control " required >
               <option value="">Please Select</option>
              @foreach($commission as $pcKey => $svalue)
                <option value="{{ $svalue->id }}" {{ isset($vendor) ? ( $vendor->commission_id==$svalue->id ? "Selected":"" ) : ""}} >{{ $svalue->name }} ( {{ $svalue->commission }}% )</option>
              @endforeach
            </select>
        </div>
          --}}
        
        <hr class="col-12" style="margin: 8px;">
        <div class="form-group col-md-3">
          <label for="inputPassword4">Image</label>       
           <input type="file" class="form-control" id="imageFile">
            <input type="hidden"  value="{{ $vendor->image }}" class="form-control" id="imageupload" name="image">
             @if($vendor->image)
           <img  src="{{ asset('').$vendor->image }}" class="" style="height: 4rem;padding-top: 2px;">
            @else
            <span style="border: 1px solid gray;padding: 1rem; position: inherit;top: 2rem;">
                  No Image
              </span>
            @endif
        </div>

         <div class="form-group col-md-3">
          <label for="inputPassword4">Cover Image</label>       
           <input type="file" class="form-control" id="cimageFile">
            <input type="hidden"  value="{{ $vendor->coverimage }}" class="form-control" id="cimageupload" name="coverimage">
             @if($vendor->coverimage)
           <img  src="{{ asset('').$vendor->coverimage }}" class="" style="height: 4rem;padding-top: 2px;">
           @else
            <span style="border: 1px solid gray;padding: 1rem; position: inherit;top: 2rem;">
                  No Image
              </span>
          @endif
            
        </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">Menu Image</label>       
           <input type="file" tabindex="10" class="form-control" id="mimageFile">
            <input type="hidden"  value="{{ old('menu') }}" class="form-control" id="mimageupload" name="menu">
             @if($vendor->menu)
           <img  src="{{ asset('').$vendor->menu }}" class="" style="height: 4rem;padding-top: 2px;">
             @else
            <span style="border: 1px solid gray;padding: 1rem; position: inherit;top: 2rem;">
                  No Image
              </span>
            @endif
        </div>
         
         {{-- <div class="form-group col-md-3">
          <label for="inputPassword4">Commission In (%) <span class="text-red">*</span></label>       
            <input type="text" class="form-control"  name="commission" required  value="{{ $vendor->commission }}" step=".01" placeholder="Commission In (%)">
        </div> --}}


        <hr class="col-12" style="margin: 8px;">
        <div class="form-group col-md-3">
          <label for="inputPassword4">Aadhar No. <span class="text-red">*</span></label>
         <input type="text" class="form-control"  name="aadhar" required  value="{{ $vendor->aadhar }}" placeholder="Aadhar Card">
         <input type="file" {{ isset($vendor) ? ( $vendor->aadharupload ? "":"required" ) : "required"}} class="form-control" id="aadharFile">
          <input type="hidden"  value="{{ $vendor->aadharupload }}" class="form-control" id="aadharupload" name="aadharupload">
          @if($vendor->aadharupload)
           <img  src="{{ asset('').$vendor->aadharupload }}" class="" style="height: 4rem;padding-top: 2px;">
             @else
            <span style="border: 1px solid gray;padding: 1rem; position: inherit;top: 2rem;">
                  No Image
              </span>
            @endif
        </div>

        <div class="form-group col-md-3">
          <label for="inputPassword4">PAN No. <span class="text-red">*</span></label>
         <input type="text" class="form-control"  name="pan" required  value="{{ $vendor->pan }}" placeholder="PAN Card">
         <input type="file" {{ isset($vendor) ? ( $vendor->panupload ? "":"required" ) : "required"}} class="form-control" id="panFile">
          <input type="hidden" value="{{ $vendor->panupload }}" class="form-control" id="panupload" name="panupload">
          @if($vendor->panupload)
           <img  src="{{ asset('').$vendor->panupload }}" class="" style="height: 4rem;padding-top: 2px;">
           @else
            <span style="border: 1px solid gray;padding: 1rem; position: inherit;top: 2rem;">
                  No Image
              </span>
            @endif

        </div>

        <div class="form-group col-md-3">
          <label for="inputPassword4">GSTIN <span class="text-red"></span></label>
         <input type="text" class="form-control" title="Goods and Services Tax Identification Number" name="gstin"   value="{{ $vendor->gstin }}" placeholder="Goods and Services Tax Identification Number ">
         <input type="file" class="form-control" id="gstinFile">
          <input type="hidden" value="{{ $vendor->gstinupload }}" class="form-control" id="gstinupload" name="gstinupload">
          @if($vendor->gstinupload)
           <img  src="{{ asset('').$vendor->gstinupload }}" class="" style="height: 4rem;padding-top: 2px;">
             @else
            <span style="border: 1px solid gray;padding: 1rem; position: inherit;top: 2rem;">
                  No Image
              </span>
            @endif
        </div>

         <div class="form-group col-md-3">
          <label for="inputPassword4">FSSAI Number </label>
         <input type="text" class="form-control" title="Food Safety & Standards Authority of India" value="{{ $vendor->fssai }}"  name="fssai" placeholder="Food Safety & Standards Authority of India">
         <input type="file" class="form-control" id="fssaiFile">
          <input type="hidden" class="form-control" id="fssaiupload" value="{{ $vendor->fssaiupload }}" name="fssaiupload">
          @if($vendor->menu)
           <img  src="{{ asset('').$vendor->fssaiupload }}" class="" style="height: 4rem;padding-top: 2px;">
             @else
           <span style="border: 1px solid gray;padding: 1rem; position: inherit;top: 2rem;">
                  No Image
              </span>
            @endif
        </div>
  
 

        
        


      </div>
      
      <div class="form-row float-right">
          <button type="submit" class="btn btn-primary ">Save</button>
      </div>
    </form>

  </div>
  <div class="card-footer text-muted">
    {{-- {!! $uData->links() !!} --}}
  </div>
</div>