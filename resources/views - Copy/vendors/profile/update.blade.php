@extends('layouts.app')
@section('scripts')
 
  <script type="text/javascript">
    $(document).ready(function () {
      //$('.select2').select2();
    });
  </script>
 
  <script type="text/javascript">
    $(document).ready(function () {
      $.ajaxSetup({
           headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
       });
/*Aadhar file Upload Start*/
      $('#imageFile').change(function() { 
        var image=$(this).val();
       
        if(image){
            var postData=new FormData();
            postData.append('file',this.files[0]);     
            var url="{{route('ajax.file.upload','vendor')}}";     
            $.ajax({               
                async:true,
                type:"post",
                url:url,
                data:postData,
                cache:false,
                contentType: false,
                processData: false,
                success:function(data){
                  console.log(data);
                  if(data.path){
                    $('#imageupload').val(data.path);
                    toastr.success('Image '+data.message, { closeButton: !0 });
                  }else{
                    toastr.error('Image '+data.message, { closeButton: !0 });
                  }
                },error: function(xhr, status, error) {
                  //console.log(xhr.responseJSON.errors.file);
                  var err=xhr.responseJSON.errors.file;
                   $.each(err,function(key,value){
                        toastr.error(value, { closeButton: !0 });
                      
                  });
                  
                }  
     
            });
        }
      }); 
/*Aadhar file Upload End*/
/*Aadhar file Upload Start*/
      $('#cimageFile').change(function() { 
        var image=$(this).val();
       
        if(image){
            var postData=new FormData();
            postData.append('file',this.files[0]);     
            var url="{{route('ajax.file.upload','vendor')}}";     
            $.ajax({               
                async:true,
                type:"post",
                url:url,
                data:postData,
                cache:false,
                contentType: false,
                processData: false,
                success:function(data){
                  console.log(data);
                  if(data.path){
                    $('#cimageupload').val(data.path);
                    toastr.success('Cover Image '+data.message, { closeButton: !0 });
                  }else{
                    toastr.error('Cover Image '+data.message, { closeButton: !0 });
                  }
                },error: function(xhr, status, error) {
                  //console.log(xhr.responseJSON.errors.file);
                  var err=xhr.responseJSON.errors.file;
                   $.each(err,function(key,value){
                        toastr.error(value, { closeButton: !0 });
                      
                  });
                  
                }  
     
            });
        }
      }); 
      $('#mimageFile').change(function() { 
        var image=$(this).val();
       
        if(image){
            var postData=new FormData();
            postData.append('file',this.files[0]);     
            var url="{{route('ajax.file.upload','vendor')}}";     
            $.ajax({               
                async:true,
                type:"post",
                url:url,
                data:postData,
                cache:false,
                contentType: false,
                processData: false,
                success:function(data){
                  console.log(data);
                  if(data.path){
                    $('#mimageupload').val(data.path);
                    toastr.success('Menu Image '+data.message, { closeButton: !0 });
                  }else{
                    toastr.error('Menu Image '+data.message, { closeButton: !0 });
                  }
                },error: function(xhr, status, error) {
                  //console.log(xhr.responseJSON.errors.file);
                  var err=xhr.responseJSON.errors.file;
                   $.each(err,function(key,value){
                        toastr.error(value, { closeButton: !0 });
                      
                  });
                  
                }  
     
            });
        }
      }); 
/*Aadhar file Upload End*/
/*Aadhar file Upload Start*/
      $('#aadharFile').change(function() { 
        var image=$(this).val();
       
        if(image){
            var postData=new FormData();
            postData.append('file',this.files[0]);     
            var url="{{route('ajax.file.upload','vendor')}}";     
            $.ajax({               
                async:true,
                type:"post",
                url:url,
                data:postData,
                cache:false,
                contentType: false,
                processData: false,
                success:function(data){
                  console.log(data);
                  if(data.path){
                    $('#aadharupload').val(data.path);
                    toastr.success('Aadhar '+data.message, { closeButton: !0 });
                  }else{
                    toastr.error('Aadhar '+data.message, { closeButton: !0 });
                  }
                },error: function(xhr, status, error) {
                  //console.log(xhr.responseJSON.errors.file);
                  var err=xhr.responseJSON.errors.file;
                   $.each(err,function(key,value){
                        toastr.error(value, { closeButton: !0 });
                      
                  });
                  
                }  
     
            });
        }
      }); 
/*Aadhar file Upload End*/
/*Pan file Upload Start*/

      $('#panFile').change(function() { 
        var image=$(this).val();
       
        if(image){
            var postData=new FormData();
            postData.append('file',this.files[0]);     
            var url="{{route('ajax.file.upload','vendor')}}";     
            $.ajax({               
                async:true,
                type:"post",
                url:url,
                data:postData,
                cache:false,
                contentType: false,
                processData: false,
                success:function(data){
                  console.log(data);
                  if(data.path){
                    $('#panupload').val(data.path);
                    toastr.success('PAN '+data.message, { closeButton: !0 });
                  }else{
                    toastr.error('PAN '+data.message, { closeButton: !0 });
                  }
                },error: function(xhr, status, error) {
                  //console.log(xhr.responseJSON.errors.file);
                  var err=xhr.responseJSON.errors.file;
                   $.each(err,function(key,value){
                        toastr.error(value, { closeButton: !0 });
                      
                  });
                  
                }  
     
            });
        }
      }); 
/*PAN file Upload End*/
/*GSTIN file Upload Start*/

      $('#gstinFile').change(function() { 
        var image=$(this).val();
       
        if(image){
            var postData=new FormData();
            postData.append('file',this.files[0]);     
            var url="{{route('ajax.file.upload','vendor')}}";     
            $.ajax({               
                async:true,
                type:"post",
                url:url,
                data:postData,
                cache:false,
                contentType: false,
                processData: false,
                success:function(data){
                  console.log(data);
                  if(data.path){
                    $('#gstinupload').val(data.path);
                    toastr.success('GSTIN '+data.message, { closeButton: !0 });
                  }else{
                    toastr.error('GSTIN '+data.message, { closeButton: !0 });
                  }
                },error: function(xhr, status, error) {
                  //console.log(xhr.responseJSON.errors.file);
                  var err=xhr.responseJSON.errors.file;
                   $.each(err,function(key,value){
                        toastr.error(value, { closeButton: !0 });
                      
                  });
                  
                }  
     
            });
        }
      }); 
/*GSTIN file Upload End*/
/*FSSAI file Upload Start*/

      $('#fssaiFile').change(function() { 
        var image=$(this).val();
       
        if(image){
            var postData=new FormData();
            postData.append('file',this.files[0]);     
            var url="{{route('ajax.file.upload','vendor')}}";     
            $.ajax({               
                async:true,
                type:"post",
                url:url,
                data:postData,
                cache:false,
                contentType: false,
                processData: false,
                success:function(data){
                  console.log(data);
                  if(data.path){
                    $('#fssaiupload').val(data.path);
                    toastr.success('FSSAI '+data.message, { closeButton: !0 });
                  }else{
                    toastr.error('FSSAI '+data.message, { closeButton: !0 });
                  }
                },error: function(xhr, status, error) {
                  //console.log(xhr.responseJSON.errors.file);
                  var err=xhr.responseJSON.errors.file;
                   $.each(err,function(key,value){
                        toastr.error(value, { closeButton: !0 });
                      
                  });
                  
                }  
     
            });
        }
      }); 
/*FSSAI file Upload End*/

    });
  </script>
 {{--  <script async defer src="https://maps.googleapis.com/maps/api/js?key={{ config('app.map_key') }}&sensor=true&libraries=places&callback=initMap"
  type="text/javascript"></script>
  
   <script type="text/javascript" src='{{ asset('js/other/vendor_map.js')}}'></script> --}}
   
   {{-- <script type="text/javascript" src='{{ asset('js/other/vendor_map.js')}}'></script>
   <script type="text/javascript" src='https://maps.google.com/maps/api/js?sensor=false&libraries=places&key=AIzaSyCKtoBv2YH27eVVbIK6SFv_4ldXDUCGqFE'></script> --}}
@endsection
@section('content')
  <div class="product-section section mt-30" >
    <div class="container" style="padding-top: 120px;padding-bottom: 120px;">
        @include('vendors.profile.editpage')
   </div>
  </div>
@endsection