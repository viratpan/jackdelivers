<!doctype html>
<html class="no-js" lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>{{ config('app.name') }} Vendor Register </title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
        ============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset(config('app.icon')) }}">
    <!-- Google Fonts
        ============================================ -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,400i,600,700,800,900" rel="stylesheet" />
    <!-- Scripts -->
   
    <link href="{{  asset('css/themes/lite-purple.min.css') }}" rel="stylesheet" />
    <style type="text/css">
        @media (max-width: 767px) {
          .auth-layout-wrap .auth-content {
            max-width: 660px!important;/*660px;*/
            margin: auto; }
           
        }

        @media (min-width: 1024px) {
          .auth-layout-wrap .auth-content {
            min-width: 660px!important; /*660px*/
            }
           
         }
         

    </style>
</head>
<div class="auth-layout-wrap" style="background-image: url(images/photo-wide-4.jpg)">
    <div class="auth-content">
        <div class="card o-hidden">
            <div class="row">
                <div class="col-md-6 text-center" style="background-size: cover;background-image: url(images/photo-long-3.jpg)">
                    <div class="pl-3 auth-right">
                        <div class="auth-logo text-center mt-4">
                            <img src="{{ asset(config('app.logo')) }}" alt="" style="width: 100%!important;">
                        </div>
                        {{-- <div class="flex-grow-1"></div>
                        <div class="w-100 mb-4"><a class="btn btn-outline-primary btn-block btn-icon-text btn-rounded" href="signin.html"><i class="i-Mail-with-At-Sign"></i> Sign in with Email</a><a class="btn btn-outline-google btn-block btn-icon-text btn-rounded"><i class="i-Google-Plus"></i> Sign in with Google</a><a class="btn btn-outline-facebook btn-block btn-icon-text btn-rounded"><i class="i-Facebook-2"></i> Sign in with Facebook</a></div>
                        <div class="flex-grow-1"></div> --}}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="p-4">
                        <h1 class="mb-3 text-18 text-center">Sign Up Vendor</h1>
                       <form method="POST" action="{{ route('register') }}{{ isset($url) ? '/'.$url : ""}}">
                        @csrf
                            
                            <div class="form-group">
                                <label for="company_name">Company Name</label>
                                <input class="form-control form-control-rounded @error('company_name') is-invalid @enderror" name="company_name" value="{{ old('company_name') }}" required autocomplete="company_name" id="company_name" type="text">
                                @error('company_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="email">Email address</label>
                                <input class="form-control form-control-rounded @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" id="email" type="email">
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="mobile">Mobile</label>
                                <input class="form-control form-control-rounded @error('mobile') is-invalid @enderror" name="mobile" value="{{ old('mobile') }}" required autocomplete="mobile" id="mobile" type="mobile">
                                @error('mobile')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="email">Category</label>
                               
                                @php 
                                $master=DB::table('masters')->get();
                                @endphp
                                 <select value="{{ old('masters_id') }}" name="masters_id" class="form-control @error('masters_id') is-invalid @enderror" required >
                                       <option value="">Please Select</option>
                                      @foreach($master as $pcKey => $svalue)
                                        <option value="{{ $svalue->id }}" >{{ $svalue->name }}</option>
                                      @endforeach
                                    </select>
                                @error('masters_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            
                            
                            <button class="btn btn-primary btn-block btn-rounded mt-3">Sign Up</button>
                        </form>
                        <div class="mt-3 text-center" >
                            
                            @if(isset($url) )
                                <a class="text-muted " style="" href="{{ route('login') }}{{ isset($url) ? '/'.$url : ""}}">
                                    Sign In
                                </a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>