@extends('layouts.app')

@section('content')
<style type="text/css">
    .comingSone{
        position: absolute;
        left: 25%;
        top: 41%;
        background: snow;
        padding: 5px;
        color: tomato;
    }
</style>
    <div class="about">
        <div class="container">
            <div class="row">
                {{-- <p>Home ->About</p> --}}
                <h3>About Bread Butter Basket</h3>
                <p>Why step out when you can get everything delivered home with the tap of a button? New Delhi's favourite delivery app gets you Food, Grocery, Medicine, Pet Supplies, Fruits & Vegetables, Meat & Fish, Health & Wellness, Gifts and Send Packages from one end of the city to the other. From your local kirana stores to your favourite brands, grocery shopping to your forgotten charger, we are always on the move for you. Why worry about your chores, when you can get it all Dun!</p>

                @foreach($master as $key => $value)
                    <div class="col-md-3 ">
                        @if($value->id==4)
                            <a href="{{ route('master',$value->slug)}}"  >
                                <img class="shadow" style="" src="{{ asset($value->image) }}" alt="{{ $value->name}}">
                            </a>
                        @else
                        <a >
                            <img class="shadow" style="" src="{{ asset($value->image) }}" alt="{{ $value->name}}">
                            <span class="comingSone">Coming Soon</span>
                        </a>
                        @endif
                    </div>
                @endforeach
                
            </div>
        </div>
    </div>
    {{-- <div class="about2">
        <div class="container">
            <div class="row">
                <h3>Top Picks for You</h3>
                
                <div class="col-md-3">
                    <img src="{{ asset('assets/images/2.jpg') }}">
                </div>
                <div class="col-md-3">
                    <img src="{{ asset('assets/images/22.jpg') }}">
                </div>
                <div class="col-md-3">
                    <img src="{{ asset('assets/images/23.jpg') }}">
                </div>
                <div class="col-md-3">
                    <img src="{{ asset('assets/images/24.jpg') }}">
                </div>
            </div>
        </div>
    </div> --}}
    <div class="app-store">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <img src="{{ asset('assets/images/mobile-screenshot.png') }}" style="width:200px;height:300px;margin-left:200px;">
                    <img src="{{ asset('assets/images/app-store.png') }}">
                    <img src="{{ asset('assets/images/play-store.png') }}">
                </div>
            </div>
        </div>
    </div>

@endsection
