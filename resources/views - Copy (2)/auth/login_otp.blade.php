<!-- @extends('layouts.app') -->

@section('content')
<br>
	     <div class="product-section section mt-30" style="margin-top: 10rem;" >
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-5">
					<div class="sign-form">
						<div class="sign-inner">

							<div class="form-dt">
								<div class="form-inpts checout-address-step">

									<div class="login">
										<h3>Verify mobile number</h3>
										<p>A text with a One Time Password (OTP) has been sent to your mobile number: {{$user->mobile}} Change</p>
                    <!-- Login Form -->
										<form method="POST" class="my-4" action="{{ route('login.otp',$user->id) }}">
												 @csrf
                      						<div class="form-group">
												<input id="otp" type="text" class="form-control lgn_input @error('email') is-invalid @enderror" name="otp" value="" placeholder="Enter OTP" required  autofocus>
											 <i class="uil uil-otp-android-alt lgn_icon"></i>
											 @error('otp')
											 	 <span class="invalid-feedback" role="alert">
											 			 <strong>{{ $message }}</strong>
											 	 </span>
											 @enderror
					                      </div>
					                      
					                      <button type="submit" class="btn mt-10 btn-primary">Create your account</button>
										</form>
											<div class="password-forgor">
												@if (isset($url))
														<a class="btn btn-link" href="{{ $url }}">
																{{ __('Re-send?') }}
														</a>
												@endif
											</div>
											

                </div>

								</div>

							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
@endsection
