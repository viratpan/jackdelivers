@extends('layouts.app')
@section('script')
@endsection
@section('content')
<div class="container" style="">
    <div class="main-body">
    
          <div class="row gutters-sm">
            {{-- User Nav --}}
            @include('user.nav')
            {{-- User Nav End --}}
            <div class="col-md-8 tab-content px-3 px-sm-0" id="nav-tabContent">
            	
					
				

				<div class="card mb-3">
<div class="card-body" style="margin:1rem;">
@if($data)
    <div class="row text-secondary" style="padding:10px;margin-bottom: 4px;border-bottom: 1px solid gray;">
        <h3 class="col-10">Order <text style="color: black;">#{{$data[0]->order_id}}</text></h3>
        <a class="col-2" href="{{ route('user.invoice',$data[0]->order_id)}}">Invoice</a>
        <div class="col-sm-12" >
          <b>From: {{$data[0]->vendor_company_name}}   ({{$data[0]->masters_name}})</b><br>
          <b>To: {{ $data[0]->address}}, {{ $data[0]->city}}, {{ $data[0]->state}}</b><br>               
        @if($data[0]->item_status==0)
        @endif
        
        <small>Delivered on {{ date('D, d M y, H:i:s A', strtotime($data[0]->created_at)) }}  by {{ $data[0]->rider_name}}</small><br><br>  
        
        <div class="progress-track">
            <ul id="progressbar">
                <li class="step0 active " id="step1">Ordered</li>
                <li class="step0 active text-center" id="step2">Shipped</li>
                <li class="step0 active text-right" id="step3">On the way</li>
                <li class="step0 text-right" id="step4">Delivered</li>
            </ul>
        </div>
        <small>{{$data[0]->items}} Item</small>           
        </div> 
      
    </div>
    <div class="row text-secondary" style="padding:0px 10px; border-bottom: 1px dotted gray;">
	@foreach($data as $key => $aval)                            
       	<div class="col-12 row">
            <div class="col-sm-10" >
              <b>{{$aval->product_name}} </b>
              <small>&nbsp;<i>( {{ $aval->product_qty.' '.$aval->attributes_name}} )</i></small>
              &nbsp;<i>X {{ $aval->item_qty}}</i>                     
            </div>                        
            {{-- <div class="col-sm-2" >
              <b>Rs.{{$aval->item_cost}}</b>                      
            </div>
            <div class="col-sm-2" >
              <b>{{$aval->item_discount}}%</b>                      
            </div> --}}
            <div class="col-sm-2" style="text-align:right">
              <small>Rs.{{$aval->item_amount}}</small>                    
            </div>
        </div>	
    @endforeach
     </div>
    <div class="row text-secondary" style="padding:0px 10px; border-bottom: 1px dotted gray;">
        <div class="col-12 row">
            <div class="col-sm-10" >
              <small>Item Total</small>                   
            </div>                        
            
            <div class="col-sm-2" style="text-align:right">
              <small>Rs.{{$aval->price}}</small>                    
            </div>
        </div>
        <div class="col-12 row" style="">
            <div class="col-sm-10" >
              <small>Delivery partner fee</small>                   
            </div>                        
            
            <div class="col-sm-2" style="text-align:right">
              <small>
                @if($aval->shipping==0.00)
                    Free
                @else
                    Rs.{{$aval->shipping}}</small>  
                @endif                 
            </div>
        </div> 
        @if($aval->discount!=0.00)
            <div class="col-12 row" style="">
            <div class="col-sm-10" >
              <small>Dicount</small>                   
            </div>                        
            
            <div class="col-sm-2" style="text-align:right">
              <small>Rs.{{$aval->discount}}</small>
            </div>
        </div> 
        @endif
        @if($aval->tax!=0.00)
            <div class="col-12 row" style="">
            <div class="col-sm-10" >
              <small>GST</small>                   
            </div>                        
            
            <div class="col-sm-2" style="text-align:right">
              <small>Rs.{{$aval->tax}}</small>
            </div>
        </div> 
        @endif
        <div class="col-12 row" style="border-top: 1px solid black;">
            <div class="col-sm-8" >
              <small>Paid Via @if($aval->payment_type==1)Cash @else Online @endif</small>                   
            </div>                        
            <div class="col-sm-2" style="text-align:right">
              <b>  Bill Total</b> 
            </div>
            <div class="col-sm-2" style="text-align:right">
              <b>  Rs.{{$aval->total_amount}}</b> 
            </div>
        </div>
    </div> 
		                    	
@else
	<div align="center" style="text-align: center;margin-top: 5rem;margin-bottom: 5rem;">
		<b>No Orders<br>
	          You have not order any product yet!</b>
	</div>
@endif
                			
                		</div>
                	</div>
				
				
					
        

      {{-- ///////////////////////////// --}}        
             
            </div>
          </div>
        </div>
    </div>
@endsection

@section('style')
<style type="text/css">
.nav-tabs .nav-item{
	text-align: left;
}
.nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active {
    color: #495057;
    background-color: #f8e7ddcc;
    border-color: #dee2e6 #dee2e6 #dee2e6;
    /*border: 1px solid gray;*/
    font-size: 
}
.nav-tabs .nav-link {
   
    border: 1px solid transparent;
    border-radius: .25rem; 
    /* border-top-right-radius: .25rem; */
}
.main-body {
	
    padding: 15px;
}
.card {
    box-shadow: 0 1px 3px 0 rgba(0,0,0,.1), 0 1px 2px 0 rgba(0,0,0,.06);
}

.card {
    position: relative;
    display: flex;
    flex-direction: column;
    min-width: 0;
    word-wrap: break-word;
    background-color: #fff;
    background-clip: border-box;
    border: 0 solid rgba(0,0,0,.125);
    border-radius: .25rem;
}

.card-body {
    flex: 1 1 auto;
    min-height: 1px;
    padding: 1rem;
}

.gutters-sm {
    margin-right: -8px;
    margin-left: -8px;
}

.gutters-sm>.col, .gutters-sm>[class*=col-] {
    padding-right: 8px;
    padding-left: 8px;
}
.mb-3, .my-3 {
    margin-bottom: 1rem!important;
}

.bg-gray-300 {
    background-color: #e2e8f0;
}
.h-100 {
    height: 100%!important;
}
.shadow-none {
    box-shadow: none!important;
}
/*process bar*/


#progressbar {
    margin-bottom: 3vh;
    overflow: hidden;
    color: rgb(252, 103, 49);
    padding-left: 0px;
    margin-top: 3vh
}

#progressbar li {
    list-style-type: none;
    font-size: x-small;
    width: 25%;
    float: left;
    position: relative;
    font-weight: 400;
    color: rgb(160, 159, 159)
}

#progressbar #step1:before {
    content: "";
    color: rgb(252, 103, 49);
    width: 5px;
    height: 5px;
    margin-left: 0px !important
}

#progressbar #step2:before {
    content: "";
    color: #fff;
    width: 5px;
    height: 5px;
    margin-left: 32%
}

#progressbar #step3:before {
    content: "";
    color: #fff;
    width: 5px;
    height: 5px;
    margin-right: 32%
}

#progressbar #step4:before {
    content: "";
    color: #fff;
    width: 5px;
    height: 5px;
    margin-right: 0px !important
}

#progressbar li:before {
    line-height: 29px;
    display: block;
    font-size: 12px;
    background: #ddd;
    border-radius: 50%;
    margin: auto;
    z-index: -1;
    margin-bottom: 1vh
}

#progressbar li:after {
    content: '';
    height: 2px;
    background: #ddd;
    position: absolute;
    left: 0%;
    right: 0%;
    margin-bottom: 2vh;
    top: 0.1rem;/*1px;*/
    z-index: 1
}

.progress-track {
    padding: 0 8%
}

#progressbar li:nth-child(2):after {
    margin-right: auto
}

#progressbar li:nth-child(1):after {
    margin: auto
}

#progressbar li:nth-child(3):after {
    float: left;
    width: 68%
}

#progressbar li:nth-child(4):after {
    margin-left: auto;
    width: 132%
}

#progressbar li.active {
    color: black
}

#progressbar li.active:before,
#progressbar li.active:after {
    background: rgb(252, 103, 49)
}
</style>
@endsection