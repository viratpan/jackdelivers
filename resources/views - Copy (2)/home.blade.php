@extends('layouts.app')

@section('content')
<!-- main page content start-->
    <main class="main-page-content">
        <!--vendor section start-->
        <section class="vendor-section pt-80">
            <div class="container-fluid">
                <div class="row">
                @foreach($master as $key => $value)
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 mb-3">
                        <a href="{{ route('master',$value->slug)}}" class="@if($value->active!=1) coming-soon  @endif">
                            <div class="card">
                                <img src="{{ asset($value->image) }}" class="card-img" alt="{{ $value->name}}">
                            </div>
                             @if($value->active!=1)
                                <div class="btn btn-soon">Coming Soon</div>
                            @endif
                        </a>
                    </div>                    
                @endforeach

                    

                </div>
            </div>
        </section>
        <!--vendor section start-->
        <!--about section start-->
        <section class="about-section pt-80 pb-100">
            <div class="container-fluid">
                <div class="heading">
                    <h4>About Jack Delivers</h4>
                </div>
                <div class="row">
                    <div class="col md-12">
                        <div class="accordion" id="accordionExample">
                            <div class="card">
                                <div class="card-header" id="headingOne">
                                    <h2 class="mb-0">
                                        <button type="button" class="btn btn-link" data-toggle="collapse" data-target="#collapseOne"><i class="fa fa-plus"></i> Heading-1</button>
                                    </h2>
                                </div>
                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut minima ratione accusamus iure vitae, odit aperiam eveniet, voluptates aspernatur sed libero fugit quod perspiciatis quas quia harum qui molestias. Maxime.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingTwo">
                                    <h2 class="mb-0">
                                        <button type="button" class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo"><i class="fa fa-plus"></i> Heading-2</button>
                                    </h2>
                                </div>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat, nisi. Nulla autem recusandae non vero est. Amet iure odio reiciendis ad, totam, neque quo tempore. Doloremque delectus iusto non a.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingThree">
                                    <h2 class="mb-0">
                                        <button type="button" class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree"><i class="fa fa-plus"></i> Heading-3</button>
                                    </h2>
                                </div>
                                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id doloremque nihil voluptates quia repudiandae hic dolorum, reprehenderit magni consequatur. Corporis omnis voluptas nobis at dolorum enim. Itaque tenetur aperiam veritatis.</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
        <!--about section end-->
    </main>
     
       
@endsection
