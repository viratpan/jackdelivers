<div class="navbar-area">
        <div class="mobile-nav">
            <a href="{{route('home')}}" class="logo">
                <img src="{{ asset(config('app.logo')) }}" alt="Logo">
            </a>
        </div>

        <div class="main-nav">
            <div class="container-fluid">
                <nav class="navbar navbar-expand-md navbar-light ">
                    <a class="navbar-brand" href="{{route('home')}}">
                        <img src="{{ asset(config('app.logo')) }}" alt="Logo">
                    </a>
                    <div class="collapse navbar-collapse mean-menu" id="navbarSupportedContent">
                        <ul class="navbar-nav m-auto">
                            {{-- <li class="nav-item">
                                <a href="{{route('home')}}" class="nav-link active"><i class="fas fa-home"></i> Home
                                </a>
                            </li> --}}

							@php 
							     $currentURl= Route::getFacadeRoot()->current()->uri();    
							@endphp
							@if(!in_array($currentURl, config('app.showMaster')))
							    <li class="nav-item">
	                                <a href="{{route('home')}}" class="nav-link active"><i class="fas fa-home"></i> Home
	                                </a>
	                            </li>
							        
							    @php  $master=DB::table('masters')->orderBy('order', 'ASC')->get(); @endphp
							       @foreach($master as $mkey => $mvalue)
							       	<li class="nav-item">
		                                <a href="{{ route('master',$mvalue->slug) }}" class="nav-link @if($mvalue->active!=1) soon  @endif">
		                                    <figure class="figure">
		                                        <img src="{{ asset($mvalue->image) }}" class="figure-img" alt="{{ $mvalue->name }}">
		                                        @if($mvalue->active!=1)
		                                        	<figcaption class="figure-caption">Coming Soon</figcaption>
		                                        @endif
		                                    </figure>
		                                </a>
		                            </li>
							        
							       @endforeach
							    
							@endif 
                            
                        </ul>

                        <div class="others-options d-flex align-items-center">
                            <div class="option-item">
                                <div class="location-btn">
                                    <a href="{{ route('location') }}" class="location-btn-icon">
                                        <i class="fas fa-map-marker-alt"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="option-item">
                                <div class="add-cart-btn">
                                    <a href="{{route('cart')}}" class="cart-btn-icon">
                                        <i class="fas fa-shopping-cart"></i>
                                        <span>0</span>
                                    </a>
                                </div>
                            </div>
                        @guest
                            <div class="option-item">
                                <div class="login-btn">
                                    <a href="{{ route('login')}}" class="login-btn-icon">
                                        <span class="ls-icon"><i class="fas fa-sign-in-alt"></i></span> <span class="ls-text">Log In/Sign Up</span>
                                    </a>
                                </div>
                            </div>
                        @else
                        <ul class="navbar-nav m-auto" >
                            <li class="nav-item dropdown pr-2" style="">
                             <span class="dropdown-toggle" id="dropdownMenu12"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button" style="font-weight: 900;font-size: 23px;">
                             {{-- <i class="fa fa-user"></i> --}} 
                             {{ ucfirst(Auth::user()->name) }}<span class="caret"></span> </span>
                             <ul class="dropdown-menu" id="dropdownMenu12ul"  aria-labelledby="dropdownMenu125213" style="text-align: center;padding:1px;">
                                <!-- <li><a href="#">My Account</a></li> -->
                                <li style="border-bottom: 1px solid rgba(0,0,0,.15);">
                                  <a href="{{route ('user.profile') }}" style="text-decoration:none;color: black;">My Profile</a>
                                </li>
                                <li style="border-bottom: 1px solid rgba(0,0,0,.15);">
                                  <a href="{{route ('user.orders') }}" style="text-decoration:none;color: black;">My Orders</a>
                                </li>

                               {{--  <li style="border-bottom: 1px solid rgba(0,0,0,.15);">
                                  <a href="{{route ('user.address') }}" style="text-decoration:none;color: black;">My Adddress</a>
                                </li> --}}

                                
                              
                                <li style="border-bottom: 1px solid rgba(0,0,0,.15);">
                                   <a href="{{ route('logout') }}" style="text-decoration:none;color: black;"
                                      onclick="event.preventDefault();
                                      document.getElementById('logout-form').submit();"><i class="uil uil-lock-alt icon__1"></i>
                                   {{ __('Logout') }}
                                   </a>
                                   <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                      @csrf
                                   </form>
                                </li>
                             </ul>
                          </li>
                        </ul>
                        @endif
                        </div>
                    </div>
                </nav>
            </div>
        </div>
        <div class="side-nav-responsive">
            <div class="container-fluid">
                <div class="dot-menu">
                    <div class="circle-inner">
                        <div class="option-item">
                            <div class="location-btn">
                                <a href="{{ route('location') }}" class="location-btn-icon">
                                    <i class="fas fa-map-marker-alt"></i>
                                </a>
                            </div>
                        </div>
                        <div class="option-item">
                            <div class="add-cart-btn">
                                <a href="{{ route('cart') }}" class="cart-btn-icon">
                                    <i class="fas fa-shopping-cart"></i>
                                    <span>0</span>
                                </a>
                            </div>
                        </div>
                        <div class="option-item">
                            <div class="login-btn">
                                <a href="#" class="login-btn-icon">
                                    <span class="ls-icon"><i class="fas fa-sign-in-alt"></i></span> <span class="ls-text">Log In/Sign Up</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

