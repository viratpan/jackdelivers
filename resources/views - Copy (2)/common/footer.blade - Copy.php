<div class="footer">
	<div class="container">
	<div class="row">
		<div class="col-md-3">
			<img src="{{ asset('assets/images/logo.png') }}" style="width:150px;">
		</div>
		<div class="col-md-3">
			<h5>Bread Butter Basket</h5>
			<ul>
				<a href="#"><li>About</a></li>
				<a href="#"><li>Contact</a></li>
				<a href="#"><li>Terms & Conditions</a></li>
				<a href="#"><li>Privacy Policy</a></li>
			</ul>

		</div>
		<div class="col-md-3">
			<h5>Get in Touch</h5>
			<ul>
				<a href="#"><li>Email</a></li>
				<a href="#"><li>Twitter</a></li>
				<a href="#"><li>Facebook</a></li>
				<a href="#"><li>Instagram</a></li>
				<a href="#"><li>Linkedin</a></li>
			</ul>

		</div>
		<div class="col-md-3">
			<img src="{{ asset('assets/images/footer-icon.png') }}" style="width:260px; height:140px;">
		</div>
		

		</div>
	</div>
</div>