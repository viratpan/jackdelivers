<!--footer area start-->
    <footer class="footer-area pt-90 pb-70">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-3 col-md-6 mb-4">
                    <div class="footer-widget">
                        <h3>Quich Links</h3>
                        <ul class="footer-list">
                            <li><a href="{{ route('home') }}">Home</a></li>
                            <li><a href="{{ route('about') }}">About Us</a></li>
                            <li><a href="{{ route('home') }}">Team</a></li>
                            <li><a href="{{ route('home') }}">Careers</a></li>
                            <li><a href="{{ route('home') }}">Blog</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 mb-4">
                    <div class="footer-widget">
                        <h3>Contact</h3>
                        <ul class="footer-list">
                            <li><a href="#">Help & Support</a></li>
                            <li><a href="#">Partner with us</a></li>
                            <li><a href="#">Ride with us</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 mb-4">
                    <div class="footer-widget">
                        <h3>Legal</h3>
                        <ul class="footer-list">
                            <li><a href="#">Terms & Conditions</a></li>
                            <li><a href="#">Refund & Cancellation</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                            <li><a href="#">Cookie Policy</a></li>
                            <li><a href="#">Offer Terms</a></li>
                            <li><a href="#">Phishing & Fraud</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 mb-4">
                    <div class="footer-widget">
                        <ul class="footer-list">
                            <li class="appStoreicon"><a href="#"><img src="{{ asset('assets/images/download-icon/icon-AppStore_lg30tv.png')}}" alt=""></a></li>
                            <li class="googlePlayicon"><a href="#"><img src="{{ asset('assets/images/download-icon/icon-GooglePlay_1_zixjxl.png')}}" alt=""></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--footer area end-->
    <!--start copy right area-->
    <div class="copy-right-area">
        <div class="container-fluid">
            <div class="copy-right-text text-center">
                <p>&copy; 2021 Jack Delivers</p>
            </div>
        </div>
    </div>