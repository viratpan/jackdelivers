<!doctype html>
<html class="no-js" lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>{{ config('app.name') }} Vendor  </title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
        ============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset(config('app.icon')) }}">
    <!-- Google Fonts
        ============================================ -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,400i,600,700,800,900" rel="stylesheet" />
    <!-- Scripts -->
   
    <link href="{{  asset('css/themes/lite-purple.min.css') }}" rel="stylesheet" />
    <style type="text/css">
        @media (max-width: 767px) {
          .auth-layout-wrap .auth-content {
            max-width: 660px!important;/*660px;*/
            margin: auto; }
           
        }

        @media (min-width: 1024px) {
          .auth-layout-wrap .auth-content {
            min-width: 660px!important; /*660px*/
            }
           
         }
         

    </style>
</head>
<div class="auth-layout-wrap" style="background-image: url(images/photo-wide-4.jpg)">
    <div class="auth-content">
        <div class="card o-hidden">
            <div class="row">
                <div class="col-md-6 text-center" style="background-size: cover;background-image: url(images/photo-long-3.jpg)">
                    <div class="pl-3 auth-right">
                        <div class="auth-logo text-center mt-4">
                            <img src="{{ asset(config('app.logo')) }}" alt="" style="width: 100%!important;">
                        </div>
                       
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="p-4">
                        <h1 class="mb-3 text-18 text-center">User Email Confirmation</h1>
                        <p>Please verify your email . <a href="{{ $url }}">Resend</a></p>

                        <div class="mt-3 text-center" >
                            
                            
                                <a class="btn btn-primary btn-block btn-rounded mt-3" style="" href="{{ route('login') }}">
                                    Sign In
                                </a>

                                <a class="btn btn-success btn-block btn-rounded mt-3" style="" href="{{ route('home') }}">
                                    Go To Home
                                </a>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>