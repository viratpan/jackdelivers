<!DOCTYPE html>
<html>
  <head>
    <title>BreadButterBasket 'My Account' Password Reset Request</title>
  </head>
  <body>
    <h2>Welcome to the BreadButterBasket.com </h2>
    Dear Vendor,<br>
    <br/>
    Your registered email-id is {{ $vendor['email'] }} , Please click on the below link to reset your email account password. 
    <br/>
    <a href="{{url('reset-password-vendor', $vendor['token'] )}}">Reset Password</a>
  </body>
</html>