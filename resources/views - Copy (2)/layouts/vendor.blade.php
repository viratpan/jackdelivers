<!doctype html>
<html class="no-js" lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>{{ config('app.name') }} Vendor Dashboard </title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
		============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset(config('app.icon')) }}">
    <!-- Google Fonts
		============================================ -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,400i,600,700,800,900" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxst.icons8.com/vue-static/landings/line-awesome/line-awesome/1.3.0/css/line-awesome.min.css">
    <!-- Scripts -->
 {{--    <script src="{{ asset('js/app.js') }}"></script>
     --}}
    <link href="{{  asset('css/themes/lite-purple.min.css') }}" rel="stylesheet" />
    <link href="{{  asset('css/plugins/perfect-scrollbar.min.css') }}" rel="stylesheet" />
    <link href="{{  asset('css/plugins/select2.min.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="{{  asset('css/plugins/toastr.css') }}" />
    <link rel="stylesheet" href="{{  asset('css/plugins/datatables.min.css') }}" />
    <link rel="stylesheet" href="{{  asset('css/admin.css') }}">
    
    <style type="text/css">@yield('style')</style>
    

</head>

<body class="text-left">
     <div id="preloaders" class="preloader"></div>
    <div class="app-admin-wrap layout-sidebar-large">
        <div class="main-header">
            <div class="logo">
                <img src="{{ asset(config('app.icon')) }}" alt="">
            </div>
            <div class="menu-toggle">
                <div></div>
                <div></div>
                <div></div>
            </div>
            <div class="d-flex align-items-center">

            </div>
            <div style="margin: auto"></div>
            <div class="header-part-right">
                <!-- Full screen toggle -->
                <i class="i-Full-Screen header-icon d-none d-sm-inline-block" data-fullscreen></i>
                <!-- Grid menu Dropdown -->
                <div class="dropdown">
                    <i class="i-Safe-Box text-muted header-icon" role="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <div class="menu-icon-grid">

                            <a href=""> States</a>



                        </div>
                    </div>
                </div>
                <!-- Notificaiton -->
                <div class="dropdown">
                    <div class="badge-top-container" role="button" id="dropdownNotification" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="badge badge-primary">3</span>
                        <i class="i-Bell text-muted header-icon"></i>
                    </div>
                    <!-- Notification dropdown -->
                    <div class="dropdown-menu dropdown-menu-right notification-dropdown rtl-ps-none" aria-labelledby="dropdownNotification" data-perfect-scrollbar data-suppress-scroll-x="true">
                        <div class="dropdown-item d-flex">
                            <div class="notification-icon">
                                <i class="i-Speach-Bubble-6 text-primary mr-1"></i>
                            </div>
                            <div class="notification-details flex-grow-1">
                                <p class="m-0 d-flex align-items-center">
                                    <span>New message</span>
                                    <span class="badge badge-pill badge-primary ml-1 mr-1">new</span>
                                    <span class="flex-grow-1"></span>
                                    <span class="text-small text-muted ml-auto">10 sec ago</span>
                                </p>
                                <p class="text-small text-muted m-0">James: Hey! are you busy?</p>
                            </div>
                        </div>
                        <div class="dropdown-item d-flex">
                            <div class="notification-icon">
                                <i class="i-Receipt-3 text-success mr-1"></i>
                            </div>
                            <div class="notification-details flex-grow-1">
                                <p class="m-0 d-flex align-items-center">
                                    <span>New order received</span>
                                    <span class="badge badge-pill badge-success ml-1 mr-1">new</span>
                                    <span class="flex-grow-1"></span>
                                    <span class="text-small text-muted ml-auto">2 hours ago</span>
                                </p>
                                <p class="text-small text-muted m-0">1 Headphone, 3 iPhone x</p>
                            </div>
                        </div>
                        <div class="dropdown-item d-flex">
                            <div class="notification-icon">
                                <i class="i-Empty-Box text-danger mr-1"></i>
                            </div>
                            <div class="notification-details flex-grow-1">
                                <p class="m-0 d-flex align-items-center">
                                    <span>Product out of stock</span>
                                    <span class="badge badge-pill badge-danger ml-1 mr-1">3</span>
                                    <span class="flex-grow-1"></span>
                                    <span class="text-small text-muted ml-auto">10 hours ago</span>
                                </p>
                                <p class="text-small text-muted m-0">Headphone E67, R98, XL90, Q77</p>
                            </div>
                        </div>
                        <div class="dropdown-item d-flex">
                            <div class="notification-icon">
                                <i class="i-Data-Power text-success mr-1"></i>
                            </div>
                            <div class="notification-details flex-grow-1">
                                <p class="m-0 d-flex align-items-center">
                                    <span>Server Up!</span>
                                    <span class="badge badge-pill badge-success ml-1 mr-1">3</span>
                                    <span class="flex-grow-1"></span>
                                    <span class="text-small text-muted ml-auto">14 hours ago</span>
                                </p>
                                <p class="text-small text-muted m-0">Server rebooted successfully</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Notificaiton End -->
                <!-- User avatar dropdown -->
                <div class="dropdown">
                    <div class="user col align-self-end">
                        <img src="{{ asset('images/img-3.jpg') }}" id="userDropdown" alt="{{ Auth::guard('vendor')->user()->name }} " data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                        @if(Auth::guard('vendor')->check())
                            <div class="dropdown-header">
                                <i class="i-Lock-User mr-1"></i>  {{ Auth::guard('vendor')->user()->name }} 
                            </div>
                            <a href="{{route('vendor.profile')}}" class="dropdown-item">Profile</a>
                            <a class="dropdown-item" href="{{ route('vendor.change-password')}}">Change Password</a>
                            <a class="dropdown-item" href="#" onclick="event.preventDefault();document.querySelector('#admin-logout-form').submit();" style="border-top: 1px solid rebeccapurple;">
                                        Sign out
                                    </a>
                                    <form id="admin-logout-form" action="{{ route('vendor.logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                        @endif

                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="side-content-wrap">
            <div class="sidebar-left open rtl-ps-none" data-perfect-scrollbar="" data-suppress-scroll-x="true">
                <ul class="navigation-left">
                    @if(Auth::guard('vendor')->check())

                        <li class="nav-item {!! (Route::is('vendor.dashboard') ? 'active' : '') !!}">
                            <a class="nav-item-hold" href="{{route('vendor.dashboard')}}"><span class="nav-text">Dashboard</span></a>
                            <div class="triangle"></div>
                        </li>
                        <li class="nav-item {!! (Route::is('vendor.add-item') ? 'active' : '') !!}">

                            <a class="nav-item-hold" href="{{route('vendor.add-item')}}"><span class="nav-text">Add Item</span></a>
                            <div class="triangle"></div>
                        </li>
                       
        {{-- Reporting --}}
                        <li class="nav-item {!! (Route::is('vendor.item-list') ? 'active' : '') !!}">

                            <a class="nav-item-hold" href="{{route('vendor.item-list')}}"><span class="nav-text">Item list</span></a>
                            <div class="triangle"></div>
                        </li>
                        <li class="nav-item {!! (Route::is('vendor.item-discount') ? 'active' : '') !!}">

                            <a class="nav-item-hold" href="{{route('vendor.item-discount')}}"><span class="nav-text">Discount</span></a>
                            <div class="triangle"></div>
                        </li>

                         <li class="nav-item {!! (Route::is('vendor.order-list') ? 'active' : '') !!}">
                            <a class="nav-item-hold" href="{{route('vendor.order-list')}}"><span class="nav-text">Order list</span></a>
                            <div class="triangle"></div>
                        </li>


                     @endif






                   {{--  <li class="nav-item" data-item="dashboard"><a class="nav-item-hold" href="#"><i class="nav-icon i-Bar-Chart"></i><span class="nav-text">Dashboard</span></a>
                        <div class="triangle"></div>
                    </li> --}}

                </ul>
            </div>
            <div class="sidebar-left-secondary rtl-ps-none" data-perfect-scrollbar="" data-suppress-scroll-x="true">
                <!-- Submenu Dashboards-->
               {{--  <ul class="childNav" data-parent="dashboard">
                    <li class="nav-item"><a href="dashboard1.html"><i class="nav-icon i-Clock-3"></i><span class="item-name">Version 1</span></a></li>
                    <li class="nav-item"><a href="dashboard2.html"><i class="nav-icon i-Clock-4"></i><span class="item-name">Version 2</span></a></li>
                    <li class="nav-item"><a href="dashboard3.html"><i class="nav-icon i-Over-Time"></i><span class="item-name">Version 3</span></a></li>
                    <li class="nav-item"><a href="dashboard4.html"><i class="nav-icon i-Clock"></i><span class="item-name">Version 4</span></a></li>
                </ul> --}}

            </div>
            <div class="sidebar-overlay"></div>
        </div>
        <!-- =============== Left side End ================-->
        <div class="main-content-wrap sidenav-open d-flex flex-column">
            <!-- ============ Body content start ============= -->
            <div class="main-content">
                @yield('content')
              
            </div>
             @include('admin.footer')
        </div>
    </div>


    <script src="{{ asset('js/plugins/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('js/plugins/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('js/plugins/perfect-scrollbar.min.js') }}"></script>
    <script src="{{ asset('js/scripts/script.min.js') }}"></script>
    <script src="{{ asset('js/scripts/sidebar.large.script.min.js') }}"></script>
    <script src="{{ asset('js/plugins/echarts.min.js') }}"></script>
    <script src="{{ asset('js/scripts/echart.options.min.js') }}"></script>
    <script src="{{ asset('js/scripts/dashboard.v1.script.min.js') }}"></script>
    {{-- toastr --}}
    <script src="{{ asset('js/plugins/toastr.min.js') }}"></script>
    <script src="{{ asset('js/scripts/toastr.script.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables.min.js') }}"></script>

{{-- <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDI40Wzmn9SR_y_I1stpj7AV8Aq4Tx4vDU&callback=initMap"
  type="text/javascript"></script> --}}
    @yield('scripts')

<script type="text/javascript">
    $(document).ready(function () {
        $('.preloader').hide();
        $('form').submit(function() {
            $('.preloader').show();
        });
        $('a').click(function() {
           // $('.preloader').show();
        });
            $.ajaxSetup({
               headers: {
                   'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                toastr.error("{{ $error }}", { closeButton: !0 });
            @endforeach
        @endif
        @if (session('success'))
            toastr.success("{{ session('success') }}", { closeButton: !0 });
        @endif
        @if (session('error'))
            toastr.error("{{ session('error') }}", { closeButton: !0 });
        @endif
        @if (session('warning'))
            toastr.warning("{{ session('warning') }}", { closeButton: !0 });
        @endif

         $('#data_table').DataTable({
            dom: "<'row'<'col-sm-12 col-md-4 w-100 datasearch'f><'col-sm-12 col-md-4 data-show-hide'C><'col-sm-12 col-md-3 data-buttons'B>>" +
                "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
                    "lengthMenu": [[-1,50, 100, 150], ["All",50, 100, 150]],
                    "scrollY": 200,
                   // "scrollX": true,
                    buttons: [
                    'pageLength', 'excel', 'pdf','print'
                    ],
                    language: { search: '', searchPlaceholder: "Search..." }
        }); // feature enable/disable
    });
</script>

{{-- push notification --}}
<link href="https://res.cloudinary.com/dxfq3iotg/raw/upload/v1557232134/toasty.css" rel="stylesheet" />
<script src="https://res.cloudinary.com/dxfq3iotg/raw/upload/v1557232134/toasty.js"></script>
<script>
          /*Echo.channel('events')
              .listen('RealTimeMessage', (e) => console.log('RealTimeMessage: ' + e.message));*/
          /*Echo.private('events')
              .listen('RealTimeMessage', (e) => console.log('Private RealTimeMessage: ' + e.message.body));*/
    var options = {
        autoClose: true,
        progressBar: true,
        enableSounds: true,
         duration: 4000,// .... Default value is set to 4000 (4 seconds). 

        transition: "slideUpDownFade",
        sounds: {
          
          info: "https://res.cloudinary.com/dxfq3iotg/video/upload/v1557233294/info.mp3",
          // path to sound for successfull message:
          success: "https://res.cloudinary.com/dxfq3iotg/video/upload/v1557233524/success.mp3",
          // path to sound for warn message:
          warning: "https://res.cloudinary.com/dxfq3iotg/video/upload/v1557233563/warning.mp3",
          // path to sound for error message:
          error: "https://res.cloudinary.com/dxfq3iotg/video/upload/v1557233574/error.mp3",
        },
      };

    var toast = new Toasty(options);
    toast.configure(options);
@if(Auth::guard('vendor')->check())
 /* Echo.private('App.Vendor.{{Auth::guard('vendor')->user()->id }}')
    .notification((notification) => {
       console.log(notification.message.user_type +' users only');
       //if(notification.message.user_type=='vendor'){
          toast.success(notification.message.body);
       //}
        
});*/

@endif
</script>
</body>



</html>
