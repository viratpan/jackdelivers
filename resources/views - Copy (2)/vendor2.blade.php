@extends('layouts.app')
@section('script')
<script type="text/javascript">
	$(document).ready(function () {

		@if($fillter!=null)				
			$("#fillter").val('{{ $fillter }}');
			 var value = '{{ $fillter }}'.toLowerCase();
		    $(".fillter").filter(function() {
		    	var div=$(this).attr('data-name').toLowerCase(); console.log(div);
		      $(this).toggle(div.indexOf(value) > -1);
		      // $(this).toggle($(this).attr('data-name').toLowerCase().indexOf(value) > -1);
		    });

		@endif

		getCart();

		$("#fillter").on("keyup", function() {
		    var value = $(this).val().toLowerCase();
		    $(".fillter").filter(function() {
		    	var div=$(this).attr('data-name').toLowerCase(); //console.log(div);
		      $(this).toggle(div.indexOf(value) > -1);
		      // $(this).toggle($(this).attr('data-name').toLowerCase().indexOf(value) > -1);
		    });
		});

    });
    function addCart(this_obj){ 
          var product_id=$(this_obj).attr('data-id');
          var product_name=$(this_obj).attr('data-name');
          var product_price_id=$(this_obj).attr('data-Pid');
          var vendor_id=$(this_obj).attr('data-vendor_id');
         // var price=$(this_obj).attr('data-price');
          var qty=$(this_obj).attr('data-qty');
          console.log('product_id: '+ product_id,
                  'product_name: '+ product_name,
                  'product_price_id: '+ product_price_id,
                  'vendor_id: '+ vendor_id,
                  'qty: '+qty);
         /* $.ajax( {
          	 type:"GET",
	          url: "{{route('ajax.addCart')}}",
	         dataType: "json",*/
           $.get('{{ route('ajax.addCart') }}', 
                { 'product_id': product_id,
                  'product_name': product_name,
                  'product_price_id': product_price_id,
                  'vendor_id': vendor_id,
                 // 'price': price,
                  'qty': qty },
              //  dataType: "json", 
                function( res ) {
                  if(res){
                      if(res.status=='success'){
                      	getCart();
                      	//updateCartDiv();
                      	updateCartCount();
                        $(this_obj).attr('data-qty',res.count);
                         $('#cart_count').html(res.cartCount);
                          $('#cartCount').addClass('cartCount');
                          $(this_obj).html('ADDED');
                          $(this_obj).attr('disable',true);
                          $('#cart_btn_'+product_id).hide();
                          $('#cart_gbtn_'+product_id).show();
                         $('#item_btn_'+product_id).html();
                         $('#qty_'+product_id).val(res.count);
                         toastr.success(product_name+" Added to cart.", { closeButton: !0 });
                      }
                      else{
                          toastr.warning(res.msg, { closeButton: !0 });
                      }
                  }else{
                      toastr.error("Refresh Page and Try Again..", { closeButton: !0 });                   
                  }
                  
                  
                }
              );
          
      }
    function getCart() {
    	
        $('#cartDiv').html(`<div align="center" style="text-align:center;">Waiting......</div>`);        
            $.ajax( {
	          url: "{{route('ajax.getCart')}}",
	         dataType: "json",
	         
	          success: function( data ) {
	          	console.log(data);
                  if(data.count==0){
				         var html =`<div align="center" style="text-align:center;">
                              
                              <img src="{{ asset('images/empty-cart.jpg')}}">
                          </div>`;
				        $('#cartDiv').html(html); 
				    }
				    else{
				        // normal response
				        var price=0;
				        var html='<div class="row" style="max-height: 350px;overflow-y: scroll;">';	
				        $.map(data.item, function (item) {
				            var discount_div='';
                    if(item.discount!=0){
                      discount_div=`<span style="text-decoration: line-through;color:gray;">Rs. `+item.cost+`</span>
                      <b style="color:tomato">(`+item.discount+`%)</b>`;
                    }
				          html +=`<div class="item" id="citem_`+item.id+`" style="width: 92% !important;margin-left: 1.5rem;">
						    		<div id="citem_btn_`+item.id+`" class="">
						    			
						    			
	<div id="cart_gbtn_`+item.id+`" class="quantity-button" style="">
        <input type="button" onclick="updateCart(this,`+item.id+`);" value ="-" data-id="`+item.id+`" data-name="`+item.name+`" data-vendor_id="`+item.vendor_id+`" data-Pid="`+item.price_id+`" data-price="`+item.price+`" data-qty="`+item.qty+`" data-action="0" class="inc-dec form-controll">
        <input type="text" id="cqty_`+item.id+`" min="0" readonly="" style="text-align: center;" value="`+item.qty+`" class="form-controll number-btn">
        <input type="button" onclick="updateCart(this,`+item.id+`);" value="+" data-id="`+item.id+`" data-name="`+item.name+`" data-vendor_id="`+item.vendor_id+`" data-Pid="`+item.price_id+`" data-price="`+item.price+`" data-qty="`+item.qty+`" data-action="1" class="form-controll inc-dec">
    </div>
						    		</div>
						    		<span><b>`+item.name+`</b>
                    <i style="padding-left: 1rem;color: gray;">( `+item.attributes+` )</i>
                    <br>
						    			<span>Rs. `+item.price+`</span> 
                      `+discount_div+`
						    			
						    		</span>
						    		
				            	</div>`; 
				            price+= item.price*item.qty;
				            total=price.toFixed(2);

				         
				         $('#qty_'+item.id).val(item.qty);
				         $('#cart_btn_'+item.id).hide();
                         $('#cart_gbtn_'+item.id).show();
                         $('#item_btn_'+item.id).html();
                         


				        });
				        html+="</div>";
				        html+=`<div id="cartprice" align="center" style="margin-top:1rem;">
				        		<h5  style="padding:10px;padding: 1rem;text-align: center;">
                            		<b>Total Amount:&nbsp;&nbsp;&nbsp;&nbsp;Rs.<span id="cartcost">`+total+`</span></b>
                            	</h5><br>
						        <a href="{{ route('checkout')}}" class="btn btn-lg btn-success">Proceed to checkout</a>
						       </div>`;
						$('#cartDiv').html(html);
				    }
                  
                  
                }
              });
    }
    function cartDivManage(argument) {
    	// body...
    }
    function updateCart(this_obj,item) {
    	var product_id=$(this_obj).attr('data-id');
          var product_name=$(this_obj).attr('data-name');
          var product_price_id=$(this_obj).attr('data-Pid');
          var vendor_id=$(this_obj).attr('data-vendor_id');
          var action=$(this_obj).attr('data-action');
          var price=$(this_obj).attr('data-price');
          var qty=$(this_obj).attr('data-qty');
          console.log('product_id: '+ product_id,
                  'product_name: '+ product_name,
                  'product_price_id: '+ product_price_id,
                  'vendor_id: '+ vendor_id,
                  'action: '+ action,
                  'qty: '+qty);
    	 $.get('{{ route('ajax.updateCart') }}', 
               { 'product_id': product_id,
                  'product_name': product_name,
                  'product_price_id': product_price_id,
                  'vendor_id': vendor_id,
                  'action': action,
                  'qty': qty },
               
                function( res ) {
                  if(res){
                      if(res.status=='success'){
                      	  updateCartCount();
                         if(res.cartCount==0){
                         	getCart();
                         }

                          $('#cart_btn_'+product_id).hide();
                          $('#cart_gbtn_'+product_id).show();

                          $('#qty_'+product_id).val(res.count);
                          $(this_obj).parents('#cart_gbtn_'+product_id).find('#cqty_'+product_id).val(res.count);
                          var tprice=$('#cartDiv').find('#cartprice h5 #cartcost').html();
                          if(res.count==0){
                          	//alert('remove');
                          		 $('#cartDiv').find('#citem_'+product_id).remove();
                          		 $('#cart_btn_'+product_id).show();
                          		$('#cart_gbtn_'+product_id).hide();
                          }
                          tprice=parseFloat(tprice) || 0;
                          price=parseFloat(price) || 0;
                          var total=0;
                          if(action==1){
                          	total=(tprice+price).toFixed(2);
                          	$('#cartDiv').find('#cartprice h5 #cartcost').html(total);
                          }else{
                          	total=(tprice-price).toFixed(2);
                          	$('#cartDiv').find('#cartprice h5 #cartcost').html(total);
                          }
                          
                      }
                      else{
                          toastr.warning(res.msg, { closeButton: !0 });
                      }
                  }else{
                      toastr.error("Refresh Page and Try Again..", { closeButton: !0 });                   
                  }
                  
                  
                }
              );
    }
</script>
@endsection
@section('content')

<div class="container" style="padding-top: 2.5rem;">
           
	            <div class="store-detail" style="padding:1rem ;">
	            	<div style="display: inline-flex;">
	            		@if($vendor->image)
		                	<img src="{{ asset($vendor->image) }}" alt="{{ $vendor->name}}" style="width:125px;height:125px;">
		                @else
		                	<img src="{{ asset(config('app.logo')) }}" alt="{{ $vendor->name}}" style="width:125px;height:100px;">
		                @endif
		                <ul style="list-style: none;padding-top: 2rem;">
			                <li><h3>{{ ucfirst($vendor->company_name) }}</h3></li>
			                <li><p>{{ $vendor->address }}</p></li>
			                <li><p><span style="color:#770071;">Open Now-</span>12noon – 12midnight (Today)</p></li>
			            </ul>
	            	</div>
	            	<div class=" container">
					<nav style="border-bottom: 1px solid #8080809e;"> 
						<div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
							
							<a class="nav-item nav-link active" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Menu</a>
              <a class="nav-item nav-link " id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">About</a>
							<a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Reviews</a>
							{{-- <a class="nav-item nav-link" id="nav-about-tab" data-toggle="tab" href="#nav-about" role="tab" aria-controls="nav-about" aria-selected="false">Menu</a> --}}
							{{-- <a class="nav-item nav-link" id="nav-about-tab" data-toggle="tab" href="#nav-about" role="tab" aria-controls="nav-about" aria-selected="false">Menu</a> --}}
						</div>
					</nav>
					<div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
						<div class="tab-pane fade " id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
							{{-- About Section --}}
							Et et consectetur ipsum labore excepteur est proident excepteur ad velit occaecat qui minim occaecat veniam. Fugiat veniam incididunt anim aliqua enim pariatur veniam sunt est aute sit dolor anim. Velit non irure adipisicing aliqua ullamco irure incididunt irure non esse consectetur nostrud minim non minim occaecat. Amet duis do nisi duis veniam non est eiusmod tempor incididunt tempor dolor ipsum in qui sit. Exercitation mollit sit culpa nisi culpa non adipisicing reprehenderit do dolore. Duis reprehenderit occaecat anim ullamco ad duis occaecat ex.
						</div>
						<div class="tab-pane fade show active" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
							{{-- product section --}}
							<div class="row" style="display:inline-flex;">
								
								<div class="col-8" style="border-right:1px solid gray;padding: 1rem;">
									<div class="row" style="max-height: 450px;overflow-y: scroll;">
									<div class=" col-12" style="margin-bottom: 10px;">
										<input type="text" id="fillter" class="form-control" placeholder="Search for an item">
									</div>
                  @php $item_arr=array(); @endphp
								    @foreach($item as $key => $value)
                     @php array_push($item_arr,$value->productsId) @endphp
								    	<div class="item col-12 fillter"  style="width: 96%;margin-left: 1rem;" data-name="{{ ucfirst($value->slug)}}">

		    		<div id="item_btn_{{$value->productsId}}" class="">

			<button id="cart_btn_{{$value->productsId}}" type="button" class="btn btn-outline-success" data-id="{{$value->productsId}}" data-name="{{$value->name}}" data-vendor_id="{{$value->vendor_id}}" data-Pid="{{ $value->product_price_id}}" data-qty="" data-price="{{$value->product_price}}" onclick="addCart(this);" style="">+ADD</button>

			<div id="cart_gbtn_{{$value->productsId}}" class="quantity-button" style="display: none;">
                <input type="button"  onclick="updateCart(this,{{$value->productsId}});" value ="-" data-id="{{$value->productsId}}" data-name="{{$value->name}}" data-vendor_id="{{$value->vendor_id}}" data-Pid="{{ $value->product_price_id}}" data-qty="" data-price="{{$value->product_price}}" data-action="0" class="inc-dec form-controll">
                <input type="text" id="qty_{{$value->productsId}}" readonly="" style="text-align: center;" value="1" class="form-controll number-btn">
                <input type="button" onclick="updateCart(this,{{$value->productsId}});" value ="+" data-id="{{$value->productsId}}" data-name="{{$value->name}}" data-vendor_id="{{$value->vendor_id}}" data-Pid="{{ $value->product_price_id}}" data-qty="" data-price="{{$value->product_price}}" data-action="1" class="form-controll inc-dec">
            </div>
		
		
		    			
		    			
		    		</div>
								    		<span><b class="item_name">{{ ucfirst($value->name)}}</b><br>
								    			<span>Rs. {{ $value->product_price}}</span>
                          @if($value->product_discount!=0)
                            <span style="text-decoration: line-through;color:gray;">Rs. {{$value->product_cost}}</span>
                            <b style="color:tomato">({{$value->product_discount}}%)</b>
                          @endif
								    			<i style="padding-left: 1rem;color: gray;">{{$value->product_qty.' '.$value->attributes_name}}</i>
								    		</span>
								    		
						            	</div>
								        
								    @endforeach    	     
								     @php var_dump($item_arr); @endphp
									</div>
								</div>
								<div class="col-4" id="cartDiv" >
									
								</div>
								
							</div>
							
							
						</div>
						<div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
							Et et consectetur ipsum labore excepteur est proident excepteur ad velit occaecat qui minim occaecat veniam. Fugiat veniam incididunt anim aliqua enim pariatur veniam sunt est aute sit dolor anim. Velit non irure adipisicing aliqua ullamco irure incididunt irure non esse consectetur nostrud minim non minim occaecat. Amet duis do nisi duis veniam non est eiusmod tempor incididunt tempor dolor ipsum in qui sit. Exercitation mollit sit culpa nisi culpa non adipisicing reprehenderit do dolore. Duis reprehenderit occaecat anim ullamco ad duis occaecat ex.
						</div>
						<div class="tab-pane fade" id="nav-about" role="tabpanel" aria-labelledby="nav-about-tab">
							Et et consectetur ipsum labore excepteur est proident excepteur ad velit occaecat qui minim occaecat veniam. Fugiat veniam incididunt anim aliqua enim pariatur veniam sunt est aute sit dolor anim. Velit non irure adipisicing aliqua ullamco irure incididunt irure non esse consectetur nostrud minim non minim occaecat. Amet duis do nisi duis veniam non est eiusmod tempor incididunt tempor dolor ipsum in qui sit. Exercitation mollit sit culpa nisi culpa non adipisicing reprehenderit do dolore. Duis reprehenderit occaecat anim ullamco ad duis occaecat ex.
						</div>
					</div>
				
				</div>
	            
            
            <!-- Nav -->


			</div>
			
			



		
</div>
@endsection
@section('style')
<style type="text/css">
	/* Tabs*/
section {
    padding: 60px 0;
}

section .section-title {
    text-align: center;
    color: #007b5e;
    margin-bottom: 50px;
    text-transform: uppercase;
}
#tabs{
	background: #007b5e;
    color: #eee;
}
#tabs h6.section-title{
    color: #eee;
}

#tabs .nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active {
    color: #f16a3b;
    background-color: transparent;
    border-color: transparent transparent #f3f3f3;
    border-bottom: 4px solid !important;
    font-size: 20px;
    font-weight: bold;
}
#tabs .nav-tabs .nav-link {
    border: 1px solid transparent;
    border-top-left-radius: .25rem;
    border-top-right-radius: .25rem;
    color: #eee;
    font-size: 20px;
}
/**/
.item-cat{
	position: sticky;
	top: 200px;
	width: 20rem;
	height: 100%;
	flex-shrink: 0;
	max-height: calc(-200px + 100vh);
	overflow: hidden auto;
}
/*qty button*/
.quantity-button {
   border: 2px solid #green;
    border-radius: 10px;
    width: 86px;
   /* margin-bottom: 10px;*/
   /* position: absolute;
    right: 1rem;*/
    float: right;
    padding: 2.5px;
    display: inline-flex;
}
.quantity-button .inc-dec {
	/*border: 2px solid #green;*/
	background-color: white;
	font-size:20px;
	font-weight:600;
	border-radius: 10px;
}
.quantity-button .inc-dec:focus {
	/*border:0px solid #fff!important;*/
	background-color: green;
	color:white;
	border-radius: 10px;
}
.quantity-button .number-btn {
	
	width:20px;
	border:none;
}

</style>
@endsection