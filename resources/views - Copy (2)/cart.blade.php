@extends('layouts.app')
@section('script')
<script type="text/javascript">
	$(document).ready(function () {
		getCart();
    });
    function getCart(){
    	$.ajax({
		  url:'{{url('/getcart') }}',
		  type:'POST',
		  
		  success:function(data){
		        if(data){
		        if(data.count){
		          
		      }
		    }
		  }
		});
    }
    function getCart() {
    	
        $('#cartDiv').html(`<div align="center" style="text-align:center;">Waiting......</div>`);         
            $.ajax( {
	          url: "{{route('ajax.getCart')}}",
	         dataType: "json",
	         
	          success: function( data ) {
                  if(!data.item.length){
				        var html =`<div align="center" style="text-align:center;">
                              
                              <img src="{{ asset('images/empty-cart.jpg')}}">
                          </div>`;
				        $('#cartDiv').html(html); 
                /*<h2>Your cart is empty<br>Add items to get started</h2>*/
				    }
				    else{
				        // normal response
				        var price=0;
				       // var html='<div class="row" style="max-height: 350px;overflow-y: scroll;">';	
                 var html='<div class="row" style="">';  
				        $.map(data.item, function (item) {
				            
				          html +=`<div class="item" id="citem_`+item.id+`" >
						    		<div id="item_btn_`+item.id+`" class="">
						    			
						    			
	<div id="cart_gbtn_`+item.id+`" class="quantity-button" style="">
        <input type="button" onclick="updateCart(this,`+item.id+`);" value ="-" data-id="`+item.id+`" data-name="`+item.name+`" data-vendor_id="`+item.vendor_id+`" data-Pid="`+item.price_id+`" data-price="`+item.price+`" data-qty="`+item.qty+`" data-action="0" class="inc-dec form-controll">
        <input type="text" id="qty_`+item.id+`" min="0" readonly="" style="text-align: center;" value="`+item.qty+`" class="form-controll number-btn">
        <input type="button" onclick="updateCart(this,`+item.id+`);" value="+" data-id="`+item.id+`" data-name="`+item.name+`" data-vendor_id="`+item.vendor_id+`" data-Pid="`+item.price_id+`" data-price="`+item.price+`" data-qty="`+item.qty+`" data-action="1" class="form-controll inc-dec">
    </div>
						    		</div>
						    		<span><b>`+item.name+`</b>
                    <i style="padding-left: 1rem;color: gray;">( `+item.attributes+` )</i><br>
						    			<span>Rs. `+item.price+`</span> 
                      <span style="text-decoration: line-through;color:gray;">Rs. `+item.cost+`</span> 
                       <b style="color:tomato">(`+item.discount+`%)</b>
						    			
						    		</span>
						    		
				            	</div>`; 
				            price+= item.price*item.qty;
				            total=price.toFixed(2);
				        });
				        html+="</div>";
				        html+=`<div id="cartprice" align="center" style="margin-top:1rem;">
				        		<h5  style="padding:10px;padding: 1rem;text-align: center;">
                            		<b>Total Amount:&nbsp;&nbsp;&nbsp;&nbsp;Rs.<span id="cartcost">`+total+`</span></b>
                            	</h5><br>
						        <a href="{{ route('checkout')}}" class="btn btn-lg btn-success">Proceed to checkout</a>
                     <a href="{{ route('cart.empty')}}" class="btn btn-lg btn-danger">Empty cart</a>
						       </div>`;
				        $('#cartDiv').html(html);
				    }
                  
                  
                }
              });
    }
    function updateCart(this_obj,item) {
    	var product_id=$(this_obj).attr('data-id');
          var product_name=$(this_obj).attr('data-name');
          var product_price_id=$(this_obj).attr('data-Pid');
          var vendor_id=$(this_obj).attr('data-vendor_id');
          var action=$(this_obj).attr('data-action');
          var price=$(this_obj).attr('data-price');
          var qty=$(this_obj).attr('data-qty');
          console.log('product_id: '+ product_id,
                  'product_name: '+ product_name,
                  'product_price_id: '+ product_price_id,
                  'vendor_id: '+ vendor_id,
                  'action: '+ action,
                  'qty: '+qty);
    	 $.get('{{ route('ajax.updateCart') }}', 
               { 'product_id': product_id,
                  'product_name': product_name,
                  'product_price_id': product_price_id,
                  'vendor_id': vendor_id,
                  'action': action,
                  'qty': qty },
               
                function( res ) {
                  if(res){
                      if(res.status=='success'){
                      	  updateCartCount();
                         if(res.cartCount==0){
                         	getCart();
                         }

                          $('#cart_btn_'+product_id).hide();
                          $('#cart_gbtn_'+product_id).show();

                          $('#qty_'+product_id).val(res.count);
                          $(this_obj).parents('#cart_gbtn_'+product_id).find('#cqty_'+product_id).val(res.count);
                          var tprice=$('#cartDiv').find('#cartprice h5 #cartcost').html();
                          if(res.count==0){
                          	//alert('remove');
                          		 $('#cartDiv').find('#citem_'+product_id).remove();
                          		 $('#cart_btn_'+product_id).show();
                          		$('#cart_gbtn_'+product_id).hide();
                          }
                          tprice=parseFloat(tprice) || 0;
                          price=parseFloat(price) || 0;
                          var total=0;
                          if(action==1){
                          	total=(tprice+price).toFixed(2);
                          	$('#cartDiv').find('#cartprice h5 #cartcost').html(total);
                          }else{
                          	total=(tprice-price).toFixed(2);
                          	$('#cartDiv').find('#cartprice h5 #cartcost').html(total);
                          }
                          
                      }
                      else{
                          toastr.warning(res.msg, { closeButton: !0 });
                      }
                  }else{
                      toastr.error("Refresh Page and Try Again..", { closeButton: !0 });                   
                  }
                  
                  
                }
              );
    }
    function updateCart22(this_obj,item) {
    	var product_id=$(this_obj).attr('data-id');
          var product_name=$(this_obj).attr('data-name');
          var product_price_id=$(this_obj).attr('data-Pid');
          var vendor_id=$(this_obj).attr('data-vendor_id');
          var action=$(this_obj).attr('data-action');
          var price=$(this_obj).attr('data-price');
          var qty=$(this_obj).attr('data-qty');
          console.log('product_id: '+ product_id,
                  'product_name: '+ product_name,
                  'product_price_id: '+ product_price_id,
                  'vendor_id: '+ vendor_id,
                  'action: '+ action,
                  'qty: '+qty);
    	 $.get('{{ route('ajax.updateCart') }}', 
               { 'product_id': product_id,
                  'product_name': product_name,
                  'product_price_id': product_price_id,
                  'vendor_id': vendor_id,
                  'action': action,
                  'qty': qty },
               
                function( res ) {
                  if(res){
                      if(res.status=='success'){
                      	  updateCartCount();
                          $('#cartDiv').find('.item .item_btn_'+product_id).find('#cart_gbtn_'+product_id).find('#qty_'+product_id).val(res.count);
                          $(this_obj).parents('#cart_gbtn_'+product_id).find('#qty_'+product_id).val(res.count);
                          var tprice=$('#cartDiv').find('#cartprice h5 #cartcost').html();
                          if(res.count==0){
                          		 $('#cartDiv').parents('#item').find('#item_btn_'+product_id).remove();
                          }
                          tprice=parseFloat(tprice) || 0;
                          price=parseFloat(price) || 0;
                          var total=0;
                          if(action==1){
                          	total=(tprice+price).toFixed(2);
                          	$('#cartDiv').find('#cartprice h5 #cartcost').html(total);
                          }else{
                          	total=(tprice-price).toFixed(2);
                          	$('#cartDiv').find('#cartprice h5 #cartcost').html(total);
                          }
                          
                      }
                      else{
                          toastr.warning(res.msg, { closeButton: !0 });
                      }
                  }else{
                      toastr.error("Refresh Page and Try Again..", { closeButton: !0 });                   
                  }
                  
                  
                }
              );
    }
</script>
@endsection
@section('content')
	<div class="checkout">
		<div class="container">
            <div class="row">                  
    			<div class="col-12" style="text-align: center;">
    				<h4>My Cart</h4>
    			</div>
                <div class="col-12" id="cartDiv">
                	
                   
				        </div>
                       

                        
</div>
</div>
</div>
@endsection
@section('style')
<style type="text/css">
	 /* Checkout */
.checkout 
{
	/*background-color: #f6dcf6;*/
	width:100%;
	margin: 0px auto;
	padding: 60px 0px;
	height: auto;
	/*position: sticky;*/
   /* top: 72px;
    z-index: 2;*/


}
.checkout .back {
	border-radius: 5px;
	height:auto;
	background-color: white;
	margin:8px;
}
.checkout .back p {
	font-size: 15px;
    font-weight: 600;
    padding-left: 20px;
    color: #770071;

}
.checkout .total-amount {
	background-color: #fff;
}
.checkout .basket-button a {
	color:white;
	text-decoration: none;
}
.checkout .basket-button button {
	padding:5px;
	border-radius: 7px;
	margin-left: 75px;
    margin-top: 0px;
}
.checkout .basket-button button:hover {
	background-color: #450242;
}
.checkout h4 {
	padding:10px;
	margin-top: 60px;
}
.checkout button {
	/*margin-top: 30px;*/
	padding:15px;
	/*width:184px;*/
	color:white;
	border:1px solid transparent;
	background-color: #770071;
	border-radius: 30px;
	margin-bottom: 10px;
	margin-left:10px;
}
.checkout .amount {
	font-size: 15px;
	font-weight: 600;
	display: contents;
}
.checkout .amount p {
	padding-right: 20px;
}
.checkout .quantity-button {
    border: 2px solid #green;
    border-radius: 10px;
    width: 86px;
   /* margin-bottom: 10px;*/
    /*position: relative;
    left: 31%;*/
    padding: 2.5px;
    display: inline-flex;
    float: right;
}
.checkout .quantity-button .inc-dec {
	/*border: 2px solid #green;*/
	background-color: white;
	font-size:20px;
	font-weight:600;
	border-radius: 10px;

}
.checkout .quantity-button .inc-dec:focus {
	/*border:0px solid #fff!important;*/
	background-color: green;
	color:white;
	border-radius: 10px;

}
.checkout .quantity-button .number-btn {
	
	width:20px;
	border:none;
}

</style>
@endsection