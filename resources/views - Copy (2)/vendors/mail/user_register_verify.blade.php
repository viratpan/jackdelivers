<!DOCTYPE html>
<html>
  <head>
    <title>BreadButterBasket 'My Account' One Time Password(OTP)</title>
  </head>
  <body>
    Dear User,<br>
    <p>Use {{ $user['token'] }} as one time password (OTP) to login to your BreadButterBasket account. Do not share this OTP to anyone for security reasons. Valid for 15 minutes.</p>
    <br/><br/>
    <p>Warm Regards,<br/>
    Team BreadButterBasket</p>
    <br/>
    <br/>
    <p>*** This is an automatically generated email, please do not reply to this email ***</p>
    
  </body>
</html>