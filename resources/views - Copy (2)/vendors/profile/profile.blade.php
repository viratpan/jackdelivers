<?php $layouts='layouts.vendor'; ?>

@if(Auth::guard('admin')->check())
@php $layouts='layouts.admin' @endphp
@endif
@if(Auth::guard('vendor')->check())
  @php  $layouts='layouts.vendor' @endphp
@endif

 @extends($layouts)


@section('style')
  <link href="{{  asset('css/plugins/select2.min.css') }}" rel="stylesheet" />
@endsection

@section('scripts')
  <script src="{{  asset('js/plugins/select2.min.js') }}" ></script>
  {{--  <script async defer src="https://maps.googleapis.com/maps/api/js?key={{ config('app.map_key') }}&sensor=true&libraries=places&callback=initMap"
  type="text/javascript"></script>   
  <script type="text/javascript" src='{{ asset('js/other/vendor_map.js')}}'></script> --}}
    $(document).ready(function () {
      //$('.select2').select2();
    });
    var vendor_lat='{{ $vendor->latitude }}';
    var vendor_long='{{ $vendor->longitude }}';
    var vendor_draggable=0;
  </script>
 
  <script type="text/javascript">
    $(document).ready(function () {
      $.ajaxSetup({
           headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
       });


    });
  </script>
@endsection

@section('content')
<style type="text/css">
  .text-red{color: red;font-weight: 700;}
</style>
<div class="card ">
  <div class="card-header">
     @if(Auth::guard('admin')->check())
      <h5 align="left">Vendor Details : {{ $vendor->name }}

        <a class="btn btn-sm btn-info float-right" style="" href="{{ route('admin.edit-vendor',$vendor->id) }}"><b>Edit</b></a>
    </h5>
     @endif
    @if(Auth::guard('vendor')->check())
      <h5 align="left">Profile

        <a class="btn btn-sm btn-info float-right" style="" href="{{ route('vendor.profile.edit') }}"><b>Edit</b></a>
    </h5>
     @endif
  </div>
  <div class="card-body">
   
   
      <div class="form-row">
        
        <div class="form-group col-md-2">
          <label for="inputPassword4">Email <span class="text-red"></span></label>
         <span class="form-control">{{ $vendor->email }}</span>
        </div>

        <div class="form-group col-md-2">
          <label for="inputEmail4">Vendor Name <span class="text-red"></span></label>
          <span class="form-control">{{ $vendor->name }}</span>
        </div>
        <div class="form-group col-md-2">
          <label for="inputEmail4">Type of Organization<span class="text-red"></span></label>
         
           <select value="{{ old('org_type') }}" readonly tabindex="2" id="" name="org_type"  class="form-control " required >
              <option value="">Please Select</option>
              <option value="1" {{ isset($vendor) ? ( $vendor->org_type==1 ? "Selected":"" ) : ""}} >Private Limited</option>
              <option value="2" {{ isset($vendor) ? ( $vendor->org_type==2 ? "Selected":"" ) : ""}} >Partnership firm/LLP</option>
              <option value="3" {{ isset($vendor) ? ( $vendor->org_type==3 ? "Selected":"" ) : ""}} >Sole Proprietorship</option>
              <option value="4" {{ isset($vendor) ? ( $vendor->org_type==4 ? "Selected":"" ) : ""}} >Independent Body</option>
                  
          </select>
        </div>
        
        <div class="form-group col-md-3">
          <label for="inputPassword4">Company Name <span class="text-red"></span></label>
        <span class="form-control">{{ $vendor->company_name }}</span>
        </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">Designation <span class="text-red"></span></label>
          <span class="form-control">{{ $vendor->designation }}</span>
        </div>

         <div class="form-group col-md-3">
          <label for="inputPassword4">Mobile Number <span class="text-red"></span></label>
         <span class="form-control">{{ $vendor->mobile }}</span>
        </div>
        
        <div class="form-group col-md-3">
          <label for="inputPassword4">Telephone Number </label>
           <span class="form-control">{{ $vendor->telephone }}</span>
        </div>
        <div class="form-group col-md-6" >
          <div id="map" style="width: 98%; height: 250px;position: absolute;">
             <img src="{{ asset('images/map.jpg') }}" style="width: 100%;">
          </div>
        </div>

        <div class="form-group col-md-6">
          <label for="inputPassword4">Company Address <span class="text-red"></span></label>
          <span class="form-control">{{ $vendor->address }}</span>
         <div class="form-row">
            <div class="form-group col-md-6">
              <label for="">City <span class="text-red"></span></label>
                <span class="form-control">{{ $vendor->city_name }}</span>
            </div>
            <div class="form-group col-md-6">
              <label for="inputPassword4">PIN Code<span class="text-red"></span></label>
             <span class="form-control">{{ $vendor->pin }}</span>
            </div>
           {{--  <div class="form-group col-md-6">
              <label for="inputPassword4">latitude <span class="text-red"></span></label>
             <span class="form-control">{{ $vendor->latitude }}</span>
            </div>
            <div class="form-group col-md-6">
              <label for="inputPassword4">longitude <span class="text-red"></span></label>
             <span class="form-control">{{ $vendor->longitude }}</span>
            </div> --}}
         </div>
        </div>
         <hr class="col-md-12" >
         
        <div class="form-group col-md-2">
          <label for="inputPassword4">Image</label><br> 
          @if($vendor->image) 
          <a target="_Blank" href="{{ asset('').$vendor->image }}">      
           <img  src="{{ asset('').$vendor->image }}" class="" style="height: 8rem;padding-top: 2px;"></a>
          @else
             <br>No Image
          @endif 
        </div>
         <div class="form-group col-md-2">
          <label for="inputPassword4">Cover Image</label><br>
          @if($vendor->coverimage)
           <a target="_Blank" href="{{ asset('').$vendor->coverimage }}">       
           <img  src="{{ asset('').$vendor->coverimage }}" class="" style="height: 8rem;padding-top: 2px;"></a>
          @else
            <span style="border: 1px solid gray;padding: 1rem; position: inherit;top: 2rem;">
                  No Image
              </span>
          @endif
        </div>
        <div class="form-group col-md-2">
          <label for="inputPassword4">Menu Image</label><br>  
          @if($vendor->coverimage)
           <a target="_Blank" href="{{ asset('').$vendor->menu }}">       
           <img  src="{{ asset('').$vendor->menu }}" class="" style="height: 8rem;padding-top: 2px;"></a>
          @else
            <span style="border: 1px solid gray;padding: 1rem; position: inherit;top: 2rem;">
                  No Image
              </span>
          @endif     
           
        </div>
        

        <hr class="col-12" style="margin: 8px;">
       <div class="form-group col-md-3">
          <label for="inputPassword4">Under Vertical</label>       
          <span class="form-control">{{ $vendor->master_name }}</span>
        </div>
        {{-- <div class="form-group col-md-3">
          <label for="inputPassword4">Commission Plan</label>       
          <span class="form-control">{{ $vendor->commissions_name }} ( {{ $vendor->commissions_on }}% )</span>
        </div> --}}

        <hr class="col-md-12" style="margin: 8px;">
        <div class="form-group col-md-3">
          <label for="inputPassword4">Aadhar No. <span class="text-red"></span></label>
         <span class="form-control" style="margin-bottom: 2rem">{{ $vendor->aadhar }}</span>
          @if($vendor->aadharupload)
          <a target="_Blank" href="{{ asset('').$vendor->aadharupload }}">
          <img  src="{{ asset('').$vendor->aadharupload }}" class="" style="height: 8rem;padding-top: 2px;"></a>
          @else
            <span style="border: 1px solid gray;padding: 1rem; ">
                  No Image
              </span>
          @endif
        </div>

        <div class="form-group col-md-3">
          <label for="inputPassword4">PAN no <span class="text-red"></span></label>
         <span class="form-control" style="margin-bottom: 2rem">{{ $vendor->pan }}</span>
          @if($vendor->panupload)
          <a target="_Blank" href="{{ asset('').$vendor->panupload }}">
          <img  src="{{ asset('').$vendor->panupload }}" class="" style="height: 8rem;padding-top: 2px;"></a>
          @else
            <span style="border: 1px solid gray;padding: 1rem; ">
                  No Image
              </span>
          @endif
        </div>

        <div class="form-group col-md-3">
          <label for="inputPassword4">GSTIN <span class="text-red"></span></label>
         <span class="form-control" style="margin-bottom: 2rem">{{ $vendor->gstin }}</span>
          @if($vendor->gstinupload)
         <a target="_Blank" href="{{ asset('').$vendor->gstinupload }}">
          <img  src="{{ asset('').$vendor->gstinupload }}" class="" style="height: 8rem;padding-top: 2px;"></a>
          @else
            <span style="border: 1px solid gray;padding: 1rem; ">
                  No Image
              </span>
          @endif
        </div>

         <div class="form-group col-md-3">
          <label for="inputPassword4">FSSAI Number </label>
         <span class="form-control" style="margin-bottom: 2rem">{{ $vendor->fssai }}</span>
          @if($vendor->fssaiupload)
         <a target="_Blank" href="{{ asset('').$vendor->fssaiupload }}">
            <img  src="{{ asset('').$vendor->fssaiupload }}" class="" style="height: 8rem;padding-top: 2px;">
          </a>
          @else
            <span style="border: 1px solid gray;padding: 1rem; ">
                  No Image
              </span>
          @endif
        </div>
  
 

        
        


      </div>
      
      

  </div>
  <div class="card-footer text-muted">
    {{-- {!! $uData->links() !!} --}}
  </div>
</div>
    
@endsection
