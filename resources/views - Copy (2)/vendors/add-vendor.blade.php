@extends('layouts.vendor')

@section('style')
  <link href="{{  asset('css/plugins/select2.min.css') }}" rel="stylesheet" />
@endsection

@section('scripts')
  <script src="{{  asset('js/plugins/select2.min.js') }}" ></script>
  <script type="text/javascript">
    $(document).ready(function () {
      //$('.select2').select2();
    });
  </script>
 
  <script type="text/javascript">
    $(document).ready(function () {
      $.ajaxSetup({
           headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
       });
/*Aadhar file Upload Start*/
      $('#aadharFile').change(function() { 
        var image=$(this).val();
       
        if(image){
            var postData=new FormData();
            postData.append('file',this.files[0]);     
            var url="{{route('ajax.file.upload','vendor')}}";     
            $.ajax({               
                async:true,
                type:"post",
                url:url,
                data:postData,
                cache:false,
                contentType: false,
                processData: false,
                success:function(data){
                  console.log(data);
                  if(data.path){
                    $('#aadharupload').val(data.path);
                    toastr.success('Aadhar '+data.message, { closeButton: !0 });
                  }else{
                    toastr.error('Aadhar '+data.message, { closeButton: !0 });
                  }
                },error: function(xhr, status, error) {
                  //console.log(xhr.responseJSON.errors.file);
                  var err=xhr.responseJSON.errors.file;
                   $.each(err,function(key,value){
                        toastr.error(value, { closeButton: !0 });
                      
                  });
                  
                }  
     
            });
        }
      }); 
/*Aadhar file Upload End*/
/*Pan file Upload Start*/

      $('#panFile').change(function() { 
        var image=$(this).val();
       
        if(image){
            var postData=new FormData();
            postData.append('file',this.files[0]);     
            var url="{{route('ajax.file.upload','vendor')}}";     
            $.ajax({               
                async:true,
                type:"post",
                url:url,
                data:postData,
                cache:false,
                contentType: false,
                processData: false,
                success:function(data){
                  console.log(data);
                  if(data.path){
                    $('#panupload').val(data.path);
                    toastr.success('PAN '+data.message, { closeButton: !0 });
                  }else{
                    toastr.error('PAN '+data.message, { closeButton: !0 });
                  }
                },error: function(xhr, status, error) {
                  //console.log(xhr.responseJSON.errors.file);
                  var err=xhr.responseJSON.errors.file;
                   $.each(err,function(key,value){
                        toastr.error(value, { closeButton: !0 });
                      
                  });
                  
                }  
     
            });
        }
      }); 
/*PAN file Upload End*/
/*GSTIN file Upload Start*/

      $('#gstinFile').change(function() { 
        var image=$(this).val();
       
        if(image){
            var postData=new FormData();
            postData.append('file',this.files[0]);     
            var url="{{route('ajax.file.upload','vendor')}}";     
            $.ajax({               
                async:true,
                type:"post",
                url:url,
                data:postData,
                cache:false,
                contentType: false,
                processData: false,
                success:function(data){
                  console.log(data);
                  if(data.path){
                    $('#gstinupload').val(data.path);
                    toastr.success('GSTIN '+data.message, { closeButton: !0 });
                  }else{
                    toastr.error('GSTIN '+data.message, { closeButton: !0 });
                  }
                },error: function(xhr, status, error) {
                  //console.log(xhr.responseJSON.errors.file);
                  var err=xhr.responseJSON.errors.file;
                   $.each(err,function(key,value){
                        toastr.error(value, { closeButton: !0 });
                      
                  });
                  
                }  
     
            });
        }
      }); 
/*GSTIN file Upload End*/
/*FSSAI file Upload Start*/

      $('#fssaiFile').change(function() { 
        var image=$(this).val();
       
        if(image){
            var postData=new FormData();
            postData.append('file',this.files[0]);     
            var url="{{route('ajax.file.upload','vendor')}}";     
            $.ajax({               
                async:true,
                type:"post",
                url:url,
                data:postData,
                cache:false,
                contentType: false,
                processData: false,
                success:function(data){
                  console.log(data);
                  if(data.path){
                    $('#fssaiupload').val(data.path);
                    toastr.success('FSSAI '+data.message, { closeButton: !0 });
                  }else{
                    toastr.error('FSSAI '+data.message, { closeButton: !0 });
                  }
                },error: function(xhr, status, error) {
                  //console.log(xhr.responseJSON.errors.file);
                  var err=xhr.responseJSON.errors.file;
                   $.each(err,function(key,value){
                        toastr.error(value, { closeButton: !0 });
                      
                  });
                  
                }  
     
            });
        }
      }); 
/*FSSAI file Upload End*/

    });
  </script>

  <script>
    var map;

    function initMap() alert('intMap');
    {
        var ip_lat = '';
        var ip_long = '';
              ip_lat = v_lat;
              ip_long = v_long;
        
        var myLatlng = new google.maps.LatLng(ip_lat,ip_long);
        var mapOptions = {
                            zoom            : 15,
                            center          : myLatlng,
                            disableDefaultUI: true,
                            panControl      : true,
                            zoomControl     : true,
                            mapTypeControl  : true,
                            streetViewControl: true,
                            mapTypeId       : google.maps.MapTypeId.ROADMAP,
                            fullscreenControl: true
                         };

        map = new google.maps.Map(document.getElementById('map'),mapOptions);
        var marker = new google.maps.Marker({
                                                position: myLatlng,
                                                map: map,
                                                draggable:true,    
                                            }); 
             
        google.maps.event.addListener(marker, 'dragend', function(e) 
        {
            var lat = this.getPosition().lat();
            var lng = this.getPosition().lng();
            $('#us3-lat').val(lat);
            $('#us3-lon').val(lng);
        });
        var input = document.getElementById('address');
        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', map);
        google.maps.event.addListener(autocomplete, 'place_changed', function () 
        {
            var place = autocomplete.getPlace();
            var lat = place.geometry.location.lat();
            var lng = place.geometry.location.lng();
            if (place.geometry.viewport) 
            {
                map.fitBounds(place.geometry.viewport);
                var myLatlng = place.geometry.location; 
                var latlng = new google.maps.LatLng(lat, lng);
                marker.setPosition(latlng);
            } 
            else 
            {
                map.setCenter(place.geometry.location); 
                map.setZoom(17);
            }
            $('#us3-lat').val(lat);
            $('#us3-lon').val(lng);
        });
   }
    //initialize_restaurant_loc();
    //google.maps.event.addDomListener(window, 'load', initialize_restaurant_loc);
    </script>
    <script type="text/javascript" src='https://maps.google.com/maps/api/js?sensor=false&libraries=places&key=AIzaSyCKtoBv2YH27eVVbIK6SFv_4ldXDUCGqFE&callback=initMap'></script>
@endsection

@section('content')
<style type="text/css">
  .text-red{color: red;font-weight: 700;}
</style>
<div class="card ">
  <div class="card-header">
    <h5 align="left">Add New Vendor
        <a class="btn btn-sm btn-info float-right" style="" href="{{ route('vendor.dashboard') }}"><b>Back</b></a>
    </h5>

  </div>
  <div class="card-body">
   
    <form method="POST" action="{{ route('vendor.store-vendor') }}" enctype="multipart/form-data">
        @csrf
      <div class="form-row">
        <div class="form-group col-md-3">
          <label for="inputEmail4">Vendor Type <span class="text-red">*</span></label>
           <select value="{{ old('vendor_categry_id') }}" name="vendor_categry_id" class="form-control" required >
              <option value="">Please Select</option>
              @foreach($VendorCategory as $svalue)
                <option value="{{ $svalue->id }}" >{{ $svalue->name }}</option>
              @endforeach
            </select>
        </div>
        <div class="form-group col-md-3">
          <label for="inputEmail4">Product Category <span class="text-red">*</span></label>
           <select value="{{ old('product_categry_id') }}" name="product_categry_id" class="form-control" required >
               <option value="">Please Select</option>
              @foreach($productCategory as $pcKey => $svalue)
                <option value="{{ $svalue->id }}" >{{ $svalue->name }}</option>
              @endforeach
            </select>
        </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">Email <span class="text-red">*</span></label>
         <input type="email" value="{{ old('email') }}" name="email" class="form-control" required=""  placeholder="Email">
        </div>

        <div class="form-group col-md-3">
          <label for="inputEmail4">Vendor Name <span class="text-red">*</span></label>
          <input type="text" value="{{ old('name') }}" name="name" class="form-control" required placeholder="Name">
        </div>
        
        <div class="form-group col-md-3">
          <label for="inputPassword4">Company Name <span class="text-red">*</span></label>
         <input type="text" required value="{{ old('company_name') }}" name="company_name" class="form-control"   placeholder="Company Name">
        </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">Designation <span class="text-red">*</span></label>
          <input type="text" required value="{{ old('designation') }}" class="form-control" name="designation" placeholder="Designation">
        </div>

         <div class="form-group col-md-3">
          <label for="inputPassword4">Mobile Number <span class="text-red">*</span></label>
         <input type="text" required value="{{ old('mobile') }}" name="mobile" class="form-control"  placeholder="Mobile Number">
        </div>
        
        <div class="form-group col-md-3">
          <label for="inputPassword4">Whatsapp Number </label>
           <input type="text"  name="telephone" value="{{ old('telephone') }}" class="form-control"  placeholder="Whatsapp Number">
        </div>

         <div class="form-group col-md-3">
          <label for="inputPassword4">City <span class="text-red">*</span></label>
           <select value="{{ old('city') }}" id="" name="city"  class="form-control " required >
               <option value="">Please Select</option>
              @foreach($city as $pcKey => $svalue)
                <option value="{{ $svalue->id }}" >{{ $svalue->name }}</option>
              @endforeach
            </select>
        </div>
        <div id="map" style="width: 400px; height: 400px"></div>
        {{-- <div id="address-map-container" class="form-group col-md-3" style="width:100%;height:400px; ">
            <div style="width: 100%; height: 100%" id="address-map"></div>
        </div> --}}
        <div class="form-group col-md-6">
          <label for="inputPassword4">Company Address <span class="text-red">*</span></label>
         <input type="text" required id="address" name="address" value="{{ old('address') }}" class="form-control"  placeholder="Company  Address">
        </div>
        <div class="form-group col-md-2">
          <label for="inputPassword4">Company Address PIN<span class="text-red">*</span></label>
         <input type="text" required id="pin" name="pin" value="{{ old('pin') }}" class="form-control"  placeholder="Company Address PIN">
        </div>
        <div class="form-group col-md-2">
          <label for="inputPassword4">Company Address PIN<span class="text-red">*</span></label>
         <input type="text" required id="pin" name="pin" value="{{ old('pin') }}" class="form-control"  placeholder="Company Address PIN">
        </div>
        <div class="form-group col-md-2">
          <label for="inputPassword4">Company Address PIN<span class="text-red">*</span></label>
         <input type="text" required id="pin" name="pin" value="{{ old('pin') }}" class="form-control"  placeholder="Company Address PIN">
        </div>
        
        <div class="form-group col-md-3">
          <label for="inputPassword4">Aadhar No. <span class="text-red">*</span></label>
         <input type="text" class="form-control"  name="aadhar" required  value="{{ old('aadhar') }}" placeholder="Aadhar Card">
         <input type="file" class="form-control" id="aadharFile">
          <input type="hidden" class="form-control" id="aadharupload" name="aadharupload">
        </div>

        <div class="form-group col-md-3">
          <label for="inputPassword4">PAN no <span class="text-red">*</span></label>
         <input type="text" class="form-control"  name="pan" required  value="{{ old('pan') }}" placeholder="PAN Card">
         <input type="file" class="form-control" id="panFile">
          <input type="hidden" class="form-control" id="panupload" name="panupload">
        </div>

        <div class="form-group col-md-3">
          <label for="inputPassword4">GSTIN <span class="text-red">*</span></label>
         <input type="text" class="form-control" title="Goods and Services Tax Identification Number" name="gstin" required  value="{{ old('gstin') }}" placeholder="Goods and Services Tax Identification Number ">
         <input type="file" class="form-control" id="gstinFile">
          <input type="hidden" class="form-control" id="gstinupload" name="gstinupload">
        </div>

         <div class="form-group col-md-3">
          <label for="inputPassword4">FSSAI Number </label>
         <input type="text" class="form-control" title="Food Safety & Standards Authority of India" value="{{ old('fssai') }}"  name="fssai" placeholder="Food Safety & Standards Authority of India">
         <input type="file" class="form-control" id="fssaiFile">
          <input type="hidden" class="form-control" id="fssaiupload" name="fssaiupload">
        </div>
  
 

        
        


      </div>
      
      <div class="form-row float-right">
          <button type="submit" class="btn btn-primary ">Save</button>
      </div>
    </form>

  </div>
  <div class="card-footer text-muted">
    {{-- {!! $uData->links() !!} --}}
  </div>
</div>
    
@endsection
