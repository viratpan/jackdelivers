@extends('layouts.admin')



@section('content')


<div class="card ">
  <div class="card-header">
    <div>
        <form method="{{ isset($stateData) ? 'PUT': "POST"}}" action="{{ route('admin.attribute') }}{{ isset($stateData) ? '/update' : "/store"}}{{ isset($stateData) ? '/'.$stateData->id : ""}}">
          @csrf
          <div class="form-row">
              <div class="form-group col-md-7">
                  <label for="inputEmail4">Unit Name</label>
                  <input type="text" value="{{ old('name') }}{{ isset($stateData) ? $stateData->name : ""}}" name="name" class="form-control" required placeholder="Unit Name">
              </div>
              <div class="form-group col-md-2">
                   <label for="inputEmail4">Under Vertical</label>
                   <select value="{{ old('masters_id') }}" name="masters_id" class="form-control" required >
                    <option value="">Select Vertical</option>
                    @foreach($master as $svalue)
                      <option value="{{ $svalue->id }}" {{ isset($stateData) ? ( $stateData->masters_id==$svalue->id ? "Selected":"" ) : ""}}>{{ $svalue->name }}</option>
                    @endforeach
                  </select>
              </div>
              <div class="form-group col-md-3 float-right" style="padding-top: 1.55rem;">
                <label for="inputEmail4"></label>
                  <button type="submit" class="btn btn-primary "> Submit</button>
                  @if(isset($stateData))
                   <a href="{{ route('admin.attribute') }}" class="btn btn-info ">Cancel</a> 
                  @endif
              </div>
          </div>                 
          
      </form>
    </div>

  </div>
  <div class="card-body">
    <?php //echo"<pre>",print_r($data),"</pre>";?>
    <div class="table-responsive">
        <table id="data_table" class="table">
            <thead>
                <tr>
                    <th>Sn.</th>       
                    <th>Unit Name</th> 
                    <th>Under Vertical</th>                         
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
              @foreach ($data as $key => $uData)
                <tr>
                   
                     <td>{{ $key+1 }}</td>
                    <td>{{ $uData->name  }}</td>
                    <td>{{ $uData->master_name }}</td> 
                    <td>
                        

                            <a class="btn btn-primary" href="{{ route('admin.attribute.edit',$uData->id) }}">Edit</a>
                           
                       {{--  <form action="{{ route('admin.attribute.destroy',$uData->id) }}" method="POST">    
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                          
              
                            <button type="submit" onclick="return confirm('Are you sure?')" class="btn btn-danger">Delete</button>
                        </form> --}}
                      
                    </td>
                </tr>
            @endforeach    
                
            </tbody>
           {{--  <tfoot>
                <tr>
                  
                    <th>Attribute Name</th>
                    
                    <th>Action</th>
                </tr>
            </tfoot> --}}
        </table>

        
    </div>
  </div>
  <div class="card-footer text-muted">
{{--     {!! $uData->links() !!} --}}
{{--  {{ $uData->links() }} --}}
  </div>
</div>
	
@endsection
