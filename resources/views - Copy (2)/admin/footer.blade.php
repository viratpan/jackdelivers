<!-- Footer Start -->
<div class="flex-grow-1"></div>
<div class="app-footer">
    <div class="row">
        <div class="col-md-9">
            <div class="d-flex align-items-center">
               {{--  <img class="logo" src="{{ asset('images/logo.png') }}" alt=""> --}}
                <div>
                     <p class="m-0">Devloped by {!! config('app.devloped_by') !!}</p>

                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="d-flex align-items-center">
               <small id="admin_location" style="position: absolute;right: 0;">...</small>
            </div>

        </div>
    </div>


</div>
<!-- fotter end -->