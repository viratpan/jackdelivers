<html class="no-js" lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>{{ config('app.name') }} Reset Password {{ isset($url) ? ucfirst($url) : ""}} </title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
        ============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset(config('app.icon')) }}">
    <!-- Google Fonts
        ============================================ -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,400i,600,700,800,900" rel="stylesheet" />
    <!-- Scripts -->
   <style type="text/css">
       @media (min-width: 1024px){
        .auth-layout-wrap .auth-content {
            min-width: 670px!important;
        }
       }
        
   </style>
    <link href="{{  asset('css/themes/lite-purple.min.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="{{  asset('css/plugins/toastr.css') }}" />
</head>
<div class="auth-layout-wrap" style="background-image: url()">
    <div class="auth-content">
        <div class="card o-hidden">
            <div class="row">
                <div class="col-md-12">
                    <div class="p-4">
                        <div class="auth-logo text-center mb-4">
                             <img src="{{ asset(config('app.logo')) }}" style="width: Auto!important;" alt="{{ config('app.name') }}"></div>
                        </div>
                        <h1 class="mb-3 text-18 text-center">Reset Password {{ isset($url) ? ucfirst($url) : ""}}</h1>
                        @if(isset($url) )
                                @if($url=='vendor')
                                    <form method="POST" action="{{ route('forget.vendor') }}">
                                @endif
                                @if($url=='admin')
                                    <form method="POST" action="{{ route('forget.admin') }}">
                                @endif
                            @endif
                        <form method="POST" action="{{ route('login') }}{{ isset($url) ? '/'.$url : ""}}">
                        @csrf
                            @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-rounded btn-primary btn-block mt-2">
                                    {{ __('Send Password Reset Link') }}
                                </button>
                            </div>
                            <div class="col-md-6 offset-md-4">
                                <a href="{{route('vendor.dashboard')}}" class="btn btn-rounded btn-info btn-block mt-2">
                                    {{ __('Vendor Sign In') }}
                                </a>
                            </div>
                        </div>
                           
                        </form>
                        
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('js/plugins/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('js/plugins/toastr.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function () {
       
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                toastr.error("{{ $error }}", { closeButton: !0 });
            @endforeach  
        @endif
        @if (session('success'))
            toastr.success("{{ session('success') }}", { closeButton: !0 });
        @endif
        @if (session('error'))
            toastr.error("{{ session('error') }}", { closeButton: !0 });
        @endif
        @if (session('warning'))
            toastr.warning("{{ session('warning') }}", { closeButton: !0 });
        @endif

       
    });
</script>