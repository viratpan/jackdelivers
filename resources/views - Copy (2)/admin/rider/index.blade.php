<?php $layouts='layouts.vendor'; ?>

@if(Auth::guard('admin')->check())
@php $layouts='layouts.admin' @endphp
@endif
@if(Auth::guard('vendor')->check())
   @php  $layouts='layouts.vendor' @endphp
@endif

@extends($layouts)

@section('style')
<style type="text/css">
  .text-green{
  color: green;
  font-size: x-large;
  }.text-danger{
  color: red;
  font-size: x-large;
  }
</style>
@endsection


@section('content')


<div class="card ">
  <div class="card-header">
    <div>
     
        <form method="GET" action="{{ route('admin.rider-list') }}">
      
      
          
          <div class="form-row">
              <div class="form-group col-md-3">
                  <label for="inputEmail4">Search</label>
                  <input type="text" value="{{ isset($request) ? $request->search : ""}}" name="search" class="form-control"  placeholder="Text">
              </div>
              

              <div class="form-group col-md-2">
                  <label for="inputEmail4">City</label>
                  <select value="{{ old('city') }}" name="city" class="form-control select2"  >
                    <option value="">Select City</option>
                    @foreach($city as $svalue)
                      <option value="{{ $svalue->id }}" {{ isset($request) ? ( $request->city==$svalue->id ? "Selected":"" ) : ""}}>{{ $svalue->name }}</option>
                    @endforeach
                  </select>
              </div>
              
              
              <div class="form-group col-md-2">
                  <label for="inputEmail4">Status</label>
                  <select value="" name="status" class="form-control"  >
                    <option value="">Select Status</option>
                    <option value="0" {{ isset($request) ? ( $request->status==0 ? "Selected":"" ) : ""}}>De-Active </option>
                    <option value="1" {{ isset($request) ? ( $request->status==1 ? "Selected":"" ) : ""}}>Active </option>
                   </select> 
              </div>

              <div class="form-group col-md-2">
                  <label for="">On-Duty</label>
                  <select value="" name="onduty" class="form-control"  >
                    <option value="">Select Status</option>
                    <option value="0" {{ isset($request) ? ( $request->status==0 ? "Selected":"" ) : "" }} "">Off-Duty </option>
                    <option value="1" {{ isset($request) ? ( $request->status==1 ? "Selected":"" ) : ""}}>On-Duty </option>
                    </select>
              </div>
              
              <div class="form-group col-md-2 float-right" style="padding-top: 1.55rem;">
                <label for="inputEmail4"></label>
                  <button type="submit" class="btn btn-primary ">Submit</button>
                     
                      <a href="{{ route('admin.rider-list') }}" class="btn btn-info ">Reset</a>
                    
              </div>

              <div class="form-group col-md-1 float-right" style="padding-top: 1.55rem;">
              
                <a class="btn btn-sm btn-info float-right" style="" href="{{ route('admin.add-rider') }}"><b>+ Add Rider</b></a>
               
               
                  
              </div>


          </div>                 
          
      </form>
    </div>

  </div>
  <div class="card-body">
    <?php //echo"<pre>",print_r($data),"</pre>";?>
    <div class="table-responsive">
        <table id="" class="table vtable">
            <thead>
                <tr>
                    <th>Sn.</th>                 
                    <th>Joining Date</th>                
                    <th>Name</th>
                    <th>Image</th>
                    <th>Email/Mobile</th>
                    <th>Dl/Validity</th>                   
                    <th>Address</th>
                    <th>Rating</th>
                    <th>On-Duty</th>
                    <th>Under Zone</th>
                    <th>Status</th>
                    <th>Action</th>
                    
                </tr>
            </thead>
            <tbody>
            @foreach ($data as $key => $uData)
                <tr>
                    <td>{{ $key+1 }}</td>                 
                    <td>{{ date('d M y', strtotime($uData->join_at)) }}</td>                 
                    <td>{{ $uData->name }}</td>
                    <td>
                      <img  src="{{ asset('').$uData->image }}" class="" style="height: 3rem;">
                    </td>             
                   
                    <td>{{ $uData->email }}<br>{{ $uData->mobile }}</td>
                    <td>{{ $uData->dl }}<br>Till : {{ date('d M y', strtotime($uData->dl_valid)) }}</td>
                   
                    <td> {{$uData->address}}</td>
                    <td>{{$uData->rating}}</td>
                    <td>
                      @if($uData->on_duty==1)
                       <i class="las la-check text-green"></i>  
                      @else
                       <i class="las la-times text-danger"></i>
                      @endif 
                    </td>
                    <td>{{$uData->city_name}}</td>
                   
                    <td>
                        @if($uData->active==0)
                            <small class="status secondary">Deactive</small>
                        @endif
                        @if($uData->active==1)
                            <small  class="status primary">Active</small>
                        @endif
                       
                    </td>
                
                <td style="display: flex;">
                        
                    <a href="{{ route('admin.edit-rider',$uData->id)}}" class="btn btn-sm btn-primary">Edit</a>
                                                    

                  @if($uData->active==0)
                      <a class="btn btn-info  btn-sm" href="{{ route('admin.rider.status',['id' => $uData->id, 'status' => 1]) }}">Active</a>
                     
                  @endif
                  @if($uData->active==1)
                      <a class="btn btn-warning  btn-sm" href="{{ route('admin.rider.status',['id' => $uData->id, 'status' => 0]) }}">Deactive</a>
                      
                  @endif

                  @if($uData->on_duty==0)
                      <a class="btn btn-light  btn-sm" href="{{ route('admin.rider.duty',['id' => $uData->id, 'status' => 1]) }}">On Duty</a>
                     
                  @endif
                  @if($uData->on_duty==1)
                      <a class="btn btn-warning  btn-sm" href="{{ route('admin.rider.duty',['id' => $uData->id, 'status' => 0]) }}">Off Duty</a>
                      
                  @endif
                        
                        
                       

                    </td>
                  </tr>
            @endforeach    
                
            </tbody>
          
        </table>

        
    </div>
  </div>
  <div class="card-footer text-muted">
{{--     {!! $uData->links() !!} --}}
{{--  {{ $uData->links() }} --}}
  </div>
</div>
	
@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function () {
    
  });
 
</script>  

@endsection