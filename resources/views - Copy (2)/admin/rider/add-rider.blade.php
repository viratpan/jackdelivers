<?php $layouts='layouts.vendor'; ?>

@if(Auth::guard('admin')->check())
@php $layouts='layouts.admin' @endphp
@endif
@if(Auth::guard('vendor')->check())
   @php  $layouts='layouts.vendor' @endphp
@endif

 @extends($layouts)

@section('scripts')
  <script src="{{  asset('js/plugins/select2.min.js') }}" ></script>
  <script type="text/javascript">
    $(document).ready(function () {
      //$('.select2').select2();
    });
  </script>
 
  <script type="text/javascript">
   
    $(document).ready(function () {
      $.ajaxSetup({
           headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
       });
     $('#imageFile').change(function() { 
        var image=$(this).val();
       
        if(image){
            var postData=new FormData();
            postData.append('file',this.files[0]);     
            var url="{{route('ajax.file.upload','rider')}}";     
            $.ajax({               
                async:true,
                type:"post",
                url:url,
                data:postData,
                cache:false,
                contentType: false,
                processData: false,
                success:function(data){
                  console.log(data);
                  if(data.path){
                    $('#imageupload').val(data.path);
                    toastr.success('Image '+data.message, { closeButton: !0 });
                  }else{
                    toastr.error('Image '+data.message, { closeButton: !0 });
                  }
                },error: function(xhr, status, error) {
                  //console.log(xhr.responseJSON.errors.file);
                  var err=xhr.responseJSON.errors.file;
                   $.each(err,function(key,value){
                        toastr.error(value, { closeButton: !0 });
                      
                  });
                  
                }  
     
            });
        }
      }); 
/*Aadhar file Upload Start*/
/*Aadhar file Upload Start*/
      $('#aadharFile').change(function() { 
        var image=$(this).val();
       
        if(image){
            var postData=new FormData();
            postData.append('file',this.files[0]);     
            var url="{{route('ajax.file.upload','rider')}}";     
            $.ajax({               
                async:true,
                type:"post",
                url:url,
                data:postData,
                cache:false,
                contentType: false,
                processData: false,
                success:function(data){
                  console.log(data);
                  if(data.path){
                    $('#aadharupload').val(data.path);
                    toastr.success('Aadhar '+data.message, { closeButton: !0 });
                  }else{
                    toastr.error('Aadhar '+data.message, { closeButton: !0 });
                  }
                },error: function(xhr, status, error) {
                  //console.log(xhr.responseJSON.errors.file);
                  var err=xhr.responseJSON.errors.file;
                   $.each(err,function(key,value){
                        toastr.error(value, { closeButton: !0 });
                      
                  });
                  
                }  
     
            });
        }
      }); 
/*Aadhar file Upload End*/
/*Pan file Upload Start*/

      $('#panFile').change(function() { 
        var image=$(this).val();
       
        if(image){
            var postData=new FormData();
            postData.append('file',this.files[0]);     
            var url="{{route('ajax.file.upload','rider')}}";     
            $.ajax({               
                async:true,
                type:"post",
                url:url,
                data:postData,
                cache:false,
                contentType: false,
                processData: false,
                success:function(data){
                  console.log(data);
                  if(data.path){
                    $('#panupload').val(data.path);
                    toastr.success('PAN '+data.message, { closeButton: !0 });
                  }else{
                    toastr.error('PAN '+data.message, { closeButton: !0 });
                  }
                },error: function(xhr, status, error) {
                  //console.log(xhr.responseJSON.errors.file);
                  var err=xhr.responseJSON.errors.file;
                   $.each(err,function(key,value){
                        toastr.error(value, { closeButton: !0 });
                      
                  });
                  
                }  
     
            });
        }
      }); 
/*PAN file Upload End*/
/*GSTIN file Upload Start*/

      $('#dlFile').change(function() { 
        var image=$(this).val();
       
        if(image){
            var postData=new FormData();
            postData.append('file',this.files[0]);     
            var url="{{route('ajax.file.upload','rider')}}";     
            $.ajax({               
                async:true,
                type:"post",
                url:url,
                data:postData,
                cache:false,
                contentType: false,
                processData: false,
                success:function(data){
                  console.log(data);
                  if(data.path){
                    $('#dlupload').val(data.path);
                    toastr.success('Dl '+data.message, { closeButton: !0 });
                  }else{
                    toastr.error('Dl '+data.message, { closeButton: !0 });
                  }
                },error: function(xhr, status, error) {
                  //console.log(xhr.responseJSON.errors.file);
                  var err=xhr.responseJSON.errors.file;
                   $.each(err,function(key,value){
                        toastr.error(value, { closeButton: !0 });
                      
                  });
                  
                }  
     
            });
        }
      });
    }); 

  </script>

@endsection


@section('content')
<style type="text/css">
  .text-red{color: red;font-weight: 700;}
</style>
<div class="card ">
  <div class="card-header">
    <h5 align="left">Add Rider
@if(Auth::guard('admin')->check())
  <a class="btn btn-sm btn-info float-right" style="" href="{{ route('admin.rider-list') }}"><b>Back</b></a>
@endif

        
    </h5>

  </div>
  <div class="card-body">
  @if(isset($data))
  <form method="PUT" action="{{ route('admin.update-rider',$data->id) }}" enctype="multipart/form-data">
  
  @else
  <form method="POST" action="{{ route('admin.store-rider') }}" enctype="multipart/form-data">
  @endif
  
    
        @csrf
        <div class="form-row">
            <div class="form-group col-md-3">
                  <label for="inputEmail4">Under Zone<span class="text-red">*</span></label>                 
                  <select value="{{ old('zone_id') }}" name="zone_id" class="form-control select2" required >
                    <option value="">Select Zone</option>
                    @foreach($city as $svalue)
                      <option value="{{ $svalue->id }}" {{ isset($data) ? ( $data->zone_id==$svalue->id ? "Selected":"" ) : ""}}>{{ $svalue->name }}</option>
                    @endforeach
                  </select>
              </div>
            
            <hr class="col-12" style="margin: 1px;">
            <div class="form-group col-md-2">
              <label for="inputEmail4">Name <span class="text-red">*</span></label>
              <input type="text" id="name" value="{{ isset($data) ? $data->name : old('name')}}" name="name" class="form-control" required placeholder="Name">             

            </div>
            <div class="form-group col-md-2">
              <label for="inputEmail4">Email <span class="text-red">*</span></label>
              <input type="email" id="email" value="{{ isset($data) ? $data->email : old('email')}}" name="email" class="form-control" required placeholder="Email">             

            </div>
            <div class="form-group col-md-2">
              <label for="inputEmail4">Mobile <span class="text-red">*</span></label>
              <input type="number" id="mobile" value="{{ isset($data) ? $data->mobile : old('mobile')}}" name="mobile" class="form-control" required placeholder="Mobile">             

            </div>
            <div class="form-group col-md-2">
              <label for="inputEmail4">Emergency Mobile <span class="text-red">*</span></label>
              <input type="number" id="" value="{{ isset($data) ? $data->emergency_mobile : old('emergency_mobile')}}" name="emergency_mobile" class="form-control" required placeholder="Emergency Mobile">             

            </div>
            <div class="form-group col-md-2">
              <label for="inputEmail4">Blood Group <span class="text-red"></span></label>
              <input type="text" id="" value="{{ isset($data) ? $data->blood_group : old('blood_group')}}" name="blood_group" class="form-control"  placeholder="Blood Group">             

            </div>
            <div class="form-group col-md-2">
              <label for="inputPassword4">Image</label>         
             <input type="file" accept="image/png, image/jpeg" class="form-control" id="image">
              <input type="hidden"  value="" class="form-control" id="aadharupload2" name="image">
            </div>
           

            <div class="form-group col-md-4">
              <label for="inputEmail4">Address <span class="text-red">*</span></label>                         
              <textarea name="address" rows="2" class="form-control" required>{{ isset($data) ? $data->address : old('address')}}</textarea>
            </div>


        <div class="form-group col-md-2">
          <label for="inputPassword4">DL No. <span class="text-red">*</span></label>
          <input type="text" tabindex="13" class="form-control"  name="dl" required  value="{{ isset($data) ? $data->dl : old('dl')}}" placeholder="Aadhar Card">
          <input type="file"  class="form-control" id="dlFile">
          <input type="hidden"  value="{{ old('dlupload') }}" class="form-control" id="dlupload" name="dl_image">
        </div>
        <div class="form-group col-md-2">
          <label for="inputPassword4">DL`s Validity<span class="text-red">*</span></label>
         <input type="date" tabindex="13" class="form-control"  name="dl_valid" required  value="{{ isset($data) ? $data->dl_valid : old('dl_valid')}}" placeholder="Dl`s Validity">
        
        </div>
        <div class="form-group col-md-2">
          <label for="inputPassword4">Aadhar No. <span class="text-red">*</span></label>
         <input type="text" tabindex="13" class="form-control"  name="aadhar" required  value="{{ isset($data) ? $data->aadhar : old('aadhar')}}" placeholder="Aadhar Card">
         <input type="file"  class="form-control" id="aadharFile">
          <input type="hidden"  value="{{ old('aadharupload') }}" class="form-control" id="aadharupload" name="aadhar_image">
        </div>

        <div class="form-group col-md-2">
          <label for="inputPassword4">PAN no <span class="text-red">*</span></label>
         <input type="text" tabindex="14" class="form-control"  name="pan" required  value="{{ isset($data) ? $data->pan : old('pan')}}" placeholder="PAN Card">
         <input type="file" class="form-control" id="panFile">
          <input type="hidden" value="{{ old('panupload') }}" class="form-control" id="panupload" name="pan_image">
        </div>

         {{-- <div class="form-group col-md-2">
              <label for="inputEmail4">Service Zone <span class="text-red">*</span></label>
              <input type="text" id="name" value="{{ isset($data) ? $data->name : old('name')}}" name="name" class="form-control" required placeholder="Name">             

            </div> --}}
        </div>
        <div class="form-row"> 
            <div class="form-group col-md-3">
              <label for="inputEmail4">A/c Name <span class="text-red">*</span></label>
              <input type="text" id="" value="{{ isset($data) ? $data->ac_no : old('ac_no')}}" name="ac_no" class="form-control" required placeholder="A/c Name">             

            </div>
            <div class="form-group col-md-3">
              <label for="inputEmail4">A/c No <span class="text-red">*</span></label>
              <input type="text" id="" value="{{ isset($data) ? $data->ac_name : old('ac_name')}}" name="ac_name" class="form-control" required placeholder="A/c No">             

            </div>
            <div class="form-group col-md-3">
              <label for="inputEmail4">IFSC <span class="text-red">*</span></label>
              <input type="text" id="" value="{{ isset($data) ? $data->ac_ifsc : old('ac_ifsc')}}" name="ac_ifsc" class="form-control" required placeholder="IFSC">             

            </div>
            <div class="form-group col-md-3">
              <label for="inputEmail4">UPI <span class="text-red"></span></label>
              <input type="text" id="" value="{{ isset($data) ? $data->upi : old('upi')}}" name="upi" class="form-control"  placeholder="UPI">             

            </div>
            <hr class="col-md-12" style="margin: 8px;">
      </div>

      
      <div class="form-row float-right">
          <button type="submit" class="btn btn-primary ">Submit</button>
      </div>
    </form>

  </div>
  <div class="card-footer text-muted">
    {{-- {!! $uData->links() !!} --}}
  </div>
</div>
    
@endsection
