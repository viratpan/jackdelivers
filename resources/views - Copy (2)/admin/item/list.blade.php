<?php $layouts='layouts.vendor'; ?>

@if(Auth::guard('admin')->check())
@php $layouts='layouts.admin' @endphp
@endif
@if(Auth::guard('vendor')->check())
  @php  $layouts='layouts.vendor' @endphp
@endif

 @extends($layouts)

@section('style')
<style type="text/css">
    .status{
        border: 0px;
        padding: 6px;
        border-radius: 8px;
    }.featured{
        background: antiquewhite;   
        padding: 6px;
        border: 0px;
        border-radius: 8px;
    }
    .status.secondary {
        background: antiquewhite;       
    }
    .status.primary {
        background: green;        
    }
    .status.danger {
        background: red;
    }
</style>
@endsection

@section('content')
<div class="card ">
  <div class="card-header">
@if(Auth::guard('admin')->check())
  <h5 align="left">Item Master
        <a class="btn btn-sm btn-info float-right" style="" href="{{ route('admin.add-item') }}"><b>+ Add  Item</b></a>
    </h5>
@endif
@if(Auth::guard('vendor')->check())
     <h5 align="left">Item Master</h5>
@endif
    

  </div>
  <div class="card-body">
    <div class="table-responsive">
        <table id="data_table" class="table">
            <thead>
                <tr>
                     <th>Sn.</th>  
                    <th>Item Name</th>
                    <th>Image</th>
                    <th>Category</th>
                    <th>SubCategory</th>                                     
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($data as $key => $uData)
                <tr>
                    <td>{{ $key+1 }}</td>
                    <td>{{ $uData->name }}<br>
                        <small>{{ $uData->slug }}</small>
                    </td>
                    <td><img  src="{{ asset('').$uData->image }}" class="" style="height: 3rem;"></td>
                    <td>{{ $uData->categories_name }}</td>
                     <td>{{ $uData->sub_categories_name }}</td>
                    
                    <td>
                        @if(Auth::guard('admin')->check())
                         
                            <a href="{{ route('admin.edit-item',$uData->id)}}" class="btn btn-sm btn-primary">Edit</a>                         
                             
                          
                        @endif
                        @if(Auth::guard('vendor')->check())
                            
                        @endif
                        
                    </td>
                    
                </tr>
            @endforeach    
                
            </tbody>
            
        </table>

        
    </div>
  </div>
  <div class="card-footer text-muted">
       
  </div>
</div>
    
   @endsection
