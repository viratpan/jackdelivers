@extends('layouts.admin')
@section('scripts')
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script>
$(function() {
  $('input[name="date"]').daterangepicker({
    opens: 'right',
    showDropdowns: true,
    minYear: 1901,
    maxYear: parseInt(moment().format('YYYY'),10),
    locale: {
            format: 'DD-MM-YYYY'
        },
    ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
  }, function(start, end, label) {
    console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
  });
});

</script>

@endsection
@section('content')


<div class="card ">
  <div class="card-header">
    <div>
        <form method="GET" action="{{ route('admin.payment-list') }}">
     
          <div class="form-row">
              <div class="form-group col-md-3">
                  <label for="">Date</label>
                 <input type="text" name="date" value="" class="form-control" >
              </div>
              <div class="form-group col-md-3">
                  <label for="">Search</label>
                  <input type="text" value="{{ isset($request) ? $request->search : ""}}" name="search" class="form-control"  placeholder="Text">
              </div>
              <div class="form-group col-md-2">
                  <label for="">Payment Type</label>
                  <select value="" name="payment_type" class="form-control"  >
                    <option value="">Select Status</option>
                    <option value="1" {{ isset($request) ? ( $request->payment_type==1 ? "Selected":"" ) : ""}}>Cash </option>
                    <option value="2" {{ isset($request) ? ( $request->payment_type==2 ? "Selected":"" ) : ""}}>Online </option>
                    
                    </select>
              </div>
              
              <div class="form-group col-md-2">
                  <label for="">Status</label>
                  <select value="" name="status" class="form-control"  >
                    <option value="">Select Status</option>
                    <option value="0" {{ isset($request) ? ( $request->status==0 ? "Selected":"" ) : ""}}>Pending </option>
                    <option value="1" {{ isset($request) ? ( $request->status==1 ? "Selected":"" ) : ""}}>Confirm </option>
                    
                    </select>
              </div>
               
              <div class="form-group col-md-2 float-right" style="padding-top: 1.55rem;">
                <label for=""></label>
                  <button type="submit" class="btn btn-primary ">Submit</button>
                 
                    <a href="{{ route('admin.payment-list') }}" class="btn btn-info ">Reset</a>
                  
              </div>
          </div>                 
          
      </form>
    </div>

  </div>
  <div class="card-body">
    <?php //echo"<pre>",print_r($data),"</pre>";?>
    <div class="table-responsive">
        <table id="" class="table vtable">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Order Id</th>
                    <th>Transition Id</th>
                    <th>Payment Type</th> 
                    <th>Coupon Code</th> 
                    <th>Discount </th> 
                    <th>Delivery</th> 
                    <th>Price</th>
                    <th>Total</th>
                    <th>Tax</th> 
                    <th>Paid Amount</th>
                    <th>Status</th>
                    <th>Recived Amount</th>          
                  
                    
                </tr>
            </thead>
            <tbody>
            @foreach ($data as $uData)
                <tr>
                    <td>{{ date('d M y', strtotime($uData->created_at)) }}</td>
                    <td>{{ $uData->OD_id }}</td>
                    <td>{{ $uData->transition_id }}</td>
                    <td>
                      @if($uData->payment_type==1)
                       Cash 
                      @elseif($uData->payment_type==2)
                       Online 
                      @endif
                    </td>
                    <td>{{ $uData->coupon_code }}</td>
                    <td>{{ $uData->discount }}</td>
                    <td>{{ $uData->shipping }}</td>
                    <td>{{ $uData->price }}</td>
                    <td>{{ $uData->amount }}</td>
                    <td>{{ $uData->tax }}</td>
                    <td><b>{{ $uData->total_amount }}</b></td>
                    <td>
                        @if($uData->status==0)
                            <small class="status secondary">Pending</small>
                        @endif                        
                        @if($uData->status==1)
                            <small  class="status primary">Confirm</small>
                        @endif
                    </td>
                    <td><b>{{ $uData->recived_amount }}</b></td>
                    
                    
                </tr>
            @endforeach    
                
            </tbody>
           {{--  <tfoot>
                <tr>
                   <th>Id</th>
                    <th>Name</th>
                    
                    <th>Action</th>
                </tr>
            </tfoot> --}}
        </table>

        
    </div>
  </div>
  <div class="card-footer text-muted">
{{--     {!! $uData->links() !!} --}}
{{--  {{ $uData->links() }} --}}
  </div>
</div>
	
   @endsection
