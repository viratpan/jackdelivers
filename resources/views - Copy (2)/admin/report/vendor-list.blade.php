@extends('layouts.admin')

@section('style')
<style type="text/css">
  .text-green{
  color: green;
  font-size: x-large;
  }.text-danger{
  color: red;
  font-size: x-large;
  }
</style>
@endsection

@section('content')


<div class="card ">
  <div class="card-header">
    <div>
        <form method="GET" action="{{ route('admin.vendor-list') }}">
     
          <div class="form-row">
              <div class="form-group col-md-3">
                  <label for="inputEmail4">Search</label>
                  <input type="text" value="{{ isset($request) ? $request->search : ""}}" name="search" class="form-control"  placeholder="Text">
              </div>
              <div class="form-group col-md-3">
                  <label for="inputEmail4">City</label>
                  <select value="{{ old('city') }}" name="city" class="form-control select2"  >
                    <option value="">Select Please</option>
                    @foreach($city as $svalue)
                      <option value="{{ $svalue->id }}" {{ isset($request) ? ( $request->city==$svalue->id ? "Selected":"" ) : ""}}>{{ $svalue->name }}</option>
                    @endforeach
                  </select>
              </div>
              <div class="form-group col-md-2">
                  <label for="inputEmail4">Master</label>
                  <select value="{{ old('master') }}" name="master" class="form-control select2"  >
                    <option value="">Select Please</option>
                    @foreach($master as $svalue)
                      <option value="{{ $svalue->id }}" {{ isset($request) ? ( $request->master==$svalue->id ? "Selected":"" ) : ""}}>{{ $svalue->name }}</option>
                    @endforeach
                  </select>
              </div>
             {{--  <div class="form-group col-md-3">
                  <label for="inputEmail4">Category</label>
                  <select value="{{ old('category') }}" name="category" class="form-control select2"  >
                    <option value="">Select Please</option>
                    @foreach($categry as $svalue)
                      <option value="{{ $svalue->id }}" {{ isset($request) ? ( $request->category==$svalue->id ? "Selected":"" ) : ""}}>{{ $svalue->name }}</option>
                    @endforeach
                  </select>
              </div> --}}
              <div class="form-group col-md-2">
                  <label for="inputEmail4">Status</label>
                  <select value="" name="status" class="form-control"  >
                    <option value="">Select</option>
                    <option value="0" {{ isset($request) ? ( $request->status==0 ? "Selected":"" ) : ""}}>De-Active </option>
                    <option value="1" {{ isset($request) ? ( $request->status==1 ? "Selected":"" ) : ""}}>Active </option>
                    </select>
              </div>
              <div class="form-group col-md-2">
                  <label for="inputEmail4">Verify</label>
                  <select value="" name="verify" class="form-control"  >
                    <option value="">Select</option>
                    <option value="0" {{ isset($request) ? ( $request->verify==0 ? "Selected":"" ) : ""}}>Not Verify </option>
                    <option value="1" {{ isset($request) ? ( $request->verify==1 ? "Selected":"" ) : ""}}>Verified </option>
                    </select>
              </div>
              
               
              <div class="form-group col-md-2 float-right" style="padding-top: 1.55rem;">
                <label for="inputEmail4"></label>
                  <button type="submit" class="btn btn-primary ">Submit</button>
                 
                    <a href="{{ route('admin.vendor-list') }}" class="btn btn-info ">Reset</a>
                   
              </div>
                <div class="form-group col-md-8 float-right" style="padding-top: 1.55rem;">
                  <a class="btn btn-sm btn-info float-right" style="" href="{{ route('admin.add-vendor') }}"><b>+ Add Vendor</b></a>
              </div>
          </div>                 
          
      </form>
    </div>

  </div>
  <div class="card-body">
    <?php //echo"<pre>",print_r($data),"</pre>";?>
    <div class="table-responsive">
        <table id="" class="table vtable">
            <thead>
                <tr>
                    <th>Sn.</th>
                    <th>Date</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Mobile</th>
                    <th>Company</th>
                    <th>Designation</th>
                    <th>Aadhar No.</th>
                    <th>PAN No.</th>
                    <th>Address</th>
                    <th>City</th>
                     <th>Deals In</th>
                      <th>Total Items</th>
                     <th>Verify</th>
                    <th>Status</th>
                   <th>Action</th> 
                </tr>
            </thead>
            <tbody>
            @foreach ($data as $key => $uData)
                <tr>
                    <td>{{ $key+1 }}</td>
                    <td>{{ date('d M y', strtotime($uData->created_at)) }}</td>
                    <td>{{ $uData->name }}</td>
                    <td>{{ $uData->email }}</td>
                    <td>{{ $uData->mobile }}</td>
                    <td>{{$uData->company_name}}</td>
                    <td>{{$uData->designation}}</td>
                    <td>{{ $uData->aadhar }}</td>
                    <td>{{ $uData->pan }}</td>
                    <td title="{{ $uData->address }} {{ $uData->pin }}">{{ Str::substr($uData->address,0,15).'..' }}</td>
                    <td>{{ $uData->city_name }}  </td>
                    <td>{{ $uData->masters_name }}</td>
                    <td>{{ App\Product::where('vendor_id', $uData->id)->count() }}</td>
                    <td>@if($uData->verify)  <i class="las la-check text-green"></i> @else  <i class="las la-times text-danger"></i> @endif </td>
                    <td>@if($uData->active)  {{ __('Active')  }} @else  {{ __('De-active')  }} @endif </td>
                    <td style="display: flex;">
                      @if($uData->verify==0)
                       <a href="{{ route('admin.vendor.verify',['id' => $uData->id, 'status' => 1])}}" class="btn btn-sm btn-primary">Verify</a>
                        @else
                       <a href="{{ route('admin.vendor.verify',['id' => $uData->id, 'status' => 0])}}" class="btn btn-sm btn-light">Un-Verify</a>
                      @endif

                      @if($uData->active==0)
                       <a href="{{ route('admin.vendor.status',['id' => $uData->id, 'status' => 1])}}" class="btn btn-sm btn-primary">Active</a>
                      @else
                       <a href="{{ route('admin.vendor.status',['id' => $uData->id, 'status' => 0])}}" class="btn btn-sm btn-light">De-Active</a>
                      @endif
                       <a href="{{ route('admin.edit-vendor',$uData->id)}}" class="btn btn-sm btn-success">Edit</a>
                    </td>
                </tr>
            @endforeach    
                
            </tbody>
           {{--  <tfoot>
                <tr>
                   <th>Id</th>
                    <th>Name</th>
                    
                    <th>Action</th>
                </tr>
            </tfoot> --}}
        </table>

        
    </div>
  </div>
  <div class="card-footer text-muted">
{{--     {!! $uData->links() !!} --}}
{{--  {{ $uData->links() }} --}}
  </div>
</div>
	
   @endsection
