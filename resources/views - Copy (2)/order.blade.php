@extends('layouts.app')
@section('script')
<script type="text/javascript">
	$(document).ready(function () {
        $('input[name="user_address"]:radio').trigger('change');
        var newAddress=` <div class="form-group col-md-6">
                    <label for="" class="">Name</label>
                    <input type="text" required class="form-control" name="name" value="" placeholder="Name">
               </div>
               <div class="form-group col-md-6">
                    <label for="" class="">Mobile</label>
                    <input type="text" required class="form-control" name="mobile" value="" placeholder="Mobile">
               </div>
               <div class="form-group col-md-12">
                    <label for="" class="">Address</label>
                    <input type="text" required class="form-control" name="address" value="" placeholder="Address">
               </div>
               <div class="form-group col-md-6">
                    <label for="" class="">landmark</label>
                    <input type="text"  class="form-control" name="landmark" value="" placeholder="landmark">
               </div>
                <div class="form-group col-md-6">
                    <label for="" class="">City</label>
                    <input type="text" required class="form-control" name="city" value="" placeholder="City">
               </div>
               <div class="form-group col-md-6">
                    <label for="" class="">State</label>
                    <input type="text" required class="form-control" name="state" value="" placeholder="State">
               </div>
            </div>`;
		$( 'input[name="user_address"]:radio' ).change(function(){
            var val=$(this).val();
           // alert(val);
            if(val=='new'){
                $('#newAddress').html(newAddress);
                $('#newAddress').css('border',' 1px solid gray');
            }else{
                $('#newAddress').html('');
                $('#newAddress').css('border','0px');
            }
        });
    });
    function addAddress(){
        $('#addAddressDiv').hide();
        $('#addressFormDiv').show();
        $('#addressFormDiv').css('display','block!important');

        var order=`<a href="{{ route('order.confirm')}}" style="border:3px solid red;border-radius: 18px;color:red;" class="btn btn-md btn-dangar">Continue</a>`;
        /*$.get('{{ route('ajax.getUserAddress') }}',
          function(data){
             //$('#addressFormDiv').html(data);
              $('#addressFormDiv').append('wfhweehfruewhruehufhuqwehfuerhyfu98fu9qfuruqr');
            if(data==null){
                $('#addressFormDiv').append('no address founds');
            }
            $('#addressFormDiv').append(order);
            $('#addressFormDiv').css('margin-bottom','3rem');

          }
        );*/
    }
</script>
@endsection
@section('content')
	<div style="margin-top: 8.5rem;">
		<div class="container">
            <div class="row">                  
    			<div class="col-12" style="text-align: center;border-bottom: 2px solid gray;">
    				<h4 style="margin-bottom: 8px;"><b>My Orders <small>( {{$count}} items)</small></b></h4>
    			</div>
                <div class="col-8" style="border-right: 3px solid gray;">
    @php
        $total=0;
        $p_arr=array(); $p_temp=0;     // all items Price array
        $d_arr=array(); $d_temp=0;    // all items Delivery array
        $g_arr=array(); $g_temp=0;    // all items GST array
        foreach ($idata as $key => $mvalue) {
            $masters_id='';$masters_name=''; $price=0; $delivery=0; $gst=0;  $m_items=0;
            if(isset($mvalue[0])){
                $m_items=count($mvalue);
                $masters_id=$mvalue[0]['masters_id'];
                $masters_name=$mvalue[0]['masters_name'];
                if($m_items!=0){

                }
                @endphp
                <div class="row" align="center" style="border-bottom: 2px solid gray;background-color: gray;">
                    <h3 class="col-11 text-center" ><b>{{ ucfirst($masters_name) }}</b></h3>
                </div>
                @php
               // echo "<hr>";
                $temp=collect($mvalue);
                $vitems = $temp->groupBy('vendor_id')->toArray();
                foreach ($vitems as $key => $value) {
                    $vendor_id='';$vendor_name='';$vendor_address='';$items=0; $v_price=0;
                    if(isset($value[0])){
                        $items=count($value); 
                        
                        $vendor_id=$value[0]['vendor_id'];
                        $vendor_name=$value[0]['vendor_name'];
                        $vendor_address=$value[0]['vendor_address'];
                        @endphp
                        <div class="row" align="center" style="overflow: hidden;">
                            {{-- <div class="col-1"></div> --}}
                            <h3 class="col-12 text-center" style="background-color: antiquewhite;margin: 2px;" >
                                {{ ucfirst($vendor_name) }}</h3>
                        {{-- </div>  --}}
                            <div class="col-12 row" align="center">
                        @php
                        foreach ($value as $key => $product) {
                           // echo $product['name'];echo "<br>";
                            $t_price=$product['qty']*$product['price'];
                             $price+=$t_price;
                                $v_price+=$t_price;
                        @endphp
                        {{-- <div class="row" align="center"> --}}
                           {{--  <div class="col-1"></div> --}}
                            <div class="container row" style="display: inline-flex;border-top: 2px solid gray;">
                                <h4 class="col-5">{{ ucfirst($product['name']) }}</h4>
                                <h4 class="col-2">{{ $product['attributes']}}</h4>
                                <h4 class="col-2">{{ $product['qty']}}<small> Qty</small></h4>
                                <h4 class="col-3">Rs. {{ $product['price']}}</h4>
                            </div>
                        {{-- </div> --}}
                        @php
                        }
                        if($items!=0){

                            $delivery+=50;
                                $v_price+=50;
                            if($masters_id==4){ $gst+=($v_price*5)/100;}
                            array_push($p_arr,$price);
                            array_push($d_arr,$delivery);
                            array_push($g_arr,$gst);
                             $p_temp=$price;
                             $d_temp=$delivery;
                             $g_temp=$gst; 

                        }
                    }
                    @endphp
                        </div></div>
                    @php
                }
            @endphp
                </div>
            @php
            }
            $total=$p_temp+$d_temp+$g_temp;
            
        }
        /*echo "<hr>";*/
        //echo "item: <pre>",print_r($idata),"</pre></hr>";die();
    @endphp
                	
				{{-- </div> --}}
				<div class="col-4" id="">
                	<div class="container">
                		<table style="width: 100%;">
                      <tr>
                        <td style="text-align: left;">Price:</td>
                        <td style="text-align: right;">Rs. {{number_format($p_temp, 2) }}</td>
                      </tr>
                      <tr>
                        <td style="text-align: left;">Delivery :</td>
                        <td style="text-align: right;">Rs. {{number_format($d_temp, 2) }}</td>
                      </tr>
                      <tr>
                        <td style="text-align: left;">Total:</td>
                        <td style="text-align: right;">Rs. {{number_format($d_temp+$p_temp, 2) }}</td>
                      </tr>
                      <tr>
                        <td style="text-align: left;">GST( <i style="color: gray;">5%</i> Only in Resturant):</td>
                        <td style="text-align: right;">Rs. {{number_format($g_temp, 2) }}</td>
                      </tr>
                    </table>
                		
                	</div>
                   
				</div>
				
				<div class="col-12" style="margin-bottom: 3rem;">
                	<div class="container" style="text-align: end;border-top: 2px solid gray;border-bottom: 2px solid gray;">                		
                		<h3 style="color: red;">Total Amount:&nbsp;&nbsp; Rs. {{number_format(round($total),2)}}</h3>
                	</div>
                    <div class="container" id="addAddressDiv" style="text-align: center;margin-top: 15px;">                       
                        <button onclick="addAddress();" style="border:3px solid red;border-radius: 18px;color:red;" class="btn btn-md btn-dangar">Continue</button>
                    </div>
				</div>
                <div id="addressFormDiv" style="display:none;margin-top: 15px;">
<form method="GET" action="{{route('order.confirm')}}">
    @csrf
    <div class="form-row" >
        <div class="form-group col-md-7">
          <h3 for="inputEmail4">Address</h3>
            @if(isset($address))
            
              @foreach($address as $key => $value)
                <div class="form-check">
                    <input type="radio" class="form-check-input" id="" name="user_address" value="{{$value->id}}">
                    <label class="form-check-label" for="">{{$value->name}} <i>( {{$value->mobile}} )</i><br>
                        {{$value->address}},{{$value->city}}
                        @if($value->states) ,{{$value->states}} @endif
                         @if($value->pincode) <i>({{$value->pincode}})</i>@endif
                    </label>
                </div>
                                           
              @endforeach 
            @endif                               
            <div class="form-check">
                <input type="radio" @if(count($address)==0) {{-- checked --}} @endif  class="form-check-input" id="" name="user_address" value="new">
                <label class="form-check-label" for="">New Address</label>
            </div>
            <div class="form-row" id="newAddress" style="padding: 1rem;margin-top: 1rem;">
              
            </div>
            <hr class="col-md-12">
        <div class="form-group col-md-7">
          <h3 for="inputEmail4">Payment</h3>
                <div class="form-check">
                    <input type="radio" class="form-check-input" id="" name="payment" value="1">
                    <label class="form-check-label" for="">COD(Cash on Delevry)</label>
                </div>
                <div class="form-check">
                    <input type="radio" class="form-check-input" checked id="" name="payment" value="2">
                    <label class="form-check-label" for="">Online</label>
                </div>
        </div>
        <div class="form-group col-md-12">
            <button type="submit" style="border:3px solid red;border-radius: 18px;color:red;" class="btn btn-md btn-dangar">Continue</button>
        </div>
    </div>
</form>
                </div>      

                        
			</div>
		</div>
	</div>
@endsection
@section('style')
<style type="text/css">
	 /* Checkout */
.checkout 
{
	/*background-color: #f6dcf6;*/
	width:100%;
	margin: 0px auto;
	padding: 60px 0px;
	height: auto;
	/*position: sticky;*/
   /* top: 72px;
    z-index: 2;*/


}
.checkout .back {
	border-radius: 5px;
	height:auto;
	background-color: white;
	margin:8px;
}
.checkout .back p {
	font-size: 15px;
    font-weight: 600;
    padding-left: 20px;
    color: #770071;

}
.checkout .total-amount {
	background-color: #fff;
}
.checkout .basket-button a {
	color:white;
	text-decoration: none;
}
.checkout .basket-button button {
	padding:5px;
	border-radius: 7px;
	margin-left: 75px;
    margin-top: 0px;
}
.checkout .basket-button button:hover {
	background-color: #450242;
}
.checkout h4 {
	padding:10px;
	margin-top: 60px;
}
.checkout button {
	/*margin-top: 30px;*/
	padding:15px;
	/*width:184px;*/
	color:white;
	border:1px solid transparent;
	background-color: #770071;
	border-radius: 30px;
	margin-bottom: 10px;
	margin-left:10px;
}
.checkout .amount {
	font-size: 15px;
	font-weight: 600;
	display: contents;
}
.checkout .amount p {
	padding-right: 20px;
}
.checkout .quantity-button {
    border: 2px solid #green;
    border-radius: 10px;
    width: 86px;
   /* margin-bottom: 10px;*/
    /*position: relative;
    left: 31%;*/
    padding: 2.5px;
    display: inline-flex;
    float: right;
}
.checkout .quantity-button .inc-dec {
	/*border: 2px solid #green;*/
	background-color: white;
	font-size:20px;
	font-weight:600;
	border-radius: 10px;

}
.checkout .quantity-button .inc-dec:focus {
	/*border:0px solid #fff!important;*/
	background-color: green;
	color:white;
	border-radius: 10px;

}
.checkout .quantity-button .number-btn {
	
	width:20px;
	border:none;
}

</style>
@endsection