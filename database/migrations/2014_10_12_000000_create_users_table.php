<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique()->nullable();
            $table->string('mobile')->unique()->nullable();
            $table->string('pincode')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->timestamp('mobile_verified_at')->nullable();
            $table->string('password');
                $table->string('tpassword')->nullable();
            /*socialite auth*/
            $table->string('provider')->nullable()->default('Auth');
            $table->string('provider_id')->nullable()->default(0);
                
            $table->string('otp')->nullable();
            $table->string('wallet_id')->unique();
            $table->decimal('ammount', 12, 2)->nullable()->default(0)->comment('Wallet Ammount')->nullable();
            $table->string('image')->nullable();

            $table->string('referrer_code')->unique()->nullable();
            $table->unsignedBigInteger('referrer_id')->nullable();
            $table->timestamp('ordered_at')->nullable();

            $table->rememberToken();
            $table->timestamps();
        });
        $statement = "ALTER TABLE users AUTO_INCREMENT = 111111;";
        DB::unprepared($statement);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
