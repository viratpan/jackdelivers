<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('masters_id');             
//////////////////////////////////////////////////////////////////////////////////////
            $table->string('name')->comment('product name');
            $table->string('slug')->unique();
//////////////////////////////////////////////////////////////////////////////////
            $table->boolean('verify')->default(0);
            $table->timestamp('verified_at')->nullable();
//////////////////////////////////////////////////////////////////////////////////
            $table->string('type')->nullable()->comment('veg,no-veg');
            $table->decimal('costoftwo', 12, 2)->nullable()->comment('Actual price');
            $table->decimal('aprice', 12, 2)->nullable()->comment('Admin price');
            
            $table->unsignedInteger('categry_id');
            $table->unsignedInteger('sub_categry_id');
            $table->unsignedInteger('attrubute_id');

            $table->longText('description')->nullable();
            $table->longText('shortdescription')->nullable();
            $table->text('meta_title')->nullable();
            $table->text('meta_keywords')->nullable();
            $table->text('meta_description')->nullable();

            $table->string('image')->nullable();
            $table->string('image2')->nullable();
            $table->string('image3')->nullable();
            $table->string('image4')->nullable();
/////////////////////////////////////////////////////////////////////////////////////

            $table->unsignedInteger('vendor_id');
             // $table->foreign('vendor_id')->references('id')->on('vendors')->onDelete('cascade');

            $table->boolean('featured')->nullable()->default(0);
            $table->boolean('status')->nullable()->default(0);
            $table->decimal('rating', 4, 2)->nullable()->default(0)->comment('Actual rating');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
