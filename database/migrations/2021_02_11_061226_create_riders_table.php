<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('riders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('mobile')->unique();
            $table->string('password');
            $table->string('image')->nullable();
            $table->string('address')->nullable();
            
            $table->string('dl')->nullable();
                $table->string('dl_image')->nullable();
            $table->string('aadhar')->nullable();
                $table->string('aadhar_image')->nullable();
            $table->date('dl_valid');
            $table->string('pan')->nullable();
                $table->string('pan_image')->nullable();
            /*emergency*/
            $table->string('emergency_mobile')->nullable();
            $table->string('blood_group')->nullable();
             /*rider on duty or not*/
            $table->boolean('on_duty')->nullable()->default(0);
            $table->decimal('rating', 4, 2)->nullable()->default(0)->comment('Actual rating');
            /*last updated location*/
            $table->double('latitude')->nullable();
            $table->double('longitude')->nullable();
            $table->date('location_updated')->nullable();
            /*Account*/
            $table->string('ac_no')->nullable();
            $table->string('ac_name')->nullable();
            $table->string('ac_ifsc')->nullable();
            $table->string('upi')->nullable();

           // $table->date('join_at');
             $table->unsignedInteger('zone_id')->default(0);
            $table->unsignedInteger('approved_by')->nullable()->default(0)->comment('created_by admin ');
                $table->string('approved_image')->nullable();
                
            $table->boolean('active')->nullable()->default(0);
            $table->timestamps();
        });
        $statement = "ALTER TABLE riders AUTO_INCREMENT = 111111;";
        DB::unprepared($statement);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('riders');
    }
}
