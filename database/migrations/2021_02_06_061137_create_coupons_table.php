<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('code')->unique();

            $table->string('type'); //1 discount ,2 wallet ,3 order price less
                $table->decimal('discount', 12, 2)->nullable()->default(0)->comment('get order discount');
                $table->decimal('wallet_amount', 12, 2)->nullable()->default(0)->comment('get wallet price');
                $table->decimal('min_price', 12, 2)->nullable()->default(0)->comment('order price less'); 

            $table->string('for'); //0 All,1 Under Vertical,2 Category,3 Sub-Category,4 Vendor,5 User,6 Product,
                $table->unsignedInteger('masters_id')->nullable()->default(0);
                $table->unsignedInteger('categry_id')->nullable()->default(0);
                $table->unsignedInteger('sub_categry_id')->nullable()->default(0);
                $table->unsignedInteger('vendor_id')->nullable()->default(0);            
                $table->unsignedInteger('user_id')->nullable()->default(0);
                    $table->integer('uses')->nullable()->default(0)->comment('Uses Per user');
                $table->unsignedInteger('product_id')->nullable()->default(0);
                $table->boolean('all')->nullable()->default(0);

            $table->date('start_at');
            $table->date('end_at')->nullable();
            $table->string('order_at')->nullable()->default(0);

            $table->text('description')->nullable();
            
            $table->unsignedInteger('created_by')->nullable()->default(0)->comment('created_by admin ');

            $table->boolean('active')->nullable()->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupons');
    }
}
