<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('status')->nullable()->default(0);
            $table->boolean('confirmed')->nullable()->default(0);
              $table->string('order_id')->unique();
              $table->string('transition_id')->comment('transition_id')->unique();

              $table->unsignedInteger('user_id');              
              $table->unsignedInteger('user_address_id');     

              $table->unsignedInteger('vendor_id');
              $table->unsignedInteger('master_id');
              
              $table->string('otp')->nullable();
              $table->string('suggest')->nullable();
              
              $table->string('items')->nullable();
              
              $table->unsignedInteger('coupon_id')->nullable()->default(0);  
              $table->string('coupon_code')->nullable();
              $table->string('coupon_value')->nullable();

              $table->decimal('discount', 12, 2)->nullable()->default(0)->comment('discount price');
              $table->decimal('shipping', 12, 2)->nullable()->default(0)->comment('shipping price'); 
              $table->decimal('price', 12, 2)->nullable()->default(0)->comment('all order product price');

              $table->decimal('amount', 12, 2)->nullable()->default(0)->comment('discount-shipping+price');
              $table->decimal('tax', 12, 2)->nullable()->default(0)->comment('order tax');
              $table->decimal('total_amount', 12, 2)->nullable()->default(0)->comment('final payable amount');

              $table->string('payment_type')->nullable()->default(1); // 1 cash 2 online

              $table->string('remark')->nullable();
              /*
              |  rider
              */
              $table->unsignedInteger('rider_id')->nullable()->default(0);
             // $table->boolean('rider_status')->nullable()->default(0);
              $table->decimal('distance', 12, 2)->nullable()->default(0)->comment('final distance');
              $table->decimal('delay_bouns', 12, 2)->nullable()->default(0)->comment('final payable amount');
               ///vendor store lat lng
                $table->string('start_latitude')->nullable();
                $table->string('start_longitude')->nullable();
            ///user address lat lng
                $table->string('end_latitude')->nullable();
                $table->string('end_longitude')->nullable();

              $table->timestamp('delivered_at')->nullable();
              $table->boolean('rated')->nullable()->default(0);
              /*
              | order current location
              */
              $table->string('current_location')->nullable();
              $table->string('last_location')->nullable();
              
              /*
              | per order our & vendor amount
              */
              $table->decimal('our_amount', 12, 2)->nullable()->default(0)->comment('our commission amount')->nullable();
              $table->decimal('vendor_amount', 12, 2)->nullable()->default(0)->comment('vendor pay amount')->nullable();
              $table->boolean('vendor_pay')->nullable()->default(0);
              

            $table->timestamps();
        });

        $statement = "ALTER TABLE orders AUTO_INCREMENT = 100000;";
        DB::unprepared($statement);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
