<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRiderPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rider_payments', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('rider_id')->default(0);
            $table->unsignedInteger('order_id')->default(0);
            ///vendor store lat lng
                $table->string('start_latitude')->nullable();
                $table->string('start_longitude')->nullable();
            ///user address lat lng
                $table->string('end_latitude')->nullable();
                $table->string('end_longitude')->nullable();
            $table->string('distance')->nullable()->default(0);
            $table->string('km_price')->nullable()->default(0)->comment('cost per Km.');
            $table->string('wait_price')->nullable()->default(0)->comment('waiting charge');
            $table->string('ontime_price')->nullable()->default(0)->comment('delivery on-time.');

            $table->string('amount')->nullable()->default(0)->comment('amount');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rider_payments');
    }
}
