<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeliveryCostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_costs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('master_id')->nullable();
            $table->unsignedBigInteger('vendor_id')->nullable();
             $table->string('remark')->nullable();
             $table->decimal('cost', 12, 2)->nullable()->default(0)->comment('delivery_costs')->nullable();
             $table->boolean('active')->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_costs');
    }
}
