<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('masters', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('slug')->unique();
            $table->string('image')->nullable();
            $table->string('search_text')->nullable();

            $table->decimal('commission', 12, 2)->nullable()->default(0)->comment('commission %');
            $table->decimal('gst', 12, 2)->nullable()->default(0)->comment('gst %');
            $table->decimal('delivery_cost', 12, 2)->nullable()->default(0)->comment('delivery cost per KM');

            /*$table->decimal('packing_cost', 12, 2)->nullable()->default(0)->comment('packing cost');
            */

            /*$table->string('delivery_cost_by')->nullable()->default(0)->comment('delivery cost- 0 none , 1 price ,2 distance, 3 weight');
            $table->string('delivery_cost_km')->nullable()->default(0)->comment('delivery cost per km');

            $table->string('delivery_cost_p1')->nullable()->default(0)->comment('delivery cost under Price');
            $table->string('delivery_cost_p2')->nullable()->default(0)->comment('delivery cost under Price');
            $table->string('delivery_cost_p3')->nullable()->default(0)->comment('delivery cost under Price');*/

            $table->unsignedInteger('cancellation_till')->nullable()->default(0)->comment('order cancellation till before status');
            $table->decimal('cancellation_cost', 12, 2)->nullable()->default(0)->comment('total price/* %');
            $table->boolean('returnable')->nullable()->default(0);
            $table->boolean('order')->nullable()->default(0);
            $table->boolean('active')->nullable()->default(0);
            $table->timestamps();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('masters');
    }
}
