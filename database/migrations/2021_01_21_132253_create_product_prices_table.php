<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_prices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('qty')->comment('product quantity');
            $table->string('inventory')->nullable()->comment('product inventory');
           /* $table->unsignedInteger('attrubute_id');*/
            /*    $table->foreign('attrubute_id')->references('id')->on('attributes')->onDelete('cascade');
*/
            $table->decimal('cost', 12, 2)->nullable()->comment('Actual price');
            $table->decimal('price', 12, 2)->nullable()->comment('Sale price');
            $table->decimal('discount', 12, 2)->nullable()->comment('Sales discount');

            $table->unsignedInteger('product_id');
                //$table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_prices');
    }
}
