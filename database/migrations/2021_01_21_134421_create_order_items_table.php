<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_items', function (Blueprint $table) {
            $table->increments('id');
              $table->boolean('status')->nullable()->default(0);
              $table->boolean('confirmed')->nullable()->default(0);
              $table->unsignedInteger('order_id');
              $table->unsignedInteger('order_rid');

              $table->unsignedInteger('product_id');
              $table->unsignedInteger('product_price_id');
              $table->unsignedInteger('vendor_id');

              $table->decimal('cost', 12, 2)->default(0)->comment('product cost')->nullable();
              $table->decimal('discount', 12, 2)->nullable()->default(0);
              $table->decimal('price', 12, 2)->default(0)->comment('product price')->nullable();
              $table->integer('qty')->comment(' prduct qty like 1,2,3..')->default(1);
              $table->decimal('amount', 12, 2)->nullable()->default(0)->comment('Price*Qty')->nullable();             
              
             
            $table->timestamps();
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_items');
    }
}
