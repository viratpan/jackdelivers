<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVendorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('mobile')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
/*////////////////////////////////////////////////////////////////////////////////*/
            $table->boolean('verify')->default(0);
            $table->timestamp('verified_at')->nullable();             
            $table->boolean('active')->default(0);
/*///////////////////////////////////////////////////////////////////////////*/
            $table->string('org_type')->nullable(); 
            $table->string('company_name')->unique();
            $table->string('slug')->unique();
            $table->string('designation')->nullable();
            $table->string('telephone')->nullable();
            $table->string('aadhar')->nullable()->comment('Aadhar No');
            $table->string('pan')->nullable()->comment('PAN no');
            $table->string('gstin')->nullable()->comment('Goods and Services Tax Identification Number');
            $table->string('fssai')->nullable()->comment('Food Safety & Standards Authority of India');
                $table->string('aadharupload')->nullable()->comment('Aadhar Upload ');
                $table->string('panupload')->nullable()->comment('PAN Upload');
                $table->string('gstinupload')->nullable()->comment('gstin Upload');
                $table->string('fssaiupload')->nullable()->comment('fssai Upload');
/*//////////////////////////////////////////////////////////////////////////*/
            $table->string('city')->nullable();
            $table->string('address')->nullable();
/*///////////////////////////////////////////////////////////////////////////*/
            $table->string('pin')->nullable();
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
/*//////////////////////////////////////////////////////////////////////////*/
            //$table->string('categories')->nullable();
            $table->unsignedInteger('masters_id');
            $table->unsignedInteger('category_id')->nullable()->default(0);
            //$table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
/*//////////////////////////////////////////////////////////////////////////*/
          //  $table->string('commission')->nullable();
            $table->unsignedInteger('commission_id')->nullable()->default(0);
           // $table->foreign('commission_id')->references('id')->on('commissions')->onDelete('cascade');

            $table->string('image')->nullable();
            $table->string('coverimage')->nullable();
            $table->string('menu')->nullable()->comment('resturent menu image');

            $table->string('ontime')->nullable();
            $table->string('offtime')->nullable();
            $table->boolean('open')->default(0);// 1 open 2 close
            
            $table->decimal('costoftwo', 12, 2)->nullable()->comment('Actual price');

            $table->longText('about')->nullable();

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendors');
    }
}
