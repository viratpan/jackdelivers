<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
             $table->boolean('status')->nullable()->default(0);
             $table->string('transition_id')->comment('transition_id');
             $table->string('payment_type')->nullable()->default(0);
              $table->string('OD_id')->comment('all orders id');
              $table->string('order_id')->nullable();
        /*////////////////////////////////////////////////////////*/
              $table->string('coupon_code')->nullable();

              $table->decimal('discount', 12, 2)->nullable()->default(0);
              $table->decimal('shipping', 12, 2)->nullable()->default(0); 
              $table->decimal('price', 12, 2)->nullable()->default(0)->comment('all order product price');

              $table->decimal('amount', 12, 2)->nullable()->default(0)->comment('discount-shipping+price');
              $table->decimal('tax', 12, 2)->nullable()->default(0)->comment('order tax');
              $table->decimal('total_amount', 12, 2)->nullable()->default(0)->comment('final payable amount');

        /*////////////////////////////////////////////////////////*/
            
            $table->decimal('recived_amount', 12, 2)->nullable()->default(0)->comment('final payable amount');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
