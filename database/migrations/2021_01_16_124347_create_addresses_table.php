<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
          $table->increments('id');
          $table->string('name');
          $table->string('address')->nullable();
          $table->string('mobile')->nullable();
          $table->string('pincode')->nullable();
          $table->string('landmark')->nullable();
          $table->string('state')->nullable();
          $table->string('city')->nullable();
          $table->string('address_status')->nullable();          
            $table->double('latitude')->nullable();
            $table->double('longitude')->nullable();
          $table->unsignedInteger('user_id');
          //$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
