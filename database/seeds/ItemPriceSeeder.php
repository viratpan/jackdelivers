<?php

use Illuminate\Database\Seeder;
use App\Product_price;

class ItemPriceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       //Product_price::create( []); 

			Product_price::create( [
			'id'=>1,
			'qty'=>'1',
			'inventory'=>NULL,
			'cost'=>150.00,
			'price'=>146.10,
			'discount'=>2.60,
			'product_id'=>1,
			'created_at'=>'2021-02-01 18:48:41',
			'updated_at'=>'2021-02-01 18:48:41'
			] );

						

			Product_price::create( [
			'id'=>2,
			'qty'=>'8',
			'inventory'=>NULL,
			'cost'=>20.00,
			'price'=>19.35,
			'discount'=>3.25,
			'product_id'=>3,
			'created_at'=>'2021-02-01 19:42:35',
			'updated_at'=>'2021-02-01 19:42:35'
			] );

						

			Product_price::create( [
			'id'=>3,
			'qty'=>'4',
			'inventory'=>NULL,
			'cost'=>20.00,
			'price'=>19.14,
			'discount'=>4.30,
			'product_id'=>4,
			'created_at'=>'2021-02-01 19:43:47',
			'updated_at'=>'2021-02-01 19:43:47'
			] );

						

			Product_price::create( [
			'id'=>4,
			'qty'=>'1',
			'inventory'=>NULL,
			'cost'=>20.00,
			'price'=>19.64,
			'discount'=>1.80,
			'product_id'=>5,
			'created_at'=>'2021-02-01 19:45:35',
			'updated_at'=>'2021-02-01 19:45:35'
			] );

						

			Product_price::create( [
			'id'=>5,
			'qty'=>'1',
			'inventory'=>NULL,
			'cost'=>200.00,
			'price'=>185.50,
			'discount'=>7.25,
			'product_id'=>6,
			'created_at'=>'2021-02-01 19:46:47',
			'updated_at'=>'2021-02-01 19:46:47'
			] );

						

			Product_price::create( [
			'id'=>6,
			'qty'=>'1',
			'inventory'=>NULL,
			'cost'=>40.00,
			'price'=>37.33,
			'discount'=>6.67,
			'product_id'=>7,
			'created_at'=>'2021-02-01 19:47:18',
			'updated_at'=>'2021-02-01 19:47:18'
			] );

						

			Product_price::create( [
			'id'=>7,
			'qty'=>'1',
			'inventory'=>NULL,
			'cost'=>30.00,
			'price'=>29.02,
			'discount'=>3.25,
			'product_id'=>8,
			'created_at'=>'2021-02-01 19:47:45',
			'updated_at'=>'2021-02-01 19:47:45'
			] );

						

			Product_price::create( [
			'id'=>8,
			'qty'=>'1',
			'inventory'=>NULL,
			'cost'=>60.00,
			'price'=>56.60,
			'discount'=>5.67,
			'product_id'=>9,
			'created_at'=>'2021-02-01 19:48:15',
			'updated_at'=>'2021-02-01 19:48:15'
			] );

						

			Product_price::create( [
			'id'=>9,
			'qty'=>'1',
			'inventory'=>NULL,
			'cost'=>100.00,
			'price'=>92.33,
			'discount'=>7.67,
			'product_id'=>10,
			'created_at'=>'2021-02-01 19:48:46',
			'updated_at'=>'2021-02-01 19:48:46'
			] );

						

			Product_price::create( [
			'id'=>10,
			'qty'=>'1',
			'inventory'=>NULL,
			'cost'=>100.00,
			'price'=>97.40,
			'discount'=>2.60,
			'product_id'=>11,
			'created_at'=>'2021-02-01 19:49:13',
			'updated_at'=>'2021-02-01 19:49:13'
			] );

						

			Product_price::create( [
			'id'=>11,
			'qty'=>'1',
			'inventory'=>NULL,
			'cost'=>180.00,
			'price'=>163.35,
			'discount'=>9.25,
			'product_id'=>12,
			'created_at'=>'2021-02-01 19:49:36',
			'updated_at'=>'2021-02-01 19:49:36'
			] );

						

			Product_price::create( [
			'id'=>12,
			'qty'=>'10',
			'inventory'=>NULL,
			'cost'=>80.00,
			'price'=>77.84,
			'discount'=>2.70,
			'product_id'=>13,
			'created_at'=>'2021-02-01 19:50:10',
			'updated_at'=>'2021-02-01 19:50:10'
			] );

						

			Product_price::create( [
			'id'=>13,
			'qty'=>'1',
			'inventory'=>NULL,
			'cost'=>220.00,
			'price'=>200.42,
			'discount'=>8.90,
			'product_id'=>14,
			'created_at'=>'2021-02-01 19:50:35',
			'updated_at'=>'2021-02-01 19:50:35'
			] );

						

			Product_price::create( [
			'id'=>14,
			'qty'=>'1',
			'inventory'=>NULL,
			'cost'=>110.00,
			'price'=>100.92,
			'discount'=>8.25,
			'product_id'=>15,
			'created_at'=>'2021-02-01 19:51:06',
			'updated_at'=>'2021-02-01 19:51:06'
			] );

						

			Product_price::create( [
			'id'=>15,
			'qty'=>'4',
			'inventory'=>NULL,
			'cost'=>150.00,
			'price'=>137.00,
			'discount'=>8.67,
			'product_id'=>16,
			'created_at'=>'2021-02-01 19:51:31',
			'updated_at'=>'2021-02-01 19:51:31'
			] );

						

			Product_price::create( [
			'id'=>16,
			'qty'=>'Half',
			'inventory'=>NULL,
			'cost'=>70.00,
			'price'=>65.10,
			'discount'=>7.00,
			'product_id'=>17,
			'created_at'=>'2021-02-01 19:51:55',
			'updated_at'=>'2021-02-01 19:51:55'
			] );
			Product_price::create( [
			'id'=>23,
			'qty'=>'Full',
			'inventory'=>NULL,
			'cost'=>140.00,
			'price'=>130.20,
			'discount'=>7.00,
			'product_id'=>17,
			'created_at'=>'2021-02-01 19:51:55',
			'updated_at'=>'2021-02-01 19:51:55'
			] );

						

			Product_price::create( [
			'id'=>17,
			'qty'=>'1',
			'inventory'=>NULL,
			'cost'=>80.00,
			'price'=>77.86,
			'discount'=>2.67,
			'product_id'=>18,
			'created_at'=>'2021-02-01 19:52:20',
			'updated_at'=>'2021-02-01 19:52:20'
			] );

						

			Product_price::create( [
			'id'=>18,
			'qty'=>'1',
			'inventory'=>NULL,
			'cost'=>32.00,
			'price'=>30.90,
			'discount'=>3.45,
			'product_id'=>19,
			'created_at'=>'2021-02-01 19:52:42',
			'updated_at'=>'2021-02-01 19:52:42'
			] );

						

			Product_price::create( [
			'id'=>19,
			'qty'=>'1',
			'inventory'=>NULL,
			'cost'=>250.00,
			'price'=>227.75,
			'discount'=>8.90,
			'product_id'=>20,
			'created_at'=>'2021-02-01 19:53:13',
			'updated_at'=>'2021-02-01 19:53:13'
			] );

						

			Product_price::create( [
			'id'=>20,
			'qty'=>'1',
			'inventory'=>NULL,
			'cost'=>45.00,
			'price'=>42.84,
			'discount'=>4.80,
			'product_id'=>21,
			'created_at'=>'2021-02-01 19:53:38',
			'updated_at'=>'2021-02-01 19:53:38'
			] );

						

			Product_price::create( [
			'id'=>21,
			'qty'=>'1',
			'inventory'=>NULL,
			'cost'=>50.00,
			'price'=>47.00,
			'discount'=>6.00,
			'product_id'=>22,
			'created_at'=>'2021-02-01 19:54:03',
			'updated_at'=>'2021-02-01 19:54:03'
			] );

						

			Product_price::create( [
			'id'=>22,
			'qty'=>'1',
			'inventory'=>NULL,
			'cost'=>110.00,
			'price'=>110.00,
			'discount'=>0.00,
			'product_id'=>2,
			'created_at'=>'2021-02-03 02:04:29',
			'updated_at'=>'2021-02-03 02:04:29'
			] );

			


    }
}
