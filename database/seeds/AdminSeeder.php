<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->insert([
            'name' => 'Admin',
            'email' => 'admin@gmail.com',
            'isAdmin'=>1,
            'password' => Hash::make("password"),//'$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'email_verified_at' =>now(),
            'active' =>1,
            'remember_token' => Str::random(10),
        ]);

        
       
       /* DB::table('commissions')->insert([
            ['name' => 'Daily','commission'=>18.8],
            ['name' => 'Weekly','commission'=>12],
            ['name' => 'Monthly','commission'=>4],
            ['name' => 'Yearly','commission'=>2]           

        ]);*/
        
        DB::table('masters')->insert([
            ['name' => 'Fruits & Vegetables','slug' => 'fruits-vegetables','image'=>'images/master/04022021-1612450680AWtQsI.jpeg','order'=>3,'active'=>0,'search_text'=>'Search for Fruits & Vegetables or Stores'],
            ['name' => 'Groceries','slug' => 'groceries','image'=>'images/master/04022021-1612450691IUEUrQ.jpeg','order'=>2,'active'=>0,'search_text'=>'Search for groceries items or stores'],
            ['name' => 'Medicines','slug' => 'medicines','image'=>'images/master/04022021-1612450715p7vrYC.jpeg','order'=>4,'active'=>0,'search_text'=>'Search for stores or Medicines '],
            ['name' => 'Restaurants','slug' => 'restaurants','image'=>'images/master/04022021-1612450725zwWPjO.jpeg','order'=>1,'active'=>1,'search_text'=>'Search for restaurants or dishes'],

        ]);


        DB::table('vendors')->insert([
            'name' => 'viratpanwar007',
            'email' => 'viratpanwar007@gmail.com',
            'mobile' => '8475072504',
            'company_name' => 'VP Pvt.LTD',
            'slug' => '8475072504',
            'email_verified_at' =>now(),
            'verified_at' =>now(),
            'verify' =>1,
            'active' =>1,
            'masters_id'=>1,
            'category_id'=>0,
            'commission_id'=>1,
            'remember_token' => Str::random(10),
            'password' => Hash::make('password'),
        ]);
        DB::table('cities')->insert([
            ['name' => 'Delhi','slug' => 'delhi'],
            ['name' => 'Indrapuram','slug' => 'indrapuram'],
            ['name' => 'Noida','slug' => 'noida'],
            ['name' => 'Bulandshahr','slug' => 'bulandshahr'],
            ['name' => 'Meerut','slug' => 'meerut'],

        ]);
        if (\DB::table('order_statuses')->count() > 0) {
            return;
        }else{
            DB::table('order_statuses')->insert([
                ['name' => 'Pending'],
                ['name' => 'Confirm'],
                ['name' => 'Baken/Packing'],
                ['name' => 'Dispatch'],
                ['name' => 'Delivered'],
                ['name' => 'Return'],
                ['name' => 'Cancelled By User'],
                ['name' => 'Cancelled By Vendor'],
                ['name' => 'Cancelled By Admin'],
                ['name' => 'Suggect'],

            ]);
        }
        
        DB::table('attributes')->insert([
            ['name' => 'Kg','masters_id'=>1],
            ['name' => 'Dozen','masters_id'=>1],['name' => 'Packet','masters_id'=>2],
            ['name' => 'ml','masters_id'=>1],
            ['name' => 'Ltr','masters_id'=>1],['name' => '1kg Packet','masters_id'=>2],
            ['name' => 'Plate','masters_id'=>4],

        ]);
       


        DB::table('categories')->insert([
            ['name' => 'Fresh Vegetables','slug' => 'fresh-vegetables','order'=>1,'masters_id'=>1],
            ['name' => 'Herbs & Seasonings','slug' => 'herbs-seasonings','order'=>2,'masters_id'=>1],
            ['name' => 'Fresh Fruits','slug' => 'fresh-fruits','order'=>3,'masters_id'=>1],
            ['name' => 'Exotic Fruits & Veggies','slug' => 'exotic-fruits-veggies','order'=>4,'masters_id'=>1],
            ['name' => 'Organic Fruits & Vegetables','slug' => 'organic-fruits-vegetables','order'=>5,'masters_id'=>1],
            ['name' => 'Cuts & Sprouts','slug' => 'cuts-sprouts','order'=>6,'masters_id'=>1],
            ['name' => 'Flower Bouquets, Bunches','slug' => 'flower-bouquets-bunches','order'=>7,'masters_id'=>1],
            ['name' => 'Main Course','slug' => 'main-course','order'=>8,'masters_id'=>4],
            ['name' => 'Chinese & Snacks','slug' => 'chinese-snacks','order'=>9,'masters_id'=>4],
            ['name' => 'Rice','slug' => 'rice','order'=>10,'masters_id'=>4],
            ['name' => 'Paratha','slug' => 'paratha','order'=>11,'masters_id'=>4],

        ]);


         DB::table('sub_categories')->insert([            
            ['name'=>'Potato, Onion & Tomato','slug'=>'potato-onion-tomato','image'=>'images/subcategory/03022021-1612329413ECgKDw.png','categry_id'=>3,'masters_id'=>1],
            ['name'=>'Leafy Vegetables','slug'=>'leafy-vegetables','image'=>'images/subcategory/03022021-1612329768pUtkSN.jpg','categry_id'=>3,'masters_id'=>1],
            ['name'=>'Root Vegetables','slug'=>'root-vegetables','image'=>'images/subcategory/03022021-1612329957hooX4y.jpg','categry_id'=>3,'masters_id'=>1],
            ['name'=>'Cucumber & Capsicum','slug'=>'cucumber-capsicum','image'=>'images/subcategory/03022021-16123300351Bby9b.crdownload','categry_id'=>3,'masters_id'=>1],
            ['name'=>'Cabbage & Cauliflower','slug'=>'cabbage-cauliflower','image'=>'images/subcategory/03022021-1612330104WAPF33.crdownload','categry_id'=>3,'masters_id'=>1],
            ['name'=>'Beans, Brinjals & Okra','slug'=>'beans-brinjals-okra','image'=>'images/subcategory/03022021-1612330163mtN98Q.crdownload','categry_id'=>3,'masters_id'=>1],
            ['name'=>'Gourd, Pumpkin, Drumstick','slug'=>'gourd-pumpkin-drumstick','image'=>'images/subcategory/03022021-1612330210gUIm9g.png','categry_id'=>3,'masters_id'=>1],
            ['name'=>'Specialty','slug'=>'specialty','image'=>'images/subcategory/03022021-1612330266kklDj6.png','categry_id'=>3,'masters_id'=>1],
            ['name'=>'Indian & Exotic Herbs','slug'=>'indian-exotic-herbs','image'=>'images/subcategory/03022021-1612330313aMdAwB.png','categry_id'=>4,'masters_id'=>1],
            ['name'=>'Lemon, Ginger & Garlic','slug'=>'lemon-ginger-garlic','image'=>'images/subcategory/03022021-16123303686GQhWd.png','categry_id'=>4,'masters_id'=>1],
            ['name'=>'Apples & Pomegranate','slug'=>'apples-pomegranate','image'=>'images/subcategory/03022021-1612330427F62N30.crdownload','categry_id'=>5,'masters_id'=>1],
            ['name'=>'Banana, Sapota & Papaya','slug'=>'banana-sapota-papaya','image'=>'images/subcategory/03022021-1612330493ru4sZ3.jpg','categry_id'=>5,'masters_id'=>1],
            ['name'=>'Kiwi, Melon, Citrus Fruit','slug'=>'kiwi-melon-citrus-fruit','image'=>'images/subcategory/03022021-1612330618ZeEzCS.png','categry_id'=>5,'masters_id'=>1],
            ['name'=>'Mangoes','slug'=>'mangoes','image'=>'images/subcategory/03022021-1612330670jst42v.jpg','categry_id'=>5,'masters_id'=>1],
            ['name'=>'Seasonal Fruits','slug'=>'seasonal-fruits','image'=>'images/subcategory/03022021-1612330729I6aVLq.png','categry_id'=>5,'masters_id'=>1],
            ['name'=>'Aloo Paratha','slug'=>'aloo-paratha','image'=>'images/subcategory/03022021-16123307749sRH9S.jpg','categry_id'=>11,'masters_id'=>0],
            ['name'=>'Aloo Pyaz Paratha','slug'=>'aloo-pyaz-paratha','image'=>'images/subcategory/03022021-1612330825HN5f1t.jpg','categry_id'=>11,'masters_id'=>0],
            ['name'=>'Chilli Garlic Paratha','slug'=>'chilli-garlic-paratha','image'=>'images/subcategory/03022021-16123308753BHU2O.jpg','categry_id'=>11,'masters_id'=>0],
            ['name'=>'Cheese Paratha','slug'=>'cheese-paratha','image'=>'images/subcategory/03022021-1612330927I1UmrW.jpg','categry_id'=>11,'masters_id'=>0],
            ['name'=>'Chefs Special Pizza Paratha','slug'=>'chefs-special-pizza-paratha','image'=>'images/subcatSandwiches','slug'=>'sandwiches','image'=>'images/subcategory/03022021-1612331146I8pbCA.jpg','categry_id'=>9,'masters_id'=>0],
            ['name'=>'Gravy Vegetable','slug'=>'rajma','image'=>'images/subcategory/03022021-1612331240NX3g7t.jpg','categry_id'=>8,'masters_id'=>0],
            ['name'=>'Maggi','slug'=>'maggi','image'=>'images/subcategory/03022021-1612331287tKapvN.jpg','categry_id'=>9,'masters_id'=>0],
            ['name'=>'Pakodas','slug'=>'pakodas','image'=>'images/subcategory/03022021-1612331330nQv6Kg.jpg','categry_id'=>9,'masters_id'=>0],
            ['name'=>'Biryani','slug'=>'biryani','image'=>'images/subcategory/03022021-1612331396wSx1yV.jpg','categry_id'=>10,'masters_id'=>0],
            ['name'=>'Exotic Fruits','slug'=>'exotic-fruits','image'=>'','categry_id'=>6,'masters_id'=>0],
            ['name'=>'Exotic Vegetables','slug'=>'exotic-vegetables','image'=>'','categry_id'=>6,'masters_id'=>0],
            ['name'=>'Cut & Peeled Veggies','slug'=>'cut-peeled-veggies','image'=>'','categry_id'=>8,'masters_id'=>0],
            ['name'=>'Cut Fruit, Tender Coconut','slug'=>'cut-fruit-tender-coconut','image'=>'','categry_id'=>8,'masters_id'=>0],
            ['name'=>'Recipe Packs','slug'=>'recipe-packs','image'=>'','categry_id'=>8,'masters_id'=>0],
            ['name'=>'Fresh Salads & Sprouts','slug'=>'fresh-salads-sprouts','image'=>'','categry_id'=>8,'masters_id'=>0],
           
            ['name'=>'Marigold','slug'=>'marigold','image'=>'','categry_id'=>9,'masters_id'=>0],
            ['name'=>'Roses','slug'=>'roses','image'=>'','categry_id'=>9,'masters_id'=>0],
            ['name'=>'Other Flowers','slug'=>'other-flowers','image'=>'','categry_id'=>9,'masters_id'=>0],


        ]);
    }
}
