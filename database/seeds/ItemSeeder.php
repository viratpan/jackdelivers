<?php

use Illuminate\Database\Seeder;
use App\Product;

class ItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::create( [
			'id'=>1,
			'masters_id'=>4,
			'name'=>'Chole1',
			'slug'=>'chole',
			'verify'=>1,
			'verified_at'=>'2021-02-01 21:46:50',
			'type'=>NULL,
			'costoftwo'=>NULL,
			'aprice'=>150.00,
			'categry_id'=>8,
			'sub_categry_id'=>22,
			'attrubute_id'=>3,
			'description'=>'Chole may be referred to as Punjabi Chole Masala or Chana Masala as it is made from white chana, a large variety of light colored chickpeas, and flavored with spicy masala seasonings.',
			'shortdescription'=>NULL,
			'meta_title'=>'Chole',
			'meta_keywords'=>'Chole',
			'meta_description'=>'Chole',
			'image'=>'images/item/03022021-16123314350stXbY.jpg',
			'image2'=>NULL,
			'image3'=>NULL,
			'image4'=>NULL,
			'vendor_id'=>5,
			'featured'=>0,
			'status'=>1,
			'rating'=>0.00,
			'created_at'=>'2021-02-01 18:18:54',
			'updated_at'=>'2021-02-02 16:59:26'
		] );

			

	Product::create( [
	'id'=>2,
	'masters_id'=>4,
	'name'=>'Matar Paneer',
	'slug'=>'matar-paneer',
	'verify'=>1,
	'verified_at'=>'2021-02-01 18:23:21',
	'type'=>NULL,
	'costoftwo'=>NULL,
	'aprice'=>110.00,
	'categry_id'=>8,
	'sub_categry_id'=>22,
	'attrubute_id'=>7,
	'description'=>'Matar paneer is a restaurant style creamy, delicious and rich peas paneer curry. Matar paneer is one of India\'s most popular paneer dishes.',
	'shortdescription'=>NULL,
	'meta_title'=>'Matar Paneer',
	'meta_keywords'=>'Matar Paneer',
	'meta_description'=>'Matar paneer is one of India\'s most popular paneer dishes.',
	'image'=>'images/item/03022021-1612331484UGogIM.jpg',
	'image2'=>NULL,
	'image3'=>NULL,
	'image4'=>NULL,
	'vendor_id'=>5,
	'featured'=>0,
	'status'=>1,
	'rating'=>0.00,
	'created_at'=>'2021-02-01 18:23:21',
	'updated_at'=>'2021-02-02 13:21:26'
	] );

				

	Product::create( [
	'id'=>3,
	'masters_id'=>4,
	'name'=>'Veg Sandwich',
	'slug'=>'veg-sandwich',
	'verify'=>1,
	'verified_at'=>'2021-02-01 18:27:15',
	'type'=>NULL,
	'costoftwo'=>NULL,
	'aprice'=>20.00,
	'categry_id'=>9,
	'sub_categry_id'=>21,
	'attrubute_id'=>3,
	'description'=>'Vegetable sandwich is a type of vegetarian sandwich consisting of a vegetable filling between bread. There are no set requirements other than the use of vegetables, and sandwiches may be toasted or untoasted.',
	'shortdescription'=>NULL,
	'meta_title'=>'Veg Sandwich',
	'meta_keywords'=>'Veg Sandwich',
	'meta_description'=>'Vegetable sandwich is a type of vegetarian sandwich consisting of a vegetable filling between bread.',
	'image'=>'images/item/03022021-1612331539wWCvWQ.jpg',
	'image2'=>NULL,
	'image3'=>NULL,
	'image4'=>NULL,
	'vendor_id'=>5,
	'featured'=>0,
	'status'=>1,
	'rating'=>0.00,
	'created_at'=>'2021-02-01 18:27:15',
	'updated_at'=>'2021-02-02 13:22:21'
	] );

				

	Product::create( [
	'id'=>4,
	'masters_id'=>4,
	'name'=>'Grilled Sandwich',
	'slug'=>'grilled-sandwich',
	'verify'=>1,
	'verified_at'=>'2021-02-01 18:35:44',
	'type'=>NULL,
	'costoftwo'=>NULL,
	'aprice'=>20.00,
	'categry_id'=>9,
	'sub_categry_id'=>21,
	'attrubute_id'=>3,
	'description'=>'The stuffing of grilled sandwiches will vary from a combination of cheeses to myraid of vegetables like potatoes, onions, cucumbers, tomatoes.',
	'shortdescription'=>NULL,
	'meta_title'=>'Grilled Sandwich',
	'meta_keywords'=>'Grilled Sandwich',
	'meta_description'=>'Grilled Sandwich',
	'image'=>'images/item/03022021-1612331584NieWmf.png',
	'image2'=>NULL,
	'image3'=>NULL,
	'image4'=>NULL,
	'vendor_id'=>5,
	'featured'=>0,
	'status'=>1,
	'rating'=>0.00,
	'created_at'=>'2021-02-01 18:35:44',
	'updated_at'=>'2021-02-02 13:23:06'
	] );

				

	Product::create( [
	'id'=>5,
	'masters_id'=>4,
	'name'=>'Plain Maggi',
	'slug'=>'plain-maggi',
	'verify'=>1,
	'verified_at'=>'2021-02-01 18:40:29',
	'type'=>NULL,
	'costoftwo'=>NULL,
	'aprice'=>20.00,
	'categry_id'=>9,
	'sub_categry_id'=>23,
	'attrubute_id'=>6,
	'description'=>'Vegetable Masala Maggi is a quick & easy Indian snack made by tossing maggi noodles along with mixed vegetables and spices.',
	'shortdescription'=>NULL,
	'meta_title'=>'Plain Maggi',
	'meta_keywords'=>'Plain Maggi',
	'meta_description'=>'Vegetable Masala Maggi is a quick & easy Indian snack made by tossing maggi noodles along with mixed vegetables and spices.',
	'image'=>'images/item/03022021-1612331603f65YP8.jpg',
	'image2'=>NULL,
	'image3'=>NULL,
	'image4'=>NULL,
	'vendor_id'=>5,
	'featured'=>0,
	'status'=>1,
	'rating'=>0.00,
	'created_at'=>'2021-02-01 18:40:29',
	'updated_at'=>'2021-02-02 13:23:25'
	] );

				

	Product::create( [
	'id'=>6,
	'masters_id'=>4,
	'name'=>'Paneer Biryani',
	'slug'=>'paneer-biryani',
	'verify'=>1,
	'verified_at'=>'2021-02-01 19:00:30',
	'type'=>NULL,
	'costoftwo'=>NULL,
	'aprice'=>200.00,
	'categry_id'=>10,
	'sub_categry_id'=>25,
	'attrubute_id'=>1,
	'description'=>'Paneer biryani is an Indian dish made with paneer, basmati rice, spices & herbs. This paneer biryani is unique, flavorful & amazingly delicious. Biryani is most commonly made with meat but this recipe uses paneer aka Indian cottage cheese.',
	'shortdescription'=>NULL,
	'meta_title'=>'Paneer Biryani',
	'meta_keywords'=>'Paneer Biryani',
	'meta_description'=>'Paneer biryani is an Indian dish made with paneer, basmati rice, spices & herbs',
	'image'=>'images/item/03022021-1612331656mkP3AY.jpg',
	'image2'=>NULL,
	'image3'=>NULL,
	'image4'=>NULL,
	'vendor_id'=>5,
	'featured'=>0,
	'status'=>1,
	'rating'=>0.00,
	'created_at'=>'2021-02-01 19:00:30',
	'updated_at'=>'2021-02-02 13:24:18'
	] );

				

	Product::create( [
	'id'=>7,
	'masters_id'=>4,
	'name'=>'Aloo Paratha',
	'slug'=>'aloo-paratha',
	'verify'=>1,
	'verified_at'=>'2021-02-01 19:02:50',
	'type'=>NULL,
	'costoftwo'=>NULL,
	'aprice'=>40.00,
	'categry_id'=>11,
	'sub_categry_id'=>16,
	'attrubute_id'=>3,
	'description'=>'Aloo Paratha is a breakfast dish originated from the Punjab region. The recipe is one of the most popular breakfast dishes throughout the western, central and northern regions of India.',
	'shortdescription'=>NULL,
	'meta_title'=>'Aloo Paratha',
	'meta_keywords'=>'Aloo Paratha',
	'meta_description'=>'Aloo Paratha is a breakfast dish originated from the Punjab region',
	'image'=>'images/item/03022021-1612331674DbMACh.jpg',
	'image2'=>NULL,
	'image3'=>NULL,
	'image4'=>NULL,
	'vendor_id'=>5,
	'featured'=>0,
	'status'=>1,
	'rating'=>0.00,
	'created_at'=>'2021-02-01 19:02:50',
	'updated_at'=>'2021-02-02 13:24:36'
	] );

				

	Product::create( [
	'id'=>8,
	'masters_id'=>4,
	'name'=>'Aloo Pyaz Paratha',
	'slug'=>'aloo-pyaz-paratha',
	'verify'=>1,
	'verified_at'=>'2021-02-01 19:04:30',
	'type'=>NULL,
	'costoftwo'=>NULL,
	'aprice'=>30.00,
	'categry_id'=>11,
	'sub_categry_id'=>17,
	'attrubute_id'=>3,
	'description'=>'Aloo Pyaaz Partatha is a stuffed whole wheat Indian flatbread which has a tasty and spicy onion potato and pepper filling.',
	'shortdescription'=>NULL,
	'meta_title'=>'Aloo Pyaz Paratha',
	'meta_keywords'=>'Aloo Pyaz Paratha',
	'meta_description'=>'Aloo Pyaaz Partatha is a stuffed whole wheat Indian flatbread which has a tasty and spicy onion potato and pepper filling.',
	'image'=>'images/item/03022021-1612331711vSe7de.jpg',
	'image2'=>NULL,
	'image3'=>NULL,
	'image4'=>NULL,
	'vendor_id'=>5,
	'featured'=>0,
	'status'=>1,
	'rating'=>0.00,
	'created_at'=>'2021-02-01 19:04:30',
	'updated_at'=>'2021-02-02 13:25:14'
	] );

				

	Product::create( [
	'id'=>9,
	'masters_id'=>4,
	'name'=>'Chilli Garlic Paratha',
	'slug'=>'chilli-garlic-paratha',
	'verify'=>1,
	'verified_at'=>'2021-02-01 19:06:29',
	'type'=>NULL,
	'costoftwo'=>NULL,
	'aprice'=>60.00,
	'categry_id'=>11,
	'sub_categry_id'=>18,
	'attrubute_id'=>3,
	'description'=>'Chilli garlic paratha is a unique style multilayered paratha with the flavours of garlic. Its much easy to make than garlic bread and can be enjoyed with pickle or any side dish of your choice.',
	'shortdescription'=>NULL,
	'meta_title'=>'Chilli Garlic Paratha',
	'meta_keywords'=>'Chilli Garlic Paratha',
	'meta_description'=>'Chilli garlic paratha is a unique style multilayered paratha with the flavours of garlic',
	'image'=>'images/item/03022021-1612331748dLdObT.jpg',
	'image2'=>NULL,
	'image3'=>NULL,
	'image4'=>NULL,
	'vendor_id'=>5,
	'featured'=>0,
	'status'=>1,
	'rating'=>0.00,
	'created_at'=>'2021-02-01 19:06:29',
	'updated_at'=>'2021-02-02 13:25:50'
	] );

				

	Product::create( [
	'id'=>10,
	'masters_id'=>4,
	'name'=>'Cheese Paratha',
	'slug'=>'cheese-paratha',
	'verify'=>1,
	'verified_at'=>'2021-02-01 19:08:30',
	'type'=>NULL,
	'costoftwo'=>NULL,
	'aprice'=>100.00,
	'categry_id'=>11,
	'sub_categry_id'=>19,
	'attrubute_id'=>3,
	'description'=>'Cheese paratha is a delicious whole wheat flatbread stuffed with a spiced cheese stuffing. These cheese flatbreads make for a wholesome breakfast or brunch.',
	'shortdescription'=>NULL,
	'meta_title'=>'Cheese Paratha',
	'meta_keywords'=>'Cheese Paratha',
	'meta_description'=>'Cheese paratha is a delicious whole wheat flatbread stuffed with a spiced cheese stuffing.',
	'image'=>'images/item/03022021-1612331769qrIAIC.jpg',
	'image2'=>NULL,
	'image3'=>NULL,
	'image4'=>NULL,
	'vendor_id'=>5,
	'featured'=>0,
	'status'=>1,
	'rating'=>0.00,
	'created_at'=>'2021-02-01 19:08:30',
	'updated_at'=>'2021-02-02 13:26:11'
	] );

				

	Product::create( [
	'id'=>11,
	'masters_id'=>4,
	'name'=>'Chefs Special Pizza Paratha',
	'slug'=>'chefs-special-pizza-paratha',
	'verify'=>1,
	'verified_at'=>'2021-02-01 19:12:40',
	'type'=>NULL,
	'costoftwo'=>NULL,
	'aprice'=>100.00,
	'categry_id'=>11,
	'sub_categry_id'=>20,
	'attrubute_id'=>3,
	'description'=>'Pizza paratha also known as roti pizza is made with Indian whole wheat flatbreads – roti, pizza sauce, cheese and herbs. This is a Paratha recipe with stuffed pizza sauce & veggies, topped with cheese.',
	'shortdescription'=>NULL,
	'meta_title'=>'Chefs Special Pizza Paratha',
	'meta_keywords'=>'Chefs Special Pizza Paratha',
	'meta_description'=>'This is a Paratha recipe with stuffed pizza sauce & veggies, topped with cheese.',
	'image'=>'images/item/03022021-1612331791AKxdOs.jpg',
	'image2'=>NULL,
	'image3'=>NULL,
	'image4'=>NULL,
	'vendor_id'=>5,
	'featured'=>0,
	'status'=>1,
	'rating'=>0.00,
	'created_at'=>'2021-02-01 19:12:40',
	'updated_at'=>'2021-02-02 13:26:32'
	] );

				

	Product::create( [
	'id'=>12,
	'masters_id'=>4,
	'name'=>'Rajma',
	'slug'=>'rajma',
	'verify'=>1,
	'verified_at'=>'2021-02-01 19:15:18',
	'type'=>NULL,
	'costoftwo'=>NULL,
	'aprice'=>180.00,
	'categry_id'=>8,
	'sub_categry_id'=>22,
	'attrubute_id'=>7,
	'description'=>'Rajma is a vegetarian dish, originating from the Indian subcontinent, consisting of red kidney beans in a thick gravy with many Indian whole spices, and is usually served with rice.',
	'shortdescription'=>NULL,
	'meta_title'=>'Rajma',
	'meta_keywords'=>'Rajma',
	'meta_description'=>'Rajma is a vegetarian dish, originating from the Indian subcontinent, consisting of red kidney beans in a thick gravy with many Indian whole spices, and is usually served with rice.',
	'image'=>'images/item/03022021-1612331885NomF1K.jpg',
	'image2'=>NULL,
	'image3'=>NULL,
	'image4'=>NULL,
	'vendor_id'=>4,
	'featured'=>0,
	'status'=>1,
	'rating'=>0.00,
	'created_at'=>'2021-02-01 19:15:18',
	'updated_at'=>'2021-02-02 13:28:07'
	] );

				

	Product::create( [
	'id'=>13,
	'masters_id'=>4,
	'name'=>'Paneer Pakoda',
	'slug'=>'paneer-pakoda',
	'verify'=>1,
	'verified_at'=>'2021-02-01 19:17:14',
	'type'=>NULL,
	'costoftwo'=>NULL,
	'aprice'=>80.00,
	'categry_id'=>9,
	'sub_categry_id'=>24,
	'attrubute_id'=>3,
	'description'=>'Paneer pakora are savory gram flour batter coated Indian cottage cheese fritters. Paneer pakora make for quick evening snack having a lovely crispy texture with a moist, soft paneer from within.',
	'shortdescription'=>NULL,
	'meta_title'=>'Paneer Pakoda',
	'meta_keywords'=>'Paneer Pakoda',
	'meta_description'=>'Paneer pakora are savory gram flour batter coated Indian cottage cheese fritters.',
	'image'=>'images/item/03022021-1612331934RNeYiN.jpg',
	'image2'=>NULL,
	'image3'=>NULL,
	'image4'=>NULL,
	'vendor_id'=>4,
	'featured'=>0,
	'status'=>1,
	'rating'=>0.00,
	'created_at'=>'2021-02-01 19:17:14',
	'updated_at'=>'2021-02-02 13:28:55'
	] );

				

	Product::create( [
	'id'=>14,
	'masters_id'=>4,
	'name'=>'Chicken Biryani',
	'slug'=>'chicken-biryani',
	'verify'=>1,
	'verified_at'=>'2021-02-01 19:18:44',
	'type'=>NULL,
	'costoftwo'=>NULL,
	'aprice'=>220.00,
	'categry_id'=>10,
	'sub_categry_id'=>25,
	'attrubute_id'=>1,
	'description'=>'Chicken Biryani is a savory chicken and rice dish that includes layers of chicken, rice, and aromatics that are steamed together.',
	'shortdescription'=>NULL,
	'meta_title'=>'Chicken Biryani',
	'meta_keywords'=>'Chicken Biryani',
	'meta_description'=>'Chicken Biryani is a savory chicken and rice dish that includes layers of chicken, rice, and aromatics that are steamed together.',
	'image'=>'images/item/03022021-1612332007PiyUnU.jpg',
	'image2'=>NULL,
	'image3'=>NULL,
	'image4'=>NULL,
	'vendor_id'=>4,
	'featured'=>0,
	'status'=>1,
	'rating'=>0.00,
	'created_at'=>'2021-02-01 19:18:44',
	'updated_at'=>'2021-02-02 13:30:09'
	] );

				

	Product::create( [
	'id'=>15,
	'masters_id'=>4,
	'name'=>'Chilli Garlic Paratha',
	'slug'=>'chilli-garlic-paratha-1',
	'verify'=>1,
	'verified_at'=>'2021-02-01 19:21:29',
	'type'=>NULL,
	'costoftwo'=>NULL,
	'aprice'=>110.00,
	'categry_id'=>11,
	'sub_categry_id'=>18,
	'attrubute_id'=>3,
	'description'=>'Chilli Cheese Garlic Paratha is an easy and delicious stuffed paratha',
	'shortdescription'=>NULL,
	'meta_title'=>'Chilli Garlic Paratha',
	'meta_keywords'=>'Chilli Garlic Paratha',
	'meta_description'=>'Chilli Cheese Garlic Paratha is an easy and delicious stuffed paratha',
	'image'=>'images/item/03022021-1612332044BX2xVw.jpg',
	'image2'=>NULL,
	'image3'=>NULL,
	'image4'=>NULL,
	'vendor_id'=>4,
	'featured'=>0,
	'status'=>1,
	'rating'=>0.00,
	'created_at'=>'2021-02-01 19:21:29',
	'updated_at'=>'2021-02-02 13:30:46'
	] );

				

	Product::create( [
	'id'=>16,
	'masters_id'=>4,
	'name'=>'Grilled Sandwich',
	'slug'=>'grilled-sandwich-1',
	'verify'=>1,
	'verified_at'=>'2021-02-01 19:24:09',
	'type'=>NULL,
	'costoftwo'=>NULL,
	'aprice'=>150.00,
	'categry_id'=>9,
	'sub_categry_id'=>21,
	'attrubute_id'=>3,
	'description'=>'A toasted sandwich, grilled cheese sandwich, cheese toastie, or grilled cheese is a hot sandwich made with one or more varieties of cheese on bread.',
	'shortdescription'=>NULL,
	'meta_title'=>'Grilled Sandwich',
	'meta_keywords'=>'Grilled Sandwich',
	'meta_description'=>'A toasted sandwich, grilled cheese sandwich, cheese toastie, or grilled cheese is a hot sandwich made with one or more varieties of cheese on bread.',
	'image'=>'images/item/03022021-16123320662d9XvU.png',
	'image2'=>NULL,
	'image3'=>NULL,
	'image4'=>NULL,
	'vendor_id'=>3,
	'featured'=>0,
	'status'=>1,
	'rating'=>0.00,
	'created_at'=>'2021-02-01 19:24:09',
	'updated_at'=>'2021-02-02 13:31:08'
	] );

				

	Product::create( [
	'id'=>17,
	'masters_id'=>4,
	'name'=>'Egg Maggi',
	'slug'=>'egg-maggi',
	'verify'=>1,
	'verified_at'=>'2021-02-01 19:26:03',
	'type'=>NULL,
	'costoftwo'=>NULL,
	'aprice'=>70.00,
	'categry_id'=>9,
	'sub_categry_id'=>23,
	'attrubute_id'=>7,
	'description'=>'Egg maggi noodles recipe is an easy and quick maggi noodles with eggs. this recipe has maggi noodles tossed with scrambled eggs and vegetables to make it filling and tasty.',
	'shortdescription'=>NULL,
	'meta_title'=>'Egg Maggi',
	'meta_keywords'=>'Egg Maggi',
	'meta_description'=>'Egg maggi noodles recipe is an easy and quick maggi noodles with eggs.',
	'image'=>'images/item/03022021-1612332177BZx9Qt.crdownload',
	'image2'=>NULL,
	'image3'=>NULL,
	'image4'=>NULL,
	'vendor_id'=>3,
	'featured'=>0,
	'status'=>1,
	'rating'=>0.00,
	'created_at'=>'2021-02-01 19:26:03',
	'updated_at'=>'2021-02-02 13:32:59'
	] );

				

	Product::create( [
	'id'=>18,
	'masters_id'=>4,
	'name'=>'Dal Makhani',
	'slug'=>'dal-makhani',
	'verify'=>1,
	'verified_at'=>'2021-02-01 19:27:40',
	'type'=>NULL,
	'costoftwo'=>NULL,
	'aprice'=>80.00,
	'categry_id'=>8,
	'sub_categry_id'=>22,
	'attrubute_id'=>7,
	'description'=>'Creamy and buttery Dal Makhani is one of India\'s most loved dal! This dal has whole black lentils cooked with butter and cream and simmered on low heat for that unique flavor. It tastes best with garlic naan!',
	'shortdescription'=>NULL,
	'meta_title'=>'Dal Makhani',
	'meta_keywords'=>'Dal Makhani',
	'meta_description'=>'This dal has whole black lentils cooked with butter and cream and simmered on low heat for that unique flavor.',
	'image'=>'images/item/03022021-1612332225DEsEiE.jpg',
	'image2'=>NULL,
	'image3'=>NULL,
	'image4'=>NULL,
	'vendor_id'=>2,
	'featured'=>0,
	'status'=>1,
	'rating'=>0.00,
	'created_at'=>'2021-02-01 19:27:40',
	'updated_at'=>'2021-02-02 13:33:48'
	] );

	Product::create( [
	'id'=>19,
	'masters_id'=>4,
	'name'=>'Veg Maggi',
	'slug'=>'veg-maggi',
	'verify'=>1,
	'verified_at'=>'2021-02-01 19:27:40',
	'type'=>NULL,
	'costoftwo'=>NULL,
	'aprice'=>30.00,
	'categry_id'=>8,
	'sub_categry_id'=>22,
	'attrubute_id'=>7,
	'description'=>'Veg Maggi',
	'shortdescription'=>NULL,
	'meta_title'=>'Veg Maggi',
	'meta_keywords'=>'Veg Maggi',
	'meta_description'=>'Veg Maggi.',
	'image'=>'images/item/03022021-1612332254GCNz0a.jpg',
	'image2'=>NULL,
	'image3'=>NULL,
	'image4'=>NULL,
	'vendor_id'=>2,
	'featured'=>0,
	'status'=>1,
	'rating'=>0.00,
	'created_at'=>'2021-02-01 19:27:40',
	'updated_at'=>'2021-02-02 13:33:48'
	] );




	Product::create( [
	'id'=>20,
	'masters_id'=>4,
	'name'=>'Paneer Biryani',
	'slug'=>'paneer-biryani-1',
	'verify'=>1,
	'verified_at'=>'2021-02-01 19:32:32',
	'type'=>NULL,
	'costoftwo'=>NULL,
	'aprice'=>250.00,
	'categry_id'=>10,
	'sub_categry_id'=>25,
	'attrubute_id'=>1,
	'description'=>'Paneer biryani is an Indian dish made with paneer, basmati rice, spices & herbs. This paneer biryani is unique, flavorful & amazingly delicious. Biryani is most commonly made with meat but this recipe uses paneer aka Indian cottage cheese.',
	'shortdescription'=>NULL,
	'meta_title'=>'Paneer Biryani',
	'meta_keywords'=>'Paneer Biryani',
	'meta_description'=>'Paneer biryani is an Indian dish made with paneer, basmati rice, spices & herbs.',
	'image'=>'images/item/03022021-16123323617aJXDR.jpg',
	'image2'=>NULL,
	'image3'=>NULL,
	'image4'=>NULL,
	'vendor_id'=>2,
	'featured'=>0,
	'status'=>1,
	'rating'=>0.00,
	'created_at'=>'2021-02-01 19:32:32',
	'updated_at'=>'2021-02-02 13:36:04'
	] );

				

	Product::create( [
	'id'=>21,
	'masters_id'=>4,
	'name'=>'Aloo Pyaz Paratha',
	'slug'=>'aloo-pyaz-paratha-1',
	'verify'=>1,
	'verified_at'=>'2021-02-01 19:34:01',
	'type'=>NULL,
	'costoftwo'=>NULL,
	'aprice'=>45.00,
	'categry_id'=>11,
	'sub_categry_id'=>17,
	'attrubute_id'=>3,
	'description'=>'Aloo Pyaaz Partatha is a stuffed whole wheat Indian flatbread which has a tasty and spicy onion potato and pepper filling.',
	'shortdescription'=>NULL,
	'meta_title'=>'Aloo Pyaz Paratha',
	'meta_keywords'=>'Aloo Pyaz Paratha',
	'meta_description'=>'Aloo Pyaaz Partatha is a stuffed whole wheat Indian flatbread which has a tasty and spicy onion potato and pepper filling.',
	'image'=>'images/item/03022021-1612332537IFuKwh.jpg',
	'image2'=>NULL,
	'image3'=>NULL,
	'image4'=>NULL,
	'vendor_id'=>2,
	'featured'=>0,
	'status'=>1,
	'rating'=>0.00,
	'created_at'=>'2021-02-01 19:34:01',
	'updated_at'=>'2021-02-02 13:38:59'
	] );

			

	Product::create( [
	'id'=>22,
	'masters_id'=>4,
	'name'=>'Cheese Paratha',
	'slug'=>'cheese-paratha-1',
	'verify'=>1,
	'verified_at'=>'2021-02-01 19:35:25',
	'type'=>NULL,
	'costoftwo'=>NULL,
	'aprice'=>50.00,
	'categry_id'=>11,
	'sub_categry_id'=>19,
	'attrubute_id'=>3,
	'description'=>'Cheese paratha is a delicious whole wheat flatbread stuffed with a spiced cheese stuffing. These cheese flatbreads make for a wholesome breakfast or brunch. I make cheese paratha mostly with leftover dough for a quick breakfast.',
	'shortdescription'=>NULL,
	'meta_title'=>'Cheese Paratha',
	'meta_keywords'=>'Cheese Paratha',
	'meta_description'=>'I make cheese paratha mostly with leftover dough for a quick breakfast.',
	'image'=>'images/item/03022021-1612332554aK8koV.jpg',
	'image2'=>NULL,
	'image3'=>NULL,
	'image4'=>NULL,
	'vendor_id'=>2,
	'featured'=>0,
	'status'=>1,
	'rating'=>0.00,
	'created_at'=>'2021-02-01 19:35:25',
	'updated_at'=>'2021-02-02 13:39:16'
	] );

			




    }
}
